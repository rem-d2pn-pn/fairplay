﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class SauveConfig

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub SauveConfig_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub SauveConfig_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de validation
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim InfoFichier As FileInfo
        Dim NomFichier As StreamWriter
        Dim ReadString As String
        Dim i As Integer
        Dim j As Integer
        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur1
        ' ====================================================================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If TextBox1.Text = "" Then
            MsgBox("Vous n'avez pas défini un nom de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ====================================================================================
        ' Vérification si le fichier existe déjà
        ' ==================================
        ' Récupération du nom du fichier de la base Fairplay
        InfoFichier = New FileInfo(Variables.Repertoire & "\" & TextBox1.Text & ".cfg")
        ' ==================================
        ' Vérification de l'existence de la base Fairplay
        If InfoFichier.Exists = True Then
            i = MsgBox("        Le fichier existe déjà !" & vbCrLf & vbCrLf & "Confirmez-vous la sauvegarde ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " Demande de confirmation")
            If i = 7 Then
                Exit Sub
            End If
        End If
        ' ==================================
        ' Ouverture du fichier de sauvegarde
        NomFichier = New StreamWriter(Variables.Repertoire & "\" & TextBox1.Text & ".cfg", False, System.Text.Encoding.UTF8)
        ' ==================================
        ' Sauvegarde des textes de traitements de la Base
        NomFichier.WriteLine(Extracteur.TextBox1.Text)
        NomFichier.WriteLine(Extracteur.TextBox2.Text)
        NomFichier.WriteLine(Extracteur.TextBox3.Text)
        NomFichier.WriteLine(Extracteur.TextBox4.Text)
        NomFichier.WriteLine(Extracteur.TextBox5.Text)
        NomFichier.WriteLine(Extracteur.TextBox6.Text)
        NomFichier.WriteLine(Extracteur.TextBox7.Text)
        NomFichier.WriteLine(Extracteur.TextBox8.Text)
        NomFichier.WriteLine(Extracteur.TextBox9.Text)
        NomFichier.WriteLine(Extracteur.TextBox10.Text)
        NomFichier.WriteLine(Extracteur.TextBox11.Text)
        NomFichier.WriteLine(Extracteur.TextBox12.Text)
        NomFichier.WriteLine(Extracteur.TextBox13.Text)
        NomFichier.WriteLine(Extracteur.TextBox14.Text)
        NomFichier.WriteLine(Extracteur.TextBox15.Text)
        NomFichier.WriteLine(Extracteur.TextBox16.Text)
        NomFichier.WriteLine(Extracteur.TextBox17.Text)
        NomFichier.WriteLine(Extracteur.TextBox18.Text)
        NomFichier.WriteLine(Extracteur.TextBox19.Text)
        ' ==================================
        ' Sauvegarde des textes de traitements du dernier résultat
        NomFichier.WriteLine(Extracteur.TextBox21.Text)
        NomFichier.WriteLine(Extracteur.TextBox22.Text)
        NomFichier.WriteLine(Extracteur.TextBox23.Text)
        NomFichier.WriteLine(Extracteur.TextBox24.Text)
        NomFichier.WriteLine(Extracteur.TextBox25.Text)
        NomFichier.WriteLine(Extracteur.TextBox26.Text)
        NomFichier.WriteLine(Extracteur.TextBox27.Text)
        NomFichier.WriteLine(Extracteur.TextBox28.Text)
        NomFichier.WriteLine(Extracteur.TextBox29.Text)
        NomFichier.WriteLine(Extracteur.TextBox30.Text)
        NomFichier.WriteLine(Extracteur.TextBox31.Text)
        NomFichier.WriteLine(Extracteur.TextBox32.Text)
        NomFichier.WriteLine(Extracteur.TextBox33.Text)
        NomFichier.WriteLine(Extracteur.TextBox34.Text)
        NomFichier.WriteLine(Extracteur.TextBox35.Text)
        NomFichier.WriteLine(Extracteur.TextBox36.Text)
        NomFichier.WriteLine(Extracteur.TextBox37.Text)
        NomFichier.WriteLine(Extracteur.TextBox38.Text)
        NomFichier.WriteLine(Extracteur.TextBox39.Text)
        ' ==================================
        ' Sauvegarde du tableau de traitements de la Base
        ReadString = ""
        For i = 1 To 19
            For j = 0 To 4
                ReadString = Variables.TabTraitBase(i, j)
                NomFichier.WriteLine(ReadString)
            Next
        Next
        ' ==================================
        ' Sauvegarde du tableau de traitements du dernier résultat
        ReadString = ""
        For i = 1 To 19
            For j = 0 To 4
                ReadString = Variables.TabTraitResul(i, j)
                NomFichier.WriteLine(ReadString)
            Next
        Next
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Fermeture de l'application
        Fermeture()
        '=========================================================================================================
TraitErreur1:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " Erreur de sauvegarde")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'effacement du texte
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        TextBox1.Text = ""
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Extracteur.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub SauveConfig_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub


End Class