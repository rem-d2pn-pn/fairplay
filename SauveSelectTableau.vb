﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class SauveSelectTableau

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub SauveSelectTableau_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub SauveSelectTableau_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de validation
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim InfoFichier As FileInfo
        Dim NomFichier As StreamWriter
        Dim ReadString As String
        Dim i As Integer
        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur1
        ' ====================================================================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If TextBox1.Text = "" Then
            MsgBox("Vous n'avez pas défini un nom de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ====================================================================================
        ' Vérification si le fichier existe déjà
        ' ==================================
        ' Récupération du nom du fichier de la base Fairplay
        InfoFichier = New FileInfo(Variables.Repertoire & "\" & TextBox1.Text & ".slt")
        ' ==================================
        ' Vérification de l'existence de la base Fairplay
        If InfoFichier.Exists = True Then
            i = MsgBox("        Le fichier existe déjà !" & vbCrLf & vbCrLf & "Confirmez-vous la sauvegarde ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, " Demande de confirmation")
            If i = 7 Then
                Exit Sub
            End If
        End If
        ' ==================================
        ' Ouverture du fichier de sauvegarde
        NomFichier = New StreamWriter(Variables.Repertoire & "\" & TextBox1.Text & ".slt", False, System.Text.Encoding.UTF8)

        ' ====================================================================================
        ' Traitement des colonnes
        ' ====================================================================================
        ' Écriture de Colonne
        NomFichier.WriteLine("Colonne")

        ' ==================================
        ' Sauvegarde à partir d'une liste alphabétique
        ' ==================================
        If Tableau.TextBox1.Text = "Code état du navire" Or Tableau.TextBox1.Text = "Classe du navire" Or Tableau.TextBox1.Text = "Pavillon du navire" Or Tableau.TextBox1.Text = "Type du navire" Then
            ' ==================================
            ' Écriture de Alphabétique
            NomFichier.WriteLine("Alphabétique")
            ' ==================================
            ' Écriture du titre
            NomFichier.WriteLine(Tableau.TextBox1.Text)
            ' ==================================
            ' Sauvegarde des informations "Code état du navire"
            If Tableau.TextBox1.Text = "Code état du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox2.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox2.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
            ' ==================================
            ' Sauvegarde des informations "Classe du navire"
            If Tableau.TextBox1.Text = "Classe du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox3.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox3.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
            ' ==================================
            ' Sauvegarde des informations "Pavillon du navire"
            If Tableau.TextBox1.Text = "Pavillon du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox4.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox4.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
            ' ==================================
            ' Sauvegarde des informations "Type du navire"
            If Tableau.TextBox1.Text = "Type du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox5.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox5.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
        End If

        ' ==================================
        ' Sauvegarde à partir de dates
        ' ==================================
        If Tableau.TextBox1.Text = "Date d'achat" Or Tableau.TextBox1.Text = "Date de livraison" Or Tableau.TextBox1.Text = "Date de perte ou de destruction" Then
            ' ==================================
            ' Écriture de Date
            NomFichier.WriteLine("Date")
            ' ==================================
            ' Écriture du titre
            NomFichier.WriteLine(Tableau.TextBox1.Text)
            ' ==================================
            ' Écriture des dates
            NomFichier.WriteLine(Tableau.TextBox110.Text)
            NomFichier.WriteLine(Tableau.TextBox111.Text)
            NomFichier.WriteLine(Tableau.TextBox112.Text)
            NomFichier.WriteLine(Tableau.TextBox113.Text)
            NomFichier.WriteLine(Tableau.TextBox114.Text)
            NomFichier.WriteLine(Tableau.TextBox115.Text)
            NomFichier.WriteLine(Tableau.TextBox116.Text)
            NomFichier.WriteLine(Tableau.TextBox117.Text)
            NomFichier.WriteLine(Tableau.TextBox118.Text)
            NomFichier.WriteLine(Tableau.TextBox119.Text)
            NomFichier.WriteLine(Tableau.TextBox120.Text)
            NomFichier.WriteLine(Tableau.TextBox121.Text)

            NomFichier.WriteLine(Tableau.TextBox122.Text)
            NomFichier.WriteLine(Tableau.TextBox123.Text)
            NomFichier.WriteLine(Tableau.TextBox124.Text)
            NomFichier.WriteLine(Tableau.TextBox125.Text)
            NomFichier.WriteLine(Tableau.TextBox126.Text)
            NomFichier.WriteLine(Tableau.TextBox127.Text)
            NomFichier.WriteLine(Tableau.TextBox128.Text)
            NomFichier.WriteLine(Tableau.TextBox129.Text)
            NomFichier.WriteLine(Tableau.TextBox130.Text)
            NomFichier.WriteLine(Tableau.TextBox131.Text)
            NomFichier.WriteLine(Tableau.TextBox132.Text)

            NomFichier.WriteLine(Tableau.TextBox133.Text)
            NomFichier.WriteLine(Tableau.TextBox134.Text)
            NomFichier.WriteLine(Tableau.TextBox135.Text)
            NomFichier.WriteLine(Tableau.TextBox136.Text)
            NomFichier.WriteLine(Tableau.TextBox137.Text)
            NomFichier.WriteLine(Tableau.TextBox138.Text)
            NomFichier.WriteLine(Tableau.TextBox139.Text)
            NomFichier.WriteLine(Tableau.TextBox140.Text)
            NomFichier.WriteLine(Tableau.TextBox141.Text)
            NomFichier.WriteLine(Tableau.TextBox142.Text)
            NomFichier.WriteLine(Tableau.TextBox143.Text)

            If Tableau.RadioButton13.Checked = True Then NomFichier.WriteLine("13")
            If Tableau.RadioButton14.Checked = True Then NomFichier.WriteLine("14")
            If Tableau.RadioButton15.Checked = True Then NomFichier.WriteLine("15")
            If Tableau.RadioButton16.Checked = True Then NomFichier.WriteLine("16")
            If Tableau.RadioButton17.Checked = True Then NomFichier.WriteLine("17")
            If Tableau.RadioButton18.Checked = True Then NomFichier.WriteLine("18")
        End If

        ' ==================================
        ' Sauvegarde à partir de valeurs numériques
        ' ==================================
        If Tableau.TextBox1.Text <> "Date d'achat" And Tableau.TextBox1.Text <> "Date de livraison" And Tableau.TextBox1.Text <> "Date de perte ou de destruction" And Tableau.TextBox1.Text <> "Code état du navire" And Tableau.TextBox1.Text <> "Classe du navire" And Tableau.TextBox1.Text <> "Pavillon du navire" And Tableau.TextBox1.Text <> "Type du navire" Then
            ' Écriture de Numérique
            NomFichier.WriteLine("Numérique")
            ' ==================================
            ' Écriture du titre
            NomFichier.WriteLine(Tableau.TextBox1.Text)
            ' ==================================
            ' Écriture des valeurs
            NomFichier.WriteLine(Tableau.TextBox21.Text)
            NomFichier.WriteLine(Tableau.TextBox22.Text)
            NomFichier.WriteLine(Tableau.TextBox23.Text)
            NomFichier.WriteLine(Tableau.TextBox24.Text)
            NomFichier.WriteLine(Tableau.TextBox25.Text)
            NomFichier.WriteLine(Tableau.TextBox26.Text)
            NomFichier.WriteLine(Tableau.TextBox27.Text)
            NomFichier.WriteLine(Tableau.TextBox28.Text)
            NomFichier.WriteLine(Tableau.TextBox29.Text)
            NomFichier.WriteLine(Tableau.TextBox30.Text)
            NomFichier.WriteLine(Tableau.TextBox31.Text)
            NomFichier.WriteLine(Tableau.TextBox32.Text)

            NomFichier.WriteLine(Tableau.TextBox52.Text)
            NomFichier.WriteLine(Tableau.TextBox53.Text)
            NomFichier.WriteLine(Tableau.TextBox54.Text)
            NomFichier.WriteLine(Tableau.TextBox55.Text)
            NomFichier.WriteLine(Tableau.TextBox56.Text)
            NomFichier.WriteLine(Tableau.TextBox57.Text)
            NomFichier.WriteLine(Tableau.TextBox58.Text)
            NomFichier.WriteLine(Tableau.TextBox59.Text)
            NomFichier.WriteLine(Tableau.TextBox60.Text)
            NomFichier.WriteLine(Tableau.TextBox61.Text)
            NomFichier.WriteLine(Tableau.TextBox62.Text)

            NomFichier.WriteLine(Tableau.TextBox63.Text)
            NomFichier.WriteLine(Tableau.TextBox64.Text)
            NomFichier.WriteLine(Tableau.TextBox65.Text)
            NomFichier.WriteLine(Tableau.TextBox66.Text)
            NomFichier.WriteLine(Tableau.TextBox67.Text)
            NomFichier.WriteLine(Tableau.TextBox68.Text)
            NomFichier.WriteLine(Tableau.TextBox69.Text)
            NomFichier.WriteLine(Tableau.TextBox70.Text)
            NomFichier.WriteLine(Tableau.TextBox71.Text)
            NomFichier.WriteLine(Tableau.TextBox72.Text)
            NomFichier.WriteLine(Tableau.TextBox73.Text)

            If Tableau.RadioButton1.Checked = True Then NomFichier.WriteLine("1")
            If Tableau.RadioButton2.Checked = True Then NomFichier.WriteLine("2")
            If Tableau.RadioButton3.Checked = True Then NomFichier.WriteLine("3")
            If Tableau.RadioButton4.Checked = True Then NomFichier.WriteLine("4")
            If Tableau.RadioButton5.Checked = True Then NomFichier.WriteLine("5")
            If Tableau.RadioButton6.Checked = True Then NomFichier.WriteLine("6")
        End If

        ' ====================================================================================
        ' Traitement des lignes
        ' ====================================================================================
        ' Écriture de Ligne
        NomFichier.WriteLine("Ligne")

        ' ==================================
        ' Sauvegarde à partir d'une liste alphabétique
        ' ==================================
        If Tableau.TextBox2.Text = "Code état du navire" Or Tableau.TextBox2.Text = "Classe du navire" Or Tableau.TextBox2.Text = "Pavillon du navire" Or Tableau.TextBox2.Text = "Type du navire" Then
            ' ==================================
            ' Écriture de Alphabétique
            NomFichier.WriteLine("Alphabétique")
            ' ==================================
            ' Écriture du titre
            NomFichier.WriteLine(Tableau.TextBox2.Text)
            ' ==================================
            ' Sauvegarde des informations "Code état du navire"
            If Tableau.TextBox2.Text = "Code état du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox2.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox2.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
            ' ==================================
            ' Sauvegarde des informations "Classe du navire"
            If Tableau.TextBox2.Text = "Classe du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox3.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox3.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
            ' ==================================
            ' Sauvegarde des informations "Pavillon du navire"
            If Tableau.TextBox2.Text = "Pavillon du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox4.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox4.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
            ' ==================================
            ' Sauvegarde des informations "Type du navire"
            If Tableau.TextBox2.Text = "Type du navire" Then
                ' ==================================
                ' Balayage de la liste avec sauvegarde des éléments sélectionnés
                For i = 0 To Tableau.ListBox5.Items.Count - 1
                    ' ==================================
                    ' Vérification si l'élément pointé a été sélectionné
                    If Tableau.ListBox5.GetSelected(i) = True Then
                        ' ==================================
                        ' Sauvegarde de l'élément pointé
                        NomFichier.WriteLine(i)
                    End If
                Next
            End If
        End If

        ' ==================================
        ' Sauvegarde à partir de dates
        ' ==================================
        If Tableau.TextBox2.Text = "Date d'achat" Or Tableau.TextBox2.Text = "Date de livraison" Or Tableau.TextBox2.Text = "Date de perte ou de destruction" Then
            ' ==================================
            ' Écriture de Date
            NomFichier.WriteLine("Date")
            ' ==================================
            ' Écriture du titre
            NomFichier.WriteLine(Tableau.TextBox2.Text)
            ' ==================================
            ' Écriture des dates
            NomFichier.WriteLine(Tableau.TextBox146.Text)
            NomFichier.WriteLine(Tableau.TextBox147.Text)
            NomFichier.WriteLine(Tableau.TextBox148.Text)
            NomFichier.WriteLine(Tableau.TextBox149.Text)
            NomFichier.WriteLine(Tableau.TextBox150.Text)
            NomFichier.WriteLine(Tableau.TextBox151.Text)
            NomFichier.WriteLine(Tableau.TextBox152.Text)
            NomFichier.WriteLine(Tableau.TextBox153.Text)
            NomFichier.WriteLine(Tableau.TextBox154.Text)
            NomFichier.WriteLine(Tableau.TextBox155.Text)
            NomFichier.WriteLine(Tableau.TextBox156.Text)
            NomFichier.WriteLine(Tableau.TextBox157.Text)

            NomFichier.WriteLine(Tableau.TextBox158.Text)
            NomFichier.WriteLine(Tableau.TextBox159.Text)
            NomFichier.WriteLine(Tableau.TextBox160.Text)
            NomFichier.WriteLine(Tableau.TextBox161.Text)
            NomFichier.WriteLine(Tableau.TextBox162.Text)
            NomFichier.WriteLine(Tableau.TextBox163.Text)
            NomFichier.WriteLine(Tableau.TextBox164.Text)
            NomFichier.WriteLine(Tableau.TextBox165.Text)
            NomFichier.WriteLine(Tableau.TextBox166.Text)
            NomFichier.WriteLine(Tableau.TextBox167.Text)
            NomFichier.WriteLine(Tableau.TextBox168.Text)

            NomFichier.WriteLine(Tableau.TextBox169.Text)
            NomFichier.WriteLine(Tableau.TextBox170.Text)
            NomFichier.WriteLine(Tableau.TextBox171.Text)
            NomFichier.WriteLine(Tableau.TextBox172.Text)
            NomFichier.WriteLine(Tableau.TextBox173.Text)
            NomFichier.WriteLine(Tableau.TextBox174.Text)
            NomFichier.WriteLine(Tableau.TextBox175.Text)
            NomFichier.WriteLine(Tableau.TextBox176.Text)
            NomFichier.WriteLine(Tableau.TextBox177.Text)
            NomFichier.WriteLine(Tableau.TextBox178.Text)
            NomFichier.WriteLine(Tableau.TextBox179.Text)

            If Tableau.RadioButton19.Checked = True Then NomFichier.WriteLine("19")
            If Tableau.RadioButton20.Checked = True Then NomFichier.WriteLine("20")
            If Tableau.RadioButton21.Checked = True Then NomFichier.WriteLine("21")
            If Tableau.RadioButton22.Checked = True Then NomFichier.WriteLine("22")
            If Tableau.RadioButton23.Checked = True Then NomFichier.WriteLine("23")
            If Tableau.RadioButton24.Checked = True Then NomFichier.WriteLine("24")
        End If

        ' ==================================
        ' Sauvegarde à partir de valeurs numériques
        ' ==================================
        If Tableau.TextBox2.Text <> "Date d'achat" And Tableau.TextBox2.Text <> "Date de livraison" And Tableau.TextBox2.Text <> "Date de perte ou de destruction" And Tableau.TextBox2.Text <> "Code état du navire" And Tableau.TextBox2.Text <> "Classe du navire" And Tableau.TextBox2.Text <> "Pavillon du navire" And Tableau.TextBox2.Text <> "Type du navire" Then
            ' Écriture de Numérique
            NomFichier.WriteLine("Numérique")
            ' ==================================
            ' Écriture du titre
            NomFichier.WriteLine(Tableau.TextBox2.Text)
            ' ==================================
            ' Écriture des valeurs
            NomFichier.WriteLine(Tableau.TextBox74.Text)
            NomFichier.WriteLine(Tableau.TextBox75.Text)
            NomFichier.WriteLine(Tableau.TextBox76.Text)
            NomFichier.WriteLine(Tableau.TextBox77.Text)
            NomFichier.WriteLine(Tableau.TextBox78.Text)
            NomFichier.WriteLine(Tableau.TextBox79.Text)
            NomFichier.WriteLine(Tableau.TextBox80.Text)
            NomFichier.WriteLine(Tableau.TextBox81.Text)
            NomFichier.WriteLine(Tableau.TextBox82.Text)
            NomFichier.WriteLine(Tableau.TextBox83.Text)
            NomFichier.WriteLine(Tableau.TextBox84.Text)
            NomFichier.WriteLine(Tableau.TextBox85.Text)

            NomFichier.WriteLine(Tableau.TextBox86.Text)
            NomFichier.WriteLine(Tableau.TextBox87.Text)
            NomFichier.WriteLine(Tableau.TextBox88.Text)
            NomFichier.WriteLine(Tableau.TextBox89.Text)
            NomFichier.WriteLine(Tableau.TextBox90.Text)
            NomFichier.WriteLine(Tableau.TextBox91.Text)
            NomFichier.WriteLine(Tableau.TextBox92.Text)
            NomFichier.WriteLine(Tableau.TextBox93.Text)
            NomFichier.WriteLine(Tableau.TextBox94.Text)
            NomFichier.WriteLine(Tableau.TextBox95.Text)
            NomFichier.WriteLine(Tableau.TextBox96.Text)

            NomFichier.WriteLine(Tableau.TextBox97.Text)
            NomFichier.WriteLine(Tableau.TextBox98.Text)
            NomFichier.WriteLine(Tableau.TextBox99.Text)
            NomFichier.WriteLine(Tableau.TextBox100.Text)
            NomFichier.WriteLine(Tableau.TextBox101.Text)
            NomFichier.WriteLine(Tableau.TextBox102.Text)
            NomFichier.WriteLine(Tableau.TextBox103.Text)
            NomFichier.WriteLine(Tableau.TextBox104.Text)
            NomFichier.WriteLine(Tableau.TextBox105.Text)
            NomFichier.WriteLine(Tableau.TextBox106.Text)
            NomFichier.WriteLine(Tableau.TextBox107.Text)

            If Tableau.RadioButton7.Checked = True Then NomFichier.WriteLine("7")
            If Tableau.RadioButton8.Checked = True Then NomFichier.WriteLine("8")
            If Tableau.RadioButton9.Checked = True Then NomFichier.WriteLine("9")
            If Tableau.RadioButton10.Checked = True Then NomFichier.WriteLine("10")
            If Tableau.RadioButton11.Checked = True Then NomFichier.WriteLine("11")
            If Tableau.RadioButton12.Checked = True Then NomFichier.WriteLine("12")
        End If
        ' ==================================
        ' Indication de la fin du fichier
        NomFichier.WriteLine("Fin")
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Fermeture de l'application
        Fermeture()
        '=========================================================================================================
TraitErreur1:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " Erreur de sauvegarde")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'effacement du texte
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        TextBox1.Text = ""
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Tableau.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub SauveSelectTableau_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub
End Class