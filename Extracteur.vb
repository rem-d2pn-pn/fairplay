﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class Extracteur

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Extracteur_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Extracteur_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Mise à jour du titre
        Me.Text = " Extracteur des informations de la Base FAIRPLAY - " & Variables.NomBaseTitre
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
        '==============================================
        ' Changement du titre du menu
        Form1.VisuToolStripMenuItem.Text = "   Visuel Tableau   "
        '==============================================
        ' Apparition du DataGridView1 et disparition du dataGridView2
        Form1.Timer3.Enabled = False
        Form1.Timer4.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DES TEXTES DE STOCKAGE DES TRAITEMENTS ÉLABORÉS
    '=============================================================================================================================================
    ' Texte 1 des traitements sur la Base
    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 1
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox1.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox1.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 2 des traitements sur la Base
    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 2
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox2.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox2.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox2_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox2.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 3 des traitements sur la Base
    Private Sub TextBox3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 3
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox3.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox3.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox3_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox3.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 4 des traitements sur la Base
    Private Sub TextBox4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 4
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox4.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox4.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox4_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox4.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 5 des traitements sur la Base
    Private Sub TextBox5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 5
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox5.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox5.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox5_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox5.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 6 des traitements sur la Base
    Private Sub TextBox6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox6.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 6
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox6.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox6.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox6_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox6.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 7 des traitements sur la Base
    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 7
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox7.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox7.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox7_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox7.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 8 des traitements sur la Base
    Private Sub TextBox8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox8.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 8
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox8.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox8.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox8_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox8.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 9 des traitements sur la Base
    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 9
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox9.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox9.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox9_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox9.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 10 des traitements sur la Base
    Private Sub TextBox10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 10
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox10.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox10.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox10_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox10.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 11 des traitements sur la Base
    Private Sub TextBox11_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox11.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 11
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox11.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox11.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox11_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox11.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 12 des traitements sur la Base
    Private Sub TextBox12_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 12
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox12.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox12.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox12_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox12.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 13 des traitements sur la Base
    Private Sub TextBox13_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox13.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 13
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox13.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox13.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox13_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox13.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 14 des traitements sur la Base
    Private Sub TextBox14_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox14.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 14
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox14.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox14.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox14_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox14.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 15 des traitements sur la Base
    Private Sub TextBox15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox15.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 15
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox15.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox15.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox15_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox15.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 16 des traitements sur la Base
    Private Sub TextBox16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox16.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 16
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox16.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox16.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox16_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox16.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 17 des traitements sur la Base
    Private Sub TextBox17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 17
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox17.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox17.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox17_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox17.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 18 des traitements sur la Base
    Private Sub TextBox18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 18
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox18.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox18.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox18_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox18.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 19 des traitements sur la Base
    Private Sub TextBox19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox19.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 19
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox19.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectBase = False
        If TextBox19.Text = "" Then Variables.SupDirectBase = True
    End Sub

    Private Sub TextBox19_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox19.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauBase()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 21 des traitements sur le dernier résultat
    Private Sub TextBox21_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox21.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 21
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox21.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox21.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox21_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox21.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 22 des traitements sur le dernier résultat
    Private Sub TextBox22_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox22.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 22
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox22.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox22.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox22_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox22.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 23 des traitements sur le dernier résultat
    Private Sub TextBox23_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox23.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 23
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox23.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox23.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox23_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox23.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 24 des traitements sur le dernier résultat
    Private Sub TextBox24_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox24.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 24
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox24.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox24.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox24_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox24.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 25 des traitements sur le dernier résultat
    Private Sub TextBox25_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox25.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 25
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox25.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox25.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox25_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox25.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 26 des traitements sur le dernier résultat
    Private Sub TextBox26_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox26.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 26
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox26.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox26.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox26_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox26.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 27 des traitements sur le dernier résultat
    Private Sub TextBox27_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox27.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 27
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox27.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox27.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox27_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox27.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 28 des traitements sur le dernier résultat
    Private Sub TextBox28_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox28.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 28
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox28.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox28.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox28_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox28.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 29 des traitements sur le dernier résultat
    Private Sub TextBox29_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox29.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 29
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox29.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox29.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox29_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox29.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 30 des traitements sur le dernier résultat
    Private Sub TextBox30_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox30.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 30
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox30.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox30.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox30_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox30.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 31 des traitements sur le dernier résultat
    Private Sub TextBox31_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox31.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 31
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox31.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox31.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox31_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox31.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 32 des traitements sur le dernier résultat
    Private Sub TextBox32_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox32.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 32
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox32.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox32.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox32_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox32.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 33 des traitements sur le dernier résultat
    Private Sub TextBox33_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox33.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 33
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox33.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox33.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox33_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox33.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 34 des traitements sur le dernier résultat
    Private Sub TextBox34_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox34.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 34
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox34.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox34.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox34_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox34.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 35 des traitements sur le dernier résultat
    Private Sub TextBox35_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox35.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 35
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox35.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox35.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox35_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox35.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 36 des traitements sur le dernier résultat
    Private Sub TextBox36_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox36.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 36
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox36.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox36.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox36_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox36.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 37 des traitements sur le dernier résultat
    Private Sub TextBox37_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox37.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 37
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox37.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox37.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox37_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox37.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 38 des traitements sur le dernier résultat
    Private Sub TextBox38_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox38.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 38
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox38.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox38.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox38_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox38.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 39 des traitements sur le dernier résultat
    Private Sub TextBox39_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox39.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Indicateur du numéro texte
        Variables.AiguilleurExtracteur = 39
        ' ==================================
        ' Programme de couleur de fond à blanc
        CouleurFondBlanc()
        ' ==================================
        ' Couleur fond en rose
        TextBox39.BackColor = Color.FromArgb(255, 210, 210)
        ' ==================================
        ' Positionnement pour un effacement direct si demandé
        Variables.SupDirectResu = False
        If TextBox39.Text = "" Then Variables.SupDirectResu = True
    End Sub

    Private Sub TextBox39_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox39.MouseDoubleClick
        ' ==================================
        ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
        TransfertInfosTableauResultat()
    End Sub

    ' ==============================================================================================================================================
    ' Texte 42 des résultats
    Private Sub TextBox42_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox42.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    ' ==============================================================================================================================================
    ' Coloration des fonds des textes en blanc
    Private Sub CouleurFondBlanc()
        TextBox1.BackColor = Color.White
        TextBox2.BackColor = Color.White
        TextBox3.BackColor = Color.White
        TextBox4.BackColor = Color.White
        TextBox5.BackColor = Color.White
        TextBox6.BackColor = Color.White
        TextBox7.BackColor = Color.White
        TextBox8.BackColor = Color.White
        TextBox9.BackColor = Color.White
        TextBox10.BackColor = Color.White
        TextBox11.BackColor = Color.White
        TextBox12.BackColor = Color.White
        TextBox13.BackColor = Color.White
        TextBox14.BackColor = Color.White
        TextBox15.BackColor = Color.White
        TextBox16.BackColor = Color.White
        TextBox17.BackColor = Color.White
        TextBox18.BackColor = Color.White
        TextBox19.BackColor = Color.White
        TextBox21.BackColor = Color.White
        TextBox22.BackColor = Color.White
        TextBox23.BackColor = Color.White
        TextBox24.BackColor = Color.White
        TextBox25.BackColor = Color.White
        TextBox26.BackColor = Color.White
        TextBox27.BackColor = Color.White
        TextBox28.BackColor = Color.White
        TextBox29.BackColor = Color.White
        TextBox30.BackColor = Color.White
        TextBox31.BackColor = Color.White
        TextBox32.BackColor = Color.White
        TextBox33.BackColor = Color.White
        TextBox34.BackColor = Color.White
        TextBox35.BackColor = Color.White
        TextBox36.BackColor = Color.White
        TextBox37.BackColor = Color.White
        TextBox38.BackColor = Color.White
        TextBox39.BackColor = Color.White
    End Sub

    ' ==============================================================================================================================================
    ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
    Private Sub TransfertInfosTableauBase()
        ' ==================================
        ' Positionnement des boutons radios
        RadioButton1.Checked = False
        RadioButton2.Checked = False
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 0) = "Je prends" Then
            RadioButton1.Checked = True
        End If
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 0) = "Je retire" Then
            RadioButton2.Checked = True
        End If
        ' ==================================
        ' Transfert dans le champ sujet
        TextBox46.Text = Variables.TabTraitBase(Variables.AiguilleurExtracteur, 1)
        ' ==================================
        ' Positionnement des boutons radios
        RadioButton3.Checked = False
        RadioButton4.Checked = False
        RadioButton5.Checked = False
        RadioButton6.Checked = False
        RadioButton7.Checked = False
        RadioButton8.Checked = False
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = "<" Then
            RadioButton3.Checked = True
        End If
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = "<=" Then
            RadioButton4.Checked = True
        End If
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = "=" Then
            RadioButton5.Checked = True
        End If
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = ">" Then
            RadioButton6.Checked = True
        End If
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = "=>" Then
            RadioButton7.Checked = True
        End If
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = "≠" Then
            RadioButton8.Checked = True
        End If
        ' ==================================
        ' Transfert du numéro de colonne correspondant dans la liste de sélection
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 20 Then ListBox1.SelectedIndex = 0
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 19 Then ListBox1.SelectedIndex = 1
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 23 Then ListBox1.SelectedIndex = 2
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 22 Then ListBox1.SelectedIndex = 3
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 21 Then ListBox1.SelectedIndex = 4
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 29 Then ListBox1.SelectedIndex = 5
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 4 Then ListBox1.SelectedIndex = 6
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 3 Then ListBox1.SelectedIndex = 7
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 8 Then ListBox1.SelectedIndex = 8
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 12 Then ListBox1.SelectedIndex = 9
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 18 Then ListBox1.SelectedIndex = 10
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 42 Then ListBox1.SelectedIndex = 11
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 14 Then ListBox1.SelectedIndex = 12
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 13 Then ListBox1.SelectedIndex = 13
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 5 Then ListBox1.SelectedIndex = 14
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 7 Then ListBox1.SelectedIndex = 15
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 34 Then ListBox1.SelectedIndex = 16
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 26 Then ListBox1.SelectedIndex = 17
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 25 Then ListBox1.SelectedIndex = 18
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 27 Then ListBox1.SelectedIndex = 19
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 32 Then ListBox1.SelectedIndex = 20
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 35 Then ListBox1.SelectedIndex = 21
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 31 Then ListBox1.SelectedIndex = 22
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 30 Then ListBox1.SelectedIndex = 23
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 38 Then ListBox1.SelectedIndex = 24
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 28 Then ListBox1.SelectedIndex = 25
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 37 Then ListBox1.SelectedIndex = 26
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 40 Then ListBox1.SelectedIndex = 27
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 1 Then ListBox1.SelectedIndex = 28
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 11 Then ListBox1.SelectedIndex = 29
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 6 Then ListBox1.SelectedIndex = 30
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 9 Then ListBox1.SelectedIndex = 31
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 16 Then ListBox1.SelectedIndex = 32
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 33 Then ListBox1.SelectedIndex = 33
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 15 Then ListBox1.SelectedIndex = 34
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 10 Then ListBox1.SelectedIndex = 35
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 41 Then ListBox1.SelectedIndex = 36
        If Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = 17 Then ListBox1.SelectedIndex = 37
        ' ==================================
        ' Transfert dans les champs de comparaison
        ' ==================================
        ' Adaptation du curseur et du texte en fonction de la sélection du type
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 36 Then
            TextBox41.Cursor = Cursors.Default
            TextBox41.ReadOnly = True
        Else
            TextBox41.Cursor = Cursors.IBeam
            TextBox41.ReadOnly = False
        End If
        ' ==================================
        ' Désactivation des signes '<', '<=', =>' et '>' pour les colonnes alphabétiques
        If ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
            RadioButton3.Enabled = False
            RadioButton4.Enabled = False
            RadioButton6.Enabled = False
            RadioButton7.Enabled = False
            RadioButton3.Checked = False
            RadioButton4.Checked = False
            RadioButton6.Checked = False
            RadioButton7.Checked = False
        Else
            RadioButton3.Enabled = True
            RadioButton4.Enabled = True
            RadioButton6.Enabled = True
            RadioButton7.Enabled = True
        End If
        ' ==================================
        ' Sélection des champs de comparaison
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
            ' ==================================
            ' Apparition du champ de comparaison
            TextBox41.Visible = True
            ' ==================================
            ' Chargement de la valeur de comapraison
            TextBox41.Text = Variables.TabTraitBase(Variables.AiguilleurExtracteur, 3)
            ' ==================================
            ' Initialisation de l'indicateur
            Variables.Barillet2 = 0
            If TextBox41.Text = "VIDE" Then Variables.Barillet2 = 1
            If TextBox41.Text = "0" Then Variables.Barillet2 = 2
        Else
            ' ==================================
            ' Chargement de la valeur de comapraison
            TextBox48.Text = Variables.TabTraitBase(Variables.AiguilleurExtracteur, 3)
            ' ==================================
            ' Calcul des valeurs Min Max
            ValeurMinMax()
            ' ==================================
            ' Changement du curseur sur le champ Autre
            TextBox48.Cursor = Cursors.IBeam
            ' ==================================
            ' Disparition du champ de comparaison
            TextBox41.Visible = False
            ' ==================================
            ' Initialisation de l'indicateur
            Variables.Barillet = 0
            If TextBox48.Text = "VIDE" Then Variables.Barillet = 1
            If TextBox48.Text = "0" Then Variables.Barillet = 2
        End If
        ' ==================================
        ' Afin de permettre l'apparition de la liste 1 si elle est demandée et éviter une deuxième demande
        Variables.NumeroList = 1
    End Sub

    ' ==============================================================================================================================================
    ' Transfert des informations du tableau correspondant dans les champs de l'élaboration de traitement
    Private Sub TransfertInfosTableauResultat()
        ' ==================================
        ' Positionnement des boutons radios
        RadioButton1.Checked = False
        RadioButton2.Checked = False
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 0) = "Je prends" Then
            RadioButton1.Checked = True
        End If
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 0) = "Je retire" Then
            RadioButton2.Checked = True
        End If
        ' ==================================
        ' Transfert dans le champ sujet
        TextBox46.Text = Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 1)
        ' ==================================
        ' Positionnement des boutons radios
        RadioButton3.Checked = False
        RadioButton4.Checked = False
        RadioButton5.Checked = False
        RadioButton6.Checked = False
        RadioButton7.Checked = False
        RadioButton8.Checked = False
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = "<" Then
            RadioButton3.Checked = True
        End If
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = "<=" Then
            RadioButton4.Checked = True
        End If
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = "=" Then
            RadioButton5.Checked = True
        End If
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = ">" Then
            RadioButton6.Checked = True
        End If
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = "=>" Then
            RadioButton7.Checked = True
        End If
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = "≠" Then
            RadioButton8.Checked = True
        End If
        ' ==================================
        ' Transfert du numéro de colonne correspondant dans la liste de sélection
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 20 Then ListBox1.SelectedIndex = 0
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 19 Then ListBox1.SelectedIndex = 1
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 23 Then ListBox1.SelectedIndex = 2
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 22 Then ListBox1.SelectedIndex = 3
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 21 Then ListBox1.SelectedIndex = 4
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 29 Then ListBox1.SelectedIndex = 5
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 4 Then ListBox1.SelectedIndex = 6
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 3 Then ListBox1.SelectedIndex = 7
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 8 Then ListBox1.SelectedIndex = 8
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 12 Then ListBox1.SelectedIndex = 9
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 18 Then ListBox1.SelectedIndex = 10

        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 42 Then ListBox1.SelectedIndex = 11
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 14 Then ListBox1.SelectedIndex = 12
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 13 Then ListBox1.SelectedIndex = 13
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 5 Then ListBox1.SelectedIndex = 14
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 7 Then ListBox1.SelectedIndex = 15
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 34 Then ListBox1.SelectedIndex = 16
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 26 Then ListBox1.SelectedIndex = 17
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 25 Then ListBox1.SelectedIndex = 18
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 27 Then ListBox1.SelectedIndex = 19
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 32 Then ListBox1.SelectedIndex = 20
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 35 Then ListBox1.SelectedIndex = 21
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 31 Then ListBox1.SelectedIndex = 22
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 30 Then ListBox1.SelectedIndex = 23
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 38 Then ListBox1.SelectedIndex = 24
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 28 Then ListBox1.SelectedIndex = 25
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 37 Then ListBox1.SelectedIndex = 26
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 40 Then ListBox1.SelectedIndex = 27
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 1 Then ListBox1.SelectedIndex = 28
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 11 Then ListBox1.SelectedIndex = 29
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 6 Then ListBox1.SelectedIndex = 30
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 9 Then ListBox1.SelectedIndex = 31
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 16 Then ListBox1.SelectedIndex = 32
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 33 Then ListBox1.SelectedIndex = 33

        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 15 Then ListBox1.SelectedIndex = 34
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 10 Then ListBox1.SelectedIndex = 35
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 41 Then ListBox1.SelectedIndex = 36
        If Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = 17 Then ListBox1.SelectedIndex = 37
        ' ==================================
        ' Transfert dans les champs de comparaison
        ' ==================================
        ' Adaptation du curseur et du texte en fonction de la sélection du type
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 36 Then
            TextBox41.Cursor = Cursors.Default
            TextBox41.ReadOnly = True
        Else
            TextBox41.Cursor = Cursors.IBeam
            TextBox41.ReadOnly = False
        End If
        ' ==================================
        ' Désactivation des signes '<', '<=', =>' et '>' pour les colonnes alphabétiques
        If ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
            RadioButton3.Enabled = False
            RadioButton4.Enabled = False
            RadioButton6.Enabled = False
            RadioButton7.Enabled = False
            RadioButton3.Checked = False
            RadioButton4.Checked = False
            RadioButton6.Checked = False
            RadioButton7.Checked = False
        Else
            RadioButton3.Enabled = True
            RadioButton4.Enabled = True
            RadioButton6.Enabled = True
            RadioButton7.Enabled = True
        End If
        ' ==================================
        ' Sélection des champs de comparaison
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
            ' ==================================
            ' Apparition du champ de comparaison
            TextBox41.Visible = True
            ' ==================================
            ' Chargement de la valeur de comapraison
            TextBox41.Text = Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 3)
            ' ==================================
            ' Initialisation de l'indicateur
            Variables.Barillet2 = 0
            If TextBox41.Text = "VIDE" Then Variables.Barillet2 = 1
            If TextBox41.Text = "0" Then Variables.Barillet2 = 2
        Else
            ' ==================================
            ' Chargement de la valeur de comapraison
            TextBox48.Text = Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 3)
            ' ==================================
            ' Calcul des valeurs Min Max
            ValeurMinMax()
            ' ==================================
            ' Changement du curseur sur le champ Autre
            TextBox48.Cursor = Cursors.IBeam
            ' ==================================
            ' Disparition du champ de comparaison
            TextBox41.Visible = False
            ' ==================================
            ' Initialisation de l'indicateur
            Variables.Barillet = 0
            If TextBox48.Text = "VIDE" Then Variables.Barillet = 1
            If TextBox48.Text = "0" Then Variables.Barillet = 2
        End If
        ' ==================================
        ' Afin de permettre l'apparition de la liste 1 si elle est demandée et éviter une deuxième demande
        Variables.NumeroList = 1
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DES BOUTONS CONCERNANT LES TRAITEMENTS
    '=============================================================================================================================================
    ' Bouton de validation des données de l'élaboration d'une extraction
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Traitement As String
        Dim AvecSans As String = ""
        Dim Signe As String = ""
        Dim Colonne As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Vérification de l'inclusion ou de l'exclusion 
        If RadioButton1.Checked = False And RadioButton2.Checked = False Then
            MsgBox(" Vous n'avez pas sélectionné 'Je prends' ou 'Je retire' !", MsgBoxStyle.Exclamation, " Erreur de validation")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Vérification du sujet de traitement
        If TextBox46.Text = "" Then
            MsgBox(" Vous n'avez pas sélectionné un sujet de traitement !", MsgBoxStyle.Exclamation, " Erreur de validation")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Vérification du signe de comparaison
        If RadioButton3.Checked = False And RadioButton4.Checked = False And RadioButton5.Checked = False And RadioButton6.Checked = False And RadioButton7.Checked = False And RadioButton8.Checked = False Then
            MsgBox(" Vous n'avez pas sélectionné un signe de comparaison !", MsgBoxStyle.Exclamation, " Erreur de validation")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Vérification de la valeur de comparaison
        ' ==================================
        ' Sélection du champ de comparaison
        If TextBox41.Visible = True Then
            ' ==================================
            ' Champ de comparaison alphabétique
            If TextBox41.Text = "" Then
                MsgBox(" Vous n'avez pas sélectionné une valeur de comparaison !", MsgBoxStyle.Exclamation, " Erreur de validation")
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        Else
            ' ==================================
            ' Champ de comparaison numérique et date
            If TextBox48.Text = "" Or TextBox48.Text = "Autre  =  Clic" Then
                MsgBox(" Vous n'avez pas sélectionné une valeur de comparaison !", MsgBoxStyle.Exclamation, " Erreur de validation")
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Vérification de la valeur de comparaison "Vide" par rapport aux signes de comparaison
        ' ==================================
        ' Sélection du champ de comparaison
        If TextBox41.Visible = True Then
            ' ==================================
            ' Champ de comparaison alphabétique
            If UCase(TextBox41.Text) = "VIDE" Then
                TextBox41.Text = "VIDE"
                If RadioButton5.Checked = False And RadioButton8.Checked = False Then
                    MsgBox(" La valeur de comparaison " & Chr(34) & "VIDE" & Chr(34) & " n'accepte que " & Chr(34) & "=" & Chr(34) & " ou " & Chr(34) & "≠" & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                    ' ==================================
                    ' Sortie
                    Exit Sub
                End If
            End If
        Else
            ' ==================================
            ' Champ de comparaison numérique et date
            If UCase(TextBox48.Text) = "VIDE" Then
                TextBox48.Text = "VIDE"
                If RadioButton5.Checked = False And RadioButton8.Checked = False Then
                    MsgBox(" La valeur de comparaison " & Chr(34) & "VIDE" & Chr(34) & " n'accepte que " & Chr(34) & "=" & Chr(34) & " ou " & Chr(34) & "≠" & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                    ' ==================================
                    ' Sortie
                    Exit Sub
                End If
            End If
        End If
        ' ==================================
        ' Vérification du bon formatage de la date de comparaison
        ' ==================================
        ' Sélection du champ de comparaison
        If TextBox41.Visible <> True Then
            ' ==================================
            ' Champ de comparaison date
            If UCase(TextBox48.Text) <> "VIDE" Then
                If TextBox46.Text = "Date d'achat" Or TextBox46.Text = "Date de livraison" Or TextBox46.Text = "Date de perte ou de destruction" Then
                    ' ==================================
                    ' Vérification de la longueur
                    If Len(TextBox48.Text) <> 10 Then
                        MsgBox("Le format de la date est " & Chr(34) & " JJ/MM/AAAA " & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                        Exit Sub
                    End If
                    ' ==================================
                    ' Vérification des intercalaires
                    If Strings.Mid(TextBox48.Text, 3, 1) <> "/" Or Strings.Mid(TextBox48.Text, 6, 1) <> "/" Then
                        MsgBox(" Les intercalaires de la date sont " & Chr(34) & " / " & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                        Exit Sub
                    End If
                    ' ==================================
                    ' Vérification des chiffres du jour
                    If Val(Strings.Left(TextBox48.Text, 2)) < 1 Or Val(Strings.Left(TextBox48.Text, 2)) > 31 Then
                        MsgBox(" Le jour de la date est entre le " & Chr(34) & " 01 " & Chr(34) & " et le " & Chr(34) & " 31 " & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                        Exit Sub
                    End If
                    ' ==================================
                    ' Vérification des chiffres du mois
                    If Val(Strings.Mid(TextBox48.Text, 4, 2)) < 1 Or Val(Strings.Mid(TextBox48.Text, 4, 2)) > 12 Then
                        MsgBox(" Le mois de la date est entre le " & Chr(34) & " 01 " & Chr(34) & " et le " & Chr(34) & " 12 " & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                        Exit Sub
                    End If
                    ' ==================================
                    ' Vérification des chiffres de l'année
                    If Val(Strings.Right(TextBox48.Text, 4)) < 1900 Or Val(Strings.Right(TextBox48.Text, 4)) > 2100 Then
                        MsgBox(" L'année de la date est entre " & Chr(34) & " 1900 " & Chr(34) & " et " & Chr(34) & " 2100 " & Chr(34) & " !", MsgBoxStyle.Exclamation, " Erreur de validation")
                        Exit Sub
                    End If
                End If
            End If
        End If
        ' ==================================
        ' Vérification de la sélection d'un champ texte
        If Variables.AiguilleurExtracteur = 0 Then
            MsgBox(" Vous n'avez pas sélectionné un champ de texte !", MsgBoxStyle.Exclamation, " Erreur de validation")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Analyse et transfert dans le champ texte sélectionné
        If RadioButton1.Checked = True Then
            AvecSans = "Je prends"
        End If
        If RadioButton2.Checked = True Then
            AvecSans = "Je retire"
        End If
        Traitement = " " & AvecSans & "  "
        ' ==================================
        ' Affectation du sujet de traitement
        Traitement = Traitement & TextBox46.Text
        ' ==================================
        ' Affectation du signe de comparaison
        If RadioButton3.Checked = True Then
            Signe = "<"
        End If
        If RadioButton4.Checked = True Then
            Signe = "<="
        End If
        If RadioButton5.Checked = True Then
            Signe = "="
        End If
        If RadioButton6.Checked = True Then
            Signe = ">"
        End If
        If RadioButton7.Checked = True Then
            Signe = "=>"
        End If
        If RadioButton8.Checked = True Then
            Signe = "≠"
        End If
        Traitement = Traitement & "  " & Signe & "  "
        ' ==================================
        ' Affectation de la valeur de comparaison
        ' ==================================
        ' Sélection du champ de comparaison
        If TextBox41.Visible = True Then
            ' ==================================
            ' Champ de comparaison alphabétique et date
            Traitement = Traitement & TextBox41.Text
        Else
            ' ==================================
            ' Champ de comparaison numérique
            Traitement = Traitement & TextBox48.Text
        End If
        ' ==================================
        ' Rangement dans le champ texte sélectionné du traitement sur la base
        If Variables.AiguilleurExtracteur = 1 Then TextBox1.Text = Traitement
        If Variables.AiguilleurExtracteur = 2 Then TextBox2.Text = Traitement
        If Variables.AiguilleurExtracteur = 3 Then TextBox3.Text = Traitement
        If Variables.AiguilleurExtracteur = 4 Then TextBox4.Text = Traitement
        If Variables.AiguilleurExtracteur = 5 Then TextBox5.Text = Traitement
        If Variables.AiguilleurExtracteur = 6 Then TextBox6.Text = Traitement
        If Variables.AiguilleurExtracteur = 7 Then TextBox7.Text = Traitement
        If Variables.AiguilleurExtracteur = 8 Then TextBox8.Text = Traitement
        If Variables.AiguilleurExtracteur = 9 Then TextBox9.Text = Traitement
        If Variables.AiguilleurExtracteur = 10 Then TextBox10.Text = Traitement
        If Variables.AiguilleurExtracteur = 11 Then TextBox11.Text = Traitement
        If Variables.AiguilleurExtracteur = 12 Then TextBox12.Text = Traitement
        If Variables.AiguilleurExtracteur = 13 Then TextBox13.Text = Traitement
        If Variables.AiguilleurExtracteur = 14 Then TextBox14.Text = Traitement
        If Variables.AiguilleurExtracteur = 15 Then TextBox15.Text = Traitement
        If Variables.AiguilleurExtracteur = 16 Then TextBox16.Text = Traitement
        If Variables.AiguilleurExtracteur = 17 Then TextBox17.Text = Traitement
        If Variables.AiguilleurExtracteur = 18 Then TextBox18.Text = Traitement
        If Variables.AiguilleurExtracteur = 19 Then TextBox19.Text = Traitement
        ' ==================================
        ' Rangement dans le champ texte sélectionné du traitement sur le dernier résultat
        If Variables.AiguilleurExtracteur = 21 Then TextBox21.Text = Traitement
        If Variables.AiguilleurExtracteur = 22 Then TextBox22.Text = Traitement
        If Variables.AiguilleurExtracteur = 23 Then TextBox23.Text = Traitement
        If Variables.AiguilleurExtracteur = 24 Then TextBox24.Text = Traitement
        If Variables.AiguilleurExtracteur = 25 Then TextBox25.Text = Traitement
        If Variables.AiguilleurExtracteur = 26 Then TextBox26.Text = Traitement
        If Variables.AiguilleurExtracteur = 27 Then TextBox27.Text = Traitement
        If Variables.AiguilleurExtracteur = 28 Then TextBox28.Text = Traitement
        If Variables.AiguilleurExtracteur = 29 Then TextBox29.Text = Traitement
        If Variables.AiguilleurExtracteur = 30 Then TextBox30.Text = Traitement
        If Variables.AiguilleurExtracteur = 31 Then TextBox31.Text = Traitement
        If Variables.AiguilleurExtracteur = 32 Then TextBox32.Text = Traitement
        If Variables.AiguilleurExtracteur = 33 Then TextBox33.Text = Traitement
        If Variables.AiguilleurExtracteur = 34 Then TextBox34.Text = Traitement
        If Variables.AiguilleurExtracteur = 35 Then TextBox35.Text = Traitement
        If Variables.AiguilleurExtracteur = 36 Then TextBox36.Text = Traitement
        If Variables.AiguilleurExtracteur = 37 Then TextBox37.Text = Traitement
        If Variables.AiguilleurExtracteur = 38 Then TextBox38.Text = Traitement
        If Variables.AiguilleurExtracteur = 39 Then TextBox39.Text = Traitement
        ' ==================================
        ' TransCodage du numéro de colonne pour la recherche
        If ListBox1.SelectedIndex = 0 Then Colonne = 20
        If ListBox1.SelectedIndex = 1 Then Colonne = 19
        If ListBox1.SelectedIndex = 2 Then Colonne = 23
        If ListBox1.SelectedIndex = 3 Then Colonne = 22
        If ListBox1.SelectedIndex = 4 Then Colonne = 21
        If ListBox1.SelectedIndex = 5 Then Colonne = 29
        If ListBox1.SelectedIndex = 6 Then Colonne = 4
        If ListBox1.SelectedIndex = 7 Then Colonne = 3
        If ListBox1.SelectedIndex = 8 Then Colonne = 8
        If ListBox1.SelectedIndex = 9 Then Colonne = 12
        If ListBox1.SelectedIndex = 10 Then Colonne = 18

        If ListBox1.SelectedIndex = 11 Then Colonne = 42 '14
        If ListBox1.SelectedIndex = 12 Then Colonne = 14 '13
        If ListBox1.SelectedIndex = 13 Then Colonne = 13 '5
        If ListBox1.SelectedIndex = 14 Then Colonne = 5 '7
        If ListBox1.SelectedIndex = 15 Then Colonne = 7 '34
        If ListBox1.SelectedIndex = 16 Then Colonne = 34 '26
        If ListBox1.SelectedIndex = 17 Then Colonne = 26 '25
        If ListBox1.SelectedIndex = 18 Then Colonne = 25 '27
        If ListBox1.SelectedIndex = 19 Then Colonne = 27 '32
        If ListBox1.SelectedIndex = 20 Then Colonne = 32 '35
        If ListBox1.SelectedIndex = 21 Then Colonne = 35 '31
        If ListBox1.SelectedIndex = 22 Then Colonne = 31 '30
        If ListBox1.SelectedIndex = 23 Then Colonne = 30 '38
        If ListBox1.SelectedIndex = 24 Then Colonne = 38 '28
        If ListBox1.SelectedIndex = 25 Then Colonne = 28 '37
        If ListBox1.SelectedIndex = 26 Then Colonne = 37 '40
        If ListBox1.SelectedIndex = 27 Then Colonne = 40 '1
        If ListBox1.SelectedIndex = 28 Then Colonne = 1 '11
        If ListBox1.SelectedIndex = 29 Then Colonne = 11 '6
        If ListBox1.SelectedIndex = 30 Then Colonne = 6 '9
        If ListBox1.SelectedIndex = 31 Then Colonne = 9 '16
        If ListBox1.SelectedIndex = 32 Then Colonne = 16 '33
        If ListBox1.SelectedIndex = 33 Then Colonne = 33 '42

        If ListBox1.SelectedIndex = 34 Then Colonne = 15
        If ListBox1.SelectedIndex = 35 Then Colonne = 10
        If ListBox1.SelectedIndex = 36 Then Colonne = 41
        If ListBox1.SelectedIndex = 37 Then Colonne = 17
        ' ==================================
        ' Sélection du champ de comparaison et rangement dans le tableau correspondant
        If TextBox41.Visible = True Then
            ' ==================================
            ' Champ de comparaison alphabétique et date
            ' ==================================
            ' Sélection des traitements sur la base
            If Variables.AiguilleurExtracteur < 20 Then
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 0) = AvecSans
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 1) = TextBox46.Text
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = Signe
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 3) = TextBox41.Text
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = Colonne
                ' ==================================
                ' Sélection des traitements sur le dernier résultat
            Else
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 0) = AvecSans
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 1) = TextBox46.Text
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = Signe
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 3) = TextBox41.Text
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = Colonne
            End If
            ' ==================================
            ' Champ de comparaison numérique
        Else
            ' ==================================
            ' Sélection des traitements sur la base
            If Variables.AiguilleurExtracteur < 20 Then
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 0) = AvecSans
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 1) = TextBox46.Text
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = Signe
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 3) = TextBox48.Text
                Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = Colonne
                ' ==================================
                ' Sélection des traitements sur le dernier résultat
            Else
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 0) = AvecSans
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 1) = TextBox46.Text
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = Signe
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 3) = TextBox48.Text
                Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = Colonne
            End If
        End If
        ' ==================================
        ' Effacement de la sélection d'un champ texte
        Variables.AiguilleurExtracteur = 0
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton d'effacement des données de l'élaboration d'une extraction
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim i As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Demande de confirmation d'effacement
        i = MsgBox("Effacement des informations !" & vbCrLf & vbCrLf & "           Confirmez ?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, " Confirmation d'effacement")
        If i = 7 Then Exit Sub
        ' ==================================
        ' Effacement
        RadioButton1.Checked = False
        RadioButton2.Checked = False
        RadioButton3.Checked = False
        RadioButton4.Checked = False
        RadioButton5.Checked = False
        RadioButton6.Checked = False
        RadioButton7.Checked = False
        RadioButton8.Checked = False
        TextBox46.Text = "Sélection d'un sujet de traitement (clic)"
        TextBox41.Text = "Sélection d'une valeur de comparaison (clavier ou clic)"
        TextBox47.Text = ""
        TextBox48.Text = ""
        TextBox49.Text = ""
        TextBox47.Visible = False
        TextBox48.Visible = False
        TextBox49.Visible = False
        TextBox41.Visible = True
        TextBox41.Cursor = Cursors.Default
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de suppression de la sélection du champ de traitements sur la base
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim i As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Vérification de la sélection d'un champ texte
        If Variables.AiguilleurExtracteur = 0 Or Variables.AiguilleurExtracteur > 19 Then
            MsgBox(" Vous n'avez pas sélectionné un champ de texte !", MsgBoxStyle.Exclamation, " Erreur de suppression")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Demande de confirmation de suppression
        If Variables.SupDirectBase = False Then
            i = MsgBox("Suppression des informations !" & vbCrLf & vbCrLf & "           Confirmez ?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, " Confirmation de suppression")
            If i = 7 Then Exit Sub
        End If
        ' ==================================
        ' Décalages des textes
        If Variables.AiguilleurExtracteur = 1 Then TextBox1.Text = TextBox2.Text
        If Variables.AiguilleurExtracteur <= 2 Then TextBox2.Text = TextBox3.Text
        If Variables.AiguilleurExtracteur <= 3 Then TextBox3.Text = TextBox4.Text
        If Variables.AiguilleurExtracteur <= 4 Then TextBox4.Text = TextBox5.Text
        If Variables.AiguilleurExtracteur <= 5 Then TextBox5.Text = TextBox6.Text
        If Variables.AiguilleurExtracteur <= 6 Then TextBox6.Text = TextBox7.Text
        If Variables.AiguilleurExtracteur <= 7 Then TextBox7.Text = TextBox8.Text
        If Variables.AiguilleurExtracteur <= 8 Then TextBox8.Text = TextBox9.Text
        If Variables.AiguilleurExtracteur <= 9 Then TextBox9.Text = TextBox10.Text
        If Variables.AiguilleurExtracteur <= 10 Then TextBox10.Text = TextBox11.Text
        If Variables.AiguilleurExtracteur <= 11 Then TextBox11.Text = TextBox12.Text
        If Variables.AiguilleurExtracteur <= 12 Then TextBox12.Text = TextBox13.Text
        If Variables.AiguilleurExtracteur <= 13 Then TextBox13.Text = TextBox14.Text
        If Variables.AiguilleurExtracteur <= 14 Then TextBox14.Text = TextBox15.Text
        If Variables.AiguilleurExtracteur <= 15 Then TextBox15.Text = TextBox16.Text
        If Variables.AiguilleurExtracteur <= 16 Then TextBox16.Text = TextBox17.Text
        If Variables.AiguilleurExtracteur <= 17 Then TextBox17.Text = TextBox18.Text
        If Variables.AiguilleurExtracteur <= 18 Then TextBox18.Text = TextBox19.Text
        TextBox19.Text = ""
        ' ==================================
        ' Décalages dans le tableau correspondant
        For i = Variables.AiguilleurExtracteur To 18
            Variables.TabTraitBase(i, 0) = Variables.TabTraitBase(i + 1, 0)
            Variables.TabTraitBase(i, 1) = Variables.TabTraitBase(i + 1, 1)
            Variables.TabTraitBase(i, 2) = Variables.TabTraitBase(i + 1, 2)
            Variables.TabTraitBase(i, 3) = Variables.TabTraitBase(i + 1, 3)
            Variables.TabTraitBase(i, 4) = Variables.TabTraitBase(i + 1, 4)
            Variables.TabTraitBase(i, 5) = Variables.TabTraitBase(i + 1, 5)
        Next
        Variables.TabTraitBase(19, 0) = ""
        Variables.TabTraitBase(19, 1) = ""
        Variables.TabTraitBase(19, 2) = ""
        Variables.TabTraitBase(19, 3) = ""
        Variables.TabTraitBase(19, 4) = ""
        Variables.TabTraitBase(19, 5) = ""
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
        Variables.AiguilleurExtracteur = 0
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de suppression des champs de traitements sur la base
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim i As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Demande de confirmation de suppression
        i = MsgBox("Suppression des informations !" & vbCrLf & vbCrLf & "           Confirmez ?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, " Confirmation de suppression")
        If i = 7 Then Exit Sub
        ' ==================================
        ' Suppression
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        TextBox17.Text = ""
        TextBox18.Text = ""
        TextBox19.Text = ""
        ' ==================================
        ' Suppression dans le tableau correspondant
        For i = 0 To 19
            Variables.TabTraitBase(i, 0) = ""
            Variables.TabTraitBase(i, 1) = ""
            Variables.TabTraitBase(i, 2) = ""
            Variables.TabTraitBase(i, 3) = ""
            Variables.TabTraitBase(i, 4) = ""
            Variables.TabTraitBase(i, 5) = ""
        Next
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton d'insertion d'un champ de traitements sur la base
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Vérification de la sélection d'un champ texte
        If Variables.AiguilleurExtracteur = 0 Or Variables.AiguilleurExtracteur > 19 Then
            MsgBox(" Vous n'avez pas sélectionné un champ de texte !", MsgBoxStyle.Exclamation, " Erreur d'insertion")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Vérification de la possibilité d'insertion d'un champ texte
        If TextBox19.Text <> "" Then
            MsgBox(" Vous n'avez pas la possibilité d'insérer un champ de texte !", MsgBoxStyle.Exclamation, " Erreur d'insertion")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Décalages des textes
        If Variables.AiguilleurExtracteur < 19 Then TextBox19.Text = TextBox18.Text
        If Variables.AiguilleurExtracteur < 18 Then TextBox18.Text = TextBox17.Text
        If Variables.AiguilleurExtracteur < 17 Then TextBox17.Text = TextBox16.Text
        If Variables.AiguilleurExtracteur < 16 Then TextBox16.Text = TextBox15.Text
        If Variables.AiguilleurExtracteur < 15 Then TextBox15.Text = TextBox14.Text
        If Variables.AiguilleurExtracteur < 14 Then TextBox14.Text = TextBox13.Text
        If Variables.AiguilleurExtracteur < 13 Then TextBox13.Text = TextBox12.Text
        If Variables.AiguilleurExtracteur < 12 Then TextBox12.Text = TextBox11.Text
        If Variables.AiguilleurExtracteur < 11 Then TextBox11.Text = TextBox10.Text
        If Variables.AiguilleurExtracteur < 10 Then TextBox10.Text = TextBox9.Text
        If Variables.AiguilleurExtracteur < 9 Then TextBox9.Text = TextBox8.Text
        If Variables.AiguilleurExtracteur < 8 Then TextBox8.Text = TextBox7.Text
        If Variables.AiguilleurExtracteur < 7 Then TextBox7.Text = TextBox6.Text
        If Variables.AiguilleurExtracteur < 6 Then TextBox6.Text = TextBox5.Text
        If Variables.AiguilleurExtracteur < 5 Then TextBox5.Text = TextBox4.Text
        If Variables.AiguilleurExtracteur < 4 Then TextBox4.Text = TextBox3.Text
        If Variables.AiguilleurExtracteur < 3 Then TextBox3.Text = TextBox2.Text
        If Variables.AiguilleurExtracteur < 2 Then TextBox2.Text = TextBox1.Text
        ' ==================================
        ' Effacement du texte
        If Variables.AiguilleurExtracteur = 18 Then TextBox18.Text = ""
        If Variables.AiguilleurExtracteur = 17 Then TextBox17.Text = ""
        If Variables.AiguilleurExtracteur = 16 Then TextBox16.Text = ""
        If Variables.AiguilleurExtracteur = 15 Then TextBox15.Text = ""
        If Variables.AiguilleurExtracteur = 14 Then TextBox14.Text = ""
        If Variables.AiguilleurExtracteur = 13 Then TextBox13.Text = ""
        If Variables.AiguilleurExtracteur = 12 Then TextBox12.Text = ""
        If Variables.AiguilleurExtracteur = 11 Then TextBox11.Text = ""
        If Variables.AiguilleurExtracteur = 10 Then TextBox10.Text = ""
        If Variables.AiguilleurExtracteur = 9 Then TextBox9.Text = ""
        If Variables.AiguilleurExtracteur = 8 Then TextBox8.Text = ""
        If Variables.AiguilleurExtracteur = 7 Then TextBox7.Text = ""
        If Variables.AiguilleurExtracteur = 6 Then TextBox6.Text = ""
        If Variables.AiguilleurExtracteur = 5 Then TextBox5.Text = ""
        If Variables.AiguilleurExtracteur = 4 Then TextBox4.Text = ""
        If Variables.AiguilleurExtracteur = 3 Then TextBox3.Text = ""
        If Variables.AiguilleurExtracteur = 2 Then TextBox2.Text = ""
        If Variables.AiguilleurExtracteur = 1 Then TextBox1.Text = ""
        ' ==================================
        ' Décalages dans le tableau correspondant
        For i = 18 To Variables.AiguilleurExtracteur Step -1
            Variables.TabTraitBase(i + 1, 0) = Variables.TabTraitBase(i, 0)
            Variables.TabTraitBase(i + 1, 1) = Variables.TabTraitBase(i, 1)
            Variables.TabTraitBase(i + 1, 2) = Variables.TabTraitBase(i, 2)
            Variables.TabTraitBase(i + 1, 3) = Variables.TabTraitBase(i, 3)
            Variables.TabTraitBase(i + 1, 4) = Variables.TabTraitBase(i, 4)
            Variables.TabTraitBase(i + 1, 5) = Variables.TabTraitBase(i, 5)
        Next
        Variables.TabTraitBase(Variables.AiguilleurExtracteur, 0) = ""
        Variables.TabTraitBase(Variables.AiguilleurExtracteur, 1) = ""
        Variables.TabTraitBase(Variables.AiguilleurExtracteur, 2) = ""
        Variables.TabTraitBase(Variables.AiguilleurExtracteur, 3) = ""
        Variables.TabTraitBase(Variables.AiguilleurExtracteur, 4) = ""
        Variables.TabTraitBase(Variables.AiguilleurExtracteur, 5) = ""
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
        Variables.AiguilleurExtracteur = 0
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de suppression de la sélection du champ de traitements sur le dernier résultat
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim i As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Vérification de la sélection d'un champ texte
        If Variables.AiguilleurExtracteur < 21 Then
            MsgBox(" Vous n'avez pas sélectionné un champ de texte !", MsgBoxStyle.Exclamation, " Erreur de suppression")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Demande de confirmation de suppression
        If Variables.SupDirectResu = False Then
            i = MsgBox("Suppression des informations !" & vbCrLf & vbCrLf & "           Confirmez ?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, " Confirmation de suppression")
            If i = 7 Then Exit Sub
        End If
        ' ==================================
        ' Décalages des textes
        If Variables.AiguilleurExtracteur = 21 Then TextBox21.Text = TextBox22.Text
        If Variables.AiguilleurExtracteur <= 22 Then TextBox22.Text = TextBox23.Text
        If Variables.AiguilleurExtracteur <= 23 Then TextBox23.Text = TextBox24.Text
        If Variables.AiguilleurExtracteur <= 24 Then TextBox24.Text = TextBox25.Text
        If Variables.AiguilleurExtracteur <= 25 Then TextBox25.Text = TextBox26.Text
        If Variables.AiguilleurExtracteur <= 26 Then TextBox26.Text = TextBox27.Text
        If Variables.AiguilleurExtracteur <= 27 Then TextBox27.Text = TextBox28.Text
        If Variables.AiguilleurExtracteur <= 28 Then TextBox28.Text = TextBox29.Text
        If Variables.AiguilleurExtracteur <= 29 Then TextBox29.Text = TextBox30.Text
        If Variables.AiguilleurExtracteur <= 30 Then TextBox30.Text = TextBox31.Text
        If Variables.AiguilleurExtracteur <= 31 Then TextBox31.Text = TextBox32.Text
        If Variables.AiguilleurExtracteur <= 32 Then TextBox32.Text = TextBox33.Text
        If Variables.AiguilleurExtracteur <= 33 Then TextBox33.Text = TextBox34.Text
        If Variables.AiguilleurExtracteur <= 34 Then TextBox34.Text = TextBox35.Text
        If Variables.AiguilleurExtracteur <= 35 Then TextBox35.Text = TextBox36.Text
        If Variables.AiguilleurExtracteur <= 36 Then TextBox36.Text = TextBox37.Text
        If Variables.AiguilleurExtracteur <= 37 Then TextBox37.Text = TextBox38.Text
        If Variables.AiguilleurExtracteur <= 38 Then TextBox38.Text = TextBox39.Text
        TextBox39.Text = ""
        ' ==================================
        ' Décalages dans le tableau correspondant
        For i = Variables.AiguilleurExtracteur To 38
            Variables.TabTraitResul(i - 20, 0) = Variables.TabTraitResul(i - 19, 0)
            Variables.TabTraitResul(i - 20, 1) = Variables.TabTraitResul(i - 19, 1)
            Variables.TabTraitResul(i - 20, 2) = Variables.TabTraitResul(i - 19, 2)
            Variables.TabTraitResul(i - 20, 3) = Variables.TabTraitResul(i - 19, 3)
            Variables.TabTraitResul(i - 20, 4) = Variables.TabTraitResul(i - 19, 4)
            Variables.TabTraitResul(i - 20, 5) = Variables.TabTraitResul(i - 19, 5)
        Next
        Variables.TabTraitResul(19, 0) = ""
        Variables.TabTraitResul(19, 1) = ""
        Variables.TabTraitResul(19, 2) = ""
        Variables.TabTraitResul(19, 3) = ""
        Variables.TabTraitResul(19, 4) = ""
        Variables.TabTraitResul(19, 5) = ""
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
        Variables.AiguilleurExtracteur = 0
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de suppression des champs de traitements sur le dernier résultat
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Dim i As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Demande de confirmation de suppression
        i = MsgBox("Suppression des informations !" & vbCrLf & vbCrLf & "           Confirmez ?", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, " Confirmation de suppression")
        If i = 7 Then Exit Sub
        ' ==================================
        ' Rangement dans le champ texte sélectionné du traitement sur le dernier résultat
        TextBox21.Text = ""
        TextBox22.Text = ""
        TextBox23.Text = ""
        TextBox24.Text = ""
        TextBox25.Text = ""
        TextBox26.Text = ""
        TextBox27.Text = ""
        TextBox28.Text = ""
        TextBox29.Text = ""
        TextBox30.Text = ""
        TextBox31.Text = ""
        TextBox32.Text = ""
        TextBox33.Text = ""
        TextBox34.Text = ""
        TextBox35.Text = ""
        TextBox36.Text = ""
        TextBox37.Text = ""
        TextBox38.Text = ""
        TextBox39.Text = ""
        ' ==================================
        ' Suppression dans le tableau correspondant
        For i = 0 To 19
            Variables.TabTraitResul(i, 0) = ""
            Variables.TabTraitResul(i, 1) = ""
            Variables.TabTraitResul(i, 2) = ""
            Variables.TabTraitResul(i, 3) = ""
            Variables.TabTraitResul(i, 4) = ""
            Variables.TabTraitResul(i, 5) = ""
        Next
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton d'insertion d'un champ de traitements sur le dernier résultat
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Vérification de la sélection d'un champ texte
        If Variables.AiguilleurExtracteur < 21 Then
            MsgBox(" Vous n'avez pas sélectionné un champ de texte !", MsgBoxStyle.Exclamation, " Erreur d'insertion")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Vérification de la possibilité d'insertion d'un champ texte
        If TextBox39.Text <> "" Then
            MsgBox(" Vous n'avez pas la possibilité d'insérer un champ de texte !", MsgBoxStyle.Exclamation, " Erreur d'insertion")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Décalages des textes
        If Variables.AiguilleurExtracteur < 39 Then TextBox39.Text = TextBox38.Text
        If Variables.AiguilleurExtracteur < 38 Then TextBox38.Text = TextBox37.Text
        If Variables.AiguilleurExtracteur < 37 Then TextBox37.Text = TextBox36.Text
        If Variables.AiguilleurExtracteur < 36 Then TextBox36.Text = TextBox35.Text
        If Variables.AiguilleurExtracteur < 35 Then TextBox35.Text = TextBox34.Text
        If Variables.AiguilleurExtracteur < 34 Then TextBox34.Text = TextBox33.Text
        If Variables.AiguilleurExtracteur < 33 Then TextBox33.Text = TextBox32.Text
        If Variables.AiguilleurExtracteur < 32 Then TextBox32.Text = TextBox31.Text
        If Variables.AiguilleurExtracteur < 31 Then TextBox31.Text = TextBox30.Text
        If Variables.AiguilleurExtracteur < 30 Then TextBox30.Text = TextBox29.Text
        If Variables.AiguilleurExtracteur < 29 Then TextBox29.Text = TextBox28.Text
        If Variables.AiguilleurExtracteur < 28 Then TextBox28.Text = TextBox27.Text
        If Variables.AiguilleurExtracteur < 27 Then TextBox27.Text = TextBox26.Text
        If Variables.AiguilleurExtracteur < 26 Then TextBox26.Text = TextBox25.Text
        If Variables.AiguilleurExtracteur < 25 Then TextBox25.Text = TextBox24.Text
        If Variables.AiguilleurExtracteur < 24 Then TextBox24.Text = TextBox23.Text
        If Variables.AiguilleurExtracteur < 23 Then TextBox23.Text = TextBox22.Text
        If Variables.AiguilleurExtracteur < 22 Then TextBox22.Text = TextBox21.Text
        ' ==================================
        ' Effacement du texte
        If Variables.AiguilleurExtracteur = 38 Then TextBox38.Text = ""
        If Variables.AiguilleurExtracteur = 37 Then TextBox37.Text = ""
        If Variables.AiguilleurExtracteur = 36 Then TextBox36.Text = ""
        If Variables.AiguilleurExtracteur = 35 Then TextBox35.Text = ""
        If Variables.AiguilleurExtracteur = 34 Then TextBox34.Text = ""
        If Variables.AiguilleurExtracteur = 33 Then TextBox33.Text = ""
        If Variables.AiguilleurExtracteur = 32 Then TextBox32.Text = ""
        If Variables.AiguilleurExtracteur = 31 Then TextBox31.Text = ""
        If Variables.AiguilleurExtracteur = 30 Then TextBox30.Text = ""
        If Variables.AiguilleurExtracteur = 29 Then TextBox29.Text = ""
        If Variables.AiguilleurExtracteur = 28 Then TextBox28.Text = ""
        If Variables.AiguilleurExtracteur = 27 Then TextBox27.Text = ""
        If Variables.AiguilleurExtracteur = 26 Then TextBox26.Text = ""
        If Variables.AiguilleurExtracteur = 25 Then TextBox25.Text = ""
        If Variables.AiguilleurExtracteur = 24 Then TextBox24.Text = ""
        If Variables.AiguilleurExtracteur = 23 Then TextBox23.Text = ""
        If Variables.AiguilleurExtracteur = 22 Then TextBox22.Text = ""
        If Variables.AiguilleurExtracteur = 21 Then TextBox21.Text = ""
        ' ==================================
        ' Décalages dans le tableau correspondant
        For i = 38 To Variables.AiguilleurExtracteur Step -1
            Variables.TabTraitResul(i - 19, 0) = Variables.TabTraitResul(i - 20, 0)
            Variables.TabTraitResul(i - 19, 1) = Variables.TabTraitResul(i - 20, 1)
            Variables.TabTraitResul(i - 19, 2) = Variables.TabTraitResul(i - 20, 2)
            Variables.TabTraitResul(i - 19, 3) = Variables.TabTraitResul(i - 20, 3)
            Variables.TabTraitResul(i - 19, 4) = Variables.TabTraitResul(i - 20, 4)
            Variables.TabTraitResul(i - 19, 5) = Variables.TabTraitResul(i - 20, 5)
        Next
        Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 0) = ""
        Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 1) = ""
        Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 2) = ""
        Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 3) = ""
        Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 4) = ""
        Variables.TabTraitResul(Variables.AiguilleurExtracteur - 20, 5) = ""
        ' ==================================
        ' Effacement de la couleur de fond du champ texte sélectionné
        CouleurFondBlanc()
        Variables.AiguilleurExtracteur = 0
    End Sub


    ' ==============================================================================================================================================
    ' PROCÉDURES DES TRAITEMENTS SUR LA BASE ET SUR LE DERNIER RÉSULTAT
    ' ==============================================================================================================================================
    ' Bouton de lancement d'une recherche
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        ' ==================================
        ' Initialisation des variables
        Dim j As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Vérification de l'existence de la base
        If Strings.Left(ChargeurBase.TextBox1.Text, 12) <> "Base chargée" Then
            MsgBox(" Vous n'avez pas chargé la Base !", MsgBoxStyle.Exclamation, " Erreur de recherche")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Vérification de l'existence d'une combinaison de recherche
        If TextBox1.Text = "" Then
            MsgBox(" Vous n'avez pas de traitement sur la Base !", MsgBoxStyle.Exclamation, " Erreur de recherche")
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Remise à zéro de la colonne de réponses 43
        For j = 0 To Variables.FinBase
            Variables.Base(j, 43) = 0
        Next
        ' ==================================
        ' Effacement du champ du résultat de la recherche
        TextBox42.Text = ""
        ' ==================================
        ' Initialisation de l'index du tableau de traitement sur la base
        Variables.IndexTabTraitBase = 1
        ' ==================================
        ' Lancement de la recherche
        Timer8.Enabled = True
        ' ==================================
        ' Initialisation des champs de comparaison
        TextBox47.Text = "Min  =  Clic"
        TextBox48.Text = "Autre  =  Clic"
        TextBox49.Text = "Max  =  Clic"
        TextBox48.Cursor = Cursors.Default
        ' ==================================
        ' Initialisation des champs de comparaison du Tableau
        Tableau.TextBox50.Text = "Min  =  Clic"
        Tableau.TextBox51.Text = "Max  =  Clic"
        Tableau.TextBox108.Text = "Min  =  Clic"
        Tableau.TextBox109.Text = "Max  =  Clic"
        Tableau.TextBox144.Text = "Min  =  Clic"
        Tableau.TextBox145.Text = "Max  =  Clic"
        Tableau.TextBox180.Text = "Min  =  Clic"
        Tableau.TextBox181.Text = "Max  =  Clic"
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de listage des résultats dans le DataGridView
    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim j As Integer
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Effacement des lignes du DataGridView
        Form1.DataGridView1.Rows.Clear()
        ' ==================================
        ' Inhibition si le premier champ des 2 tableaux est vide
        '        If TextBox1.Text = "" And TextBox21.Text = "" Then
        '        Button10.Enabled = False
        '        End If
        ' ==================================
        ' Initialisation de la variable du DataGridView
        Variables.NumLigneData = -1
        ' ==================================
        ' Recherche des lignes sélectionnées dans le tableau sur la Base
        For j = 1 To Variables.FinBase
            If Variables.Base(j, 43) = 1 Then
                Form1.DataGridView1.Rows.Add()
                Variables.NumLigneBase = j
                Variables.NumLigneData = Variables.NumLigneData + 1
                TransfertDonnées()
            End If
        Next
    End Sub

    '=============================================================================================================================================
    ' Procédure de transfert des informations sélectionnées dans la Base dans le DatagridView
    Public Sub TransfertDonnées()
        Dim ColumName As String
        Dim PermutDate As String
        ' ==================================
        ' Numéro Ligne
        ColumName = "ColNumLigne" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.NumLigneBase
        ' ==================================
        ' Numéro Fairplay
        If Variables.Base(Variables.NumLigneBase, 1) <> "" Then
            ColumName = "ColNumFairplay" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 1))
        End If
        ' ==================================
        ' Ex Numéro Fairplay
        If Variables.Base(Variables.NumLigneBase, 2) <> "" Then
            ColumName = "ColExNumFairplay" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 2))
        End If
        ' ==================================
        ' Code du navire
        ColumName = "ColCode" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 3)
        ' ==================================
        ' Classe du navire
        If Variables.Base(Variables.NumLigneBase, 4) <> "" Then
            ColumName = "ColClasse" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 4))
        End If
        ' ==================================
        ' Nom du navire
        ColumName = "ColNomNavire" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 5)
        ' ==================================
        ' Pavillon du navire
        ColumName = "ColPavillon" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 6)
        ' ==================================
        ' Nom du propriétaire
        ColumName = "ColNomProprietaire" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 7)
        ' ==================================
        ' Date d'achat
        If Variables.Base(Variables.NumLigneBase, 8) <> "" Then
            PermutDate = Variables.Base(Variables.NumLigneBase, 8)
            ColumName = "ColDateAchat" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Strings.Right(PermutDate, 4) & Strings.Mid(PermutDate, 3, 4) & Strings.Left(PermutDate, 2)
        End If
        ' ==================================
        ' Port en lourd
        If Variables.Base(Variables.NumLigneBase, 9) <> "" Then
            ColumName = "ColPortLourd" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 9))
        End If
        ' ==================================
        ' Tonnage en brut
        If Variables.Base(Variables.NumLigneBase, 10) <> "" Then
            ColumName = "ColTonnage" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 10))
        End If
        ' ==================================
        ' Numéro LLOYDS
        If Variables.Base(Variables.NumLigneBase, 11) <> "" Then
            ColumName = "ColNumLloyds" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 11))
        End If
        ' ==================================
        ' Date de livraison
        If Variables.Base(Variables.NumLigneBase, 12) <> "" Then
            PermutDate = Variables.Base(Variables.NumLigneBase, 12)
            ColumName = "ColDateLivraison" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Strings.Right(PermutDate, 4) & Strings.Mid(PermutDate, 3, 4) & Strings.Left(PermutDate, 2)
        End If
        ' ==================================
        ' Longueur
        If Variables.Base(Variables.NumLigneBase, 13) <> "" Then
            ColumName = "ColLongueur" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 13))
        End If
        ' ==================================
        ' Largeur
        If Variables.Base(Variables.NumLigneBase, 14) <> "" Then
            ColumName = "ColLargeur" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 14))
        End If
        ' ==================================
        ' Tirant d'eau
        If Variables.Base(Variables.NumLigneBase, 15) <> "" Then
            ColumName = "ColTirantEau" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 15))
        End If
        ' ==================================
        ' Profondeur
        If Variables.Base(Variables.NumLigneBase, 16) <> "" Then
            ColumName = "ColProfondeur" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 16))
        End If
        ' ==================================
        ' Vitesse
        If Variables.Base(Variables.NumLigneBase, 17) <> "" Then
            ColumName = "ColVitesse" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 17))
        End If
        ' ==================================
        ' Date de perte ou de destruction
        If Variables.Base(Variables.NumLigneBase, 18) <> "" Then
            PermutDate = Variables.Base(Variables.NumLigneBase, 18)
            ColumName = "ColDatePerte" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Strings.Right(PermutDate, 4) & Strings.Mid(PermutDate, 3, 4) & Strings.Left(PermutDate, 2)
        End If
        ' ==================================
        ' Capacité en céréale
        If Variables.Base(Variables.NumLigneBase, 19) <> "" Then
            ColumName = "ColCapCereale" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 19))
        End If
        ' ==================================
        ' Capacité en balles
        If Variables.Base(Variables.NumLigneBase, 20) <> "" Then
            ColumName = "ColCapBalle" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 20))
        End If
        ' ==================================
        ' Capacité liquide
        If Variables.Base(Variables.NumLigneBase, 21) <> "" Then
            ColumName = "ColCapLiquide" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 21))
        End If
        ' ==================================
        ' Capacité frigorifique
        If Variables.Base(Variables.NumLigneBase, 22) <> "" Then
            ColumName = "ColCapFrigo" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 22))
        End If
        ' ==================================
        ' Capacité conteneurs
        If Variables.Base(Variables.NumLigneBase, 23) <> "" Then
            ColumName = "ColCapConteneur" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 23))
        End If
        ' ==================================
        ' Arrangement conteneurs
        ColumName = "ColArrangementConteneur" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 24)
        ' ==================================
        ' Nombre de conteneurs
        If Variables.Base(Variables.NumLigneBase, 25) <> "" Then
            ColumName = "ColNbrConteneur" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 25))
        End If
        ' ==================================
        ' Nombre de cales
        If Variables.Base(Variables.NumLigneBase, 26) <> "" Then
            ColumName = "ColNbrCale" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 26))
        End If
        ' ==================================
        ' Nombre d'écoutilles
        If Variables.Base(Variables.NumLigneBase, 27) <> "" Then
            ColumName = "ColNbrEcoutille" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 27))
        End If
        ' ==================================
        ' Nombre de réservoirs
        If Variables.Base(Variables.NumLigneBase, 28) <> "" Then
            ColumName = "ColNbrReservoir" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 28))
        End If
        ' ==================================
        ' Capacité en minerai
        If Variables.Base(Variables.NumLigneBase, 29) <> "" Then
            ColumName = "ColCapMinerai" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 29))
        End If
        ' ==================================
        ' Nombre de porte latérale babôrd
        If Variables.Base(Variables.NumLigneBase, 30) <> "" Then
            ColumName = "ColPorLatBabord" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 30))
        End If
        ' ==================================
        ' Nombre de porte latéraletribord
        If Variables.Base(Variables.NumLigneBase, 31) <> "" Then
            ColumName = "ColPorLatTribord" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 31))
        End If
        ' ==================================
        ' Nombre de passagers
        If Variables.Base(Variables.NumLigneBase, 32) <> "" Then
            ColumName = "ColNbrPassager" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 32))
        End If
        ' ==================================
        ' Route fréquentée
        ColumName = "ColRouteFrequente" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 33)
        ' ==================================
        ' Nombre de barges amarrables
        If Variables.Base(Variables.NumLigneBase, 34) <> "" Then
            ColumName = "ColBargeAmarrable" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 34))
        End If
        ' ==================================
        ' Nombre de ponts
        If Variables.Base(Variables.NumLigneBase, 35) <> "" Then
            ColumName = "ColNbrPont" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 35))
        End If
        ' ==================================
        ' FMHS
        If Variables.Base(Variables.NumLigneBase, 36) <> "" Then
            ColumName = "ColFmhs" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 36))
        End If
        ' ==================================
        ' Nombre de voitures
        If Variables.Base(Variables.NumLigneBase, 37) <> "" Then
            ColumName = "ColNbrVoiture" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 37))
        End If
        ' ==================================
        ' Nombre de remorques
        If Variables.Base(Variables.NumLigneBase, 38) <> "" Then
            ColumName = "ColNbrRemorque" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 38))
        End If
        ' ==================================
        ' Répartition voitures / remorques
        ColumName = "ColRepVoitureRemorque" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 39)
        ' ==================================
        ' Nombre de wagon
        If Variables.Base(Variables.NumLigneBase, 40) <> "" Then
            ColumName = "ColNbrWagon" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 40))
        End If
        ' ==================================
        ' Type de navire
        ColumName = "ColType" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Variables.Base(Variables.NumLigneBase, 41)
        ' ==================================
        ' Tirant d'air
        If Variables.Base(Variables.NumLigneBase, 42) <> "" Then
            ColumName = "ColTirantAir" : Form1.DataGridView1.Item(ColumName, Variables.NumLigneData).Value = Val(Variables.Base(Variables.NumLigneBase, 42))
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de transfert des résultats dans le Presse Papier
    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        Dim Info As String
        Dim i As Integer
        Dim Transfert As String
        ' ============================================================= 
        ' Pointeur sablier
        Cursor = Cursors.WaitCursor
        ' ============================================================= 
        ' Remise à zéro du presse papier
        Clipboard.Clear()
        ' ============================================================= 
        ' Ligne des titres des colonnes
        info = "N° FAIRPLAY" & Chr("9") & "Ex N° FAIRPLAY" & Chr("9") & "Code état" & Chr("9") & "Classe" & Chr("9") & "Nom du navire" & Chr("9") & "Pavillon du navire" & Chr("9") & "Nom du propriétaire" & Chr("9") & "Date d'achat" & Chr("9") & "Port en lourd" & Chr("9") & "Tonnage brut" & Chr("9") & "N° LLOYDS" & Chr("9") & "Date de livraison" & Chr("9") & "Longueur" & Chr("9") & "Largeur" & Chr("9") & "Tirant d'eau" & Chr("9") & "Profondeur" & Chr("9") & "Vitesse" & Chr("9") & "Date perte" & Chr("9") & "Capacité céréales" & Chr("9") & "Capacités balles" & Chr("9") & "Capacité liquide" & Chr("9") & "Capacité frigorifique" & Chr("9") & "Capacité conteneurs" & Chr("9") & "Arrangement" & Chr("9") & "Nbr conteneurs" & Chr("9") & "Nbr de cales" & Chr("9") & "Nbr d'écoutilles" & Chr("9") & "Nbr réservoirs" & Chr("9") & "Capacité minerai" & Chr("9") & "Porte latérale bâbord" & Chr("9") & "Porte latérale tribord" & Chr("9") & "Nbr passagers" & Chr("9") & "Route fréquentée" & Chr("9") & "Barges amarrables" & Chr("9") & "Nbr de ponts" & Chr("9") & "FHMS" & Chr("9") & "Nbr de voitures" & Chr("9") & "Nbr de remorques" & Chr("9") & "Répartition voit/rem" & Chr("9") & "Nbr de wagons" & Chr("9") & "Type du navire" & Chr("9") & "Hauteur totale"
        Transfert = info
        ' ============================================================= 
        ' Recherche des lignes sélectionnées dans le tableau sur la Base
        For i = 1 To Variables.FinBase
            If Variables.Base(i, 43) = 1 Then
                info = Variables.Base(i, 1) & Chr("9") & Variables.Base(i, 2) & Chr("9") & Variables.Base(i, 3) & Chr("9") & Variables.Base(i, 4) & Chr("9") & Variables.Base(i, 5) & Chr("9") & Variables.Base(i, 6) & Chr("9") & Variables.Base(i, 7) & Chr("9") & Variables.Base(i, 8) & Chr("9") & Variables.Base(i, 9) & Chr("9") & Variables.Base(i, 10) & Chr("9") & Variables.Base(i, 11) & Chr("9") & Variables.Base(i, 12) & Chr("9") & Variables.Base(i, 13) & Chr("9") & Variables.Base(i, 14) & Chr("9") & Variables.Base(i, 15) & Chr("9") & Variables.Base(i, 16) & Chr("9") & Variables.Base(i, 17) & Chr("9") & Variables.Base(i, 18) & Chr("9") & Variables.Base(i, 19) & Chr("9") & Variables.Base(i, 20) & Chr("9") & Variables.Base(i, 21) & Chr("9") & Variables.Base(i, 22) & Chr("9") & Variables.Base(i, 23) & Chr("9") & Variables.Base(i, 24) & Chr("9") & Variables.Base(i, 25) & Chr("9") & Variables.Base(i, 26) & Chr("9") & Variables.Base(i, 27) & Chr("9") & Variables.Base(i, 28) & Chr("9") & Variables.Base(i, 29) & Chr("9") & Variables.Base(i, 30) & Chr("9") & Variables.Base(i, 31) & Chr("9") & Variables.Base(i, 32) & Chr("9") & Variables.Base(i, 33) & Chr("9") & Variables.Base(i, 34) & Chr("9") & Variables.Base(i, 35) & Chr("9") & Variables.Base(i, 36) & Chr("9") & Variables.Base(i, 37) & Chr("9") & Variables.Base(i, 38) & Chr("9") & Variables.Base(i, 39) & Chr("9") & Variables.Base(i, 40) & Chr("9") & Variables.Base(i, 41) & Chr("9") & Variables.Base(i, 42)
                ' ============================================================= 
                ' Remplacement des points par des  virgules pour éviter de passer en langue Anglaise dans Calc qui remplace certaines valeurs par des dates
                Info = Replace(Info, ".", ",")
                ' ============================================================= 
                'Clipboard.SetText(Clipboard.GetText & Chr("13") & info)
                Transfert = Transfert & Chr("13") & info
            End If
        Next
        ' ============================================================= 
        ' Transfert dans le Presse papier
        Clipboard.SetText(Transfert)
        ' ============================================================= 
        ' Pointeur flèche
        Cursor = Cursors.Default
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE CHARGEMENT ET DE SAUVEGARDE DE LA CONFIGURATION DE TRAITEMENT
    '=============================================================================================================================================
    ' Bouton de chargement d'une configuration de traitements
    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Procédure de lancement du chargement
        ChgtConfig.ShowDialog()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de sauvegarde de la configuration de traitements
    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Procédure de lancement de la sauvegarde
        SauveConfig.ShowDialog()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE TRAITEMENTS DES TEXTES D'ÉLABORATION DES TRAITEMENTS
    '=============================================================================================================================================
    ' Sélection du sujet sur lequel porte le tri
    Private Sub TextBox46_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox46.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox46_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox46.MouseClick
        ' ==================================
        ' Traitement pour la liste des sujets si une autre liste est en cours
        If Variables.NumeroList <> 0 Then
            Variables.NumeroListSuivant = 1
            ' ==================================
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer2.Enabled = False
            ' Arrêt du timer de la procédure d'apparition des listes
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du retrait du sélecteur
            Timer4.Enabled = True
            Exit Sub
        End If
        ' ==================================
        ' Variable d'affichage du Listbox de la liste des sujets
        Variables.NumeroList = 1
        ' ==================================
        ' Déclenchement du timer de la procédure d'apparition des listes
        Timer2.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Sélection des différentes listes
    Private Sub TextBox41_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox41.GotFocus
        ' ==================================
        ' Vérification du renvoi du select sur le bouton caché
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 36 Then
            Button15.Select()
        End If
        ' ==================================
        ' Vérification du renvoi du select sur le bouton caché si pas de sélection de sujet
        If TextBox46.Text = "Sélection d'un sujet de traitement (clic)" Then
            Button15.Select()
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Récupération du clic droit de la souris
    Private Sub TextBox41_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox41.MouseDown
        ' ==================================
        ' Vérification du clic droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ==================================
            ' Vérification si "Vide" doit être utilisé
            If Variables.Barillet2 = 0 Then
                TextBox41.Text = "VIDE"
            End If
            '           ' ==================================
            '           ' Vérification si "0" doit être utilisé
            '           If Variables.Barillet2 = 1 Then
            '           TextBox41.Text = 0
            '       End If
            ' ==================================
            ' Vérification si l'effacement du champ doit être réalisé
            If Variables.Barillet2 = 1 Then
                TextBox41.Text = ""
            End If
            ' ==================================
            ' Incrémentation de l'indicateur
            Variables.Barillet2 = Variables.Barillet2 + 1
            If Variables.Barillet2 = 2 Then
                Variables.Barillet2 = 0
            End If
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Sélection des différentes listes
    Private Sub TextBox41_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox41.MouseClick
        ' ==================================
        ' Vérification si une liste est en cours
        If Variables.NumeroList <> 0 Then
            ' ==================================
            ' Traitement pour la liste des codes États si une autre liste est en cours
            If ListBox1.SelectedIndex = 7 Then
                ' ==================================
                ' Numéro de la liste demandée
                Variables.NumeroListSuivant = 2
                ' ==================================
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer2.Enabled = False
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer3.Enabled = False
                ' ==================================
                ' Déclenchement du retrait du sélecteur
                Timer4.Enabled = True
            End If
            ' ==================================
            ' Traitement pour la liste des codes Classes si une autre liste est en cours
            If ListBox1.SelectedIndex = 6 Then
                ' ==================================
                ' Numéro de la liste demandée
                Variables.NumeroListSuivant = 3
                ' ==================================
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer2.Enabled = False
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer3.Enabled = False
                ' ==================================
                ' Déclenchement du retrait du sélecteur
                Timer4.Enabled = True
            End If
            ' ==================================
            ' Traitement pour la liste des codes Pavillons si une autre liste est en cours
            If ListBox1.SelectedIndex = 30 Then
                ' ==================================
                ' Numéro de la liste demandée
                Variables.NumeroListSuivant = 4
                ' ==================================
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer2.Enabled = False
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer3.Enabled = False
                ' ==================================
                ' Déclenchement du retrait du sélecteur
                Timer4.Enabled = True
            End If
            ' ==================================
            ' Traitement pour la liste des codes Types si une autre liste est en cours
            If ListBox1.SelectedIndex = 36 Then
                ' ==================================
                ' Numéro de la liste demandée
                Variables.NumeroListSuivant = 5
                ' ==================================
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer2.Enabled = False
                ' Arrêt du timer de la procédure d'apparition des listes
                Timer3.Enabled = False
                ' ==================================
                ' Déclenchement du retrait du sélecteur
                Timer4.Enabled = True
            End If
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour la liste des codes États
        If ListBox1.SelectedIndex = 7 Then
            ' ==================================
            ' Numéro de la liste demandée
            Variables.NumeroList = 2
            ' ==================================
            ' Déclenchement du timer de la procédure d'apparition des listes
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Traitement pour la liste des codes Classes
        If ListBox1.SelectedIndex = 6 Then
            ' ==================================
            ' Numéro de la liste demandée
            Variables.NumeroList = 3
            ' ==================================
            ' Déclenchement du timer de la procédure d'apparition des listes
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Traitement pour la liste des codes Pavillons
        If ListBox1.SelectedIndex = 30 Then
            ' ==================================
            ' Numéro de la liste demandée
            Variables.NumeroList = 4
            ' ==================================
            ' Déclenchement du timer de la procédure d'apparition des listes
            Timer2.Enabled = True
        End If
        ' ==================================
        ' Traitement pour la liste des codes
        If ListBox1.SelectedIndex = 36 Then
            ' ==================================
            ' Numéro de la liste demandée
            Variables.NumeroList = 5
            ' ==================================
            ' Déclenchement du timer de la procédure d'apparition des listes
            Timer2.Enabled = True
        End If
        ' ==================================
    End Sub

    ' ==============================================================================================================================================
    ' Sélection de la valeur Min de comparaison
    Private Sub TextBox47_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox47.MouseClick
        '==============================================
        If TextBox47.Text = "Min  =  Clic" Then
            ' ==================================
            ' Lancement du module de calculs des valeurs Min Max
            ValeurMinMax()
        Else
            ' ==================================
            ' Transfert de la valeur Min dans le champ de comparaison
            If TextBox46.Text = "Date d'achat" Or TextBox46.Text = "Date de livraison" Or TextBox46.Text = "Date de perte ou de destruction" Then
                ' ==================================
                ' Vérification qu'il y a date
                If TextBox47.Text <> "Min  =  P.D.D" Then
                    ' ==================================
                    ' Récupération de la date
                    TextBox48.Text = Strings.Right(TextBox47.Text, Len(TextBox47.Text) - 6)
                Else
                    TextBox48.Text = "VIDE"
                End If
            Else
                ' ==================================
                ' Vérification qu'il y a une valeur
                If TextBox47.Text <> "Min  =  P.D.V" Then
                    ' ==================================
                    ' Récupération de la valeur
                    TextBox48.Text = Strings.Right(TextBox47.Text, Len(TextBox47.Text) - 8)
                Else
                    TextBox48.Text = "VIDE"
                End If
            End If
            ' ==================================
            ' Changement du curseur sur le champ Autre
            TextBox48.Cursor = Cursors.Default
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Propositions du champ Autre sur le clic droit
    Private Sub TextBox48_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox48.MouseDown
        ' ==================================
        ' Vérification du clic droit
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' ==================================
            ' Vérification si "Vide" doit être utilisé
            If Variables.Barillet = 0 Then
                TextBox48.Text = "VIDE"
            End If
            ' ==================================
            ' Vérification si "0" doit être utilisé sur une valeur numérique
            If Variables.Barillet = 1 Then
                ' ==================================
                ' Vérification si le critère est une date
                If InStr(TextBox46.Text, "Date") > 0 Then
                    Variables.Barillet = Variables.Barillet + 1
                Else
                    TextBox48.Text = 0
                End If
            End If
            ' ==================================
            ' Vérification si l'effacement du champ doit être réalisé
            If Variables.Barillet = 2 Then
                TextBox48.Text = ""
            End If
            ' ==================================
            ' Incrémentation de l'indicateur
            Variables.Barillet = Variables.Barillet + 1
            If Variables.Barillet = 3 Then
                Variables.Barillet = 0
            End If
            Exit Sub
        End If
        ' ==================================
        ' Changement du curseur sur le champ Autre
        TextBox48.Cursor = Cursors.IBeam
    End Sub

    ' ==============================================================================================================================================
    ' Sélection de la valeur Autre de comparaison
    Private Sub TextBox48_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox48.MouseClick
        ' ==================================
        ' Raz du champ
        If TextBox48.Text = "Autre  =  Clic" Then
            TextBox48.Text = ""
            ' ==================================
            ' Changement du curseur sur le champ Autre
            TextBox48.Cursor = Cursors.IBeam
        End If
        ' ==================================
        ' Apparition du curseur dans le champ
        TextBox48.Select()
    End Sub

    ' ==============================================================================================================================================
    ' Sélection de la valeur Max de comparaison
    Private Sub TextBox49_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox49.MouseClick
        '==============================================
        If TextBox49.Text = "Max  =  Clic" Then
            ' ==================================
            ' Lancement du module de calculs des valeurs Min Max
            ValeurMinMax()
        Else
            ' ==================================
            ' Transfert de la valeur Max dans le champ de comparaison
            If TextBox46.Text = "Date d'achat" Or TextBox46.Text = "Date de livraison" Or TextBox46.Text = "Date de perte ou de destruction" Then
                ' ==================================
                ' Vérification qu'il y a date
                If TextBox49.Text <> "Max  =  P.D.D" Then
                    ' ==================================
                    ' Récupération de la date
                    TextBox48.Text = Strings.Right(TextBox49.Text, Len(TextBox49.Text) - 6)
                Else
                    TextBox48.Text = "VIDE"
                End If
            Else
                ' ==================================
                ' Vérification qu'il y a une valeur
                If TextBox49.Text <> "Max  =  P.D.V" Then
                    ' ==================================
                    ' Récupération de la valeur
                    TextBox48.Text = Strings.Right(TextBox49.Text, Len(TextBox49.Text) - 8)
                Else
                    TextBox48.Text = "VIDE"
                End If
            End If
            ' ==================================
            ' Changement du curseur sur le champ Autre
            TextBox48.Cursor = Cursors.Default
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Calculs des valeurs Min et Max pour les colonnes numériques et dates
    Private Sub ValeurMinMax()
        Dim i As Integer
        Dim Colonne As Integer
        Dim Date1 As Date
        ' ==================================
        ' TransCodage du numéro de colonne numérique pour le calcul
        If ListBox1.SelectedIndex = 0 Then Colonne = 20
        If ListBox1.SelectedIndex = 1 Then Colonne = 19
        If ListBox1.SelectedIndex = 2 Then Colonne = 23
        If ListBox1.SelectedIndex = 3 Then Colonne = 22
        If ListBox1.SelectedIndex = 4 Then Colonne = 21
        If ListBox1.SelectedIndex = 5 Then Colonne = 29
        If ListBox1.SelectedIndex = 6 Then Colonne = 4
        '   If ListBox1.SelectedIndex = 7 Then Colonne = 3
        If ListBox1.SelectedIndex = 8 Then Colonne = 8
        If ListBox1.SelectedIndex = 9 Then Colonne = 12
        If ListBox1.SelectedIndex = 10 Then Colonne = 18
        If ListBox1.SelectedIndex = 11 Then Colonne = 42
        If ListBox1.SelectedIndex = 12 Then Colonne = 14
        If ListBox1.SelectedIndex = 13 Then Colonne = 13
        '   If ListBox1.SelectedIndex = 14 Then Colonne = 5
        '   If ListBox1.SelectedIndex = 15 Then Colonne = 7
        If ListBox1.SelectedIndex = 16 Then Colonne = 34
        If ListBox1.SelectedIndex = 17 Then Colonne = 26
        If ListBox1.SelectedIndex = 18 Then Colonne = 25
        If ListBox1.SelectedIndex = 19 Then Colonne = 27
        If ListBox1.SelectedIndex = 20 Then Colonne = 32
        If ListBox1.SelectedIndex = 21 Then Colonne = 35
        If ListBox1.SelectedIndex = 22 Then Colonne = 31
        If ListBox1.SelectedIndex = 23 Then Colonne = 30
        If ListBox1.SelectedIndex = 24 Then Colonne = 38
        If ListBox1.SelectedIndex = 25 Then Colonne = 28
        If ListBox1.SelectedIndex = 26 Then Colonne = 37
        If ListBox1.SelectedIndex = 27 Then Colonne = 40
        If ListBox1.SelectedIndex = 28 Then Colonne = 1
        If ListBox1.SelectedIndex = 29 Then Colonne = 11
        '   If ListBox1.SelectedIndex = 30 Then Colonne = 6
        If ListBox1.SelectedIndex = 31 Then Colonne = 9
        If ListBox1.SelectedIndex = 32 Then Colonne = 16
        '   If ListBox1.SelectedIndex = 33 Then Colonne = 33
        If ListBox1.SelectedIndex = 34 Then Colonne = 15
        If ListBox1.SelectedIndex = 35 Then Colonne = 10
        '   If ListBox1.SelectedIndex = 36 Then Colonne = 41
        If ListBox1.SelectedIndex = 37 Then Colonne = 17
        '==============================================
        ' Vérification pour donner les valeurs Min et Max à partir de toute la Base si le premier champ est vide
        If TextBox1.Text = "" Then
            For i = 0 To Variables.FinBase
                Variables.Base(i, 43) = 1
            Next
        End If
        ' ==================================
        ' Aiguillage pour la recherche de dates et de valeurs numériques
        If Colonne <> 8 And Colonne <> 12 And Colonne <> 18 Then
            ' ==================================
            ' Calcul de la valeur numérique Min de la colonne sélectionnée
            ' ==================================
            ' Initialisation de la variable
            Variables.ValeurMin = -1
            ' ==================================
            ' Trouver une valeur éventuellement différente de 0 pour Min
            For i = 1 To Variables.FinBase
                ' ==================================
                ' Parcours de la colonne de résultats
                If Variables.Base(i, 43) = 1 Then
                    ' ==================================
                    ' Recherche de l'existence d'une valeur
                    If Variables.Base(i, Colonne) <> "" Then
                        ' ==================================
                        ' Recherche de la première valeur minimale de la base
                        If Variables.ValeurMin < Val(Variables.Base(i, Colonne)) Then
                            ' ==================================
                            ' Affectation de la valeur trouvée
                            Variables.ValeurMin = Val(Variables.Base(i, Colonne))
                        End If
                        ' ==================================
                        ' Sortie de la boucle
                        Exit For
                    End If
                End If
            Next
            ' ==================================
            ' Recherche de la valeur numérique minimale de la colonne
            For i = 1 To Variables.FinBase
                ' ==================================
                ' Parcours de la colonne de résultats
                If Variables.Base(i, 43) = 1 Then
                    ' ==================================
                    ' Recherche de l'existence d'une valeur
                    If Variables.Base(i, Colonne) <> "" Then
                        ' ==================================
                        ' Comparaison d'infériorité
                        If Variables.ValeurMin >= Val(Variables.Base(i, Colonne)) Then
                            ' ==================================
                            ' Affectation de la valeur trouvée
                            Variables.ValeurMin = Val(Variables.Base(i, Colonne))
                        End If
                    End If
                End If
            Next
            ' ==================================
            ' Calcul de la valeur numérique Max de la colonne sélectionnée
            ' ==================================
            ' Initialisation de la variable
            Variables.ValeurMax = -1
            ' ==================================
            ' Recherche de la valeur maximale de la colonne
            For i = 1 To Variables.FinBase
                ' ==================================
                ' Parcours de la colonne de résultats
                If Variables.Base(i, 43) = 1 Then
                    ' ==================================
                    ' Recherche de l'existence d'une valeur
                    If Variables.Base(i, Colonne) <> "" Then
                        ' ==================================
                        ' Comparaison de supériorité
                        If Variables.ValeurMax <= Val(Variables.Base(i, Colonne)) Then
                            ' ==================================
                            ' Affectation de la valeur trouvée
                            Variables.ValeurMax = Val(Variables.Base(i, Colonne))
                        End If
                    End If
                End If
            Next
            ' ==================================
            ' Vérification qu'une valeur a été trouvée
            If Variables.ValeurMin <> -1 Then
                ' ==================================
                ' Positionnement de la valeur numérique Min
                ' TextBox47.Text = "Min  =  " & Replace(Format(Variables.ValeurMin, "0.00"), ",", ".")
                TextBox47.Text = "Min  =  " & Replace(Variables.ValeurMin, ",", ".")
                ToolTip1.SetToolTip(Me.TextBox47, "")
                ' ==================================
                ' Positionnement de la valeur numérique Max
                TextBox49.Text = "Max  =  " & Replace(Variables.ValeurMax, ",", ".")
                ToolTip1.SetToolTip(Me.TextBox49, "")
            Else
                ' ==================================
                ' Information d'absence de valeur Min
                TextBox47.Text = "Min  =  P.D.V"
                ToolTip1.SetToolTip(Me.TextBox47, " Pas De Valeur ")
                ' ==================================
                ' Information d'absence de valeur Max
                TextBox49.Text = "Max  =  P.D.V"
                ToolTip1.SetToolTip(Me.TextBox49, " Pas De Valeur ")
            End If
        Else
            ' ==================================
            ' Calcul de la date Min de la colonne sélectionnée
            ' ==================================
            ' Initialisation de la variable
            Variables.ValeurDateMin = "01/01/1900"
            ' ==================================
            ' Trouver une date éventuellement différente de 01/01/1900 pour Min
            For i = 1 To Variables.FinBase
                ' ==================================
                ' Parcours de la colonne de résultats
                If Variables.Base(i, 43) = 1 Then
                    ' ==================================
                    ' Recherche de l'existence d'une date
                    If Variables.Base(i, Colonne) <> "" Then
                        ' ==================================
                        ' Recherche de la première date minimale de la base
                        Date1 = Variables.Base(i, Colonne)
                        If DateDiff(DateInterval.Day, Date1, Variables.ValeurDateMin) <= 0 Then
                            ' ==================================
                            ' Affectation de la date trouvée
                            Variables.ValeurDateMin = Variables.Base(i, Colonne)
                        End If
                        ' ==================================
                        ' Sortie de la boucle
                        Exit For
                    End If
                End If
            Next
            ' ==================================
            ' Recherche de la date minimale de la colonne
            For i = 1 To Variables.FinBase
                ' ==================================
                ' Parcours de la colonne de résultats
                If Variables.Base(i, 43) = 1 Then
                    ' ==================================
                    ' Recherche de l'existence d'une date
                    If Variables.Base(i, Colonne) <> "" Then
                        ' ==================================
                        ' Comparaison d'infériorité
                        Date1 = Variables.Base(i, Colonne)
                        If DateDiff(DateInterval.Day, Date1, Variables.ValeurDateMin) >= 0 Then
                            ' ==================================
                            ' Affectation de la date trouvée
                            Variables.ValeurDateMin = Variables.Base(i, Colonne)
                        End If
                    End If
                End If
            Next
            ' ==================================
            ' Calcul de la date Max de la colonne sélectionnée
            ' ==================================
            ' Initialisation de la variable
            Variables.ValeurDateMax = "01/01/1900"
            ' ==================================
            ' Recherche de la date maximale de la colonne
            For i = 1 To Variables.FinBase
                ' ==================================
                ' Parcours de la colonne de résultats
                If Variables.Base(i, 43) = 1 Then
                    ' ==================================
                    ' Recherche de l'existence d'une date
                    If Variables.Base(i, Colonne) <> "" Then
                        ' ==================================
                        ' Comparaison de supériorité
                        Date1 = Variables.Base(i, Colonne)
                        If DateDiff(DateInterval.Day, Date1, Variables.ValeurDateMax) <= 0 Then
                            ' ==================================
                            ' Affectation de la date trouvée
                            Variables.ValeurDateMax = Variables.Base(i, Colonne)
                        End If
                    End If
                End If
            Next
            ' ==================================
            ' Vérification qu'une date a été trouvée
            If Variables.ValeurDateMin <> "01/01/1900" Then
                ' ==================================
                ' Positionnement de la date Min
                TextBox47.Text = "Min = " & Variables.ValeurDateMin
                ToolTip1.SetToolTip(Me.TextBox47, "")
                ' ==================================
                ' Positionnement de la date Max
                TextBox49.Text = "Max = " & Variables.ValeurDateMax
                ToolTip1.SetToolTip(Me.TextBox49, "")
            Else
                ' ==================================
                ' Information d'absence de valeur Min
                TextBox47.Text = "Min  =  P.D.D"
                ToolTip1.SetToolTip(Me.TextBox47, " Pas De Date ")
                ' ==================================
                ' Information d'absence de valeur Max
                TextBox49.Text = "Max  =  P.D.D"
                ToolTip1.SetToolTip(Me.TextBox49, " Pas De Date ")
            End If
        End If
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DIVERSES
    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Extracteur_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' Traitement de réduction des fenêtres
    Private Sub Extracteur_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
        ' ==================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE SÉLECTION DANS LES LISTES
    '=============================================================================================================================================
    ' Traitement pour la liste des sujets
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        ' ==================================
        ' Arrêt du timer de temporisation de lecture pour éviter un conflit lorsque la liste se d éroule et que l'on a cliqué sur une sélection et que le DatGridView1 se déroule en arrière
        Timer3.Enabled = False
        ' ==================================
        ' Transfert de la sélection
        TextBox46.Text = Strings.LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Effacement de la sélection d'une valeur de comparaison
        TextBox41.Text = ""
        ' ==================================
        ' Adaptation du curseur et du texte en fonction de la sélection du type
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 36 Then
            TextBox41.Cursor = Cursors.Default
            TextBox41.ReadOnly = True
        Else
            TextBox41.Cursor = Cursors.IBeam
            TextBox41.ReadOnly = False
        End If
        ' ==================================
        ' Désactivation des signes '<', '<=', =>' et '>' pour les colonnes alphabétiques
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
            RadioButton3.Enabled = False
            RadioButton4.Enabled = False
            RadioButton6.Enabled = False
            RadioButton7.Enabled = False
            RadioButton3.Checked = False
            RadioButton4.Checked = False
            RadioButton6.Checked = False
            RadioButton7.Checked = False
        Else
            RadioButton3.Enabled = True
            RadioButton4.Enabled = True
            RadioButton6.Enabled = True
            RadioButton7.Enabled = True
        End If
        ' ==================================
        ' Apparition des champs Min, Max et Autre
        '    If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 8 Or ListBox1.SelectedIndex = 9 Or ListBox1.SelectedIndex = 10 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
        If ListBox1.SelectedIndex = 6 Or ListBox1.SelectedIndex = 7 Or ListBox1.SelectedIndex = 14 Or ListBox1.SelectedIndex = 15 Or ListBox1.SelectedIndex = 30 Or ListBox1.SelectedIndex = 33 Or ListBox1.SelectedIndex = 36 Then
            ' ==================================
            ' Apparition du champ de comparaison
            TextBox41.Visible = True
            ' ==================================
            ' Initialisation de l'indicateur
            Variables.Barillet2 = 0
        Else
            ' ==================================
            ' Apparition des champs de comparaison
            TextBox47.Visible = True
            TextBox48.Visible = True
            TextBox49.Visible = True
            ' ==================================
            ' Positionnement du champ Autre
            TextBox48.Text = "Autre  =  Clic"
            '==============================================
            ' Recherche des valeurs Min et Max
            ValeurMinMax()
            ' ==================================
            ' Changement du curseur sur le champ Autre
            TextBox48.Cursor = Cursors.Default
            ' ==================================
            ' Disparition du champ de comparaison
            TextBox41.Visible = False
            ' ==================================
            ' Initialisation de l'indicateur
            Variables.Barillet = 0
        End If
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    '=============================================================================================================================================
    ' Traitement pour les codes États
    Private Sub ListBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox2.SelectedIndexChanged
        ' ==================================
        ' Transfert de la sélection
        TextBox41.Text = Strings.LTrim(Strings.Left(ListBox2.SelectedItem, 2))
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrête du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    '=============================================================================================================================================
    ' Traitement pour les codes Classes
    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        ' ==================================
        ' Transfert de la sélection
        TextBox41.Text = Strings.Trim(Strings.Left(ListBox3.SelectedItem, 3))
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrête du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    '=============================================================================================================================================
    ' Traitement pour les codes Pavillons
    Private Sub ListBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox4.SelectedIndexChanged
        ' ==================================
        ' Transfert de la sélection
        TextBox41.Text = Strings.Trim(Strings.Left(ListBox4.SelectedItem, 4))
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrête du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    '=============================================================================================================================================
    ' Traitement pour les codes Types
    Private Sub ListBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox5.SelectedIndexChanged
        ' ==================================
        ' Transfert de la sélection
        TextBox41.Text = Strings.LTrim(Strings.Left(ListBox5.SelectedItem, 3))
        ' ==================================
        ' Déclenchement du retrait du sélecteur
        Timer4.Enabled = True
        ' ==================================
        ' Arrête du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    '=============================================================================================================================================
    ' Renvoi des focus des textes en entête des listes
    Private Sub TextBox20_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox40_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox40.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox43_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox43.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox44_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox44.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox45_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox45.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox47_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox47.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub

    Private Sub TextBox49_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox49.GotFocus
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button15.Select()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES D'APPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Déclenche la sortie du sélecteur
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des sujets est arrivé à sa position
        If Variables.NumeroList = 1 And Panel5.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des codes États est arrivé à sa position
        If Variables.NumeroList = 2 And Panel6.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des codes Classes est arrivé à sa position
        If Variables.NumeroList = 3 And Panel7.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des codes Pavillons est arrivé à sa position
        If Variables.NumeroList = 4 And Panel8.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Procédure lorsque le sélecteur de la liste des codes Types est arrivé à sa position
        If Variables.NumeroList = 5 And Panel9.Top >= 0 Then
            ' ==================================
            ' Arrêt du timer
            Timer2.Enabled = False
            ' ==================================
            ' Déclenchement du timer de la temporisation de lecture
            Timer3.Enabled = True
            ' ==================================
            ' Sortie
            Exit Sub
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des sujets
        If Variables.NumeroList = 1 Then Panel5.Top = Panel5.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes États
        If Variables.NumeroList = 2 Then Panel6.Top = Panel6.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes Classes
        If Variables.NumeroList = 3 Then Panel7.Top = Panel7.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes Pavillons
        If Variables.NumeroList = 4 Then Panel8.Top = Panel8.Top + 2
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes Types
        If Variables.NumeroList = 5 Then Panel9.Top = Panel9.Top + 2
    End Sub

    ' ==============================================================================================================================================
    ' Déclenche l'attente de lecture et l'affichage de la liste
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des sujets
        If Variables.NumeroList = 1 Then
            Panel5.Height = Panel5.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox1.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes États
        If Variables.NumeroList = 2 Then
            Panel6.Height = Panel6.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox2.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes Classes
        If Variables.NumeroList = 3 Then
            Panel7.Height = Panel7.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox3.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes Pavillons
        If Variables.NumeroList = 4 Then
            Panel8.Height = Panel8.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox4.Visible = True
        End If
        ' ==================================
        ' Incrémentation du déplacement du sélecteur de la liste des codes Types
        If Variables.NumeroList = 5 Then
            Panel9.Height = Panel9.Height + 16
            ' ==================================
            ' Apparition de la liste correspondante
            ListBox5.Visible = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des sujets
        If Variables.NumeroList = 1 And Panel5.Height >= 660 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes États
        If Variables.NumeroList = 2 And Panel6.Height >= 162 Then
            Panel6.Height = 168
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes Classes
        If Variables.NumeroList = 3 And Panel7.Height >= 210 Then
            Panel7.Height = 214
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes Pavillons
        If Variables.NumeroList = 4 And Panel8.Height >= 660 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes Types
        If Variables.NumeroList = 5 And Panel9.Height >= 660 Then
            ' ==================================
            ' Arrête du timer de temporisation de lecture
            Timer3.Enabled = False
            ' ==================================
            ' Déclenchement du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = True
            ' ==================================
            ' Déclenchement du timer de retrait automatique
            Timer7.Enabled = True
        End If
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DES DISPARITION DES SÉLECTEURS
    '=============================================================================================================================================
    ' Déclenche la disparition du sélecteur
    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des sujets
        If Variables.NumeroList = 1 And Panel5.Height > 50 Then
            Panel5.Height = Panel5.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes États
        If Variables.NumeroList = 2 And Panel6.Height > 50 Then
            Panel6.Height = Panel6.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes Classes
        If Variables.NumeroList = 3 And Panel7.Height > 50 Then
            Panel7.Height = Panel7.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes Pavillons
        If Variables.NumeroList = 4 And Panel8.Height > 50 Then
            Panel8.Height = Panel8.Height - 16
        End If
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes Types
        If Variables.NumeroList = 5 And Panel9.Height > 50 Then
            Panel9.Height = Panel9.Height - 16
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des sujets
        If Variables.NumeroList = 1 And Panel5.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox1.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes États
        If Variables.NumeroList = 2 And Panel6.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox2.Visible = False
            ' ==================================
            ' Ajustement du conteneur
            Panel6.Height = 50
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes Classes
        If Variables.NumeroList = 3 And Panel7.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox3.Visible = False
            ' ==================================
            ' Ajustement du conteneur
            Panel7.Height = 50
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes Pavillons
        If Variables.NumeroList = 4 And Panel8.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox4.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
        ' ==================================
        ' Vérification de la fin du déplacement du sélecteur de la liste des codes Types
        If Variables.NumeroList = 5 And Panel9.Height <= 50 Then
            ' ==================================
            ' Retrait de la liste correspondante
            ListBox5.Visible = False
            ' ==================================
            ' Arrêt du timer
            Timer4.Enabled = False
            ' Déclenchement du timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
            Timer5.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait du conteneur de la liste correspondante et lancement éventuel d'une liste demandée
    Private Sub Timer5_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer5.Tick
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des sujets
        If Variables.NumeroList = 1 Then Panel5.Top = Panel5.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes États
        If Variables.NumeroList = 2 Then Panel6.Top = Panel6.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes Classes
        If Variables.NumeroList = 3 Then Panel7.Top = Panel7.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes Pavillons
        If Variables.NumeroList = 4 Then Panel8.Top = Panel8.Top - 2
        ' ==================================
        ' Décrémentation du déplacement du sélecteur de la liste des codes Types
        If Variables.NumeroList = 5 Then Panel9.Top = Panel9.Top - 2
        ' ==================================
        ' Traitement pour la liste des sujets
        If Variables.NumeroList = 1 And Panel5.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour les codes États
        If Variables.NumeroList = 2 And Panel6.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour les codes Classes
        If Variables.NumeroList = 3 And Panel7.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour les codes Pavillons
        If Variables.NumeroList = 4 And Panel8.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
            Exit Sub
        End If
        ' ==================================
        ' Traitement pour les codes Types
        If Variables.NumeroList = 5 And Panel9.Top <= -50 Then
            ' ==================================
            ' Remise à zéro de la variable de numéro de la liste
            Variables.NumeroList = 0
            ' ==================================
            ' Arrêt du timer
            Timer5.Enabled = False
            ' ==================================
            ' Arrêt du timer de déclenchement de la procédure de retrait
            Timer6.Enabled = False
            ' ==================================
            ' Vérification si l'apparition d'une liste a été demandée
            If Variables.NumeroListSuivant <> 0 Then
                ' ==================================
                ' Transfert du numéro de la liste suivante dans celui de la liste
                Variables.NumeroList = Variables.NumeroListSuivant
                ' ==================================
                ' Remise à zéro de la variable de numéro de la liste suivante
                Variables.NumeroListSuivant = 0
                ' ==================================
                ' Déclenchement du timer de la procédure d'apparition des listes
                Timer2.Enabled = True
            End If
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Timer de déclenchement de la procédure de retrait de la liste affichée
    Private Sub Timer6_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer6.Tick
        ' ==================================
        ' Déclenchement du timer de la procédure de retrait
        Timer4.Enabled = True
        ' ==================================
        ' Arrêt du timer
        Timer6.Enabled = False
        ' ==================================
        ' Arrêt du timer de retrait automatique
        Timer7.Enabled = False
    End Sub

    ' ==============================================================================================================================================
    ' Timer de retrait automatique lorsque le curseur n'est pas sur la zone correspondante
    Private Sub Timer7_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer7.Tick
        ' ==================================
        ' Traitement pour la liste des sujets
        If Variables.NumeroList = 1 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 393 And (MousePosition.X - Me.Left) < 659 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 695 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour les codes États
        If Variables.NumeroList = 2 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 423 And (MousePosition.X - Me.Left) < 627 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 196 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour les codes Classes
        If Variables.NumeroList = 3 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 408 And (MousePosition.X - Me.Left) < 642 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 242 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour les codes Pavillons
        If Variables.NumeroList = 4 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 362 And (MousePosition.X - Me.Left) < 686 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 695 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Traitement pour les codes Types
        If Variables.NumeroList = 5 Then
            ' ==================================
            ' Vérification si le curseur est dans la zone correspondante
            If (MousePosition.X - Me.Left) > 323 And (MousePosition.X - Me.Left) < 728 And (MousePosition.Y - Me.Top) > 30 And (MousePosition.Y - Me.Top) < 695 Then
                ' ==================================
                ' Arrêt du timer de déclenchement de la procédure de retrait
                Timer6.Enabled = False
                ' ==================================
                ' Sortie
                Exit Sub
            End If
        End If
        ' ==================================
        ' Déclenchement du timer de déclenchement de la procédure de retrait
        Timer6.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURES DE TRAITEMENTS SUR LA BASE ET SUR LE DERNIER RÉSULTAT
    '=============================================================================================================================================
    ' Timer de lancement d'un traitement sur la Base
    Private Sub Timer8_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer8.Tick
        ' ==================================
        ' Arrêt du timer
        Timer8.Enabled = False
        ' ==================================
        ' Sortie de la boucle si le balayage du tableau de traitements est terminé
        If Variables.IndexTabTraitBase = 20 Then
            ' ==================================
            ' Lancement du traitement sur le dernier résultat si le tableau en comporte
            If TextBox21.Text <> "" Then
                ' ==================================
                ' Déclaration des variables
                Dim j As Integer
                Dim i As Integer
                ' ==================================
                ' Initialisation de l'index du tableau des traitements du dernier résultat
                Variables.IndexTabTraitResul = 1
                ' ==================================
                ' Permet l'inertie de visualisation de la dernière ligne de traitement de la Base
                For j = 0 To Variables.FinBase
                    i = Variables.Base(j, 43)
                    Variables.Base(j, 43) = i
                Next
                ' ==================================
                ' Lancement des traitements du tableau des traitements du dernier résultat
                Timer10.Enabled = True
            Else
                ' ==================================
                ' Déclaration des variables
                Dim Resultats As Integer
                Dim j As Integer
                Resultats = 0
                ' ==================================
                ' Calcul du nombre de réponses
                For j = 1 To Variables.FinBase
                    If Variables.Base(j, 43) = 1 Then
                        Resultats = Resultats + 1
                    End If
                Next
                ' ==================================
                ' Affichage du nombre de réponse obtenues
                TextBox42.Text = Resultats
                ' ==================================
                ' Validation du bouton Lister si le nombre de résultats est < 5000
                If Resultats <> 0 And Resultats < 5001 Then Button10.Enabled = True
                ' ==================================
            End If
            ' ==================================
            ' Programme de couleur de fond à blanc
            CouleurFondBlanc()
            ' ==================================
            ' Sortie suite à la fin des traitements
            Exit Sub
        End If
        ' ==================================
        ' Sortie de la boucle si la ligne du tableau de traitements est vide
        If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "" Then
            ' ==================================
            ' Lancement du traitement sur le dernier résultat si le tableau en comporte
            If TextBox21.Text <> "" Then
                ' ==================================
                ' Déclaration des variables
                Dim j As Integer
                Dim i As Integer
                ' ==================================
                ' Initialisation de l'index du tableau des traitements du dernier résultat
                Variables.IndexTabTraitResul = 1
                ' ==================================
                ' Permet l'inertie de visualisation de la dernière ligne de traitement de la Base
                For j = 0 To Variables.FinBase
                    i = Variables.Base(j, 43)
                    Variables.Base(j, 43) = i
                Next
                ' ==================================
                ' Lancement des traitements du tableau des traitements du dernier résultat
                Timer10.Enabled = True
            Else
                ' ==================================
                ' Déclaration des variables
                Dim Resultats As Integer
                Dim j As Integer
                Resultats = 0
                ' ==================================
                ' Calcul du nombre de réponses
                For j = 1 To Variables.FinBase
                    If Variables.Base(j, 43) = 1 Then
                        Resultats = Resultats + 1
                    End If
                Next
                ' ==================================
                ' Affichage du nombre de réponse obtenues
                TextBox42.Text = Resultats
                ' ==================================
                ' Validation du bouton Lister si le nombre de résultats est < 5000
                If Resultats <> 0 And Resultats < 5001 Then Button10.Enabled = True
                ' ==================================
            End If
            ' ==================================
            ' Programme de couleur de fond à blanc
            CouleurFondBlanc()
            ' ==================================
            ' Sortie suite à la fin des traitements
            Exit Sub
        End If
        ' ==================================
        ' Activation du timer de réalisation d'un traitement
        Timer9.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Timer de réalisation d'un traitement sur la Base
    Private Sub Timer9_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer9.Tick
        ' ==================================
        ' Arrêt du timer
        Timer9.Enabled = False
        ' ==================================
        ' Déclaration des variables
        Dim j As Integer
        Dim Colonne As Integer
        Dim Signe As String
        Dim Comparaison As String
        Dim Date1 As Date
        Dim Date2 As Date
        ' ==================================
        ' Inhibition du bouton Lister
        Button10.Enabled = False
        ' ==============================================================================================================================================
        ' BALAYAGE DU TABLEAU DES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Couleur fond en bleu
        If Variables.IndexTabTraitBase = 1 Then TextBox1.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 2 Then TextBox1.BackColor = Color.White : TextBox2.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 3 Then TextBox2.BackColor = Color.White : TextBox3.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 4 Then TextBox3.BackColor = Color.White : TextBox4.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 5 Then TextBox4.BackColor = Color.White : TextBox5.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 6 Then TextBox5.BackColor = Color.White : TextBox6.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 7 Then TextBox6.BackColor = Color.White : TextBox7.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 8 Then TextBox7.BackColor = Color.White : TextBox8.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 9 Then TextBox8.BackColor = Color.White : TextBox9.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 10 Then TextBox9.BackColor = Color.White : TextBox10.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 11 Then TextBox10.BackColor = Color.White : TextBox11.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 12 Then TextBox11.BackColor = Color.White : TextBox12.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 13 Then TextBox12.BackColor = Color.White : TextBox13.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 14 Then TextBox13.BackColor = Color.White : TextBox14.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 15 Then TextBox14.BackColor = Color.White : TextBox15.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 16 Then TextBox15.BackColor = Color.White : TextBox16.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 17 Then TextBox16.BackColor = Color.White : TextBox17.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 18 Then TextBox17.BackColor = Color.White : TextBox18.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitBase = 19 Then TextBox18.BackColor = Color.White : TextBox19.BackColor = Color.FromArgb(210, 210, 255)
        ' ==================================
        ' Récupération du sujet de recherche
        Colonne = Variables.TabTraitBase(Variables.IndexTabTraitBase, 4)
        ' ==================================
        ' Récupération du signe de comparaison
        Signe = Variables.TabTraitBase(Variables.IndexTabTraitBase, 2)
        ' ==================================
        ' Récupération de la valeur de comparaison
        Comparaison = Replace(Variables.TabTraitBase(Variables.IndexTabTraitBase, 3), ",", ".")
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES NUMÉRIQUES POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne numérique
        If Colonne <> 3 And Colonne <> 5 And Colonne <> 6 And Colonne <> 7 And Colonne <> 8 And Colonne <> 12 And Colonne <> 18 And Colonne <> 33 And Colonne <> 41 Then
            ' ==================================
            ' Traitement avec "Je prends"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) < Val(Comparaison) Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) <= Val(Comparaison) Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) = Val(Comparaison) Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = "" Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) > Val(Comparaison) Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) >= Val(Comparaison) Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) <> Val(Comparaison) Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) < Val(Comparaison) Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) <= Val(Comparaison) Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) = Val(Comparaison) Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = "" Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) > Val(Comparaison) Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) >= Val(Comparaison) Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Val(Variables.Base(j, Colonne)) <> Val(Comparaison) Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
            End If
        End If
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES ALPHABÉTIQUES AVEC MOT CONTENU TOTALEMENT POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne alphabétique
        If Colonne = 3 Or Colonne = 6 Or Colonne = 41 Then
            ' ==================================
            ' Traitement avec "Je prends"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "="
                If Signe = "=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = Comparaison Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠"
                If Signe = "≠" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> Comparaison Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "="
                If Signe = "=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = Comparaison Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠"
                If Signe = "≠" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> Comparaison Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
            End If
        End If
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES ALPHABÉTIQUES AVEC MOT CONTENU PARTIELLEMENT POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne alphabétique
        If Colonne = 5 Or Colonne = 7 Or Colonne = 33 Then
            ' ==================================
            ' Traitement avec "Je prends"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) > 0 Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = "" Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) = 0 Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) > 0 Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = "" Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) = 0 Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
            End If
        End If
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES DATES POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne date
        If Colonne = 8 Or Colonne = 12 Or Colonne = 18 Then
            ' ==================================
            ' Traitement avec "Je prends"
            If Comparaison <> "VIDE" Then
                Date1 = Comparaison
            End If
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) < 0 Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) <= 0 Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, Colonne) = Comparaison Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = "" Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) > 0 Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) >= 0 Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, Colonne) <> Comparaison Then
                                If Variables.Base(j, 43) = 0 Then
                                    Variables.Base(j, 43) = 1
                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, 43) = 0 Then
                                Variables.Base(j, 43) = 1
                            End If
                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitBase(Variables.IndexTabTraitBase, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) < 0 Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) <= 0 Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, Colonne) = Comparaison Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) = "" Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) > 0 Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Date2 = Variables.Base(j, Colonne)
                            If DateDiff(DateInterval.Day, Date1, Date2) >= 0 Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            If Variables.Base(j, Colonne) <> Comparaison Then
                                Variables.Base(j, 43) = 2
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, Colonne) <> "" Then
                            Variables.Base(j, 43) = 2
                        End If
                    Next
                End If
            End If
        End If
        ' ==================================
        ' Incrémentation de l'index du Tableau des traitements
        Variables.IndexTabTraitBase = Variables.IndexTabTraitBase + 1
        ' ==================================
        ' Activation du timer de gestion du tableau des traitements
        Timer8.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Timer de lancement d'un traitement sur le dernier résultat
    Private Sub Timer10_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer10.Tick
        ' ==================================
        ' Arrêt du timer
        Timer10.Enabled = False
        ' ==================================
        ' Sortie de la boucle si le balayage du tableau de traitements est terminé
        If Variables.IndexTabTraitResul = 20 Then
            ' ==================================
            ' Programme de couleur de fond à blanc
            CouleurFondBlanc()
            ' ==================================
            ' Déclaration des variables
            Dim Resultats As Integer
            Dim j As Integer
            Resultats = 0
            ' ==================================
            ' Calcul du nombre de réponses
            For j = 1 To Variables.FinBase
                If Variables.Base(j, 43) = 1 Then
                    Resultats = Resultats + 1
                End If
            Next
            ' ==================================
            ' Affichage du nombre de réponse obtenues
            TextBox42.Text = Resultats
            ' ==================================
            ' Validation du bouton Lister si le nombre de résultats est < 5000
            If Resultats <> 0 And Resultats < 5001 Then Button10.Enabled = True
            ' ==================================
            ' Sortie suite à la fin des traitements
            Exit Sub
        End If
        ' ==================================
        ' Sortie de la boucle si la ligne du tableau de traitements est vide
        If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "" Then
            ' ==================================
            ' Programme de couleur de fond à blanc
            CouleurFondBlanc()
            ' ==================================
            ' Déclaration des variables
            Dim Resultats As Integer
            Dim j As Integer
            Resultats = 0
            ' ==================================
            ' Calcul du nombre de réponses
            For j = 1 To Variables.FinBase
                If Variables.Base(j, 43) = 1 Then
                    Resultats = Resultats + 1
                End If
            Next
            ' ==================================
            ' Affichage du nombre de réponse obtenues
            TextBox42.Text = Resultats
            ' ==================================
            ' Validation du bouton Lister si le nombre de résultats est < 5000
            If Resultats <> 0 And Resultats < 5001 Then Button10.Enabled = True
            ' ==================================
            ' Sortie suite à la fin des traitements
            Exit Sub
        End If
        ' ==================================
        ' Mise à jour de la colonne 44 pour le prochain traitement sur le dernier résultat
        '        For j = 1 To Variables.FinBase
        'If Variables.Base(j, 44) < 2 Then
        'Variables.Base(j, 44) = 0
        '       End If
        '       Next
        ' ==================================
        ' Activation du timer de réalisation d'un traitement
        Timer11.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Timer de réalisation d'un traitement sur le dernier résultat
    Private Sub Timer11_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer11.Tick
        ' ==================================
        ' Arrêt du timer
        Timer11.Enabled = False
        ' ==================================
        ' Déclaration des variables
        Dim j As Integer
        Dim Colonne As Integer
        Dim Signe As String
        Dim Comparaison As String
        Dim Date1 As Date
        Dim Date2 As Date
        ' ==================================
        ' Inhibition du bouton Lister
        Button10.Enabled = False
        ' ==============================================================================================================================================
        ' BALAYAGE DU TABLEAU DES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Couleur fond en bleu
        If Variables.IndexTabTraitResul = 1 Then TextBox21.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 2 Then TextBox21.BackColor = Color.White : TextBox22.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 3 Then TextBox22.BackColor = Color.White : TextBox23.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 4 Then TextBox23.BackColor = Color.White : TextBox24.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 5 Then TextBox24.BackColor = Color.White : TextBox25.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 6 Then TextBox25.BackColor = Color.White : TextBox26.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 7 Then TextBox26.BackColor = Color.White : TextBox27.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 8 Then TextBox27.BackColor = Color.White : TextBox28.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 9 Then TextBox28.BackColor = Color.White : TextBox29.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 10 Then TextBox29.BackColor = Color.White : TextBox30.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 11 Then TextBox30.BackColor = Color.White : TextBox31.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 12 Then TextBox31.BackColor = Color.White : TextBox32.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 13 Then TextBox32.BackColor = Color.White : TextBox33.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 14 Then TextBox33.BackColor = Color.White : TextBox34.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 15 Then TextBox34.BackColor = Color.White : TextBox35.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 16 Then TextBox35.BackColor = Color.White : TextBox36.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 17 Then TextBox36.BackColor = Color.White : TextBox37.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 18 Then TextBox37.BackColor = Color.White : TextBox38.BackColor = Color.FromArgb(210, 210, 255)
        If Variables.IndexTabTraitResul = 19 Then TextBox38.BackColor = Color.White : TextBox39.BackColor = Color.FromArgb(210, 210, 255)
        ' ==================================
        ' Récupération du sujet de recherche
        Colonne = Variables.TabTraitResul(Variables.IndexTabTraitResul, 4)
        ' ==================================
        ' Récupération du signe de comparaison
        Signe = Variables.TabTraitResul(Variables.IndexTabTraitResul, 2)
        ' ==================================
        ' Récupération de la valeur de comparaison
        Comparaison = Replace(Variables.TabTraitResul(Variables.IndexTabTraitResul, 3), ",", ".")
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES NUMÉRIQUES POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne numérique
        If Colonne <> 3 And Colonne <> 5 And Colonne <> 6 And Colonne <> 7 And Colonne <> 8 And Colonne <> 12 And Colonne <> 18 And Colonne <> 33 And Colonne <> 41 Then
            ' ==================================
            ' Traitement avec "Je prends"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Val(Variables.Base(j, Colonne)) >= Val(Comparaison) Then
                                    Variables.Base(j, 43) = 0
                                End If

                                ' If Val(Variables.Base(j, Colonne)) < Val(Comparaison) Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Val(Variables.Base(j, Colonne)) > Val(Comparaison) Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Val(Variables.Base(j, Colonne)) <= Val(Comparaison) Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Val(Variables.Base(j, Colonne)) <> Val(Comparaison) Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Val(Variables.Base(j, Colonne)) = Val(Comparaison) Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) <> "" Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) = "" Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            'End If
                            'End If

                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Val(Variables.Base(j, Colonne)) <= Val(Comparaison) Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Val(Variables.Base(j, Colonne)) > Val(Comparaison) Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Val(Variables.Base(j, Colonne)) < Val(Comparaison) Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Val(Variables.Base(j, Colonne)) >= Val(Comparaison) Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Val(Variables.Base(j, Colonne)) = Val(Comparaison) Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Val(Variables.Base(j, Colonne)) <> Val(Comparaison) Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) = "" Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) <> "" Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            'End If
                            'End If

                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Val(Variables.Base(j, Colonne)) < Val(Comparaison) Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Val(Variables.Base(j, Colonne)) <= Val(Comparaison) Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Val(Variables.Base(j, Colonne)) = Val(Comparaison) Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) = "" Then

                                Variables.Base(j, 43) = 0

                                ' Variables.Base(j, 44) = 2

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Val(Variables.Base(j, Colonne)) > Val(Comparaison) Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Val(Variables.Base(j, Colonne)) >= Val(Comparaison) Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Val(Variables.Base(j, Colonne)) <> Val(Comparaison) Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                Variables.Base(j, 43) = 0

                                ' Variables.Base(j, 44) = 2

                            End If
                        End If
                    Next
                End If
            End If
        End If
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES ALPHABÉTIQUES AVEC MOT CONTENU TOTALEMENT POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne alphabétique
        If Colonne = 3 Or Colonne = 6 Or Colonne = 41 Then
            ' ==================================
            ' Traitement avec "Je prends"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "="
                If Signe = "=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) <> Comparaison Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) = Comparaison Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            'End If
                            'End If

                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠"
                If Signe = "≠" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) = Comparaison Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) <> Comparaison Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            'End If
                            'End If

                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "="
                If Signe = "=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) = Comparaison Then
                                Variables.Base(j, 43) = 0
                            End If

                            ' If Variables.Base(j, Colonne) = Comparaison Then
                            'Variables.Base(j, 44) = 2
                            'End If

                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠"
                If Signe = "≠" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) <> Comparaison Then
                                Variables.Base(j, 43) = 0
                            End If

                            ' If Variables.Base(j, Colonne) <> Comparaison Then
                            'Variables.Base(j, 44) = 2
                            'End If

                        End If
                    Next
                End If
            End If
        End If
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES ALPHABÉTIQUES AVEC MOT CONTENU PARTIELLEMENT POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne alphabétique
        If Colonne = 5 Or Colonne = 7 Or Colonne = 33 Then
            ' ==================================
            ' Traitement avec "Je prends"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) = 0 Then
                                    Variables.Base(j, 43) = 0
                                End If

                                ' If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) > 0 Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) <> "" Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) = "" Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            'End If
                            'End If

                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) > 0 Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) = 0 Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) = "" Then
                                Variables.Base(j, 43) = 0
                            End If

                            ' If Variables.Base(j, Colonne) <> "" Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            ' End If
                            ' End If

                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) > 0 Then

                                    Variables.Base(j, 43) = 0

                                    ' Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) = "" Then

                                Variables.Base(j, 43) = 0

                                ' Variables.Base(j, 44) = 2

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If InStr(Variables.Base(j, Colonne), Comparaison, CompareMethod.Text) = 0 Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                Variables.Base(j, 43) = 0

                                ' Variables.Base(j, 44) = 2

                            End If
                        End If
                    Next
                End If
            End If
        End If
        ' ==============================================================================================================================================
        ' PROCÉDURES DES COLONNES DATES POUR LES TRAITEMENTS SUR LA BASE
        ' ==============================================================================================================================================
        ' Vérification si le traitement concerne une colonne date
        If Colonne = 8 Or Colonne = 12 Or Colonne = 18 Then
            ' ==================================
            ' Traitement avec "Je prends"
            If Comparaison <> "VIDE" Then
                Date1 = Comparaison
            End If
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je prends" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)

                                If DateDiff(DateInterval.Day, Date1, Date2) >= 0 Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If DateDiff(DateInterval.Day, Date1, Date2) < 0 Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)

                                If DateDiff(DateInterval.Day, Date1, Date2) > 0 Then
                                    Variables.Base(j, 43) = 0
                                End If

                                ' If DateDiff(DateInterval.Day, Date1, Date2) <= 0 Then
                                ' If Variables.Base(j, 44) = 0 Then
                                ' Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Variables.Base(j, Colonne) <> Comparaison Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Variables.Base(j, Colonne) = Comparaison Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) <> "" Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) = "" Then
                            ' If Variables.Base(j, 44) = 0 Then
                            ' Variables.Base(j, 44) = 1
                            '  End If
                            '  End If

                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)

                                If DateDiff(DateInterval.Day, Date1, Date2) <= 0 Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If DateDiff(DateInterval.Day, Date1, Date2) > 0 Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)

                                If DateDiff(DateInterval.Day, Date1, Date2) < 0 Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If DateDiff(DateInterval.Day, Date1, Date2) >= 0 Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                If Variables.Base(j, Colonne) = Comparaison Then
                                    Variables.Base(j, 43) = 0
                                End If

                                'If Variables.Base(j, Colonne) <> Comparaison Then
                                'If Variables.Base(j, 44) = 0 Then
                                'Variables.Base(j, 44) = 1
                                'End If
                                'End If

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then

                            If Variables.Base(j, Colonne) = "" Then
                                Variables.Base(j, 43) = 0
                            End If

                            'If Variables.Base(j, Colonne) <> "" Then
                            'If Variables.Base(j, 44) = 0 Then
                            'Variables.Base(j, 44) = 1
                            'End If
                            'End If

                        End If
                    Next
                End If
            End If
            ' ==================================
            ' Traitement avec "Je retire"
            ' ==================================
            If Variables.TabTraitResul(Variables.IndexTabTraitResul, 0) = "Je retire" Then
                ' ==================================
                ' Traitement avec "<"
                If Signe = "<" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)
                                If DateDiff(DateInterval.Day, Date1, Date2) < 0 Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "<="
                If Signe = "<=" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)
                                If DateDiff(DateInterval.Day, Date1, Date2) <= 0 Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison <> Vide
                If Signe = "=" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Variables.Base(j, Colonne) = Comparaison Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=" et Comparaison = Vide
                If Signe = "=" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) = "" Then

                                Variables.Base(j, 43) = 0

                                'Variables.Base(j, 44) = 2

                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec ">"
                If Signe = ">" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)
                                If DateDiff(DateInterval.Day, Date1, Date2) > 0 Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "=>"
                If Signe = "=>" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                Date2 = Variables.Base(j, Colonne)
                                If DateDiff(DateInterval.Day, Date1, Date2) >= 0 Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison <> Vide
                If Signe = "≠" And Comparaison <> "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then
                                If Variables.Base(j, Colonne) <> Comparaison Then

                                    Variables.Base(j, 43) = 0

                                    'Variables.Base(j, 44) = 2

                                End If
                            End If
                        End If
                    Next
                End If
                ' ==================================
                ' Traitement avec "≠" et Comparaison = Vide
                If Signe = "≠" And Comparaison = "VIDE" Then
                    For j = 1 To Variables.FinBase
                        If Variables.Base(j, 43) = 1 Then
                            If Variables.Base(j, Colonne) <> "" Then

                                Variables.Base(j, 43) = 0

                                'Variables.Base(j, 44) = 2

                            End If
                        End If
                    Next
                End If
            End If
        End If
        ' ==================================
        ' Mise à jour de la colonne 43 pour le prochain traitement sur le dernier résultat
        '        For j = 1 To Variables.FinBase
        'If Variables.Base(j, 44) <> 1 Then
        'Variables.Base(j, 43) = 0
        '        End If
        '       Next
        ' ==================================
        ' Incrémentation de l'index du Tableau des traitements
        Variables.IndexTabTraitResul = Variables.IndexTabTraitResul + 1
        ' ==================================
        ' Activation du timer de gestion du tableau des traitements
        Timer10.Enabled = True
    End Sub




End Class