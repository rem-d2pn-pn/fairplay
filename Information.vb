﻿Public Class Information

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Information_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Information_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Button4.Enabled = True
        Button5.Enabled = True
        '==============================================
        ' Inhibition s'il n'y a qu'une ligne dans le DataGrid
        If Variables.NumLigneData = 0 Then
            Button4.Enabled = False
            Button5.Enabled = False
        End If
        '==============================================
        ' Inhibition s'il y a égalité avec les extrémités des lignes dans le DataGrid
        If Variables.NumLigSel = 0 Then
            Button4.Enabled = False
        End If
        If Variables.NumLigSel = Variables.NumLigneData Then
            Button5.Enabled = False
        End If
        '==============================================
        ' Transfert des infos dans les champs
        AffichageInfos()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Transfert des infos sélectionnées dans la base dans les champs
    Private Sub AffichageInfos()
        '==============================================
        ' Chargement des informations sélectionnées dans les champs
        EffaceCouleurChamps()

        TextBox1.Text = Variables.Base(Variables.TransNumLigne, 1)
        TextBox2.Text = Variables.Base(Variables.TransNumLigne, 2)
        TextBox3.Text = Variables.Base(Variables.TransNumLigne, 3)
        TextBox4.Text = Variables.Base(Variables.TransNumLigne, 4)
        TextBox5.Text = Variables.Base(Variables.TransNumLigne, 5)
        TextBox6.Text = Variables.Base(Variables.TransNumLigne, 6)
        TextBox7.Text = Variables.Base(Variables.TransNumLigne, 7)
        TextBox8.Text = Variables.Base(Variables.TransNumLigne, 8)
        TextBox9.Text = Variables.Base(Variables.TransNumLigne, 9)
        TextBox10.Text = Variables.Base(Variables.TransNumLigne, 10)

        TextBox11.Text = Variables.Base(Variables.TransNumLigne, 11)
        TextBox12.Text = Variables.Base(Variables.TransNumLigne, 12)
        TextBox13.Text = Variables.Base(Variables.TransNumLigne, 13)
        TextBox14.Text = Variables.Base(Variables.TransNumLigne, 14)
        TextBox15.Text = Variables.Base(Variables.TransNumLigne, 15)
        TextBox16.Text = Variables.Base(Variables.TransNumLigne, 16)
        TextBox17.Text = Variables.Base(Variables.TransNumLigne, 17)
        TextBox18.Text = Variables.Base(Variables.TransNumLigne, 18)
        TextBox19.Text = Variables.Base(Variables.TransNumLigne, 19)
        TextBox20.Text = Variables.Base(Variables.TransNumLigne, 20)

        TextBox21.Text = Variables.Base(Variables.TransNumLigne, 21)
        TextBox22.Text = Variables.Base(Variables.TransNumLigne, 22)
        TextBox23.Text = Variables.Base(Variables.TransNumLigne, 23)
        TextBox24.Text = Variables.Base(Variables.TransNumLigne, 24)
        TextBox25.Text = Variables.Base(Variables.TransNumLigne, 25)
        TextBox26.Text = Variables.Base(Variables.TransNumLigne, 26)
        TextBox27.Text = Variables.Base(Variables.TransNumLigne, 27)
        TextBox28.Text = Variables.Base(Variables.TransNumLigne, 28)
        TextBox29.Text = Variables.Base(Variables.TransNumLigne, 29)
        TextBox30.Text = Variables.Base(Variables.TransNumLigne, 30)

        TextBox31.Text = Variables.Base(Variables.TransNumLigne, 31)
        TextBox32.Text = Variables.Base(Variables.TransNumLigne, 32)
        TextBox33.Text = Variables.Base(Variables.TransNumLigne, 33)
        TextBox34.Text = Variables.Base(Variables.TransNumLigne, 34)
        TextBox35.Text = Variables.Base(Variables.TransNumLigne, 35)
        TextBox36.Text = Variables.Base(Variables.TransNumLigne, 36)
        TextBox37.Text = Variables.Base(Variables.TransNumLigne, 37)
        TextBox38.Text = Variables.Base(Variables.TransNumLigne, 38)
        TextBox39.Text = Variables.Base(Variables.TransNumLigne, 39)
        TextBox40.Text = Variables.Base(Variables.TransNumLigne, 40)

        TextBox41.Text = Variables.Base(Variables.TransNumLigne, 41)
        TextBox42.Text = Variables.Base(Variables.TransNumLigne, 42)

        '==============================================
        ' Signification du code état du navire
        TextBox43.Text = ""
        If TextBox3.Text = "D" Then TextBox43.Text = "En service"
        If TextBox3.Text = "U" Then TextBox43.Text = "En commande"
        If TextBox3.Text = "P" Then TextBox43.Text = "En fabrication"
        If TextBox3.Text = "L" Then TextBox43.Text = "Perdu"
        If TextBox3.Text = "S" Then TextBox43.Text = "Détruit"
        If TextBox3.Text = "X" Then TextBox43.Text = "Erreur"
        If TextBox3.Text = "Z" Then TextBox43.Text = "Annulé"

        '==============================================
        ' Signification du code classe du navire
        TextBox44.Text = ""
        If TextBox4.Text = "1" Then TextBox44.Text = "General Dry Cargo"
        If TextBox4.Text = "2" Then TextBox44.Text = "Containships"
        If TextBox4.Text = "3" Then TextBox44.Text = "Tankers"
        If TextBox4.Text = "4" Then TextBox44.Text = "Bulk Carrier"
        If TextBox4.Text = "5" Then TextBox44.Text = "Passengers / Ferry Vessels"
        If TextBox4.Text = "6" Then TextBox44.Text = "Miscellaneous Vessels"
        If TextBox4.Text = "7" Then TextBox44.Text = "Roll On Rool Off Vessels"
        If TextBox4.Text = "8" Then TextBox44.Text = "Refrigerated Vessels"
        If TextBox4.Text = "9" Then TextBox44.Text = "Combination Carriers"
        If TextBox4.Text = "10" Then TextBox44.Text = "Offshore Vessels"

        '==============================================
        ' Signification du code du pavillon du navire
        TextBox45.Text = ""
        If TextBox6.Text = "A" Then TextBox45.Text = "AUSTRALIE"
        If TextBox6.Text = "B" Then TextBox45.Text = "BELGIQUE"
        If TextBox6.Text = "C" Then TextBox45.Text = "CUBA"
        If TextBox6.Text = "D" Then TextBox45.Text = "ALLEMAGNE"
        If TextBox6.Text = "E" Then TextBox45.Text = "ESPAGNE"
        If TextBox6.Text = "F" Then TextBox45.Text = "FRANCE"
        If TextBox6.Text = "G" Then TextBox45.Text = "GUINÉE"
        If TextBox6.Text = "H" Then TextBox45.Text = "HONGRIE"
        If TextBox6.Text = "I" Then TextBox45.Text = "ITALIE"
        If TextBox6.Text = "N" Then TextBox45.Text = "NORVÈGE"
        If TextBox6.Text = "P" Then TextBox45.Text = "PORTUGAL"
        If TextBox6.Text = "Q" Then TextBox45.Text = "QATAR"
        If TextBox6.Text = "R" Then TextBox45.Text = "ROUMANIE"
        If TextBox6.Text = "S" Then TextBox45.Text = "SUÈDE"
        If TextBox6.Text = "T" Then TextBox45.Text = "THAÏLANDE"
        If TextBox6.Text = "U" Then TextBox45.Text = "URUGUAY"

        If TextBox6.Text = "AF" Then TextBox45.Text = "AFGHANISTAN"
        If TextBox6.Text = "AG" Then TextBox45.Text = "ALGÉRIE"
        If TextBox6.Text = "AL" Then TextBox45.Text = "ALBANIE"
        If TextBox6.Text = "AN" Then TextBox45.Text = "ANGOLA"
        If TextBox6.Text = "AT" Then TextBox45.Text = "ANTIGUA"
        If TextBox6.Text = "AZ" Then TextBox45.Text = "ILE DES AÇORES"
        If TextBox6.Text = "BA" Then TextBox45.Text = "BIRMANIE"
        If TextBox6.Text = "BB" Then TextBox45.Text = "BARBADE"
        If TextBox6.Text = "BE" Then TextBox45.Text = "ILES DES BERMUDES"
        If TextBox6.Text = "BG" Then TextBox45.Text = "BULGARIE"
        If TextBox6.Text = "BL" Then TextBox45.Text = "BELIZE"
        If TextBox6.Text = "BR" Then TextBox45.Text = "BRÉSIL"
        If TextBox6.Text = "BS" Then TextBox45.Text = "BAHAMAS"
        If TextBox6.Text = "BW" Then TextBox45.Text = "BOTSWANA"
        If TextBox6.Text = "BX" Then TextBox45.Text = "BRUNEÏ"
        If TextBox6.Text = "CA" Then TextBox45.Text = "ILES DES CANARIES"
        If TextBox6.Text = "CH" Then TextBox45.Text = "SUISSE"
        If TextBox6.Text = "CI" Then TextBox45.Text = "COTE D'IVOIRE"
        If TextBox6.Text = "CL" Then TextBox45.Text = "SRI LANKA"
        If TextBox6.Text = "CN" Then TextBox45.Text = "CONGO"
        If TextBox6.Text = "CO" Then TextBox45.Text = "COLOMBIE"
        If TextBox6.Text = "CR" Then TextBox45.Text = "COSTA RICA"
        If TextBox6.Text = "CS" Then TextBox45.Text = "TCHÉCOSLOVAQUIE"
        If TextBox6.Text = "CY" Then TextBox45.Text = "CHYPRE"
        If TextBox6.Text = "DJ" Then TextBox45.Text = "DJIBOUTI"
        If TextBox6.Text = "DK" Then TextBox45.Text = "DANEMARK"
        If TextBox6.Text = "EQ" Then TextBox45.Text = "ÉQUATEUR"
        If TextBox6.Text = "ES" Then TextBox45.Text = "EL SALVADOR"
        If TextBox6.Text = "ET" Then TextBox45.Text = "ÉGYPTE"
        If TextBox6.Text = "FA" Then TextBox45.Text = "ILES FÉROÉ"
        If TextBox6.Text = "FI" Then TextBox45.Text = "ILES FALKLAND"
        If TextBox6.Text = "FJ" Then TextBox45.Text = "ILES FIDJI"
        If TextBox6.Text = "GB" Then TextBox45.Text = "GRANDE BRETAGNE"
        If TextBox6.Text = "GD" Then TextBox45.Text = "GUADELOUPE"
        If TextBox6.Text = "GE" Then TextBox45.Text = "GROENLAND"
        If TextBox6.Text = "GH" Then TextBox45.Text = "GHANA"
        If TextBox6.Text = "GR" Then TextBox45.Text = "GRÈCE"
        If TextBox6.Text = "GS" Then TextBox45.Text = "GÉORGIE DU SUD"
        If TextBox6.Text = "GU" Then TextBox45.Text = "GUINÉE - BISSAU"
        If TextBox6.Text = "GY" Then TextBox45.Text = "GUYANE"
        If TextBox6.Text = "HK" Then TextBox45.Text = "HONG KONG"
        If TextBox6.Text = "IL" Then TextBox45.Text = "ISRAËL"
        If TextBox6.Text = "IR" Then TextBox45.Text = "IRAN"
        If TextBox6.Text = "IS" Then TextBox45.Text = "ISLANDE"
        If TextBox6.Text = "KI" Then TextBox45.Text = "KIRIBATI"
        If TextBox6.Text = "LY" Then TextBox45.Text = "LIBYE"
        If TextBox6.Text = "MA" Then TextBox45.Text = "MAROC"
        If TextBox6.Text = "MB" Then TextBox45.Text = "MOZAMBIQUE"
        If TextBox6.Text = "MC" Then TextBox45.Text = "MONACO"
        If TextBox6.Text = "MG" Then TextBox45.Text = "MONGOLIE"
        If TextBox6.Text = "ML" Then TextBox45.Text = "ILES MARSHALL"
        If TextBox6.Text = "MS" Then TextBox45.Text = "ILE MAURICE"
        If TextBox6.Text = "MV" Then TextBox45.Text = "ILE DES MALDIVES"
        If TextBox6.Text = "MY" Then TextBox45.Text = "MYANMAR"
        If TextBox6.Text = "NA" Then TextBox45.Text = "ANTILLES PAYS-BAS"
        If TextBox6.Text = "NC" Then TextBox45.Text = "NOUVELLE CALÉDONIE"
        If TextBox6.Text = "NF" Then TextBox45.Text = "ILE NORFOLK"
        If TextBox6.Text = "NK" Then TextBox45.Text = "CORÉE DU NORD"
        If TextBox6.Text = "NL" Then TextBox45.Text = "PAYS-BAS"
        If TextBox6.Text = "NZ" Then TextBox45.Text = "NOUVELLE ZÉLANDE"
        If TextBox6.Text = "OM" Then TextBox45.Text = "OMAN"
        If TextBox6.Text = "PA" Then TextBox45.Text = "PANAMA"
        If TextBox6.Text = "PE" Then TextBox45.Text = "PÉROU"
        If TextBox6.Text = "PI" Then TextBox45.Text = "PHILIPPINES"
        If TextBox6.Text = "PL" Then TextBox45.Text = "POLOGNE"
        If TextBox6.Text = "PR" Then TextBox45.Text = "PORTO RICO"
        If TextBox6.Text = "PY" Then TextBox45.Text = "PARAGUAY"
        If TextBox6.Text = "RA" Then TextBox45.Text = "ARGENTINE"
        If TextBox6.Text = "RH" Then TextBox45.Text = "HAITI"
        If TextBox6.Text = "RI" Then TextBox45.Text = "INDONÉSIE"
        If TextBox6.Text = "RL" Then TextBox45.Text = "LIBAN"
        If TextBox6.Text = "RU" Then TextBox45.Text = "RUSSIE"
        If TextBox6.Text = "RW" Then TextBox45.Text = "RWANDA"
        If TextBox6.Text = "SA" Then TextBox45.Text = "SAÉ TOMÉ ET PRINCIPE"
        If TextBox6.Text = "SE" Then TextBox45.Text = "SEYCHELLES"
        If TextBox6.Text = "SF" Then TextBox45.Text = "FINLANDE"
        If TextBox6.Text = "SI" Then TextBox45.Text = "ILES SALOMON"
        If TextBox6.Text = "SK" Then TextBox45.Text = "CORÉE DU SUD"
        If TextBox6.Text = "SL" Then TextBox45.Text = "SIERRA LÉONE"
        If TextBox6.Text = "SM" Then TextBox45.Text = "MAROC ESPAGNOL"
        If TextBox6.Text = "SN" Then TextBox45.Text = "SÉNÉGAL"
        If TextBox6.Text = "SR" Then TextBox45.Text = "SURINAME"
        If TextBox6.Text = "ST" Then TextBox45.Text = "ILES SAINT HÉLÈNE"
        If TextBox6.Text = "SU" Then TextBox45.Text = "RUSSIE"
        If TextBox6.Text = "SV" Then TextBox45.Text = "SAINT VINCENT"
        If TextBox6.Text = "TN" Then TextBox45.Text = "TUNISIE"
        If TextBox6.Text = "TO" Then TextBox45.Text = "TONGA"
        If TextBox6.Text = "TP" Then TextBox45.Text = "TIMOR ORIENTAL"
        If TextBox6.Text = "TR" Then TextBox45.Text = "TURQUIE"
        If TextBox6.Text = "TT" Then TextBox45.Text = "TRINITÉ ET TOBAGO"
        If TextBox6.Text = "UA" Then TextBox45.Text = "ÉMIRATS ARABES UNIS"
        If TextBox6.Text = "UG" Then TextBox45.Text = "OUGANDA"
        If TextBox6.Text = "VA" Then TextBox45.Text = "VANUATU"
        If TextBox6.Text = "VN" Then TextBox45.Text = "VIETNAM"
        If TextBox6.Text = "WF" Then TextBox45.Text = "ILES WALLIS ET FUTUNA"
        If TextBox6.Text = "WS" Then TextBox45.Text = "SAMOA"
        If TextBox6.Text = "YU" Then TextBox45.Text = "YOUGOSLAVIE"
        If TextBox6.Text = "YV" Then TextBox45.Text = "VÉNÉZUELA"
        If TextBox6.Text = "ZA" Then TextBox45.Text = "AFRIQUE DU SUD"
        If TextBox6.Text = "ZB" Then TextBox45.Text = "ZIMBABWE"
        If TextBox6.Text = "ZZ" Then TextBox45.Text = "TANZANIE"

        If TextBox6.Text = "ABW" Then TextBox45.Text = "ARUBA"
        If TextBox6.Text = "AGO" Then TextBox45.Text = "ANGOLA"
        If TextBox6.Text = "AGL" Then TextBox45.Text = "ANGUILLA"
        If TextBox6.Text = "AIA" Then TextBox45.Text = "ANGUILLA"
        If TextBox6.Text = "ALB" Then TextBox45.Text = "ALBANIE"
        If TextBox6.Text = "ALD" Then TextBox45.Text = "ILES ALAND"
        If TextBox6.Text = "AND" Then TextBox45.Text = "ANDORRE"
        If TextBox6.Text = "ANT" Then TextBox45.Text = "ILES NÉERLANDAISES"
        If TextBox6.Text = "ARE" Then TextBox45.Text = "ÉMIRATS ARABES UNIS"
        If TextBox6.Text = "ARG" Then TextBox45.Text = "ARGENTINE"
        If TextBox6.Text = "ARM" Then TextBox45.Text = "ARMÉNIE"
        If TextBox6.Text = "ARU" Then TextBox45.Text = "ARUBA"
        If TextBox6.Text = "ATF" Then TextBox45.Text = "ILES KERGUÉLEN"
        If TextBox6.Text = "ATG" Then TextBox45.Text = "ANTIGUA ET BARBUDE"
        If TextBox6.Text = "AUS" Then TextBox45.Text = "AUSTRALIE"
        If TextBox6.Text = "AUT" Then TextBox45.Text = "AUTRICHE"
        If TextBox6.Text = "AZB" Then TextBox45.Text = "AZERBAIDJAN"
        If TextBox6.Text = "AZE" Then TextBox45.Text = "AZERBAIDJAN"
        If TextBox6.Text = "BEL" Then TextBox45.Text = "BELGIQUE"
        If TextBox6.Text = "BEN" Then TextBox45.Text = "BÉNIN"
        If TextBox6.Text = "BDI" Then TextBox45.Text = "BURUNDI"
        If TextBox6.Text = "BGD" Then TextBox45.Text = "BANGLADESH"
        If TextBox6.Text = "BGR" Then TextBox45.Text = "BULGARIE"
        If TextBox6.Text = "BHR" Then TextBox45.Text = "BAHREIN"
        If TextBox6.Text = "BHS" Then TextBox45.Text = "BAHAMAS"
        If TextBox6.Text = "BIO" Then TextBox45.Text = "TERRITOIRES ANGLAIS OCÉAN INDIEN"
        If TextBox6.Text = "BKA" Then TextBox45.Text = "BURQUINA FASSO"
        If TextBox6.Text = "BLZ" Then TextBox45.Text = "BELIZE"
        If TextBox6.Text = "BMU" Then TextBox45.Text = "ILES DES BERMUDES"
        If TextBox6.Text = "BNG" Then TextBox45.Text = "BANGLADESH"
        If TextBox6.Text = "BOL" Then TextBox45.Text = "BOLIVIE"
        If TextBox6.Text = "BOS" Then TextBox45.Text = "BOSNIE HERZÉGOVINE"
        If TextBox6.Text = "BRA" Then TextBox45.Text = "BRÉSIL"
        If TextBox6.Text = "BRB" Then TextBox45.Text = "BARBADE"
        If TextBox6.Text = "BRN" Then TextBox45.Text = "BRUNEÏ"
        If TextBox6.Text = "BRU" Then TextBox45.Text = "BIÉLORUSSIE"
        If TextBox6.Text = "CAB" Then TextBox45.Text = "CAMBODGE"
        If TextBox6.Text = "CAM" Then TextBox45.Text = "CAMÉROUN"
        If TextBox6.Text = "CAN" Then TextBox45.Text = "CANADA"
        If TextBox6.Text = "CAY" Then TextBox45.Text = "ILES CAÏMAN"
        If TextBox6.Text = "CHA" Then TextBox45.Text = "TCHAD"
        If TextBox6.Text = "CHE" Then TextBox45.Text = "SUISSE"
        If TextBox6.Text = "CHI" Then TextBox45.Text = "CHINE"
        If TextBox6.Text = "CHL" Then TextBox45.Text = "CHILI"
        If TextBox6.Text = "CHN" Then TextBox45.Text = "RÉPUBLIQUE POPULAIRE DE CHINE"
        If TextBox6.Text = "CIV" Then TextBox45.Text = "COTE D'IVOIRE"
        If TextBox6.Text = "CMI" Then TextBox45.Text = "ILES CHRISTMAS"
        If TextBox6.Text = "CMR" Then TextBox45.Text = "CAMÉROUN"
        If TextBox6.Text = "COC" Then TextBox45.Text = "ILES COCOS"
        If TextBox6.Text = "COG" Then TextBox45.Text = "CONGO"
        If TextBox6.Text = "COL" Then TextBox45.Text = "COLOMBIE"
        If TextBox6.Text = "COM" Then TextBox45.Text = "COMORES"
        If TextBox6.Text = "CON" Then TextBox45.Text = "CONGO"
        If TextBox6.Text = "COO" Then TextBox45.Text = "ILES COOK"
        If TextBox6.Text = "CPV" Then TextBox45.Text = "ILES DU CAP-VERT"
        If TextBox6.Text = "CRI" Then TextBox45.Text = "COSTA RICA"
        If TextBox6.Text = "CRO" Then TextBox45.Text = "CROATIE"
        If TextBox6.Text = "CSR" Then TextBox45.Text = "DÉFINITION EN ATTENTE"
        If TextBox6.Text = "CUB" Then TextBox45.Text = "CUBA"
        If TextBox6.Text = "CUR" Then TextBox45.Text = "DÉFINITION EN ATTENTE"
        If TextBox6.Text = "CVI" Then TextBox45.Text = "ILES DU CAP VERT"
        If TextBox6.Text = "CYM" Then TextBox45.Text = "ILES CAÏMAN"
        If TextBox6.Text = "CYP" Then TextBox45.Text = "CHYPRE"
        If TextBox6.Text = "CZE" Then TextBox45.Text = "RÉPUBLIQUE TCHÈQUE"
        If TextBox6.Text = "CZR" Then TextBox45.Text = "RÉPUBLIQUE TCHÈQUE"
        If TextBox6.Text = "DDR" Then TextBox45.Text = "ALLEMAGNE DE L'EST"
        If TextBox6.Text = "DEU" Then TextBox45.Text = "ALLEMAGNE"
        If TextBox6.Text = "DIS" Then TextBox45.Text = "REGISTRE INTERNATIONAL DANOIS"
        If TextBox6.Text = "DJI" Then TextBox45.Text = "DJIBOUTI"
        If TextBox6.Text = "DMA" Then TextBox45.Text = "DOMINIQUE"
        If TextBox6.Text = "DMC" Then TextBox45.Text = "DOMINIQUE"
        If TextBox6.Text = "DNK" Then TextBox45.Text = "DANEMARK"
        If TextBox6.Text = "DOM" Then TextBox45.Text = "RÉPUBLIQUE DOMINICAINE"
        If TextBox6.Text = "DZA" Then TextBox45.Text = "ALGÉRIE"
        If TextBox6.Text = "ECU" Then TextBox45.Text = "ÉQUATEUR"
        If TextBox6.Text = "EGY" Then TextBox45.Text = "ÉGYPTE"
        If TextBox6.Text = "EGU" Then TextBox45.Text = "GUINÉE ÉQUATORIALE"
        If TextBox6.Text = "ERI" Then TextBox45.Text = "ÉRYTHRÉE"
        If TextBox6.Text = "ESP" Then TextBox45.Text = "ESPAGNE"
        If TextBox6.Text = "EST" Then TextBox45.Text = "ESTONIE"
        If TextBox6.Text = "ETH" Then TextBox45.Text = "ÉTHIOPIE"
        If TextBox6.Text = "FAS" Then TextBox45.Text = "ILES FÉROÉ"
        If TextBox6.Text = "FDR" Then TextBox45.Text = "ALLEMAGNE DE L'EST"
        If TextBox6.Text = "FGU" Then TextBox45.Text = "GUYANE"
        If TextBox6.Text = "FIN" Then TextBox45.Text = "FINLANDE"
        If TextBox6.Text = "FIS" Then TextBox45.Text = "FRANCE(FIS)"
        If TextBox6.Text = "FJI" Then TextBox45.Text = "ILES FIDJI"
        If TextBox6.Text = "FNR" Then TextBox45.Text = "DÉFINITION EN ATTENTE"
        If TextBox6.Text = "FPO" Then TextBox45.Text = "POLYNÉSIE FRANÇAISE"
        If TextBox6.Text = "FRA" Then TextBox45.Text = "FRANCE"
        If TextBox6.Text = "FRO" Then TextBox45.Text = "ILES FÉROÉ"
        If TextBox6.Text = "FUN" Then TextBox45.Text = "PAVILLON INCONNU"
        If TextBox6.Text = "GAB" Then TextBox45.Text = "GABON"
        If TextBox6.Text = "GAM" Then TextBox45.Text = "GAMBIE"
        If TextBox6.Text = "GBR" Then TextBox45.Text = "GRANDE BRETAGNE"
        If TextBox6.Text = "GBC" Then TextBox45.Text = "ILES ANGLO-NORMANDES"
        If TextBox6.Text = "GCA" Then TextBox45.Text = "GUATÉMALA"
        If TextBox6.Text = "GEO" Then TextBox45.Text = "GÉORGIE"
        If TextBox6.Text = "GHA" Then TextBox45.Text = "GHANA"
        If TextBox6.Text = "GIB" Then TextBox45.Text = "GIBRALTAR"
        If TextBox6.Text = "GIN" Then TextBox45.Text = "GUINÉE"
        If TextBox6.Text = "GMB" Then TextBox45.Text = "GAMBIE"
        If TextBox6.Text = "GNB" Then TextBox45.Text = "GUINÉE BISSAU"
        If TextBox6.Text = "GNQ" Then TextBox45.Text = "GUINÉE ÉQUATORIALE"
        If TextBox6.Text = "GRC" Then TextBox45.Text = "GRÈCE"
        If TextBox6.Text = "GRD" Then TextBox45.Text = "GRENADE"
        If TextBox6.Text = "GRL" Then TextBox45.Text = "GROENLAND"
        If TextBox6.Text = "GRN" Then TextBox45.Text = "GRENADE"
        If TextBox6.Text = "GTM" Then TextBox45.Text = "GUATÉMALA"
        If TextBox6.Text = "GUE" Then TextBox45.Text = "DÉFINITION EN ATTENTE"
        If TextBox6.Text = "GUY" Then TextBox45.Text = "GUYANA"
        If TextBox6.Text = "HKG" Then TextBox45.Text = "HONG KONG"
        If TextBox6.Text = "HND" Then TextBox45.Text = "HONDURAS"
        If TextBox6.Text = "HON" Then TextBox45.Text = "HONDURAS"
        If TextBox6.Text = "GUA" Then TextBox45.Text = "ILE DE GUAM"
        If TextBox6.Text = "HRV" Then TextBox45.Text = "CROATIE"
        If TextBox6.Text = "HTI" Then TextBox45.Text = "HAITI"
        If TextBox6.Text = "HUN" Then TextBox45.Text = "HONGRIE"
        If TextBox6.Text = "IBN" Then TextBox45.Text = "ARABIE SAOUDITE"
        If TextBox6.Text = "IDN" Then TextBox45.Text = "INDONÉSIE"
        If TextBox6.Text = "IND" Then TextBox45.Text = "INDE"
        If TextBox6.Text = "INT" Then TextBox45.Text = "COMPAGNIES INTERNATIONALES"
        If TextBox6.Text = "IOM" Then TextBox45.Text = "ILE DE MAN"
        If TextBox6.Text = "IRL" Then TextBox45.Text = "RÉPUBLIQUE D'IRLANDE"
        If TextBox6.Text = "IRN" Then TextBox45.Text = "IRAN"
        If TextBox6.Text = "IRQ" Then TextBox45.Text = "IRAK"
        If TextBox6.Text = "ISL" Then TextBox45.Text = "ISLANDE"
        If TextBox6.Text = "ISR" Then TextBox45.Text = "ISRAËL"
        If TextBox6.Text = "ITA" Then TextBox45.Text = "ITALIE"
        If TextBox6.Text = "JAM" Then TextBox45.Text = "JAMAÏQUE"
        If TextBox6.Text = "JAP" Then TextBox45.Text = "JAPON"
        If TextBox6.Text = "JER" Then TextBox45.Text = "DÉFINITION EN ATTENTE"
        If TextBox6.Text = "JOR" Then TextBox45.Text = "JORDANIE"
        If TextBox6.Text = "JPN" Then TextBox45.Text = "JAPON"
        If TextBox6.Text = "KAZ" Then TextBox45.Text = "KAZAKHSTAN"
        If TextBox6.Text = "KEN" Then TextBox45.Text = "KENYA"
        If TextBox6.Text = "KER" Then TextBox45.Text = "ILES KERGUÉLEN"
        If TextBox6.Text = "KHM" Then TextBox45.Text = "CAMBODGE"
        If TextBox6.Text = "KIR" Then TextBox45.Text = "KIRIBATI (ANCIENNES ILES GILBERT)"
        If TextBox6.Text = "KOR" Then TextBox45.Text = "CORÉE DU SUD"
        If TextBox6.Text = "KWT" Then TextBox45.Text = "KOWEIT"
        If TextBox6.Text = "LAO" Then TextBox45.Text = "LAOS"
        If TextBox6.Text = "LAT" Then TextBox45.Text = "LÉTTONIE"
        If TextBox6.Text = "LBN" Then TextBox45.Text = "LEBANON"
        If TextBox6.Text = "LBR" Then TextBox45.Text = "LIBERIA"
        If TextBox6.Text = "LBY" Then TextBox45.Text = "LIBYE"
        If TextBox6.Text = "LCA" Then TextBox45.Text = "SAINTE LUCIE"
        If TextBox6.Text = "LIB" Then TextBox45.Text = "LIBÉRIA"
        If TextBox6.Text = "LIC" Then TextBox45.Text = "LIECHTENSTEIN"
        If TextBox6.Text = "LIT" Then TextBox45.Text = "LITHUANIE"
        If TextBox6.Text = "LKA" Then TextBox45.Text = "SRI LANKA"
        If TextBox6.Text = "LTU" Then TextBox45.Text = "LITUANIE"
        If TextBox6.Text = "LUX" Then TextBox45.Text = "LUXEMBOURG"
        If TextBox6.Text = "LVA" Then TextBox45.Text = "LÉTTONIE"
        If TextBox6.Text = "MAC" Then TextBox45.Text = "MACAO"
        If TextBox6.Text = "MAD" Then TextBox45.Text = "MADEIRE"
        If TextBox6.Text = "MAP" Then TextBox45.Text = "MADEIRE(MAR)"
        If TextBox6.Text = "MAR" Then TextBox45.Text = "MARTINIQUE"
        If TextBox6.Text = "MAS" Then TextBox45.Text = "MADAGASCAR"
        If TextBox6.Text = "MAU" Then TextBox45.Text = "MAURITANIE"
        If TextBox6.Text = "MCD" Then TextBox45.Text = "MACÉDONIE"
        If TextBox6.Text = "MCO" Then TextBox45.Text = "MONACO"
        If TextBox6.Text = "MDG" Then TextBox45.Text = "MADAGASCAR"
        If TextBox6.Text = "MDV" Then TextBox45.Text = "ILES MALDIVES"
        If TextBox6.Text = "MEX" Then TextBox45.Text = "MEXIQUE"
        If TextBox6.Text = "MHL" Then TextBox45.Text = "ILES MARSHALL"
        If TextBox6.Text = "MIC" Then TextBox45.Text = "MICRONÉSIE"
        If TextBox6.Text = "MLI" Then TextBox45.Text = "MALI"
        If TextBox6.Text = "MLT" Then TextBox45.Text = "MALTE"
        If TextBox6.Text = "MLW" Then TextBox45.Text = "MALAWI"
        If TextBox6.Text = "MMR" Then TextBox45.Text = "MYANMAR (BIRMANIE)"
        If TextBox6.Text = "MNG" Then TextBox45.Text = "MONGOLIE"
        If TextBox6.Text = "MOL" Then TextBox45.Text = "MOLDAVIE"
        If TextBox6.Text = "MON" Then TextBox45.Text = "MONTSERRAT"
        If TextBox6.Text = "MOZ" Then TextBox45.Text = "MOZAMBIQUE"
        If TextBox6.Text = "MRA" Then TextBox45.Text = "ILES MARIANNES DU NORD"
        If TextBox6.Text = "MRT" Then TextBox45.Text = "MAURITANIE"
        If TextBox6.Text = "MTN" Then TextBox45.Text = "MONTÉNÉGRO"
        If TextBox6.Text = "MUS" Then TextBox45.Text = "ILE MAURICE"
        If TextBox6.Text = "MYS" Then TextBox45.Text = "MALAISIE"
        If TextBox6.Text = "MYT" Then TextBox45.Text = "MAYOTTE"
        If TextBox6.Text = "NAM" Then TextBox45.Text = "NAMIBIE"
        If TextBox6.Text = "NAU" Then TextBox45.Text = "NAURU"
        If TextBox6.Text = "NGA" Then TextBox45.Text = "NIGÉRIA"
        If TextBox6.Text = "NGR" Then TextBox45.Text = "NIGER"
        If TextBox6.Text = "NIC" Then TextBox45.Text = "NICARAGUA"
        If TextBox6.Text = "NIG" Then TextBox45.Text = "NIGER"
        If TextBox6.Text = "NIS" Then TextBox45.Text = "REGISTRE INTERNATIONAL NORVÉGIEN"
        If TextBox6.Text = "NIU" Then TextBox45.Text = "NIUE"
        If TextBox6.Text = "NLD" Then TextBox45.Text = "PAYS BAS"
        If TextBox6.Text = "NOR" Then TextBox45.Text = "NORVÈGE"
        If TextBox6.Text = "NPL" Then TextBox45.Text = "NÉPAL"
        If TextBox6.Text = "NRU" Then TextBox45.Text = "NAURU"
        If TextBox6.Text = "NZL" Then TextBox45.Text = "NOUVELLE ZÉLANDE"
        If TextBox6.Text = "OMN" Then TextBox45.Text = "OMAN"
        If TextBox6.Text = "PAK" Then TextBox45.Text = "PAKISTAN"
        If TextBox6.Text = "PAN" Then TextBox45.Text = "PANAMA"
        If TextBox6.Text = "PER" Then TextBox45.Text = "PÉROU"
        If TextBox6.Text = "PHL" Then TextBox45.Text = "PHILIPPINES"
        If TextBox6.Text = "PLW" Then TextBox45.Text = "PALAOS"
        If TextBox6.Text = "PMD" Then TextBox45.Text = "MADÈRE"
        If TextBox6.Text = "PNG" Then TextBox45.Text = "PAPOUASIE NOUVELLE GUINÉE"
        If TextBox6.Text = "POL" Then TextBox45.Text = "POLOGNE"
        If TextBox6.Text = "PRK" Then TextBox45.Text = "CORÉE DU NORD"
        If TextBox6.Text = "PRT" Then TextBox45.Text = "PORTUGAL"
        If TextBox6.Text = "PRY" Then TextBox45.Text = "PARAGUAY"
        If TextBox6.Text = "PTM" Then TextBox45.Text = "MALAYSIE"
        If TextBox6.Text = "QAT" Then TextBox45.Text = "QATAR"
        If TextBox6.Text = "RCH" Then TextBox45.Text = "CHILI"
        If TextBox6.Text = "REU" Then TextBox45.Text = "RÉUNION"
        If TextBox6.Text = "ROM" Then TextBox45.Text = "ROUMANIE"
        If TextBox6.Text = "RUS" Then TextBox45.Text = "RUSSIE"
        If TextBox6.Text = "SAM" Then TextBox45.Text = "SAMOA AMÉRICAINES"
        If TextBox6.Text = "SAU" Then TextBox45.Text = "ARABIE SAOUDITE"
        If TextBox6.Text = "SDN" Then TextBox45.Text = "SOUDAN"
        If TextBox6.Text = "SEM" Then TextBox45.Text = "SERBIE ET MONTÉNÉGRO"
        If TextBox6.Text = "SEN" Then TextBox45.Text = "SÉNÉGAL"
        If TextBox6.Text = "SGP" Then TextBox45.Text = "SINGAPOUR"
        If TextBox6.Text = "SKN" Then TextBox45.Text = "SAINT CHRISTOPHE ET NIÉVÈS"
        If TextBox6.Text = "SLB" Then TextBox45.Text = "ILES SALOMON"
        If TextBox6.Text = "SLE" Then TextBox45.Text = "SIERRA LÉONE"
        If TextBox6.Text = "SLO" Then TextBox45.Text = "SLOVÉNIE"
        If TextBox6.Text = "SLU" Then TextBox45.Text = "SAINTE LUCIE"
        If TextBox6.Text = "SLV" Then TextBox45.Text = "SLOVÉQUIE"
        If TextBox6.Text = "SMA" Then TextBox45.Text = "SAN MARIN"
        If TextBox6.Text = "SNG" Then TextBox45.Text = "SINGAPOUR"
        If TextBox6.Text = "SOM" Then TextBox45.Text = "SOMALIE"
        If TextBox6.Text = "SPM" Then TextBox45.Text = "SAINT PIERRE ET MIQUELON"
        If TextBox6.Text = "SRB" Then TextBox45.Text = "SERBIE"
        If TextBox6.Text = "STP" Then TextBox45.Text = "SAO TOME ET PRINCIPE"
        If TextBox6.Text = "SUD" Then TextBox45.Text = "SOUDAN"
        If TextBox6.Text = "SUR" Then TextBox45.Text = "SURINAM"
        If TextBox6.Text = "SVB" Then TextBox45.Text = "SPITZBERG"
        If TextBox6.Text = "SVK" Then TextBox45.Text = "SLOVAQUIE"
        If TextBox6.Text = "SVN" Then TextBox45.Text = "SLOVÉNIE"
        If TextBox6.Text = "SWE" Then TextBox45.Text = "SUÈDE"
        If TextBox6.Text = "SYC" Then TextBox45.Text = "SEYCHELLES"
        If TextBox6.Text = "SYR" Then TextBox45.Text = "SYRIE"
        If TextBox6.Text = "TAH" Then TextBox45.Text = "TAHITI"
        If TextBox6.Text = "TAI" Then TextBox45.Text = "TAÏWAN"
        If TextBox6.Text = "TCI" Then TextBox45.Text = "ILES TURQUES ET CAÏQUES"
        If TextBox6.Text = "TGO" Then TextBox45.Text = "TOGO"
        If TextBox6.Text = "THA" Then TextBox45.Text = "THAÏLANDE"
        If TextBox6.Text = "TKM" Then TextBox45.Text = "TURKMÉNISTAN"
        If TextBox6.Text = "TOG" Then TextBox45.Text = "TOGO"
        If TextBox6.Text = "TON" Then TextBox45.Text = "TONGA"
        If TextBox6.Text = "TTO" Then TextBox45.Text = "TRINITÉ ET TOBAGO"
        If TextBox6.Text = "TUN" Then TextBox45.Text = "TUNISIE"
        If TextBox6.Text = "TUR" Then TextBox45.Text = "TURQUIE"
        If TextBox6.Text = "TUV" Then TextBox45.Text = "TUVALU"
        If TextBox6.Text = "TWN" Then TextBox45.Text = "TAIWAN"
        If TextBox6.Text = "TZA" Then TextBox45.Text = "TANZANIE"
        If TextBox6.Text = "TZZ" Then TextBox45.Text = "DÉFINITION EN ATTENTE"
        If TextBox6.Text = "UGA" Then TextBox45.Text = "OUGANDA"
        If TextBox6.Text = "UKR" Then TextBox45.Text = "UKRAINE"
        If TextBox6.Text = "URY" Then TextBox45.Text = "URUGUAY"
        If TextBox6.Text = "USA" Then TextBox45.Text = "ÉTATS UNIS"
        If TextBox6.Text = "UZB" Then TextBox45.Text = "OUSBÉKISTAN"
        If TextBox6.Text = "VCT" Then TextBox45.Text = "SAINT VINCENT ET GRENADINES"
        If TextBox6.Text = "VEN" Then TextBox45.Text = "VÉNÉZUELA"
        If TextBox6.Text = "VGB" Then TextBox45.Text = "ILES VIERGES"
        If TextBox6.Text = "VIB" Then TextBox45.Text = "ILES VIERGES (GRANDE BRETAGNE)"
        If TextBox6.Text = "VIU" Then TextBox45.Text = "ILES VIERGES (ÉTATS UNIS)"
        If TextBox6.Text = "VNM" Then TextBox45.Text = "VIETNAM"
        If TextBox6.Text = "VUT" Then TextBox45.Text = "VANUATU"
        If TextBox6.Text = "WAN" Then TextBox45.Text = "NIGÉRIA"
        If TextBox6.Text = "WSA" Then TextBox45.Text = "SAHARA OCCIDENTAL"
        If TextBox6.Text = "WSM" Then TextBox45.Text = "SAMOA OCCIDENTALES"
        If TextBox6.Text = "YEM" Then TextBox45.Text = "YÉMEN"
        If TextBox6.Text = "ZAF" Then TextBox45.Text = "AFRIQUE DU SUD"
        If TextBox6.Text = "ZAM" Then TextBox45.Text = "ZAMBIE"
        If TextBox6.Text = "ZAR" Then TextBox45.Text = "ZAÏRE"

        '==============================================
        ' Lieu géographique du pavillon du navire
        TextBox46.Text = ""
        If TextBox6.Text = "AL" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "AG" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "SAM" Then TextBox46.Text = "0C"
        If TextBox6.Text = "AND" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "AN" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "AGL" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "AT" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "RA" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "ARM" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "ARU" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "ASC" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "AUS" Then TextBox46.Text = "OC"
        If TextBox6.Text = "A" Then TextBox46.Text = "EUCE"
        If TextBox6.Text = "AZB" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "AZ" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "BS" Then TextBox46.Text = "AMNO"
        If TextBox6.Text = "BRN" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "BNG" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "BB" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "BRU" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "B" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "BL" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "BEN" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "BE" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "BOL" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "BOS" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "BR" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "BIO" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "BX" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "BG" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "BKA" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "BA" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "BDI" Then TextBox46.Text = "AFCE"
        If TextBox6.Text = "CAB" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "CAM" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "CAN" Then TextBox46.Text = "AMNO"
        If TextBox6.Text = "CA" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "CVI" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "CAY" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "GBC" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "RCH" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "CHI" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "CMI" Then TextBox46.Text = "OC"
        If TextBox6.Text = "COC" Then TextBox46.Text = "OC"
        If TextBox6.Text = "CO" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "COM" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "CN" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "CON" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "COO" Then TextBox46.Text = "OC"
        If TextBox6.Text = "CR" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "CRO" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "C" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "CY" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "CZR" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "CS" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "DIS" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "DK" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "DJ" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "DMC" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "DOM" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "EI" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "EQ" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "ET" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "ES" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "EGU" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "ERI" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "EST" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "ETH" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "FI" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "FA" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "FJ" Then TextBox46.Text = "OC"
        If TextBox6.Text = "SF" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "F" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "FGU" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "FPO" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "GAB" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "GAM" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "GEO" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "D" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "DDR" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "FDR" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "GH" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "GIB" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "GR" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "GE" Then TextBox46.Text = "AMNO"
        If TextBox6.Text = "GRN" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "GD" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "GUA" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "GCA" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "G" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "GU" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "GY" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "RH" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "HON" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "HK" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "H" Then TextBox46.Text = "EUCE"
        If TextBox6.Text = "IS" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "IND" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "RI" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "IR" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "IRQ" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "IRL" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "IOM" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "IL" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "I" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "CI" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "JAM" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "JAP" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "JOR" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "KAZ" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "KEN" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "KER" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "KI" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "NK" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "SK" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "KWT" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "LAO" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "LAT" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "RL" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "LIB" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "LY" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "LIC" Then TextBox46.Text = "EUCE"
        If TextBox6.Text = "LI" Then TextBox46.Text = "OC"
        If TextBox6.Text = "LIT" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "LUX" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "MAC" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "MCD" Then TextBox46.Text = "EUCE"
        If TextBox6.Text = "MAS" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "MAD" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "MLW" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "PTM" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "MV" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "MLI" Then TextBox46.Text = "AFCE"
        If TextBox6.Text = "MLT" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "ML" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "MAR" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "MAU" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "MS" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "MYT" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "MEX" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "MIC" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "MOL" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "MC" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "MG" Then TextBox46.Text = "ASCE"
        If TextBox6.Text = "MON" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "MA" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "MB" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "MY" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "NAM" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "NAU" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "NPL" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "NL" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "NA" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "NC" Then TextBox46.Text = "OC"
        If TextBox6.Text = "NZ" Then TextBox46.Text = "OC"
        If TextBox6.Text = "NIC" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "NIG" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "WAN" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "MRA" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "N" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "NIS" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "OM" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "PAK" Then TextBox46.Text = "ASSU"
        If TextBox6.Text = "PLW" Then TextBox46.Text = "OC"
        If TextBox6.Text = "PA" Then TextBox46.Text = "AMCE"
        If TextBox6.Text = "PNG" Then TextBox46.Text = "OC"
        If TextBox6.Text = "PY" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "PE" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "PI" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "PL" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "P" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "PR" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "Q" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "REU" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "R" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "RU" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "RW" Then TextBox46.Text = "AFCE"
        If TextBox6.Text = "WS" Then TextBox46.Text = "OC"
        If TextBox6.Text = "SA" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "IBN" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "SN" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "SE" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "SH" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "SL" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "SNG" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "SLV" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "SLO" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "SI" Then TextBox46.Text = "OC"
        If TextBox6.Text = "SOM" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "ZA" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "E" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "SM" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "CL" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "ST" Then TextBox46.Text = "AFSU"
        If TextBox6.Text = "SKN" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "SLU" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "SPM" Then TextBox46.Text = "AMNO"
        If TextBox6.Text = "SV" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "SUD" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "SR" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "S" Then TextBox46.Text = "EUNO"
        If TextBox6.Text = "CH" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "SYR" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "TAI" Then TextBox46.Text = "ASOR"
        If TextBox6.Text = "ZZ" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "T" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "TOG" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "TO" Then TextBox46.Text = "OC"
        If TextBox6.Text = "TT" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "TN" Then TextBox46.Text = "AFNO"
        If TextBox6.Text = "TR" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "TKM" Then TextBox46.Text = "ASCE"
        If TextBox6.Text = "TCI" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "TUV" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "SU" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "UG" Then TextBox46.Text = "AFOR"
        If TextBox6.Text = "UKR" Then TextBox46.Text = "EUOR"
        If TextBox6.Text = "UA" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "GB" Then TextBox46.Text = "EUOC"
        If TextBox6.Text = "USA" Then TextBox46.Text = "AMNO"
        If TextBox6.Text = "FUN" Then TextBox46.Text = ""
        If TextBox6.Text = "U" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "UZB" Then TextBox46.Text = "ASCE"
        If TextBox6.Text = "VA" Then TextBox46.Text = "OC"
        If TextBox6.Text = "YV" Then TextBox46.Text = "AMSU"
        If TextBox6.Text = "VN" Then TextBox46.Text = "ASSE"
        If TextBox6.Text = "VIB" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "VIU" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "WF" Then TextBox46.Text = "OC"
        If TextBox6.Text = "WSA" Then TextBox46.Text = "AFOC"
        If TextBox6.Text = "WI" Then TextBox46.Text = "AMAN"
        If TextBox6.Text = "YEM" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "NYM" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "SYM" Then TextBox46.Text = "ASOC"
        If TextBox6.Text = "YU" Then TextBox46.Text = "EUSU"
        If TextBox6.Text = "ZAM" Then TextBox46.Text = "AFEQ"
        If TextBox6.Text = "ZB" Then TextBox46.Text = "AFEQ"

        If TextBox46.Text = "" Then TextBox46.Text = "NON DÉFINI"

        '==============================================
        ' Signification du code du lieu géographique du pavillon du navire
        TextBox47.Text = ""
        If TextBox46.Text = "AF" Then TextBox47.Text = "AFRIQUE"
        If TextBox46.Text = "AFEQ" Then TextBox47.Text = "AFRIQUE ÉQUATORIALE"
        If TextBox46.Text = "AFNO" Then TextBox47.Text = "AFRIQUE DU NORD ET DU NORD-EST"
        If TextBox46.Text = "AFOC" Then TextBox47.Text = "AFRIQUE OCCIDENTALE"
        If TextBox46.Text = "AFOR" Then TextBox47.Text = "AFRIQUE ORIENTALE"
        If TextBox46.Text = "AFSU" Then TextBox47.Text = "AFRIQUE DU SUD"
        If TextBox46.Text = "AM" Then TextBox47.Text = "AMÉRIQUE"
        If TextBox46.Text = "AMAN" Then TextBox47.Text = "ANTILLES"
        If TextBox46.Text = "AMCE" Then TextBox47.Text = "AMÉRIQUE CENTRALE"
        If TextBox46.Text = "AMNO" Then TextBox47.Text = "AMÉRIQUE DU NORD"
        If TextBox46.Text = "AMSU" Then TextBox47.Text = "AMÉRIQUE DU SUD"
        If TextBox46.Text = "AS" Then TextBox47.Text = "ASIE"
        If TextBox46.Text = "ASCE" Then TextBox47.Text = "ASIE CENTRALE"
        If TextBox46.Text = "ASOC" Then TextBox47.Text = "ASIE OCCIDENTALE ET PAYS DU GOLFE"
        If TextBox46.Text = "ASOR" Then TextBox47.Text = "ASIE ORIENTALE"
        If TextBox46.Text = "ASSE" Then TextBox47.Text = "ASIE DU SUD-EST"
        If TextBox46.Text = "ASSU" Then TextBox47.Text = "ASIE DU SUD"
        If TextBox46.Text = "EU" Then TextBox47.Text = "EUROPE"
        If TextBox46.Text = "EUCE" Then TextBox47.Text = "EUROPE CENTRALE"
        If TextBox46.Text = "EUNO" Then TextBox47.Text = "EUROPE DU NORD"
        If TextBox46.Text = "EUOC" Then TextBox47.Text = "EUROPE OCCIDENTALE"
        If TextBox46.Text = "EUOR" Then TextBox47.Text = "EUROPE ORIENTALE"
        If TextBox46.Text = "EUSU" Then TextBox47.Text = "EUROPE DU SUD"
        If TextBox46.Text = "OC" Then TextBox47.Text = "OCÉANIE"
        If TextBox46.Text = "ANTA" Then TextBox47.Text = "ANTARCTIQUE"

        If TextBox47.Text = "" Then TextBox47.Text = "NON DÉFINI"

        '==============================================
        ' Signification du code du type du navire
        TextBox48.Text = ""
        If TextBox41.Text = "AA" Then TextBox48.Text = "Accommodation Vessel"
        If TextBox41.Text = "AB" Then TextBox48.Text = "Ro-Ro Barge"
        If TextBox41.Text = "AC" Then TextBox48.Text = "Container Barge"
        If TextBox41.Text = "AD" Then TextBox48.Text = "Drilling Barge"
        If TextBox41.Text = "AH" Then TextBox48.Text = "Hopper Barge"
        If TextBox41.Text = "AJ" Then TextBox48.Text = "Asphalt Barge"
        If TextBox41.Text = "AM" Then TextBox48.Text = "Cement Storage Barge"
        If TextBox41.Text = "AN" Then TextBox48.Text = "Barge"
        If TextBox41.Text = "AO" Then TextBox48.Text = "Oil Barge"
        If TextBox41.Text = "AP" Then TextBox48.Text = "Pusher Tug"
        If TextBox41.Text = "AR" Then TextBox48.Text = "Crane / Derrick Barge"
        If TextBox41.Text = "AT" Then TextBox48.Text = "Tug & Barge Combination"
        If TextBox41.Text = "AU" Then TextBox48.Text = "Oil Storage Barge"
        If TextBox41.Text = "AV" Then TextBox48.Text = "Transshipment Vessel"
        If TextBox41.Text = "AX" Then TextBox48.Text = "Semi-Sub Pipe Laying Barge"
        If TextBox41.Text = "AY" Then TextBox48.Text = "Pipe Laying Barge"
        If TextBox41.Text = "AZ" Then TextBox48.Text = "Pontoon"
        If TextBox41.Text = "BC" Then TextBox48.Text = "Bulk / Container Carrier"
        If TextBox41.Text = "BH" Then TextBox48.Text = "Bulk Wood Chip Carrier"
        If TextBox41.Text = "BK" Then TextBox48.Text = "Bulk Limestone Carrier"
        If TextBox41.Text = "BJ" Then TextBox48.Text = "Bulk Gypsum Carrier"
        If TextBox41.Text = "BL" Then TextBox48.Text = "Bulker - Great Lakes Only"
        If TextBox41.Text = "BM" Then TextBox48.Text = "Bulk Cement Carrier"
        If TextBox41.Text = "BN" Then TextBox48.Text = "Bulker"
        If TextBox41.Text = "BO" Then TextBox48.Text = "Ore Carrier"
        If TextBox41.Text = "BS" Then TextBox48.Text = "Bulk Carrier Ore Strengthened"
        If TextBox41.Text = "BV" Then TextBox48.Text = "Bulk Vehicle Carrier"
        If TextBox41.Text = "BX" Then TextBox48.Text = "Bulker Great Lakes only / Dumb"
        If TextBox41.Text = "BZ" Then TextBox48.Text = "Bulker Open Hatch"
        If TextBox41.Text = "CF" Then TextBox48.Text = "Container Ship / all Reefer"
        If TextBox41.Text = "CL" Then TextBox48.Text = "Container Liner"
        If TextBox41.Text = "CN" Then TextBox48.Text = "Container Ship"
        If TextBox41.Text = "CR" Then TextBox48.Text = "Container / Reefer General Cargo"
        If TextBox41.Text = "DA" Then TextBox48.Text = "Semi-Sub Heavy Lift Vessel"
        If TextBox41.Text = "DC" Then TextBox48.Text = "Multi-Purpose Ship"
        If TextBox41.Text = "DH" Then TextBox48.Text = "Heavy Lift Ship"
        If TextBox41.Text = "DK" Then TextBox48.Text = "Livestock Carrier"
        If TextBox41.Text = "DL" Then TextBox48.Text = "Log Tipping Vessel"
        If TextBox41.Text = "DM" Then TextBox48.Text = "Cement Carrier"
        If TextBox41.Text = "DN" Then TextBox48.Text = "General Cargo Ship"
        If TextBox41.Text = "DR" Then TextBox48.Text = "General Cargo / Part Refrigerated Ship"
        If TextBox41.Text = "DS" Then TextBox48.Text = "Deck Cargo Ship"
        If TextBox41.Text = "DY" Then TextBox48.Text = "Yacht Carrier"
        If TextBox41.Text = "FL" Then TextBox48.Text = "Live Fish Carrier"
        If TextBox41.Text = "FP" Then TextBox48.Text = "Fisheries Protection Vessel"
        If TextBox41.Text = "FR" Then TextBox48.Text = "Fisheries Research"
        If TextBox41.Text = "FS" Then TextBox48.Text = "Sealing Vessel"
        If TextBox41.Text = "FT" Then TextBox48.Text = "Fisheries Training"
        If TextBox41.Text = "FV" Then TextBox48.Text = "Fishing Vessel"
        If TextBox41.Text = "FW" Then TextBox48.Text = "Whaling Vessel"
        If TextBox41.Text = "GB" Then TextBox48.Text = "Bucket Dredger"
        If TextBox41.Text = "GC" Then TextBox48.Text = "Cutter Suction Dredger"
        If TextBox41.Text = "GD" Then TextBox48.Text = "Dragger Dredger"
        If TextBox41.Text = "GF" Then TextBox48.Text = "Hopper Suction Dredger"
        If TextBox41.Text = "GG" Then TextBox48.Text = "Grab Dredger"
        If TextBox41.Text = "GH" Then TextBox48.Text = "Dipper Dredger"
        If TextBox41.Text = "GK" Then TextBox48.Text = "Cutter Dredger"
        If TextBox41.Text = "GL" Then TextBox48.Text = "Sand Loading Dredger"
        If TextBox41.Text = "GN" Then TextBox48.Text = "Dredger"
        If TextBox41.Text = "GP" Then TextBox48.Text = "Trailing Suction Hopper Dredger"
        If TextBox41.Text = "GR" Then TextBox48.Text = "Rock Breaking Dreger"
        If TextBox41.Text = "GS" Then TextBox48.Text = "Suction Dredger"
        If TextBox41.Text = "GT" Then TextBox48.Text = "Trailing Suction Dredger"
        If TextBox41.Text = "IB" Then TextBox48.Text = "Icebreaker / Buoy Tender"
        If TextBox41.Text = "IN" Then TextBox48.Text = "Icebreaker"
        If TextBox41.Text = "IR" Then TextBox48.Text = "Icebreaker / Research Vessel"
        If TextBox41.Text = "IS" Then TextBox48.Text = "Salvage Vessel"
        If TextBox41.Text = "IT" Then TextBox48.Text = "Salvage Tug"
        If TextBox41.Text = "IZ" Then TextBox48.Text = "Search And Rescue Vessel"
        If TextBox41.Text = "JF" Then TextBox48.Text = "Fisheries Training"
        If TextBox41.Text = "JN" Then TextBox48.Text = "Training Vessel"
        If TextBox41.Text = "JP" Then TextBox48.Text = "Pleasure Craft"
        If TextBox41.Text = "JX" Then TextBox48.Text = "Sail Training Vessel"
        If TextBox41.Text = "LN" Then TextBox48.Text = "Cable Ship"
        If TextBox41.Text = "LZ" Then TextBox48.Text = "Cable Repair Ship"
        If TextBox41.Text = "MA" Then TextBox48.Text = "Product / Ore / Bulk / Oil"
        If TextBox41.Text = "MB" Then TextBox48.Text = "Ore / Bulk / Oil Carrier"
        If TextBox41.Text = "MN" Then TextBox48.Text = "Bulk / Oil Carrier"
        If TextBox41.Text = "MS" Then TextBox48.Text = "Ore / Oil Carrier"
        If TextBox41.Text = "NA" Then TextBox48.Text = "Anti-Pollution Vessel"
        If TextBox41.Text = "NB" Then TextBox48.Text = "Buoy Tender"
        If TextBox41.Text = "NC" Then TextBox48.Text = "Pilot Vessel"
        If TextBox41.Text = "ND" Then TextBox48.Text = "Floating Dock"
        If TextBox41.Text = "NE" Then TextBox48.Text = "Maintenance / Utility Vessel"
        If TextBox41.Text = "NF" Then TextBox48.Text = "Fire Fighting Vessel"
        If TextBox41.Text = "NG" Then TextBox48.Text = "Desalination Vessel"
        If TextBox41.Text = "NL" Then TextBox48.Text = "Lightship"
        If TextBox41.Text = "NP" Then TextBox48.Text = "Floating Nuclear Power Station"
        If TextBox41.Text = "NR" Then TextBox48.Text = "Floating Wave Powered Power Station"
        If TextBox41.Text = "NS" Then TextBox48.Text = "Floating Power Station"
        If TextBox41.Text = "NT" Then TextBox48.Text = "Tug"
        If TextBox41.Text = "NU" Then TextBox48.Text = "Patrol Vessel"
        If TextBox41.Text = "NV" Then TextBox48.Text = "Naval Vessel"
        If TextBox41.Text = "NW" Then TextBox48.Text = "Radioactive Waste Carrier"
        If TextBox41.Text = "OA" Then TextBox48.Text = "Anchor Handling / Tug / Supply"
        If TextBox41.Text = "OB" Then TextBox48.Text = "Anchor Handling / Tug"
        If TextBox41.Text = "OC" Then TextBox48.Text = "Crewboat"
        If TextBox41.Text = "OD" Then TextBox48.Text = "Aht / Salvage"
        If TextBox41.Text = "OE" Then TextBox48.Text = "Diving Support Vessel"
        If TextBox41.Text = "OF" Then TextBox48.Text = "Platform Supply Vessel"
        If TextBox41.Text = "OG" Then TextBox48.Text = "Pipe Carrier / Platform Supply"
        If TextBox41.Text = "OH" Then TextBox48.Text = "Pipe Carrier"
        If TextBox41.Text = "OI" Then TextBox48.Text = "Seismic Survey Vessel"
        If TextBox41.Text = "OJ" Then TextBox48.Text = "Safety Standby Vessel"
        If TextBox41.Text = "OK" Then TextBox48.Text = "Offshore Maintenance / Utility Vessel"
        If TextBox41.Text = "OL" Then TextBox48.Text = "Offshore Cargo Barge"
        If TextBox41.Text = "OR" Then TextBox48.Text = "Survey Ship Rov Support"
        If TextBox41.Text = "OS" Then TextBox48.Text = "Supply Vessel"
        If TextBox41.Text = "OT" Then TextBox48.Text = "Salvage Tug "
        If TextBox41.Text = "OV" Then TextBox48.Text = "Offshore Support Vessel"
        If TextBox41.Text = "PC" Then TextBox48.Text = "Multi-Hull Passenger Ferry"
        If TextBox41.Text = "PD" Then TextBox48.Text = "Multi-Hull Passenger / Vehicle Ferry"
        If TextBox41.Text = "PE" Then TextBox48.Text = "Passenger Excursion Vessel"
        If TextBox41.Text = "PF" Then TextBox48.Text = "Passenger / Cargo Ship"
        If TextBox41.Text = "PG" Then TextBox48.Text = "Casino Ship"
        If TextBox41.Text = "PH" Then TextBox48.Text = "Passenger Hovercraft"
        If TextBox41.Text = "PI" Then TextBox48.Text = "Passenger / Vehicle Hovercraft"
        If TextBox41.Text = "PM" Then TextBox48.Text = "Passenger / Train / Vehicle Ferry"
        If TextBox41.Text = "PN" Then TextBox48.Text = "Passenger Vessel"
        If TextBox41.Text = "PR" Then TextBox48.Text = "River Cruise Ship"
        If TextBox41.Text = "PS" Then TextBox48.Text = "Surface Effect Passenger Ferry"
        If TextBox41.Text = "PT" Then TextBox48.Text = "Surface Effect Passenger / Vehicle Ferry"
        If TextBox41.Text = "PU" Then TextBox48.Text = "Cruise Ship"
        If TextBox41.Text = "PV" Then TextBox48.Text = "Passenger / Vehicle Ferry"
        If TextBox41.Text = "PY" Then TextBox48.Text = "Passenger Hydrofoil"
        If TextBox41.Text = "QA" Then TextBox48.Text = "Aircraft Transport"
        If TextBox41.Text = "QB" Then TextBox48.Text = "Hatch Cover Carrier"
        If TextBox41.Text = "QC" Then TextBox48.Text = "Hospital Ship"
        If TextBox41.Text = "QE" Then TextBox48.Text = "Geophysical Research Vessel"
        If TextBox41.Text = "QF" Then TextBox48.Text = "Fisheries Research"
        If TextBox41.Text = "QH" Then TextBox48.Text = "Fisheries Protection Vessel"
        If TextBox41.Text = "QN" Then TextBox48.Text = "Research Vessel"
        If TextBox41.Text = "QP" Then TextBox48.Text = "Polar Research Vessel"
        If TextBox41.Text = "QR" Then TextBox48.Text = "Radio Station"
        If TextBox41.Text = "QS" Then TextBox48.Text = "Oceanographic Vessel"
        If TextBox41.Text = "QT" Then TextBox48.Text = "Satellite Tracking Ship"
        If TextBox41.Text = "QV" Then TextBox48.Text = "Survey Ship"
        If TextBox41.Text = "QW" Then TextBox48.Text = "Weather Ship"
        If TextBox41.Text = "QX" Then TextBox48.Text = "Rocket Launch Vessel"
        If TextBox41.Text = "QY" Then TextBox48.Text = "Survey / Research Vessel"
        If TextBox41.Text = "RF" Then TextBox48.Text = "Reefer"
        If TextBox41.Text = "RH" Then TextBox48.Text = "Refrigerated Fish Carrier"
        If TextBox41.Text = "SD" Then TextBox48.Text = "Spent Nuclear Fuel Carrier"
        If TextBox41.Text = "SF" Then TextBox48.Text = "Floating Hotel"
        If TextBox41.Text = "SM" Then TextBox48.Text = "Museum Ship"
        If TextBox41.Text = "SO" Then TextBox48.Text = "Replenishment Tanker"
        If TextBox41.Text = "SP" Then TextBox48.Text = "Support Ship"
        If TextBox41.Text = "SQ" Then TextBox48.Text = "Floating Car Park"
        If TextBox41.Text = "SR" Then TextBox48.Text = "Restaurant Ship"
        If TextBox41.Text = "SS" Then TextBox48.Text = "Service Vessel"
        If TextBox41.Text = "SU" Then TextBox48.Text = "Store Ship"
        If TextBox41.Text = "SV" Then TextBox48.Text = "Logistics Vessel"
        If TextBox41.Text = "SX" Then TextBox48.Text = "Exhibition Vessel"
        If TextBox41.Text = "SZ" Then TextBox48.Text = "Houseboat"
        If TextBox41.Text = "TA" Then TextBox48.Text = "Asphalt Tanker"
        If TextBox41.Text = "TB" Then TextBox48.Text = "Bitumen Tanker"
        If TextBox41.Text = "TC" Then TextBox48.Text = "Chemical Tanker"
        If TextBox41.Text = "TD" Then TextBox48.Text = "Fruit Juice Tanker"
        If TextBox41.Text = "TG" Then TextBox48.Text = "LPG Carrier"
        If TextBox41.Text = "TH" Then TextBox48.Text = "LNG Carrier"
        If TextBox41.Text = "TI" Then TextBox48.Text = "Storage Tanker"
        If TextBox41.Text = "TK" Then TextBox48.Text = "Bunker Tanker"
        If TextBox41.Text = "TN" Then TextBox48.Text = "Tanker"
        If TextBox41.Text = "TO" Then TextBox48.Text = "Crude Oil Tanker"
        If TextBox41.Text = "TP" Then TextBox48.Text = "Parcels Tanker"
        If TextBox41.Text = "TQ" Then TextBox48.Text = "Chemical / Oil Tanker"
        If TextBox41.Text = "TR" Then TextBox48.Text = "Products Tanker"
        If TextBox41.Text = "TS" Then TextBox48.Text = "Replenishment Tanker"
        If TextBox41.Text = "TT" Then TextBox48.Text = "Water Tanker"
        If TextBox41.Text = "TU" Then TextBox48.Text = "Phosphoric Acid Carrier"
        If TextBox41.Text = "TW" Then TextBox48.Text = "Wine Tanker"
        If TextBox41.Text = "TX" Then TextBox48.Text = "LPG"
        If TextBox41.Text = "TY" Then TextBox48.Text = "Ethylene Tanker"
        If TextBox41.Text = "TZ" Then TextBox48.Text = "Sulphur Tanker"
        If TextBox41.Text = "UN" Then TextBox48.Text = "Unknown"
        If TextBox41.Text = "WA" Then TextBox48.Text = "Ro - Ro"
        If TextBox41.Text = "WB" Then TextBox48.Text = "Ro - Lo"
        If TextBox41.Text = "WC" Then TextBox48.Text = "Multi Hull Ro - Ro Freight"
        If TextBox41.Text = "WD" Then TextBox48.Text = "Vehicle Carrier"
        If TextBox41.Text = "WF" Then TextBox48.Text = "Pallet Vessel"
        If TextBox41.Text = "WG" Then TextBox48.Text = "Barge Carrier"
        If TextBox41.Text = "WH" Then TextBox48.Text = "Ro - Ro / Heavylift"
        If TextBox41.Text = "WL" Then TextBox48.Text = "Ro - Ro / Cellular"
        If TextBox41.Text = "WN" Then TextBox48.Text = "Ro - Ro / General Cargo"
        If TextBox41.Text = "XC" Then TextBox48.Text = "Tank Cleaning Vessel"
        If TextBox41.Text = "XD" Then TextBox48.Text = "Waste Disposal Vessel(Liq)"
        If TextBox41.Text = "XI" Then TextBox48.Text = "Incinerator & Waste Disposal Vessel"
        If TextBox41.Text = "XM" Then TextBox48.Text = "Diamond Mining Vessel"
        If TextBox41.Text = "XP" Then TextBox48.Text = "Pile Driving Vessel"
        If TextBox41.Text = "XS" Then TextBox48.Text = "Sludge Carrier"
        If TextBox41.Text = "XW" Then TextBox48.Text = "Self Elevating Wind Turbine Installation Vessel"
        If TextBox41.Text = "XY" Then TextBox48.Text = "SWATH Wind Turbine Installation Vessel"
        If TextBox41.Text = "ZA" Then TextBox48.Text = "Offshore Accommodation Vessel"
        If TextBox41.Text = "ZB" Then TextBox48.Text = "Multi Function Service Vessel"
        If TextBox41.Text = "ZC" Then TextBox48.Text = "Offshore Construction Vessel"
        If TextBox41.Text = "ZD" Then TextBox48.Text = "Rock Laying Ship"
        If TextBox41.Text = "ZE" Then TextBox48.Text = "Self-Elevating Mobile Offshore Drilling Unit"
        If TextBox41.Text = "ZF" Then TextBox48.Text = "FPSO"
        If TextBox41.Text = "ZG" Then TextBox48.Text = "Oil Well Stimulation Vessel"
        If TextBox41.Text = "ZH" Then TextBox48.Text = "Self-Elevating Production Unit"
        If TextBox41.Text = "ZN" Then TextBox48.Text = "Drillship"
        If TextBox41.Text = "ZO" Then TextBox48.Text = "Offshore Drilling Barge"
        If TextBox41.Text = "ZP" Then TextBox48.Text = "Floating Production Unit"
        If TextBox41.Text = "ZS" Then TextBox48.Text = "Floating Storage Offtake"
        If TextBox41.Text = "ZT" Then TextBox48.Text = "Oil Well Production Test Vessel"
        If TextBox41.Text = "ZU" Then TextBox48.Text = "Semi Submersible Mobile Offshore Drilling Unit"
        If TextBox41.Text = "ZV" Then TextBox48.Text = "Pipelay Vessel"
        If TextBox41.Text = "ZX" Then TextBox48.Text = "Semi-Sub Pipe Laybarge"
        If TextBox41.Text = "ZY" Then TextBox48.Text = "Pipe Laybarge"
        If TextBox41.Text = "ZZ" Then TextBox48.Text = "Trenching Vessel"

    End Sub

    ' ==============================================================================================================================================
    ' Transfert des informations pour l'extracteur
    Private Sub TextBox1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox1.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 28
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox1.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox1.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox2.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox2.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox3.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 7
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox3.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox3.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox4.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 6
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox4.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox4.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox5_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox5.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 14
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox5.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox5.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox6_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox6.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 30
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox6.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox6.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox7_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox7.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 15
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox7.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox7.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox8_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox8.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 8
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox8.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox8.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox9_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox9.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 31
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox9.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox9.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox10_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox10.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 35
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox10.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox10.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox11_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox11.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 29
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox11.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox11.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox12_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox12.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 9
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox12.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox12.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox13_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox13.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 13
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox13.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox13.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox14_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox14.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 12
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox14.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox14.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox15_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox15.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 34
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox15.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox15.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox16_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox16.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 32
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox16.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox16.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox17_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox17.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 37
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox17.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox17.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox18_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox18.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 10
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox18.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox18.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox19_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox19.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 1
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox19.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox19.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox20_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox20.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 0
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox20.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox20.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox21_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox21.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 4
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox21.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox21.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox22_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox22.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 3
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox22.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox22.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox23_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox23.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 2
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox23.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox23.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox24_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox24.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox24.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox25_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox25.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 18
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox25.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox25.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox26_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox26.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 17
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox26.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox26.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox27_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox27.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 19
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox27.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox27.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox28_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox28.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 25
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox28.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox28.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox29_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox29.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 5
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox29.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox29.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox30_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox30.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 23
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox30.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox30.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox31_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox31.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 22
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox31.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox31.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox32_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox32.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 20
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox32.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox32.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox33_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox33.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 33
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox33.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox33.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox34_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox34.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 16
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox34.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox34.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox35_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox35.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 21
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox35.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox35.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox36_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox36.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox36.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox37_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox37.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 26
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox37.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox37.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox38_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox38.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 24
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox38.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox38.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox39_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox39.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox39.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox40_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox40.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 27
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox40.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox40.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox41_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox41.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 36
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox41.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox41.BackColor = Color.FromArgb(255, 200, 200)
    End Sub
    Private Sub TextBox42_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox42.MouseClick
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.TransSujet = 11
        ' ==================================
        ' Transfert de la valeur de comparaison
        Variables.TransComparaison = TextBox42.Text
        ' ==================================
        ' Colorisation rouge clair
        EffaceCouleurChamps()
        TextBox42.BackColor = Color.FromArgb(255, 200, 200)
    End Sub

    Private Sub TextBox43_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox43.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox43.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox44_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox44.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox44.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox45_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox45.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox45.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox46_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox46.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox46.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox47_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox47.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox47.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub
    Private Sub TextBox48_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox48.MouseClick
        ' ==================================
        ' Colorisation rouge clair fugitive
        EffaceCouleurChamps()
        TextBox48.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Colorisation fugitive
        Timer2.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Apparition fugitive du fond en rose pour les textes ne permettant pas un transfert extracteur
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Timer2.Enabled = False
        EffaceCouleurChamps()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button3.Select()
        '==============================================
        ' Demande de fermeture
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de transfert vers l'extracteur
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button3.Select()
        ' ==================================
        ' Vérification d'une sélection
        If Variables.TransSujet = "" Then
            MsgBox(" Vous n'avez pas réalisé de sélection !", MsgBoxStyle.Exclamation, " Erreur de transfert")
            Exit Sub
        End If
        ' ==================================
        ' Sélection du sujet de la Liste
        Variables.NumeroList = 1
        Extracteur.ListBox1.SelectedIndex = Variables.TransSujet
        ' ==================================
        ' Transfert de la valeur de comparaison
        If Variables.TransSujet = 6 Or Variables.TransSujet = 7 Or Variables.TransSujet = 8 Or Variables.TransSujet = 9 Or Variables.TransSujet = 10 Or Variables.TransSujet = 14 Or Variables.TransSujet = 15 Or Variables.TransSujet = 30 Or Variables.TransSujet = 33 Or Variables.TransSujet = 36 Then
            ' ==================================
            ' Champ de comparaison alphabétique ou date
            Extracteur.TextBox41.Text = Variables.TransComparaison
        Else
            ' ==================================
            ' Champ de comparaison numérique
            Extracteur.TextBox48.Text = Variables.TransComparaison
        End If
        ' ==================================
        ' Initialisation des boutons radio
        Extracteur.RadioButton1.Checked = False
        Extracteur.RadioButton2.Checked = False
        Extracteur.RadioButton3.Checked = False
        Extracteur.RadioButton4.Checked = False
        Extracteur.RadioButton5.Checked = False
        Extracteur.RadioButton6.Checked = False
        Extracteur.RadioButton7.Checked = False
        Extracteur.RadioButton8.Checked = False
        ' ==================================
        ' Initialisation des variables
        Variables.TransSujet = ""
        Variables.TransComparaison = ""
        '==============================================
        ' Demande de fermeture
        Fermeture()
        ' ==================================
        ' Lancement de l'extracteur
        Extracteur.ShowDialog()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton des informations du navire précédent
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        '==============================================
        ' Vérification de la possibilité de décrémentation du numéro de ligne
        If Variables.NumLigSel > 0 Then
            Variables.NumLigSel = Variables.NumLigSel - 1
            '==============================================
            ' Validation du bouton de la ligne suivante
            Button5.Enabled = True
        End If
        '==============================================
        ' Sélection de la ligne pointée
        Form1.DataGridView1.Item(1, Variables.NumLigSel).Selected = True
        '==============================================
        ' Récupération du numéro de la ligne dans la base
        Variables.TransNumLigne = Form1.DataGridView1.Item(0, Variables.NumLigSel).Value
        ' Transfert des infos dans les champs
        AffichageInfos()
        '==============================================
        ' Invalidation du bouton précédent si le numéro de ligne est 0
        If Variables.NumLigSel = 0 Then
            Button4.Enabled = False
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Bouton des informations du navire suivant
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        '==============================================
        ' Vérification de la possibilité d'incrémentation du numéro de ligne
        If Variables.NumLigSel < Variables.NumLigneData Then
            Variables.NumLigSel = Variables.NumLigSel + 1
            '==============================================
            ' Validation du bouton de la ligne précédente
            Button4.Enabled = True
        End If
        '==============================================
        ' Sélection de la ligne pointée
        Form1.DataGridView1.Item(1, Variables.NumLigSel).Selected = True
        '==============================================
        ' Récupération du numéro de la ligne dans la base
        Variables.TransNumLigne = Form1.DataGridView1.Item(0, Variables.NumLigSel).Value
        ' Transfert des infos dans les champs
        AffichageInfos()
        '==============================================
        ' Invalidation du bouton suivant si le numéro de ligne est le dernier
        If Variables.NumLigSel = Variables.NumLigneData Then
            Button5.Enabled = False
        End If
    End Sub

    '=============================================================================================================================================
    ' Transfert vers le Presse Papier pour la fiche du navire
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        ' ============================================================= 
        ' Remise à zéro du presse papier
        Clipboard.Clear()
        ' ============================================================= 
        ' Ligne 1 de transfert dans le Presse papier
        Clipboard.SetText(TextBox5.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox7.Text & Chr("9"))
        ' ============================================================= 
        ' Ligne 2 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox1.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox3.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox43.Text & Chr("9"))
        ' ============================================================= 
        ' Ligne 3 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox2.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox4.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox44.Text & Chr("9"))
        ' ============================================================= 
        ' Ligne 4 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox11.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox41.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox48.Text & Chr("9"))
        ' ============================================================= 
        ' Ligne 5 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox6.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox45.Text & Chr("9"))
        ' ============================================================= 
        ' Ligne 6 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox46.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox47.Text & Chr("9"))
        ' ============================================================= 
        ' Ligne 7 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox33.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox9.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 8 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & TextBox8.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox12.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & TextBox18.Text & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox10.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 9 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox13.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox14.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox15.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox42.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 10 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox16.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox17.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox19.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox20.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 11 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox21.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox22.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox23.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox24.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 12 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox29.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox25.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox26.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox27.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 13 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox28.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox37.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox38.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox39.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 14 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox32.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox40.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox35.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox36.Text, ".", ",") & Chr("9"))
        ' ============================================================= 
        ' Ligne 15 de transfert dans le Presse papier
        Clipboard.SetText(Clipboard.GetText & Chr("13") & Chr("13") & Chr("13") & Replace(TextBox34.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox30.Text, ".", ",") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Chr("9") & Replace(TextBox31.Text, ".", ",") & Chr("9"))
    End Sub

    '=============================================================================================================================================
    ' Bouton Photos
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button3.Select()
        ' ==================================
        ' Connexion site
        Process.Start("http://www.shipspotting.com/gallery/search.php?query=" & TextBox11.Text)
    End Sub

    '=============================================================================================================================================
    ' Bouton Localisation
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button3.Select()
        ' ==================================
        ' Message si le navire n'est pas en service
        If TextBox3.Text <> "D" Then
            MsgBox("Le navire n'est pas en service !", MsgBoxStyle.Information, " Problème de localisation")
            Exit Sub
        End If
        ' ==================================
        ' Connexion site
        Process.Start("http://www.vesselfinder.com/fr/?imo=" & TextBox11.Text)
    End Sub

    ' ==============================================================================================================================================
    ' Suppression du curseur dans les champs en lecture seule par sélection du bouton caché
    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox6.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox8.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox11_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox11.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox12_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox13_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox13.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox14_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox14.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox15.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox16.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox19.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox20_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox21_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox21.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox22_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox22.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox23_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox23.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox24_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox24.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox25_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox25.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox26_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox26.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox27_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox27.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox28_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox28.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox29_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox29.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox30_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox30.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox31_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox31.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox32_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox32.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox33_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox33.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox34_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox34.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox35_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox35.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox36_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox36.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox37_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox37.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox38_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox38.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox39_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox39.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox40_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox40.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox41_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox41.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox42_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox42.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox43_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox43.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox44_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox44.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox45_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox45.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox46_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox46.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox47_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox47.GotFocus
        Button3.Select()
    End Sub
    Private Sub TextBox48_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox48.GotFocus
        Button3.Select()
    End Sub

    '=============================================================================================================================================
    ' Effacement de la couleur de fond des champs
    Private Sub EffaceCouleurChamps()
        TextBox1.BackColor = Color.White
        TextBox2.BackColor = Color.White
        TextBox3.BackColor = Color.White
        TextBox4.BackColor = Color.White
        TextBox5.BackColor = Color.White
        TextBox6.BackColor = Color.White
        TextBox7.BackColor = Color.White
        TextBox8.BackColor = Color.White
        TextBox9.BackColor = Color.White
        TextBox10.BackColor = Color.White
        TextBox11.BackColor = Color.White
        TextBox12.BackColor = Color.White
        TextBox13.BackColor = Color.White
        TextBox14.BackColor = Color.White
        TextBox15.BackColor = Color.White
        TextBox16.BackColor = Color.White
        TextBox17.BackColor = Color.White
        TextBox18.BackColor = Color.White
        TextBox19.BackColor = Color.White
        TextBox20.BackColor = Color.White
        TextBox21.BackColor = Color.White
        TextBox22.BackColor = Color.White
        TextBox23.BackColor = Color.White
        TextBox24.BackColor = Color.White
        TextBox25.BackColor = Color.White
        TextBox26.BackColor = Color.White
        TextBox27.BackColor = Color.White
        TextBox28.BackColor = Color.White
        TextBox29.BackColor = Color.White
        TextBox30.BackColor = Color.White
        TextBox31.BackColor = Color.White
        TextBox32.BackColor = Color.White
        TextBox33.BackColor = Color.White
        TextBox34.BackColor = Color.White
        TextBox35.BackColor = Color.White
        TextBox36.BackColor = Color.White
        TextBox37.BackColor = Color.White
        TextBox38.BackColor = Color.White
        TextBox39.BackColor = Color.White
        TextBox40.BackColor = Color.White
        TextBox41.BackColor = Color.White
        TextBox42.BackColor = Color.White
        TextBox43.BackColor = Color.White
        TextBox44.BackColor = Color.White
        TextBox45.BackColor = Color.White
        TextBox46.BackColor = Color.White
        TextBox47.BackColor = Color.White
        TextBox48.BackColor = Color.White
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Information_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE DE RÉDUCTION DES FENÊTRES
    '=============================================================================================================================================
    Private Sub Information_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub


End Class