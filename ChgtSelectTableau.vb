﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class ChgtSelectTableau
    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub ChgtSelectTableau_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub ChgtSelectTableau_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Recherche des fichiers de Base
        ListBox1.Items.Clear()
        For Each FichiersSelect As String In My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.CurrentDirectory, FileIO.SearchOption.SearchTopLevelOnly, "*.slt")
            ' ==================================
            ' Mise en forme des noms des fichiers trouvés
            FichiersSelect = "   " & Strings.Right(FichiersSelect, Len(FichiersSelect) - Strings.InStrRev(FichiersSelect, "\"))
            FichiersSelect = Strings.Left(FichiersSelect, Len(FichiersSelect) - 4)
            ' ==================================
            ' Rangement des noms de fichiers dans la liste
            ListBox1.Items.Add(FichiersSelect)
        Next
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de sélection du nom du fichier de sélection dans la liste
    Private Sub ListBox1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListBox1.MouseDoubleClick
        '==============================================
        ' Récupération du nom du fichier sélectionné
        Variables.NomSelect = "\" & Strings.LTrim(ListBox1.SelectedItem) & ".slt"
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim NomFichier As StreamReader
        Dim i As Integer
        Dim j As Integer
        Dim Lecture As String
        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur2
        ' ====================================================================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If Variables.NomSelect = "" Then
            MsgBox("Vous n'avez pas réalisé de sélection de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ==================================
        ' Ouverture du fichier de chargement
        NomFichier = New StreamReader(Variables.Repertoire & Variables.NomSelect, System.Text.Encoding.Default)

        ' ==================================
        ' Lecture du fichier 
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Extraction de l'enregistrement
            Lecture = NomFichier.ReadLine
            ' ==================================
            ' Test de l'enregistrement
            If Lecture = "Colonne" Then

                ' ==================================
                ' Extraction de l'enregistrement
                Lecture = NomFichier.ReadLine
                ' ==================================
                ' Vérification si traitement Alphabétique
                If Lecture = "Alphabétique" Then
                    ' ==================================
                    ' Information dans le texte
                    Tableau.TextBox5.Text = "En attente de sélection"
                    ' ==================================
                    ' Extraction de l'enregistrement
                    Lecture = NomFichier.ReadLine

                    ' ==================================
                    ' Vérification si traitement "Code état du navire"
                    If Lecture = "Code état du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox1.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox2.Items.Count - 1
                            Tableau.ListBox2.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Code état du navire"
                        For i = 0 To Tableau.ListBox2.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Ligne" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox2.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox5.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Vérification si traitement "Classe du navire"
                    If Lecture = "Classe du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox1.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox3.Items.Count - 1
                            Tableau.ListBox3.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Classe du navire"
                        For i = 0 To Tableau.ListBox3.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Ligne" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox3.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox5.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Vérification si traitement "Pavillon du navire"
                    If Lecture = "Pavillon du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox1.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox4.Items.Count - 1
                            Tableau.ListBox4.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Pavillon du navire"
                        For i = 0 To Tableau.ListBox4.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Ligne" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox4.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox5.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Vérification si traitement "Type du navire"
                    If Lecture = "Type du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox1.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox5.Items.Count - 1
                            Tableau.ListBox5.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Type du navire"
                        For i = 0 To Tableau.ListBox5.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Ligne" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox5.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox5.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Apparition du panneau Alphabétique
                    ' ============================================
                    ' Vérification si la descente peut être réalisée de suite
                    If Variables.CfgColDescente = "" Then
                        ' ============================================
                        ' Demande de descente
                        Variables.CfgColDescente = "Alpha"
                        ' ============================================
                        ' Activation du timer de descente et de montée
                        Tableau.Timer2.Enabled = True
                        ' ============================================
                        ' Réalisation de la descente en différé
                    Else
                        ' ============================================
                        ' Demande de descente à la fin de la descente précédente
                        Variables.CfgColDescenteSuivant = "Alpha"
                    End If
                End If

                ' ==================================
                ' Vérification si traitement Numérique
                If Lecture = "Numérique" Then
                    ' ==================================
                    ' Extraction de l'enregistrement
                    Lecture = NomFichier.ReadLine
                    ' ==================================
                    ' Affectation du titre de la sélection
                    Tableau.TextBox1.Text = Lecture
                    ' ==================================
                    ' Extraction des enregistrements et affectations
                    Tableau.TextBox21.Text = NomFichier.ReadLine
                    Tableau.TextBox22.Text = NomFichier.ReadLine
                    Tableau.TextBox23.Text = NomFichier.ReadLine
                    Tableau.TextBox24.Text = NomFichier.ReadLine
                    Tableau.TextBox25.Text = NomFichier.ReadLine
                    Tableau.TextBox26.Text = NomFichier.ReadLine
                    Tableau.TextBox27.Text = NomFichier.ReadLine
                    Tableau.TextBox28.Text = NomFichier.ReadLine
                    Tableau.TextBox29.Text = NomFichier.ReadLine
                    Tableau.TextBox30.Text = NomFichier.ReadLine
                    Tableau.TextBox31.Text = NomFichier.ReadLine
                    Tableau.TextBox32.Text = NomFichier.ReadLine

                    Tableau.TextBox52.Text = NomFichier.ReadLine
                    Tableau.TextBox53.Text = NomFichier.ReadLine
                    Tableau.TextBox54.Text = NomFichier.ReadLine
                    Tableau.TextBox55.Text = NomFichier.ReadLine
                    Tableau.TextBox56.Text = NomFichier.ReadLine
                    Tableau.TextBox57.Text = NomFichier.ReadLine
                    Tableau.TextBox58.Text = NomFichier.ReadLine
                    Tableau.TextBox59.Text = NomFichier.ReadLine
                    Tableau.TextBox60.Text = NomFichier.ReadLine
                    Tableau.TextBox61.Text = NomFichier.ReadLine
                    Tableau.TextBox62.Text = NomFichier.ReadLine

                    Tableau.TextBox63.Text = NomFichier.ReadLine
                    Tableau.TextBox64.Text = NomFichier.ReadLine
                    Tableau.TextBox65.Text = NomFichier.ReadLine
                    Tableau.TextBox66.Text = NomFichier.ReadLine
                    Tableau.TextBox67.Text = NomFichier.ReadLine
                    Tableau.TextBox68.Text = NomFichier.ReadLine
                    Tableau.TextBox69.Text = NomFichier.ReadLine
                    Tableau.TextBox70.Text = NomFichier.ReadLine
                    Tableau.TextBox71.Text = NomFichier.ReadLine
                    Tableau.TextBox72.Text = NomFichier.ReadLine
                    Tableau.TextBox73.Text = NomFichier.ReadLine
                    ' ==================================
                    ' Extraction des enregistrements boutons et affectations
                    Lecture = NomFichier.ReadLine
                    If Lecture = "1" Then Tableau.RadioButton1.Checked = True
                    If Lecture = "2" Then Tableau.RadioButton2.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "3" Then Tableau.RadioButton3.Checked = True
                    If Lecture = "4" Then Tableau.RadioButton4.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "5" Then Tableau.RadioButton5.Checked = True
                    If Lecture = "6" Then Tableau.RadioButton6.Checked = True
                    ' ==================================
                    ' Aiguillage des listes pour les colonnes
                    Variables.ListeColonneLigne = True
                    ' ==================================
                    ' TransCodage du nom de la sélection pour le calcul Min Max
                    If Tableau.TextBox1.Text = "Capacité en balles" Then Variables.SelectNumColMinMax = 20
                    If Tableau.TextBox1.Text = "Capacité en céréales" Then Variables.SelectNumColMinMax = 19
                    If Tableau.TextBox1.Text = "Capacité en conteneurs" Then Variables.SelectNumColMinMax = 23
                    If Tableau.TextBox1.Text = "Capacité en conteneurs frigorifiques" Then Variables.SelectNumColMinMax = 22
                    If Tableau.TextBox1.Text = "Capacité en liquide" Then Variables.SelectNumColMinMax = 21
                    If Tableau.TextBox1.Text = "Capacité en minerai" Then Variables.SelectNumColMinMax = 29
                    If Tableau.TextBox1.Text = "Hauteur" Then Variables.SelectNumColMinMax = 42
                    If Tableau.TextBox1.Text = "Largeur" Then Variables.SelectNumColMinMax = 14
                    If Tableau.TextBox1.Text = "Longueur" Then Variables.SelectNumColMinMax = 13
                    If Tableau.TextBox1.Text = "Nombre de barges amarrables" Then Variables.SelectNumColMinMax = 34
                    If Tableau.TextBox1.Text = "Nombre de cales" Then Variables.SelectNumColMinMax = 26
                    If Tableau.TextBox1.Text = "Nombre de conteneurs" Then Variables.SelectNumColMinMax = 25
                    If Tableau.TextBox1.Text = "Nombre d'écoutilles" Then Variables.SelectNumColMinMax = 27
                    If Tableau.TextBox1.Text = "Nombre de passagers" Then Variables.SelectNumColMinMax = 32
                    If Tableau.TextBox1.Text = "Nombre de ponts" Then Variables.SelectNumColMinMax = 35
                    If Tableau.TextBox1.Text = "Nombre de portes latérales droites" Then Variables.SelectNumColMinMax = 31
                    If Tableau.TextBox1.Text = "Nombre de portes latérales gauches" Then Variables.SelectNumColMinMax = 30
                    If Tableau.TextBox1.Text = "Nombre de remorques" Then Variables.SelectNumColMinMax = 38
                    If Tableau.TextBox1.Text = "Nombre de réservoirs" Then Variables.SelectNumColMinMax = 28
                    If Tableau.TextBox1.Text = "Nombre de voitures" Then Variables.SelectNumColMinMax = 37
                    If Tableau.TextBox1.Text = "Nombre de wagons" Then Variables.SelectNumColMinMax = 40
                    If Tableau.TextBox1.Text = "Numéro FAIRPLAY" Then Variables.SelectNumColMinMax = 1
                    If Tableau.TextBox1.Text = "Numéro LLOYDS" Then Variables.SelectNumColMinMax = 11
                    If Tableau.TextBox1.Text = "Port en lourd" Then Variables.SelectNumColMinMax = 9
                    If Tableau.TextBox1.Text = "Profondeur" Then Variables.SelectNumColMinMax = 16
                    If Tableau.TextBox1.Text = "Tirant d'eau" Then Variables.SelectNumColMinMax = 15
                    If Tableau.TextBox1.Text = "Tonnage" Then Variables.SelectNumColMinMax = 10
                    If Tableau.TextBox1.Text = "Vitesse" Then Variables.SelectNumColMinMax = 17
                    ' ==================================
                    ' Demande de calcul des valeurs Min et Max
                    Tableau.ValeurMinMax()
                    ' ==================================
                    ' Apparition du panneau Numérique
                    ' ============================================
                    ' Vérification si la descente peut être réalisée de suite
                    If Variables.CfgColDescente = "" Then
                        ' ============================================
                        ' Demande de descente
                        Variables.CfgColDescente = "Numé"
                        ' ============================================
                        ' Activation du timer de descente et de montée
                        Tableau.Timer2.Enabled = True
                        ' ============================================
                        ' Réalisation de la descente en différé
                    Else
                        ' ============================================
                        ' Demande de descente à la fin de la descente précédente
                        Variables.CfgColDescenteSuivant = "Numé"
                    End If
                End If

                ' ==================================
                ' Vérification si traitement Date
                If Lecture = "Date" Then
                    ' ==================================
                    ' Extraction de l'enregistrement
                    Lecture = NomFichier.ReadLine
                    ' ==================================
                    ' Affectation du titre de la sélection
                    Tableau.TextBox1.Text = Lecture
                    ' ==================================
                    ' Extraction des enregistrements et affectations
                    Tableau.TextBox110.Text = NomFichier.ReadLine
                    Tableau.TextBox111.Text = NomFichier.ReadLine
                    Tableau.TextBox112.Text = NomFichier.ReadLine
                    Tableau.TextBox113.Text = NomFichier.ReadLine
                    Tableau.TextBox114.Text = NomFichier.ReadLine
                    Tableau.TextBox115.Text = NomFichier.ReadLine
                    Tableau.TextBox116.Text = NomFichier.ReadLine
                    Tableau.TextBox117.Text = NomFichier.ReadLine
                    Tableau.TextBox118.Text = NomFichier.ReadLine
                    Tableau.TextBox119.Text = NomFichier.ReadLine
                    Tableau.TextBox120.Text = NomFichier.ReadLine
                    Tableau.TextBox121.Text = NomFichier.ReadLine

                    Tableau.TextBox122.Text = NomFichier.ReadLine
                    Tableau.TextBox123.Text = NomFichier.ReadLine
                    Tableau.TextBox124.Text = NomFichier.ReadLine
                    Tableau.TextBox125.Text = NomFichier.ReadLine
                    Tableau.TextBox126.Text = NomFichier.ReadLine
                    Tableau.TextBox127.Text = NomFichier.ReadLine
                    Tableau.TextBox128.Text = NomFichier.ReadLine
                    Tableau.TextBox129.Text = NomFichier.ReadLine
                    Tableau.TextBox130.Text = NomFichier.ReadLine
                    Tableau.TextBox131.Text = NomFichier.ReadLine
                    Tableau.TextBox132.Text = NomFichier.ReadLine

                    Tableau.TextBox133.Text = NomFichier.ReadLine
                    Tableau.TextBox134.Text = NomFichier.ReadLine
                    Tableau.TextBox135.Text = NomFichier.ReadLine
                    Tableau.TextBox136.Text = NomFichier.ReadLine
                    Tableau.TextBox137.Text = NomFichier.ReadLine
                    Tableau.TextBox138.Text = NomFichier.ReadLine
                    Tableau.TextBox139.Text = NomFichier.ReadLine
                    Tableau.TextBox140.Text = NomFichier.ReadLine
                    Tableau.TextBox141.Text = NomFichier.ReadLine
                    Tableau.TextBox142.Text = NomFichier.ReadLine
                    Tableau.TextBox143.Text = NomFichier.ReadLine
                    ' ==================================
                    ' Extraction des enregistrements boutons et affectations
                    Lecture = NomFichier.ReadLine
                    If Lecture = "13" Then Tableau.RadioButton13.Checked = True
                    If Lecture = "14" Then Tableau.RadioButton14.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "15" Then Tableau.RadioButton15.Checked = True
                    If Lecture = "16" Then Tableau.RadioButton16.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "17" Then Tableau.RadioButton17.Checked = True
                    If Lecture = "18" Then Tableau.RadioButton18.Checked = True
                    ' ==================================
                    ' Aiguillage des listes pour les colonnes
                    Variables.ListeColonneLigne = True
                    ' ==================================
                    ' TransCodage du nom de la sélection pour le calcul Min Max
                    If Tableau.TextBox1.Text = "Date d'achat" Then Variables.SelectNumColMinMax = 8
                    If Tableau.TextBox1.Text = "Date de livraison" Then Variables.SelectNumColMinMax = 12
                    If Tableau.TextBox1.Text = "Date de perte ou de destruction" Then Variables.SelectNumColMinMax = 18
                    ' ==================================
                    ' Demande de calcul des valeurs Min et Max
                    Tableau.ValeurMinMax()
                    ' ==================================
                    ' Apparition du panneau Date
                    ' ============================================
                    ' Vérification si la descente peut être réalisée de suite
                    If Variables.CfgColDescente = "" Then
                        ' ============================================
                        ' Demande de descente
                        Variables.CfgColDescente = "Date"
                        ' ============================================
                        ' Activation du timer de descente et de montée
                        Tableau.Timer2.Enabled = True
                        ' ============================================
                        ' Réalisation de la descente en différé
                    Else
                        ' ============================================
                        ' Demande de descente à la fin de la descente précédente
                        Variables.CfgColDescenteSuivant = "Date"
                    End If
                End If
            End If

            ' ==================================
            ' Test de l'enregistrement
            If Lecture = "Ligne" Then

                ' ==================================
                ' Extraction de l'enregistrement
                Lecture = NomFichier.ReadLine
                ' ==================================
                ' Vérification si traitement Alphabétique
                If Lecture = "Alphabétique" Then
                    ' ==================================
                    ' Information dans le texte
                    Tableau.TextBox8.Text = "En attente de sélection"
                    ' ==================================
                    ' Extraction de l'enregistrement
                    Lecture = NomFichier.ReadLine

                    ' ==================================
                    ' Vérification si traitement "Code état du navire"
                    If Lecture = "Code état du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox2.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox2.Items.Count - 1
                            Tableau.ListBox2.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Code état du navire"
                        For i = 0 To Tableau.ListBox2.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Fin" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox2.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox8.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Vérification si traitement "Classe du navire"
                    If Lecture = "Classe du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox2.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox3.Items.Count - 1
                            Tableau.ListBox3.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Classe du navire"
                        For i = 0 To Tableau.ListBox3.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Fin" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox3.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox8.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Vérification si traitement "Pavillon du navire"
                    If Lecture = "Pavillon du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox2.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox4.Items.Count - 1
                            Tableau.ListBox4.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Pavillon du navire"
                        For i = 0 To Tableau.ListBox4.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Fin" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox4.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox8.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Vérification si traitement "Type du navire"
                    If Lecture = "Type du navire" Then
                        ' ==================================
                        ' Affectation du titre de la sélection
                        Tableau.TextBox2.Text = Lecture
                        ' ==================================
                        ' RAZ des éléments de la liste
                        For i = 0 To Tableau.ListBox5.Items.Count - 1
                            Tableau.ListBox5.SetSelected(i, False)
                        Next
                        ' ==================================
                        ' Sélection dans la liste "Type du navire"
                        For i = 0 To Tableau.ListBox5.Items.Count - 1
                            ' ==================================
                            ' Lecture de l'enregistrement
                            Lecture = NomFichier.ReadLine
                            ' ==================================
                            ' Vérification si la fin de liste est atteinte
                            If Lecture = "Fin" Then Exit For
                            ' ==================================
                            ' Validation de l'enregistrement dans la liste
                            Tableau.ListBox5.SetSelected(Val(Lecture), True)
                        Next
                        ' ==================================
                        ' Vérification s'il y a eu au moins un enregistrement dans la liste
                        If i > 0 Then
                            ' ==================================
                            ' Information dans le texte
                            Tableau.TextBox8.Text = "Sélection dans la liste"
                        End If
                    End If

                    ' ==================================
                    ' Apparition du panneau Alphabétique
                    ' ============================================
                    ' Vérification si la descente peut être réalisée de suite
                    If Variables.CfgLigDescente = "" Then
                        ' ============================================
                        ' Demande de descente
                        Variables.CfgLigDescente = "Alpha"
                        ' ============================================
                        ' Activation du timer de descente et de montée
                        Tableau.Timer3.Enabled = True
                        ' ============================================
                        ' Réalisation de la descente en différé
                    Else
                        ' ============================================
                        ' Demande de descente à la fin de la descente précédente
                        Variables.CfgLigDescenteSuivant = "Alpha"
                    End If
                End If

                ' ==================================
                ' Vérification si traitement Numérique
                If Lecture = "Numérique" Then
                    ' ==================================
                    ' Extraction de l'enregistrement
                    Lecture = NomFichier.ReadLine
                    ' ==================================
                    ' Affectation du titre de la sélection
                    Tableau.TextBox2.Text = Lecture
                    ' ==================================
                    ' Extraction des enregistrements et affectations
                    Tableau.TextBox74.Text = NomFichier.ReadLine
                    Tableau.TextBox75.Text = NomFichier.ReadLine
                    Tableau.TextBox76.Text = NomFichier.ReadLine
                    Tableau.TextBox77.Text = NomFichier.ReadLine
                    Tableau.TextBox78.Text = NomFichier.ReadLine
                    Tableau.TextBox79.Text = NomFichier.ReadLine
                    Tableau.TextBox80.Text = NomFichier.ReadLine
                    Tableau.TextBox81.Text = NomFichier.ReadLine
                    Tableau.TextBox82.Text = NomFichier.ReadLine
                    Tableau.TextBox83.Text = NomFichier.ReadLine
                    Tableau.TextBox84.Text = NomFichier.ReadLine
                    Tableau.TextBox85.Text = NomFichier.ReadLine

                    Tableau.TextBox86.Text = NomFichier.ReadLine
                    Tableau.TextBox87.Text = NomFichier.ReadLine
                    Tableau.TextBox88.Text = NomFichier.ReadLine
                    Tableau.TextBox89.Text = NomFichier.ReadLine
                    Tableau.TextBox90.Text = NomFichier.ReadLine
                    Tableau.TextBox91.Text = NomFichier.ReadLine
                    Tableau.TextBox92.Text = NomFichier.ReadLine
                    Tableau.TextBox93.Text = NomFichier.ReadLine
                    Tableau.TextBox94.Text = NomFichier.ReadLine
                    Tableau.TextBox95.Text = NomFichier.ReadLine
                    Tableau.TextBox96.Text = NomFichier.ReadLine

                    Tableau.TextBox97.Text = NomFichier.ReadLine
                    Tableau.TextBox98.Text = NomFichier.ReadLine
                    Tableau.TextBox99.Text = NomFichier.ReadLine
                    Tableau.TextBox100.Text = NomFichier.ReadLine
                    Tableau.TextBox101.Text = NomFichier.ReadLine
                    Tableau.TextBox102.Text = NomFichier.ReadLine
                    Tableau.TextBox103.Text = NomFichier.ReadLine
                    Tableau.TextBox104.Text = NomFichier.ReadLine
                    Tableau.TextBox105.Text = NomFichier.ReadLine
                    Tableau.TextBox106.Text = NomFichier.ReadLine
                    Tableau.TextBox107.Text = NomFichier.ReadLine
                    ' ==================================
                    ' Extraction des enregistrements boutons et affectations
                    Lecture = NomFichier.ReadLine
                    If Lecture = "7" Then Tableau.RadioButton7.Checked = True
                    If Lecture = "8" Then Tableau.RadioButton8.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "9" Then Tableau.RadioButton9.Checked = True
                    If Lecture = "10" Then Tableau.RadioButton10.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "11" Then Tableau.RadioButton11.Checked = True
                    If Lecture = "12" Then Tableau.RadioButton12.Checked = True
                    ' ==================================
                    ' Aiguillage des listes pour les colonnes
                    Variables.ListeColonneLigne = False
                    ' ==================================
                    ' TransCodage du nom de la sélection pour le calcul Min Max
                    If Tableau.TextBox2.Text = "Capacité en balles" Then Variables.SelectNumLigMinMax = 20
                    If Tableau.TextBox2.Text = "Capacité en céréales" Then Variables.SelectNumLigMinMax = 19
                    If Tableau.TextBox2.Text = "Capacité en conteneurs" Then Variables.SelectNumLigMinMax = 23
                    If Tableau.TextBox2.Text = "Capacité en conteneurs frigorifiques" Then Variables.SelectNumLigMinMax = 22
                    If Tableau.TextBox2.Text = "Capacité en liquide" Then Variables.SelectNumLigMinMax = 21
                    If Tableau.TextBox2.Text = "Capacité en minerai" Then Variables.SelectNumLigMinMax = 29
                    If Tableau.TextBox2.Text = "Hauteur" Then Variables.SelectNumLigMinMax = 42
                    If Tableau.TextBox2.Text = "Largeur" Then Variables.SelectNumLigMinMax = 14
                    If Tableau.TextBox2.Text = "Longueur" Then Variables.SelectNumLigMinMax = 13
                    If Tableau.TextBox2.Text = "Nombre de barges amarrables" Then Variables.SelectNumLigMinMax = 34
                    If Tableau.TextBox2.Text = "Nombre de cales" Then Variables.SelectNumLigMinMax = 26
                    If Tableau.TextBox2.Text = "Nombre de conteneurs" Then Variables.SelectNumLigMinMax = 25
                    If Tableau.TextBox2.Text = "Nombre d'écoutilles" Then Variables.SelectNumLigMinMax = 27
                    If Tableau.TextBox2.Text = "Nombre de passagers" Then Variables.SelectNumLigMinMax = 32
                    If Tableau.TextBox2.Text = "Nombre de ponts" Then Variables.SelectNumLigMinMax = 35
                    If Tableau.TextBox2.Text = "Nombre de portes latérales droites" Then Variables.SelectNumLigMinMax = 31
                    If Tableau.TextBox2.Text = "Nombre de portes latérales gauches" Then Variables.SelectNumLigMinMax = 30
                    If Tableau.TextBox2.Text = "Nombre de remorques" Then Variables.SelectNumLigMinMax = 38
                    If Tableau.TextBox2.Text = "Nombre de réservoirs" Then Variables.SelectNumLigMinMax = 28
                    If Tableau.TextBox2.Text = "Nombre de voitures" Then Variables.SelectNumLigMinMax = 37
                    If Tableau.TextBox2.Text = "Nombre de wagons" Then Variables.SelectNumLigMinMax = 40
                    If Tableau.TextBox2.Text = "Numéro FAIRPLAY" Then Variables.SelectNumLigMinMax = 1
                    If Tableau.TextBox2.Text = "Numéro LLOYDS" Then Variables.SelectNumLigMinMax = 11
                    If Tableau.TextBox2.Text = "Port en lourd" Then Variables.SelectNumLigMinMax = 9
                    If Tableau.TextBox2.Text = "Profondeur" Then Variables.SelectNumLigMinMax = 16
                    If Tableau.TextBox2.Text = "Tirant d'eau" Then Variables.SelectNumLigMinMax = 15
                    If Tableau.TextBox2.Text = "Tonnage" Then Variables.SelectNumLigMinMax = 10
                    If Tableau.TextBox2.Text = "Vitesse" Then Variables.SelectNumLigMinMax = 17
                    ' ==================================
                    ' Demande de calcul des valeurs Min et Max
                    Tableau.ValeurMinMax()
                    ' ==================================
                    ' Apparition du panneau Numérique
                    ' ============================================
                    ' Vérification si la descente peut être réalisée de suite
                    If Variables.CfgLigDescente = "" Then
                        ' ============================================
                        ' Demande de descente
                        Variables.CfgLigDescente = "Numé"
                        ' ============================================
                        ' Activation du timer de descente et de montée
                        Tableau.Timer3.Enabled = True
                        ' ============================================
                        ' Réalisation de la descente en différé
                    Else
                        ' ============================================
                        ' Demande de descente à la fin de la descente précédente
                        Variables.CfgLigDescenteSuivant = "Numé"
                    End If
                End If

                ' ==================================
                ' Vérification si traitement Date
                If Lecture = "Date" Then
                    ' ==================================
                    ' Extraction de l'enregistrement
                    Lecture = NomFichier.ReadLine
                    ' ==================================
                    ' Affectation du titre de la sélection
                    Tableau.TextBox2.Text = Lecture
                    ' ==================================
                    ' Extraction des enregistrements et affectations
                    Tableau.TextBox146.Text = NomFichier.ReadLine
                    Tableau.TextBox147.Text = NomFichier.ReadLine
                    Tableau.TextBox148.Text = NomFichier.ReadLine
                    Tableau.TextBox149.Text = NomFichier.ReadLine
                    Tableau.TextBox150.Text = NomFichier.ReadLine
                    Tableau.TextBox151.Text = NomFichier.ReadLine
                    Tableau.TextBox152.Text = NomFichier.ReadLine
                    Tableau.TextBox153.Text = NomFichier.ReadLine
                    Tableau.TextBox154.Text = NomFichier.ReadLine
                    Tableau.TextBox155.Text = NomFichier.ReadLine
                    Tableau.TextBox156.Text = NomFichier.ReadLine
                    Tableau.TextBox157.Text = NomFichier.ReadLine

                    Tableau.TextBox158.Text = NomFichier.ReadLine
                    Tableau.TextBox159.Text = NomFichier.ReadLine
                    Tableau.TextBox160.Text = NomFichier.ReadLine
                    Tableau.TextBox161.Text = NomFichier.ReadLine
                    Tableau.TextBox162.Text = NomFichier.ReadLine
                    Tableau.TextBox163.Text = NomFichier.ReadLine
                    Tableau.TextBox164.Text = NomFichier.ReadLine
                    Tableau.TextBox165.Text = NomFichier.ReadLine
                    Tableau.TextBox166.Text = NomFichier.ReadLine
                    Tableau.TextBox167.Text = NomFichier.ReadLine
                    Tableau.TextBox168.Text = NomFichier.ReadLine

                    Tableau.TextBox169.Text = NomFichier.ReadLine
                    Tableau.TextBox170.Text = NomFichier.ReadLine
                    Tableau.TextBox171.Text = NomFichier.ReadLine
                    Tableau.TextBox172.Text = NomFichier.ReadLine
                    Tableau.TextBox173.Text = NomFichier.ReadLine
                    Tableau.TextBox174.Text = NomFichier.ReadLine
                    Tableau.TextBox175.Text = NomFichier.ReadLine
                    Tableau.TextBox176.Text = NomFichier.ReadLine
                    Tableau.TextBox177.Text = NomFichier.ReadLine
                    Tableau.TextBox178.Text = NomFichier.ReadLine
                    Tableau.TextBox179.Text = NomFichier.ReadLine
                    ' ==================================
                    ' Extraction des enregistrements boutons et affectations
                    Lecture = NomFichier.ReadLine
                    If Lecture = "19" Then Tableau.RadioButton19.Checked = True
                    If Lecture = "20" Then Tableau.RadioButton20.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "21" Then Tableau.RadioButton21.Checked = True
                    If Lecture = "22" Then Tableau.RadioButton22.Checked = True
                    Lecture = NomFichier.ReadLine
                    If Lecture = "23" Then Tableau.RadioButton23.Checked = True
                    If Lecture = "24" Then Tableau.RadioButton24.Checked = True
                    ' ==================================
                    ' Aiguillage des listes pour les colonnes
                    Variables.ListeColonneLigne = False
                    ' ==================================
                    ' TransCodage du nom de la sélection pour le calcul Min Max
                    If Tableau.TextBox2.Text = "Date d'achat" Then Variables.SelectNumLigMinMax = 8
                    If Tableau.TextBox2.Text = "Date de livraison" Then Variables.SelectNumLigMinMax = 12
                    If Tableau.TextBox2.Text = "Date de perte ou de destruction" Then Variables.SelectNumLigMinMax = 18
                    ' ==================================
                    ' Demande de calcul des valeurs Min et Max
                    Tableau.ValeurMinMax()
                    ' ==================================
                    ' Apparition du panneau Date
                    ' ============================================
                    ' Vérification si la descente peut être réalisée de suite
                    If Variables.CfgLigDescente = "" Then
                        ' ============================================
                        ' Demande de descente
                        Variables.CfgLigDescente = "Date"
                        ' ============================================
                        ' Activation du timer de descente et de montée
                        Tableau.Timer3.Enabled = True
                        ' ============================================
                        ' Réalisation de la descente en différé
                    Else
                        ' ============================================
                        ' Demande de descente à la fin de la descente précédente
                        Variables.CfgLigDescenteSuivant = "Date"
                    End If
                End If
            End If
        Loop
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Recopie du nom du fichier de sélecion chargé
        TextBox1.Text = Strings.LTrim(ListBox1.SelectedItem)
        SauveSelectTableau.TextBox1.Text = Strings.LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Effacement du nom du fichier de sélection chargé
        Variables.NomSelect = ""
        ' ==================================
        ' Fermeture de l'application
        Fermeture()
        ' ==================================
TraitErreur2:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " Erreur de chargement")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Tableau.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub ChgtSelectTableau_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub
End Class