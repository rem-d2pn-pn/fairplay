﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class Form1


    '=============================================================================================================================================
    ' Procédure de fermeture par la croix
    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' ==================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        ' ==================================
        ' Lancement de la procédure de fermeture
        FermetureFeuille()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Disparition de la feuille Parent
        Me.Visible = False
        ' ==================================
        ' Suppression de la barre supérieure de la feuille Parent
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        ' ==================================
        ' Suppression de la barre des menus de la feuille Parent
        MenuStrip1.Visible = False
        ' ==================================
        ' Prise en compte de la surface écran utilisable
        Variables.FormHauteur = My.Computer.Screen.WorkingArea.Size.Height - 1
        Variables.FormLargeur = My.Computer.Screen.WorkingArea.Size.Width
        ' ==================================
        ' Récupération du chemin vers les fichiers à charger à partir du répertoire de l'application
        Variables.Repertoire = My.Computer.FileSystem.CurrentDirectory
        ' ==================================
        ' Lancement de la procédure d'ouverture de la feuille
        OuvertureFeuille()
        ' ==================================
        ' Adaptation de l'application au format de l'écran utilisable
        '        Me.Height = My.Computer.Screen.WorkingArea.Size.Height - 1
        '        Me.Width = My.Computer.Screen.WorkingArea.Size.Width
        ' ==================================
        ' Adaptation du DataGridView 1
        '        DataGridView1.Height = My.Computer.Screen.WorkingArea.Size.Height - 56
        '        DataGridView1.Width = My.Computer.Screen.WorkingArea.Size.Width - 5
        ' ==================================
        ' Adaptation du DataGridView 2
        '        DataGridView2.Height = My.Computer.Screen.WorkingArea.Size.Height - 56
        '        DataGridView2.Width = My.Computer.Screen.WorkingArea.Size.Width - 5
        ' ==================================
        ' Mise en cache du DataGridview 2
        '        DataGridView2.Top = 25 - DataGridView2.Height
        ' ==================================
        ' Positionnement de la feuille au coin en haut à gauche
        '        Me.Location = New Point(0, 0)
        '==============================================
        ' Demande d'ouverture par Fondu
        '        Me.Opacity = 0
        '        Timer1.Enabled = True
        ' ==================================
        ' Affichage état de l'application
        '        Variables.EtatAppli = "Base non chargée"
        ' ==================================
        ' Chargement de la base
        '        Timer2.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Chargement de la base
    Private Sub ChargementDeLaBaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChargementDeLaBaseToolStripMenuItem.Click
        ChargeurBase.ShowDialog()
        ' ==================================
        ' Définition des variables
        Dim InfoFichier As FileInfo
        Dim DeltaDate As Integer
        Dim DateBase As DateTime
        Dim BaseExiste As String
        ' ==================================
        ' Récupération de la date de la base Fairplay
        InfoFichier = New FileInfo(Variables.Repertoire & Variables.NomFichierBase)
        ' ==================================
        ' Vérification de l'existence de la base Fairplay
        BaseExiste = InfoFichier.Exists
        ' ==================================
        ' Récupération des dates
        DateBase = InfoFichier.LastWriteTime.ToShortDateString
        DeltaDate = DateDiff(DateInterval.Day, DateBase, Now)
        ' ==================================
        ' Message d'information si la base a plus d'un mois
        If DeltaDate > 30 And BaseExiste = True And Strings.Left(ChargeurBase.TextBox1.Text, 12) = "Base chargée" Then
            MsgBox("La Base chargée a  " & DeltaDate & "  jours !", MsgBoxStyle.Exclamation, " Ancienneté de la Base Fairplay")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de l'application
    Private Sub QuitterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitterToolStripMenuItem.Click
        ' ==================================
        ' Lancement de la procédure de fermeture
        FermetureFeuille()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de lancement de l'analyseur
    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Analyseur.ShowDialog()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de lancement de l'extracteur
    Private Sub ExtracteurToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExtracteurToolStripMenuItem.Click
        Extracteur.ShowDialog()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de lancement du Tableau de corrélation
    Private Sub TableauToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TableauToolStripMenuItem.Click
        Tableau.ShowDialog()
    End Sub

    '=============================================================================================================================================
    ' À propos ...
    Private Sub ÀProposToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÀProposToolStripMenuItem.Click
        MsgBox("         Logiciel  FAIRPLAY  Version  07-11-2018" & vbCr & vbLf & vbLf & "                        Jacques  BROVELLI" & vbCr & vbLf & vbLf & "  Développeur  Informatique  CETMEF/DS/SI/SMR   ", , " FAIRPLAY - Version")
    End Sub

    '=============================================================================================================================================
    ' Demande de la Notice
    Private Sub NoticeToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoticeToolStripMenuItem1.Click
        '        MsgBox(" La notice est en cours d'élaboration. ", MsgBoxStyle.Information, " FAIRPLAY - Information")
        '       Exit Sub

        On Error GoTo TraitementErreur
        Process.Start(Application.StartupPath & "\Notice FAIRPLAY.pdf")
        Exit Sub

TraitementErreur:
        MsgBox("Je n'ai pas trouvé la notice ! ", MsgBoxStyle.Information, " FAIRPLAY - Information")
    End Sub

    '=============================================================================================================================================
    ' Procédure d'identification de la cellule sélectionnée et présentation des informations
    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        '==============================================
        ' Sortie si le double clic se produit dans le bandeau du nom de colonnes
        If e.RowIndex = -1 Then Exit Sub
        '==============================================
        ' Indication du numéro de la ligne sélectionnée
        Variables.NumLigSel = e.RowIndex
        '==============================================
        ' Indication du numéro de la ligne sélectionnée
        Variables.TransNumLigne = DataGridView1.Item(0, e.RowIndex).Value
        '==============================================
        Information.ShowDialog()
    End Sub

    ' ==============================================================================================================================================
    ' Chargement de la base au démarrage
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Timer2.Enabled = False
        ' ==================================
        ' Recherche des fichiers de Base
        ChargeurBase.ListBox1.Items.Clear()
        For Each FichiersBase As String In My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.CurrentDirectory, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
            ' ==================================
            ' Mise en forme des noms des fichiers trouvés
            FichiersBase = "   " & Strings.Right(FichiersBase, Len(FichiersBase) - Strings.InStrRev(FichiersBase, "\"))
            FichiersBase = Strings.Left(FichiersBase, Len(FichiersBase) - 4)
            ' ==================================
            ' Rangement des noms de fichiers dans la liste
            ChargeurBase.ListBox1.Items.Add(FichiersBase)
        Next
        ' ==================================
        ' Chargement de la base
        ChargeurBase.ShowDialog()
        ' ==================================
        ' Définition des variables
        Dim InfoFichier As FileInfo
        Dim DeltaDate As Integer
        Dim DateBase As DateTime
        Dim BaseExiste As String
        ' ==================================
        ' Récupération de la date de la base Fairplay
        InfoFichier = New FileInfo(Variables.Repertoire & Variables.NomFichierBase)
        ' ==================================
        ' Vérification de l'existence de la base Fairplay
        BaseExiste = InfoFichier.Exists
        ' ==================================
        ' Récupération des dates
        DateBase = InfoFichier.LastWriteTime.ToShortDateString
        DeltaDate = DateDiff(DateInterval.Day, DateBase, Now)
        ' ==================================
        ' Message d'information si la base a plus d'un mois
        If DeltaDate > 30 And BaseExiste = True And Strings.Left(ChargeurBase.TextBox1.Text, 12) = "Base chargée" Then
            MsgBox("La Base chargée a  " & DeltaDate & "  jours !", MsgBoxStyle.Exclamation, " Ancienneté de la Base Fairplay")
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Form1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        ' ==================================
        Me.CenterToScreen()
    End Sub

    Private Sub DataGridView1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.MouseEnter
        ' ==================================
        Me.CenterToScreen()
    End Sub


    Private Sub DataGridView2_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView2.MouseEnter
        ' ==================================
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE DES CLICS SUR UNE CELLULE DU DATAGRIDVIEW2
    '=============================================================================================================================================
    ' Indication de la cellule cliqué par coloration de la cellule, de la colonne et de la ligne correspondantes
    Private Sub DataGridView2_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        ' ==================================
        ' Vérification si les titres Colonne et Ligne sont demandés
        If e.ColumnIndex = -1 And e.RowIndex = -1 Then
            MsgBox(" Intitulé Colonne = " & UCase(Variables.TitreColonne) & " " & vbCrLf & vbCrLf & " Intitulé Ligne = " & UCase(Variables.TitreLigne) & " ", , " Intitulés du Tableau")
            Exit Sub
        End If
        ' ==================================
        ' Ancienne couleur provient de "Tableau" à la création du tableau couleur du premier titre colonne
        ' ==================================
        ' Coloration des cellules en-têtes en couleur par défaut
        ' ==================================
        ' Vérification pour éviter une erreur si tableau redimensionné
        If DataGridView2.Rows.Count > Variables.AncienLig Then
            DataGridView2.Rows.Item(Variables.AncienLig).HeaderCell.Style.BackColor = Variables.AncienneCouleur
        End If
        ' ==================================
        ' Vérification pour éviter une erreur si tableau redimensionné
        If DataGridView2.Columns.Count > Variables.AncienCol Then
            DataGridView2.Columns.Item(Variables.AncienCol).HeaderCell.Style.BackColor = Variables.AncienneCouleur
        End If
        ' ==================================
        ' Récupération des coordonnées de la cellule cliquée
        If e.ColumnIndex <> -1 And e.RowIndex <> -1 Then
            ' ==================================
            ' Coloration de la cellule de tête de la ligne en rose
            DataGridView2.Rows.Item(e.RowIndex).HeaderCell.Style.BackColor = Color.Pink
            ' ==================================
            ' Coloration de la cellule de tête de la colonne en rose
            DataGridView2.Columns.Item(e.ColumnIndex).HeaderCell.Style.BackColor = Color.Pink
            ' ==================================
            ' Conservation de la coordonnée colonne pour le prochain traitement
            Variables.AncienCol = e.ColumnIndex
            ' ==================================
            ' Conservation de la coordonnée ligne pour le prochain traitement
            Variables.AncienLig = e.RowIndex
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Changement de sélection ligne colonne par double clic sur la line ou la colonne des en-têtes
    Private Sub DataGridView2_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellDoubleClick
        Dim i As Integer
        Dim NomCritereLigne As String = ""
        Dim NomCritereColonne As String = ""
        Dim Comparaison As String = ""
        Dim CompVal1 As String = ""
        Dim CompVal2 As String = ""
        Dim PremPlace As Integer
        Dim PremPlaceBase As Integer
        Dim InfoTexte As String
        Dim Signe As String = ""
        ' ==============================================================================================================================================
        ' Basculement en sélection des colonnes
        If e.ColumnIndex <> -1 And e.RowIndex = -1 Then
            DataGridView2.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect
            Exit Sub
        End If
        ' ==================================
        ' Basculement en sélection des lignes
        If e.ColumnIndex = -1 And e.RowIndex <> -1 Then
            DataGridView2.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect
            Exit Sub
        End If
        ' ==============================================================================================================================================
        ' Traitement si la cellule sélectionnée est différente de celle de l'intersection des totaux
        If e.ColumnIndex <> DataGridView2.Columns.Count - 1 Or e.RowIndex <> DataGridView2.Rows.Count - 1 Then

            ' ==================================
            ' Vérification que la cellule sélectionnée n'est pas vide
            If DataGridView2.Item(e.ColumnIndex, e.RowIndex).ToString <> "" Then

                ' ==============================================================================================================================================
                ' Effacement des inscriptions d'une demande antérieure sur la base dans l'extracteur
                If Variables.TabTraitBase(1, 5) = "1" Then Extracteur.TextBox1.Text = ""
                If Variables.TabTraitBase(2, 5) = "1" Then Extracteur.TextBox2.Text = ""
                If Variables.TabTraitBase(3, 5) = "1" Then Extracteur.TextBox3.Text = ""
                If Variables.TabTraitBase(4, 5) = "1" Then Extracteur.TextBox4.Text = ""
                If Variables.TabTraitBase(5, 5) = "1" Then Extracteur.TextBox5.Text = ""
                If Variables.TabTraitBase(6, 5) = "1" Then Extracteur.TextBox6.Text = ""
                If Variables.TabTraitBase(7, 5) = "1" Then Extracteur.TextBox7.Text = ""
                If Variables.TabTraitBase(8, 5) = "1" Then Extracteur.TextBox8.Text = ""
                If Variables.TabTraitBase(9, 5) = "1" Then Extracteur.TextBox9.Text = ""
                If Variables.TabTraitBase(10, 5) = "1" Then Extracteur.TextBox10.Text = ""
                If Variables.TabTraitBase(11, 5) = "1" Then Extracteur.TextBox11.Text = ""
                If Variables.TabTraitBase(12, 5) = "1" Then Extracteur.TextBox12.Text = ""
                If Variables.TabTraitBase(13, 5) = "1" Then Extracteur.TextBox13.Text = ""
                If Variables.TabTraitBase(14, 5) = "1" Then Extracteur.TextBox14.Text = ""
                If Variables.TabTraitBase(15, 5) = "1" Then Extracteur.TextBox15.Text = ""
                If Variables.TabTraitBase(16, 5) = "1" Then Extracteur.TextBox16.Text = ""
                If Variables.TabTraitBase(17, 5) = "1" Then Extracteur.TextBox17.Text = ""
                If Variables.TabTraitBase(18, 5) = "1" Then Extracteur.TextBox18.Text = ""
                If Variables.TabTraitBase(19, 5) = "1" Then Extracteur.TextBox19.Text = ""

                ' ==================================
                ' Effacement des inscriptions d'une demande antérieure sur la base dans le tableau de la base
                For i = 1 To 19
                    If Variables.TabTraitBase(i, 5) = "1" Then
                        Variables.TabTraitBase(i, 0) = ""
                        Variables.TabTraitBase(i, 1) = ""
                        Variables.TabTraitBase(i, 2) = ""
                        Variables.TabTraitBase(i, 3) = ""
                        Variables.TabTraitBase(i, 4) = ""
                        Variables.TabTraitBase(i, 5) = ""
                    End If
                Next

                ' ==================================
                ' Vérification si des inscriptions existent déjà dans l'extracteur sur la base
                For PremPlaceBase = 1 To 19
                    ' ==================================
                    ' Exploration du tableau de la base pour trouver la première place utilisable
                    If Variables.TabTraitBase(PremPlaceBase, 1) = "" Then
                        Exit For
                    End If
                Next

                ' ==============================================================================================================================================
                ' Effacement des inscriptions d'une demande antérieure sur le dernier résultat dans l'extracteur
                If Variables.TabTraitResul(1, 5) = "1" Then Extracteur.TextBox21.Text = ""
                If Variables.TabTraitResul(2, 5) = "1" Then Extracteur.TextBox22.Text = ""
                If Variables.TabTraitResul(3, 5) = "1" Then Extracteur.TextBox23.Text = ""
                If Variables.TabTraitResul(4, 5) = "1" Then Extracteur.TextBox24.Text = ""
                If Variables.TabTraitResul(5, 5) = "1" Then Extracteur.TextBox25.Text = ""
                If Variables.TabTraitResul(6, 5) = "1" Then Extracteur.TextBox26.Text = ""
                If Variables.TabTraitResul(7, 5) = "1" Then Extracteur.TextBox27.Text = ""
                If Variables.TabTraitResul(8, 5) = "1" Then Extracteur.TextBox28.Text = ""
                If Variables.TabTraitResul(9, 5) = "1" Then Extracteur.TextBox29.Text = ""
                If Variables.TabTraitResul(10, 5) = "1" Then Extracteur.TextBox30.Text = ""
                If Variables.TabTraitResul(11, 5) = "1" Then Extracteur.TextBox31.Text = ""
                If Variables.TabTraitResul(12, 5) = "1" Then Extracteur.TextBox32.Text = ""
                If Variables.TabTraitResul(13, 5) = "1" Then Extracteur.TextBox33.Text = ""
                If Variables.TabTraitResul(14, 5) = "1" Then Extracteur.TextBox34.Text = ""
                If Variables.TabTraitResul(15, 5) = "1" Then Extracteur.TextBox35.Text = ""
                If Variables.TabTraitResul(16, 5) = "1" Then Extracteur.TextBox36.Text = ""
                If Variables.TabTraitResul(17, 5) = "1" Then Extracteur.TextBox37.Text = ""
                If Variables.TabTraitResul(18, 5) = "1" Then Extracteur.TextBox38.Text = ""
                If Variables.TabTraitResul(19, 5) = "1" Then Extracteur.TextBox39.Text = ""

                ' ==================================
                ' Effacement des inscriptions d'une demande antérieure sur le dernier résultat dans le tableau du dernier résultat
                For i = 1 To 19
                    If Variables.TabTraitResul(i, 5) = "1" Then
                        Variables.TabTraitResul(i, 0) = ""
                        Variables.TabTraitResul(i, 1) = ""
                        Variables.TabTraitResul(i, 2) = ""
                        Variables.TabTraitResul(i, 3) = ""
                        Variables.TabTraitResul(i, 4) = ""
                        Variables.TabTraitResul(i, 5) = ""
                    End If
                Next

                ' ==================================
                ' Vérification si des inscriptions existent déjà dans l'extracteur sur le dernier résultat
                For PremPlace = 1 To 19
                    ' ==================================
                    ' Exploration du tableau du dernier résultat pour trouver la première place utilisable
                    If Variables.TabTraitResul(PremPlace, 1) = "" Then
                        Exit For
                    End If
                Next

                ' ==============================================================================================================================================
                ' Vérification que l'on peut inscrire les combinaisons dans l'extracteur
                If Extracteur.TextBox36.Text <> "" Or Extracteur.TextBox37.Text <> "" Or Extracteur.TextBox38.Text <> "" Or Extracteur.TextBox39.Text <> "" Then
                    ' ==================================
                    ' Information de non possibilité d'inscription des combinaisons dans l'extracteur
                    MsgBox("Je ne peux pas inscrire les combinaisons dans l'extracteur !", MsgBoxStyle.Exclamation, " Action de l'utilisateur requise")
                    Exit Sub
                End If

                ' ==============================================================================================================================================
                ' Traitement si pas de sélection sur la colonne des totaux
                If e.ColumnIndex <> DataGridView2.Columns.Count - 1 Then

                    ' ==============================================================================================================================================
                    ' Traitement d'une colonne alphabétique
                    ' ==============================================================================================================================================
                    If Variables.NumCritereColonne = 3 Or Variables.NumCritereColonne = 4 Or Variables.NumCritereColonne = 6 Or Variables.NumCritereColonne = 41 Then
                        ' ==================================
                        ' Recherche du critère de colonne
                        If Variables.NumCritereColonne = 3 Then NomCritereColonne = "Code état du navire"
                        If Variables.NumCritereColonne = 4 Then NomCritereColonne = "Classe du navire"
                        If Variables.NumCritereColonne = 6 Then NomCritereColonne = "Pavillon du navire"
                        If Variables.NumCritereColonne = 41 Then NomCritereColonne = "Type du navire"
                        ' ==================================
                        ' Traitement de la comparaison
                        Comparaison = DataGridView2.Columns.Item(e.ColumnIndex).HeaderCell.Value
                        ' ==================================
                        ' Établissement du texte de combinaison sur le dernier résultat
                        InfoTexte = " Je prends  " & NomCritereColonne & "  =  " & Comparaison
                        ' ==================================
                        ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                        If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                        If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                        If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                        If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                        If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                        If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                        If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                        If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                        If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                        If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                        If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                        If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                        If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                        If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                        If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                        If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                        If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                        If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                        If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                        ' ==================================
                        ' Rangement dans le tableau sur le dernier résultat
                        Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                        Variables.TabTraitResul(PremPlace, 1) = NomCritereColonne
                        Variables.TabTraitResul(PremPlace, 2) = "="
                        Variables.TabTraitResul(PremPlace, 3) = Comparaison
                        Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereColonne
                        Variables.TabTraitResul(PremPlace, 5) = "1"
                        ' ==================================
                        ' Pointage sur la ligne d'inscription suivante
                        PremPlace = PremPlace + 1
                    End If

                    ' ==============================================================================================================================================
                    ' Traitement d'une colonne date
                    ' ==============================================================================================================================================
                    If Variables.NumCritereColonne = 8 Or Variables.NumCritereColonne = 12 Or Variables.NumCritereColonne = 18 Then

                        ' ==================================
                        ' Recherche du critère de colonne
                        If Variables.NumCritereColonne = 8 Then NomCritereColonne = "Date d'achat"
                        If Variables.NumCritereColonne = 12 Then NomCritereColonne = "Date de livraison"
                        If Variables.NumCritereColonne = 18 Then NomCritereColonne = "Date de perte ou de destruction"
                        ' ==================================
                        ' Traitement de la comparaison et des 4 possibilités
                        Comparaison = DataGridView2.Columns.Item(e.ColumnIndex).HeaderCell.Value
                        If InStr(Comparaison, "Dates") > 0 Then
                            Signe = "="
                            CompVal1 = "VIDE"
                        End If
                        If InStr(Comparaison, "Inférieures") > 0 Then
                            Signe = "<="
                            CompVal1 = Strings.Right(Comparaison, 10)
                        End If
                        If InStr(Comparaison, "Du") > 0 Then
                            Signe = "=>"
                            CompVal1 = Strings.Mid(Comparaison, 4, 10)
                            CompVal2 = Strings.Right(Comparaison, 10)
                        End If
                        If InStr(Comparaison, "Supérieures") > 0 Then
                            Signe = "=>"
                            CompVal1 = Strings.Right(Comparaison, 10)
                        End If

                        ' ==================================
                        ' Établissement du texte de combinaison sur le dernier résultat
                        InfoTexte = " Je prends  " & NomCritereColonne & "  " & Signe & "  " & CompVal1
                        ' ==================================
                        ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                        If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                        If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                        If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                        If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                        If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                        If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                        If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                        If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                        If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                        If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                        If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                        If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                        If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                        If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                        If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                        If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                        If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                        If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                        If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                        ' ==================================
                        ' Rangement dans le tableau sur le dernier résultat
                        Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                        Variables.TabTraitResul(PremPlace, 1) = NomCritereColonne
                        Variables.TabTraitResul(PremPlace, 2) = Signe
                        Variables.TabTraitResul(PremPlace, 3) = CompVal1
                        Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereColonne
                        Variables.TabTraitResul(PremPlace, 5) = "1"
                        ' ==================================
                        ' Pointage sur la ligne d'inscription suivante
                        PremPlace = PremPlace + 1

                        ' ==================================
                        ' Traitement s'il faut retirer les cellules vides sur la base
                        If CompVal1 <> "VIDE" Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur la base
                            InfoTexte = " Je retire  " & NomCritereColonne & "  =  VIDE"
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur la base
                            If PremPlaceBase = 1 Then Extracteur.TextBox1.Text = InfoTexte
                            If PremPlaceBase = 2 Then Extracteur.TextBox2.Text = InfoTexte
                            If PremPlaceBase = 3 Then Extracteur.TextBox3.Text = InfoTexte
                            If PremPlaceBase = 4 Then Extracteur.TextBox4.Text = InfoTexte
                            If PremPlaceBase = 5 Then Extracteur.TextBox5.Text = InfoTexte
                            If PremPlaceBase = 6 Then Extracteur.TextBox6.Text = InfoTexte
                            If PremPlaceBase = 7 Then Extracteur.TextBox7.Text = InfoTexte
                            If PremPlaceBase = 8 Then Extracteur.TextBox8.Text = InfoTexte
                            If PremPlaceBase = 9 Then Extracteur.TextBox9.Text = InfoTexte
                            If PremPlaceBase = 10 Then Extracteur.TextBox10.Text = InfoTexte
                            If PremPlaceBase = 11 Then Extracteur.TextBox11.Text = InfoTexte
                            If PremPlaceBase = 12 Then Extracteur.TextBox12.Text = InfoTexte
                            If PremPlaceBase = 13 Then Extracteur.TextBox13.Text = InfoTexte
                            If PremPlaceBase = 14 Then Extracteur.TextBox14.Text = InfoTexte
                            If PremPlaceBase = 15 Then Extracteur.TextBox15.Text = InfoTexte
                            If PremPlaceBase = 16 Then Extracteur.TextBox16.Text = InfoTexte
                            If PremPlaceBase = 17 Then Extracteur.TextBox17.Text = InfoTexte
                            If PremPlaceBase = 18 Then Extracteur.TextBox18.Text = InfoTexte
                            If PremPlaceBase = 19 Then Extracteur.TextBox19.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur la base
                            Variables.TabTraitBase(PremPlaceBase, 0) = "Je retire"
                            Variables.TabTraitBase(PremPlaceBase, 1) = NomCritereColonne
                            Variables.TabTraitBase(PremPlaceBase, 2) = "="
                            Variables.TabTraitBase(PremPlaceBase, 3) = "VIDE"
                            Variables.TabTraitBase(PremPlaceBase, 4) = Variables.NumCritereColonne
                            Variables.TabTraitBase(PremPlaceBase, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante sur la base
                            PremPlaceBase = PremPlaceBase + 1
                        End If

                        ' ==================================
                        ' Traitement s'il y a une deuxième valeur de comparaison
                        If InStr(Comparaison, "Du") > 0 Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur le dernier résultat
                            InfoTexte = " Je prends  " & NomCritereColonne & "  <=  " & CompVal2
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                            If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                            If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                            If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                            If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                            If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                            If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                            If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                            If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                            If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                            If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                            If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                            If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                            If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                            If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                            If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                            If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                            If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                            If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                            If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur le dernier résultat
                            Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                            Variables.TabTraitResul(PremPlace, 1) = NomCritereColonne
                            Variables.TabTraitResul(PremPlace, 2) = "<="
                            Variables.TabTraitResul(PremPlace, 3) = CompVal2
                            Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereColonne
                            Variables.TabTraitResul(PremPlace, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante
                            PremPlace = PremPlace + 1
                        End If
                    End If

                    ' ==============================================================================================================================================
                    ' Traitement d'une colonne numérique
                    ' ==============================================================================================================================================
                    If Variables.NumCritereColonne <> 3 And Variables.NumCritereColonne <> 4 And Variables.NumCritereColonne <> 6 And Variables.NumCritereColonne <> 41 And Variables.NumCritereColonne <> 8 And Variables.NumCritereColonne <> 12 And Variables.NumCritereColonne <> 18 Then

                        ' ==================================
                        ' Recherche du critère de colonne
                        If Variables.NumCritereColonne = 20 Then NomCritereColonne = "Capacité en balles"
                        If Variables.NumCritereColonne = 19 Then NomCritereColonne = "Capacité en céréales"
                        If Variables.NumCritereColonne = 23 Then NomCritereColonne = "Capacité en conteneurs"
                        If Variables.NumCritereColonne = 22 Then NomCritereColonne = "Capacité en conteneurs frigorifiques"
                        If Variables.NumCritereColonne = 21 Then NomCritereColonne = "Capacité en liquide"
                        If Variables.NumCritereColonne = 29 Then NomCritereColonne = "Capacité en minerai"
                        If Variables.NumCritereColonne = 42 Then NomCritereColonne = "Hauteur"
                        If Variables.NumCritereColonne = 14 Then NomCritereColonne = "Largeur"
                        If Variables.NumCritereColonne = 13 Then NomCritereColonne = "Longueur"
                        If Variables.NumCritereColonne = 34 Then NomCritereColonne = "Nombre de barges amarrables"
                        If Variables.NumCritereColonne = 26 Then NomCritereColonne = "Nombre de cales"
                        If Variables.NumCritereColonne = 25 Then NomCritereColonne = "Nombre de conteneurs"
                        If Variables.NumCritereColonne = 27 Then NomCritereColonne = "Nombre d'écoutilles"
                        If Variables.NumCritereColonne = 32 Then NomCritereColonne = "Nombre de passagers"
                        If Variables.NumCritereColonne = 35 Then NomCritereColonne = "Nombre de ponts"
                        If Variables.NumCritereColonne = 31 Then NomCritereColonne = "Nombre de portes latérales droites"
                        If Variables.NumCritereColonne = 30 Then NomCritereColonne = "Nombre de portes latérales gauches"
                        If Variables.NumCritereColonne = 38 Then NomCritereColonne = "Nombre de remorques"
                        If Variables.NumCritereColonne = 28 Then NomCritereColonne = "Nombre de réservoirs"
                        If Variables.NumCritereColonne = 37 Then NomCritereColonne = "Nombre de voitures"
                        If Variables.NumCritereColonne = 40 Then NomCritereColonne = "Nombre de wagons"
                        If Variables.NumCritereColonne = 1 Then NomCritereColonne = "Numéro FAIRPLAY"
                        If Variables.NumCritereColonne = 11 Then NomCritereColonne = "Numéro LLOYDS"
                        If Variables.NumCritereColonne = 9 Then NomCritereColonne = "Port en lourd"
                        If Variables.NumCritereColonne = 16 Then NomCritereColonne = "Profondeur"
                        If Variables.NumCritereColonne = 15 Then NomCritereColonne = "Tirant d'eau"
                        If Variables.NumCritereColonne = 10 Then NomCritereColonne = "Tonnage"
                        If Variables.NumCritereColonne = 17 Then NomCritereColonne = "Vitesse"

                        ' ==================================
                        ' Traitement de la comparaison et des 4 possibilités
                        Comparaison = DataGridView2.Columns.Item(e.ColumnIndex).HeaderCell.Value
                        If InStr(Comparaison, "Valeurs") > 0 Then
                            Signe = "="
                            CompVal1 = "VIDE"
                        End If
                        If InStr(Comparaison, "Inférieures") > 0 Then
                            Signe = "<="
                            i = InStr(Comparaison, "À")
                            CompVal1 = Strings.Right(Comparaison, Len(Comparaison) - i - 1)
                        End If
                        If InStr(Comparaison, "De") > 0 Then
                            Signe = "=>"
                            i = InStr(Comparaison, "À")
                            CompVal1 = Strings.Mid(Comparaison, 4, i - 6)
                            CompVal2 = Strings.Right(Comparaison, Len(Comparaison) - i - 1)
                        End If
                        If InStr(Comparaison, "Supérieures") > 0 Then
                            Signe = "=>"
                            i = InStr(Comparaison, "À")
                            CompVal1 = Strings.Right(Comparaison, Len(Comparaison) - i - 1)
                        End If

                        ' ==================================
                        ' Établissement du texte de combinaison sur le dernier résultat
                        InfoTexte = " Je prends  " & NomCritereColonne & "  " & Signe & "  " & CompVal1
                        ' ==================================
                        ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                        If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                        If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                        If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                        If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                        If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                        If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                        If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                        If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                        If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                        If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                        If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                        If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                        If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                        If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                        If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                        If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                        If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                        If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                        If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                        ' ==================================
                        ' Rangement dans le tableau sur le dernier résultat
                        Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                        Variables.TabTraitResul(PremPlace, 1) = NomCritereColonne
                        Variables.TabTraitResul(PremPlace, 2) = Signe
                        Variables.TabTraitResul(PremPlace, 3) = CompVal1
                        Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereColonne
                        Variables.TabTraitResul(PremPlace, 5) = "1"
                        ' ==================================
                        ' Pointage sur la ligne d'inscription suivante
                        PremPlace = PremPlace + 1

                        ' ==================================
                        ' Traitement s'il faut retirer les cellules vides sur la base
                        If CompVal1 <> "VIDE" Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur la base
                            InfoTexte = " Je retire  " & NomCritereColonne & "  =  VIDE"
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur la base
                            If PremPlaceBase = 1 Then Extracteur.TextBox1.Text = InfoTexte
                            If PremPlaceBase = 2 Then Extracteur.TextBox2.Text = InfoTexte
                            If PremPlaceBase = 3 Then Extracteur.TextBox3.Text = InfoTexte
                            If PremPlaceBase = 4 Then Extracteur.TextBox4.Text = InfoTexte
                            If PremPlaceBase = 5 Then Extracteur.TextBox5.Text = InfoTexte
                            If PremPlaceBase = 6 Then Extracteur.TextBox6.Text = InfoTexte
                            If PremPlaceBase = 7 Then Extracteur.TextBox7.Text = InfoTexte
                            If PremPlaceBase = 8 Then Extracteur.TextBox8.Text = InfoTexte
                            If PremPlaceBase = 9 Then Extracteur.TextBox9.Text = InfoTexte
                            If PremPlaceBase = 10 Then Extracteur.TextBox10.Text = InfoTexte
                            If PremPlaceBase = 11 Then Extracteur.TextBox11.Text = InfoTexte
                            If PremPlaceBase = 12 Then Extracteur.TextBox12.Text = InfoTexte
                            If PremPlaceBase = 13 Then Extracteur.TextBox13.Text = InfoTexte
                            If PremPlaceBase = 14 Then Extracteur.TextBox14.Text = InfoTexte
                            If PremPlaceBase = 15 Then Extracteur.TextBox15.Text = InfoTexte
                            If PremPlaceBase = 16 Then Extracteur.TextBox16.Text = InfoTexte
                            If PremPlaceBase = 17 Then Extracteur.TextBox17.Text = InfoTexte
                            If PremPlaceBase = 18 Then Extracteur.TextBox18.Text = InfoTexte
                            If PremPlaceBase = 19 Then Extracteur.TextBox19.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur la base
                            Variables.TabTraitBase(PremPlaceBase, 0) = "Je retire"
                            Variables.TabTraitBase(PremPlaceBase, 1) = NomCritereColonne
                            Variables.TabTraitBase(PremPlaceBase, 2) = "="
                            Variables.TabTraitBase(PremPlaceBase, 3) = "VIDE"
                            Variables.TabTraitBase(PremPlaceBase, 4) = Variables.NumCritereColonne
                            Variables.TabTraitBase(PremPlaceBase, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante sur la base
                            PremPlaceBase = PremPlaceBase + 1
                        End If

                        ' ==================================
                        ' Traitement s'il y a une deuxième valeur de comparaison
                        If InStr(Comparaison, "De") > 0 Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur le dernier résultat
                            InfoTexte = " Je prends  " & NomCritereColonne & "  <=  " & CompVal2
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                            If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                            If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                            If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                            If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                            If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                            If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                            If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                            If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                            If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                            If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                            If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                            If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                            If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                            If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                            If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                            If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                            If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                            If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                            If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur le dernier résultat
                            Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                            Variables.TabTraitResul(PremPlace, 1) = NomCritereColonne
                            Variables.TabTraitResul(PremPlace, 2) = "<="
                            Variables.TabTraitResul(PremPlace, 3) = CompVal2
                            Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereColonne
                            Variables.TabTraitResul(PremPlace, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante
                            PremPlace = PremPlace + 1
                        End If
                    End If

                End If

                ' ==============================================================================================================================================
                ' Sortie si le nom de critère colonne est le même que le critère ligne et la sélection pas sur la colonne des totaux
                ' ==============================================================================================================================================
                If Variables.NumCritereColonne = Variables.NumCritereLigne And e.ColumnIndex <> DataGridView2.Columns.Count - 1 Then
                    ' ==================================
                    ' Lancement de l'extracteur
                    Extracteur.ShowDialog()
                    ' ==================================
                    Exit Sub
                End If

                ' ==============================================================================================================================================
                ' Traitement si pas de sélection sur la ligne des totaux
                If e.RowIndex <> DataGridView2.Rows.Count - 1 Then
                    ' ==============================================================================================================================================
                    ' Traitement d'une ligne alphabétique
                    ' ==============================================================================================================================================
                    If Variables.NumCritereLigne = 3 Or Variables.NumCritereLigne = 4 Or Variables.NumCritereLigne = 6 Or Variables.NumCritereLigne = 41 Then
                        ' ==================================
                        ' Recherche du critère de ligne
                        If Variables.NumCritereLigne = 3 Then NomCritereLigne = "Code état du navire"
                        If Variables.NumCritereLigne = 4 Then NomCritereLigne = "Classe du navire"
                        If Variables.NumCritereLigne = 6 Then NomCritereLigne = "Pavillon du navire"
                        If Variables.NumCritereLigne = 41 Then NomCritereLigne = "Type du navire"
                        ' ==================================
                        ' Traitement de la comparaison
                        Comparaison = DataGridView2.Rows.Item(e.RowIndex).HeaderCell.Value
                        ' ==================================
                        ' Établissement du texte de combinaison sur le dernier résultat
                        InfoTexte = " Je prends  " & NomCritereLigne & "  =  " & Comparaison
                        ' ==================================
                        ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                        If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                        If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                        If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                        If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                        If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                        If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                        If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                        If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                        If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                        If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                        If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                        If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                        If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                        If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                        If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                        If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                        If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                        If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                        If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                        ' ==================================
                        ' Rangement dans le tableau sur le dernier résultat
                        Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                        Variables.TabTraitResul(PremPlace, 1) = NomCritereLigne
                        Variables.TabTraitResul(PremPlace, 2) = "="
                        Variables.TabTraitResul(PremPlace, 3) = Comparaison
                        Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereLigne
                        Variables.TabTraitResul(PremPlace, 5) = "1"
                        ' ==================================
                        ' Pointage sur la ligne d'inscription suivante
                        PremPlace = PremPlace + 1
                    End If

                    ' ==============================================================================================================================================
                    ' Traitement d'une ligne date
                    ' ==============================================================================================================================================
                    If Variables.NumCritereLigne = 8 Or Variables.NumCritereLigne = 12 Or Variables.NumCritereLigne = 18 Then

                        ' ==================================
                        ' Recherche du critère de ligne
                        If Variables.NumCritereLigne = 8 Then NomCritereLigne = "Date d'achat"
                        If Variables.NumCritereLigne = 12 Then NomCritereLigne = "Date de livraison"
                        If Variables.NumCritereLigne = 18 Then NomCritereLigne = "Date de perte ou de destruction"
                        ' ==================================
                        ' Traitement de la comparaison et des 4 possibilités
                        Comparaison = DataGridView2.Rows.Item(e.RowIndex).HeaderCell.Value
                        If InStr(Comparaison, "Dates") > 0 Then
                            Signe = "="
                            CompVal1 = "VIDE"
                        End If
                        If InStr(Comparaison, "Inférieures") > 0 Then
                            Signe = "<="
                            CompVal1 = Strings.Right(Comparaison, 10)
                        End If
                        If InStr(Comparaison, "Du") > 0 Then
                            Signe = "=>"
                            CompVal1 = Strings.Mid(Comparaison, 4, 10)
                            CompVal2 = Strings.Right(Comparaison, 10)
                        End If
                        If InStr(Comparaison, "Supérieures") > 0 Then
                            Signe = "=>"
                            CompVal1 = Strings.Right(Comparaison, 10)
                        End If

                        ' ==================================
                        ' Établissement du texte de combinaison sur le dernier résultat
                        InfoTexte = " Je prends  " & NomCritereLigne & "  " & Signe & "  " & CompVal1
                        ' ==================================
                        ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                        If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                        If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                        If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                        If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                        If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                        If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                        If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                        If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                        If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                        If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                        If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                        If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                        If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                        If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                        If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                        If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                        If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                        If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                        If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                        ' ==================================
                        ' Rangement dans le tableau sur le dernier résultat
                        Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                        Variables.TabTraitResul(PremPlace, 1) = NomCritereLigne
                        Variables.TabTraitResul(PremPlace, 2) = Signe
                        Variables.TabTraitResul(PremPlace, 3) = CompVal1
                        Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereLigne
                        Variables.TabTraitResul(PremPlace, 5) = "1"
                        ' ==================================
                        ' Pointage sur la ligne d'inscription suivante
                        PremPlace = PremPlace + 1

                        ' ==================================
                        ' Traitement s'il faut retirer les cellules vides sur la base
                        If CompVal1 <> "VIDE" Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur la base
                            InfoTexte = " Je retire  " & NomCritereLigne & "  =  VIDE"
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur la base
                            If PremPlaceBase = 1 Then Extracteur.TextBox1.Text = InfoTexte
                            If PremPlaceBase = 2 Then Extracteur.TextBox2.Text = InfoTexte
                            If PremPlaceBase = 3 Then Extracteur.TextBox3.Text = InfoTexte
                            If PremPlaceBase = 4 Then Extracteur.TextBox4.Text = InfoTexte
                            If PremPlaceBase = 5 Then Extracteur.TextBox5.Text = InfoTexte
                            If PremPlaceBase = 6 Then Extracteur.TextBox6.Text = InfoTexte
                            If PremPlaceBase = 7 Then Extracteur.TextBox7.Text = InfoTexte
                            If PremPlaceBase = 8 Then Extracteur.TextBox8.Text = InfoTexte
                            If PremPlaceBase = 9 Then Extracteur.TextBox9.Text = InfoTexte
                            If PremPlaceBase = 10 Then Extracteur.TextBox10.Text = InfoTexte
                            If PremPlaceBase = 11 Then Extracteur.TextBox11.Text = InfoTexte
                            If PremPlaceBase = 12 Then Extracteur.TextBox12.Text = InfoTexte
                            If PremPlaceBase = 13 Then Extracteur.TextBox13.Text = InfoTexte
                            If PremPlaceBase = 14 Then Extracteur.TextBox14.Text = InfoTexte
                            If PremPlaceBase = 15 Then Extracteur.TextBox15.Text = InfoTexte
                            If PremPlaceBase = 16 Then Extracteur.TextBox16.Text = InfoTexte
                            If PremPlaceBase = 17 Then Extracteur.TextBox17.Text = InfoTexte
                            If PremPlaceBase = 18 Then Extracteur.TextBox18.Text = InfoTexte
                            If PremPlaceBase = 19 Then Extracteur.TextBox19.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur la base
                            Variables.TabTraitBase(PremPlaceBase, 0) = "Je retire"
                            Variables.TabTraitBase(PremPlaceBase, 1) = NomCritereLigne
                            Variables.TabTraitBase(PremPlaceBase, 2) = "="
                            Variables.TabTraitBase(PremPlaceBase, 3) = "VIDE"
                            Variables.TabTraitBase(PremPlaceBase, 4) = Variables.NumCritereLigne
                            Variables.TabTraitBase(PremPlaceBase, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante sur la base
                            PremPlaceBase = PremPlaceBase + 1
                        End If

                        ' ==================================
                        ' Traitement s'il y a une deuxième valeur de comparaison
                        If InStr(Comparaison, "Du") > 0 Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur le dernier résultat
                            InfoTexte = " Je prends  " & NomCritereLigne & "  <=  " & CompVal2
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                            If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                            If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                            If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                            If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                            If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                            If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                            If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                            If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                            If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                            If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                            If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                            If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                            If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                            If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                            If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                            If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                            If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                            If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                            If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur le dernier résultat
                            Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                            Variables.TabTraitResul(PremPlace, 1) = NomCritereLigne
                            Variables.TabTraitResul(PremPlace, 2) = "<="
                            Variables.TabTraitResul(PremPlace, 3) = CompVal2
                            Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereLigne
                            Variables.TabTraitResul(PremPlace, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante
                            PremPlace = PremPlace + 1
                        End If
                    End If

                    ' ==============================================================================================================================================
                    ' Traitement d'une ligne numérique
                    ' ==============================================================================================================================================
                    If Variables.NumCritereLigne <> 3 And Variables.NumCritereLigne <> 4 And Variables.NumCritereLigne <> 6 And Variables.NumCritereLigne <> 41 And Variables.NumCritereLigne <> 8 And Variables.NumCritereLigne <> 12 And Variables.NumCritereLigne <> 18 Then
                        ' ==================================
                        ' Recherche du critère de ligne
                        If Variables.NumCritereLigne = 20 Then NomCritereLigne = "Capacité en balles"
                        If Variables.NumCritereLigne = 19 Then NomCritereLigne = "Capacité en céréales"
                        If Variables.NumCritereLigne = 23 Then NomCritereLigne = "Capacité en conteneurs"
                        If Variables.NumCritereLigne = 22 Then NomCritereLigne = "Capacité en conteneurs frigorifiques"
                        If Variables.NumCritereLigne = 21 Then NomCritereLigne = "Capacité en liquide"
                        If Variables.NumCritereLigne = 29 Then NomCritereLigne = "Capacité en minerai"
                        If Variables.NumCritereLigne = 42 Then NomCritereLigne = "Hauteur"
                        If Variables.NumCritereLigne = 14 Then NomCritereLigne = "Largeur"
                        If Variables.NumCritereLigne = 13 Then NomCritereLigne = "Longueur"
                        If Variables.NumCritereLigne = 34 Then NomCritereLigne = "Nombre de barges amarrables"
                        If Variables.NumCritereLigne = 26 Then NomCritereLigne = "Nombre de cales"
                        If Variables.NumCritereLigne = 25 Then NomCritereLigne = "Nombre de conteneurs"
                        If Variables.NumCritereLigne = 27 Then NomCritereLigne = "Nombre d'écoutilles"
                        If Variables.NumCritereLigne = 32 Then NomCritereLigne = "Nombre de passagers"
                        If Variables.NumCritereLigne = 35 Then NomCritereLigne = "Nombre de ponts"
                        If Variables.NumCritereLigne = 31 Then NomCritereLigne = "Nombre de portes latérales droites"
                        If Variables.NumCritereLigne = 30 Then NomCritereLigne = "Nombre de portes latérales gauches"
                        If Variables.NumCritereLigne = 38 Then NomCritereLigne = "Nombre de remorques"
                        If Variables.NumCritereLigne = 28 Then NomCritereLigne = "Nombre de réservoirs"
                        If Variables.NumCritereLigne = 37 Then NomCritereLigne = "Nombre de voitures"
                        If Variables.NumCritereLigne = 40 Then NomCritereLigne = "Nombre de wagons"
                        If Variables.NumCritereLigne = 1 Then NomCritereLigne = "Numéro FAIRPLAY"
                        If Variables.NumCritereLigne = 11 Then NomCritereLigne = "Numéro LLOYDS"
                        If Variables.NumCritereLigne = 9 Then NomCritereLigne = "Port en lourd"
                        If Variables.NumCritereLigne = 16 Then NomCritereLigne = "Profondeur"
                        If Variables.NumCritereLigne = 15 Then NomCritereLigne = "Tirant d'eau"
                        If Variables.NumCritereLigne = 10 Then NomCritereLigne = "Tonnage"
                        If Variables.NumCritereLigne = 17 Then NomCritereLigne = "Vitesse"

                        ' ==================================
                        ' Traitement de la comparaison et des 4 possibilités
                        Comparaison = DataGridView2.Rows.Item(e.RowIndex).HeaderCell.Value
                        If InStr(Comparaison, "Valeurs") > 0 Then
                            Signe = "="
                            CompVal1 = "VIDE"
                        End If
                        If InStr(Comparaison, "Inférieures") > 0 Then
                            Signe = "<="
                            i = InStr(Comparaison, "À")
                            CompVal1 = Strings.Right(Comparaison, Len(Comparaison) - i - 1)
                        End If
                        If InStr(Comparaison, "De") > 0 Then
                            Signe = "=>"
                            i = InStr(Comparaison, "À")
                            CompVal1 = Strings.Mid(Comparaison, 4, i - 5)
                            CompVal2 = Strings.Right(Comparaison, Len(Comparaison) - i - 1)
                        End If
                        If InStr(Comparaison, "Supérieures") > 0 Then
                            Signe = "=>"
                            i = InStr(Comparaison, "À")
                            CompVal1 = Strings.Right(Comparaison, Len(Comparaison) - i - 1)
                        End If

                        ' ==================================
                        ' Établissement du texte de combinaison sur le dernier résultat
                        InfoTexte = " Je prends  " & NomCritereLigne & "  " & Signe & "  " & CompVal1
                        ' ==================================
                        ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                        If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                        If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                        If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                        If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                        If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                        If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                        If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                        If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                        If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                        If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                        If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                        If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                        If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                        If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                        If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                        If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                        If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                        If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                        If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                        ' ==================================
                        ' Rangement dans le tableau sur le dernier résultat
                        Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                        Variables.TabTraitResul(PremPlace, 1) = NomCritereLigne
                        Variables.TabTraitResul(PremPlace, 2) = Signe
                        Variables.TabTraitResul(PremPlace, 3) = CompVal1
                        Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereLigne
                        Variables.TabTraitResul(PremPlace, 5) = "1"
                        ' ==================================
                        ' Pointage sur la ligne d'inscription suivante
                        PremPlace = PremPlace + 1

                        ' ==================================
                        ' Traitement s'il faut retirer les cellules vides sur la base
                        If CompVal1 <> "VIDE" Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur la base
                            InfoTexte = " Je retire  " & NomCritereLigne & "  =  VIDE"
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur la base
                            If PremPlaceBase = 1 Then Extracteur.TextBox1.Text = InfoTexte
                            If PremPlaceBase = 2 Then Extracteur.TextBox2.Text = InfoTexte
                            If PremPlaceBase = 3 Then Extracteur.TextBox3.Text = InfoTexte
                            If PremPlaceBase = 4 Then Extracteur.TextBox4.Text = InfoTexte
                            If PremPlaceBase = 5 Then Extracteur.TextBox5.Text = InfoTexte
                            If PremPlaceBase = 6 Then Extracteur.TextBox6.Text = InfoTexte
                            If PremPlaceBase = 7 Then Extracteur.TextBox7.Text = InfoTexte
                            If PremPlaceBase = 8 Then Extracteur.TextBox8.Text = InfoTexte
                            If PremPlaceBase = 9 Then Extracteur.TextBox9.Text = InfoTexte
                            If PremPlaceBase = 10 Then Extracteur.TextBox10.Text = InfoTexte
                            If PremPlaceBase = 11 Then Extracteur.TextBox11.Text = InfoTexte
                            If PremPlaceBase = 12 Then Extracteur.TextBox12.Text = InfoTexte
                            If PremPlaceBase = 13 Then Extracteur.TextBox13.Text = InfoTexte
                            If PremPlaceBase = 14 Then Extracteur.TextBox14.Text = InfoTexte
                            If PremPlaceBase = 15 Then Extracteur.TextBox15.Text = InfoTexte
                            If PremPlaceBase = 16 Then Extracteur.TextBox16.Text = InfoTexte
                            If PremPlaceBase = 17 Then Extracteur.TextBox17.Text = InfoTexte
                            If PremPlaceBase = 18 Then Extracteur.TextBox18.Text = InfoTexte
                            If PremPlaceBase = 19 Then Extracteur.TextBox19.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur la base
                            Variables.TabTraitBase(PremPlaceBase, 0) = "Je retire"
                            Variables.TabTraitBase(PremPlaceBase, 1) = NomCritereLigne
                            Variables.TabTraitBase(PremPlaceBase, 2) = "="
                            Variables.TabTraitBase(PremPlaceBase, 3) = "VIDE"
                            Variables.TabTraitBase(PremPlaceBase, 4) = Variables.NumCritereLigne
                            Variables.TabTraitBase(PremPlaceBase, 5) = "1"
                            ' ==================================
                            ' Pointage sur la ligne d'inscription suivante sur la base
                            PremPlaceBase = PremPlaceBase + 1
                        End If

                        ' ==================================
                        ' Traitement s'il y a une deuxième valeur de comparaison
                        If InStr(Comparaison, "De") > 0 Then
                            ' ==================================
                            ' Établissement du texte de combinaison sur le dernier résultat
                            InfoTexte = " Je prends  " & NomCritereLigne & "  <=  " & CompVal2
                            ' ==================================
                            ' Rangement dans le texte correspondant texte de combinaison sur le dernier résultat
                            If PremPlace = 1 Then Extracteur.TextBox21.Text = InfoTexte
                            If PremPlace = 2 Then Extracteur.TextBox22.Text = InfoTexte
                            If PremPlace = 3 Then Extracteur.TextBox23.Text = InfoTexte
                            If PremPlace = 4 Then Extracteur.TextBox24.Text = InfoTexte
                            If PremPlace = 5 Then Extracteur.TextBox25.Text = InfoTexte
                            If PremPlace = 6 Then Extracteur.TextBox26.Text = InfoTexte
                            If PremPlace = 7 Then Extracteur.TextBox27.Text = InfoTexte
                            If PremPlace = 8 Then Extracteur.TextBox28.Text = InfoTexte
                            If PremPlace = 9 Then Extracteur.TextBox29.Text = InfoTexte
                            If PremPlace = 10 Then Extracteur.TextBox30.Text = InfoTexte
                            If PremPlace = 11 Then Extracteur.TextBox31.Text = InfoTexte
                            If PremPlace = 12 Then Extracteur.TextBox32.Text = InfoTexte
                            If PremPlace = 13 Then Extracteur.TextBox33.Text = InfoTexte
                            If PremPlace = 14 Then Extracteur.TextBox34.Text = InfoTexte
                            If PremPlace = 15 Then Extracteur.TextBox35.Text = InfoTexte
                            If PremPlace = 16 Then Extracteur.TextBox36.Text = InfoTexte
                            If PremPlace = 17 Then Extracteur.TextBox37.Text = InfoTexte
                            If PremPlace = 18 Then Extracteur.TextBox38.Text = InfoTexte
                            If PremPlace = 19 Then Extracteur.TextBox39.Text = InfoTexte
                            ' ==================================
                            ' Rangement dans le tableau sur le dernier résultat
                            Variables.TabTraitResul(PremPlace, 0) = "Je prends"
                            Variables.TabTraitResul(PremPlace, 1) = NomCritereLigne
                            Variables.TabTraitResul(PremPlace, 2) = "<="
                            Variables.TabTraitResul(PremPlace, 3) = CompVal2
                            Variables.TabTraitResul(PremPlace, 4) = Variables.NumCritereLigne
                            Variables.TabTraitResul(PremPlace, 5) = "1"
                        End If
                    End If
                End If
            End If

            ' ==================================
            ' Lancement de l'extracteur
            Extracteur.ShowDialog()


        End If
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE DE MONTÉE ET DE DESCENTE DES DATAGRIDVIEW
    '=============================================================================================================================================
    ' Procédure des ascenseurs DataGridView 1 et 2
    Private Sub VisuToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisuToolStripMenuItem.Click
        ' ==================================
        ' Traitement si le Timer 3 est actif
        If Timer3.Enabled = True Then
            Timer3.Enabled = False
            Timer4.Enabled = True
            '==============================================
            ' Changement du titre du menu
            VisuToolStripMenuItem.Text = "   Visuel Extracteur   "
            Exit Sub
        End If
        ' ==================================
        ' Traitement si le Timer 4 est actif
        If Timer4.Enabled = True Then
            Timer3.Enabled = True
            Timer4.Enabled = False
            '==============================================
            ' Changement du titre du menu
            VisuToolStripMenuItem.Text = "   Visuel Tableau   "
            Exit Sub
        End If
        ' ==================================
        ' Traitement si le DataGridView1 est visible
        If DataGridView1.Top = 25 Then
            Timer3.Enabled = True
            Timer4.Enabled = False
            '==============================================
            ' Changement du titre du menu
            VisuToolStripMenuItem.Text = "   Visuel Extracteur   "
            Exit Sub
        End If
        ' ==================================
        ' Traitement si le DataGridView2 est visible
        If DataGridView2.Top = 25 Then
            Timer3.Enabled = False
            Timer4.Enabled = True
            '==============================================
            ' Changement du titre du menu
            VisuToolStripMenuItem.Text = "   Visuel Tableau   "
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Montée du DataGrid1 et descente du DataGrid2
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        If DataGridView2.Top < 21 Then
            ' ==================================
            ' Déplacement
            DataGridView1.Top = DataGridView1.Top - 5
            DataGridView2.Top = DataGridView2.Top + 5
        Else
            ' ==================================
            ' Fin du déplacement
            Timer3.Enabled = False
            DataGridView1.Top = 25 - DataGridView1.Height
            DataGridView2.Top = 25
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Montée du DataGrid2 et descente du DataGrid1
    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        If DataGridView1.Top < 21 Then
            ' ==================================
            ' Déplacement
            DataGridView1.Top = DataGridView1.Top + 5
            DataGridView2.Top = DataGridView2.Top - 5
        Else
            ' ==================================
            ' Fin du déplacement
            Timer4.Enabled = False
            DataGridView1.Top = 25
            DataGridView2.Top = 25 - DataGridView1.Height
        End If
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE DE LA FEUILLE PARENT ET LA FERMETURE DU MASQUE AVEC APPARITION DU MOT "BONJOUR"
    '=============================================================================================================================================
    ' Procédure d'ouverture de la feuille
    Private Sub OuvertureFeuille()
        ' ==================================
        ' Affectation des variables largeur et hauteur
        Variables.Largeur = 1
        Variables.Hauteur = 1
        ' ==================================
        ' Affectation du pas de la largeur et de la hauteur
        Variables.PasLargeur = Variables.FormLargeur / 100 : Variables.PasHauteur = Variables.FormHauteur / 100
        ' ==================================
        ' Initialiation des variables couleurs
        Variables.CoulRouge = 0 : Variables.CoulVert = 0 : Variables.CoulBleu = 0
        ' ==================================
        ' Initialisation des variables d'incrémentation de pas avec la couleur à obtenir
        Variables.PasRouge = 230 / 100 : Variables.PasVert = 230 / 100 : Variables.PasBleu = 230 / 100
        ' ==================================
        ' Lancement de la procédure
        Timer11.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'apparition du masque pour la feuille parent
    Private Sub Timer11_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer11.Tick
        ' ==================================
        ' Incrementation des variables largeur et la hauteur avec le pas défini
        Variables.Largeur = Variables.Largeur + Variables.PasLargeur
        Variables.Hauteur = Variables.Hauteur + Variables.PasHauteur
        ' ==================================
        ' Affectation des variables de dimension à la feuille
        Me.Size = New System.Drawing.Size(Variables.Largeur, Variables.Hauteur)
        ' ==================================
        ' Positionnement de la feuille au centre de l'écran
        Me.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (Me.Size.Width / 2), (My.Computer.Screen.Bounds.Height / 2) - (Me.Size.Height / 2))
        ' ==================================
        ' Incrémentation des couleurs
        Variables.CoulRouge = Variables.CoulRouge + Variables.PasRouge ': If Variables.CoulRouge > 255 Then Variables.CoulRouge = 255
        Variables.CoulVert = Variables.CoulVert + Variables.PasVert ': If Variables.CoulVert > 255 Then Variables.CoulVert = 255
        Variables.CoulBleu = Variables.CoulBleu + Variables.PasBleu ': If Variables.CoulBleu > 255 Then Variables.CoulBleu = 255
        ' ==================================
        ' affectation des couleurs à la zone backcolor du masque et de la feuille
        Me.BackColor = Color.FromArgb(Variables.CoulRouge, Variables.CoulVert, Variables.CoulBleu)
        ' ==================================
        ' Apparition de la feuille et du masque
        Me.Visible = True
        ' ==================================
        ' Vérification de l'expension complète du masque
        If Variables.Largeur >= Variables.FormLargeur Then
            ' ==================================
            ' Arrêt de la procédure de fermeture de la feuille
            Timer11.Enabled = False
            ' ==================================
            ' Apparition de la barre supérieure de la feuille
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
            ' ==================================
            ' Apparition de la barre des menus
            MenuStrip1.Visible = True
            ' ==================================
            ' Affectation des dimensions à la feuille
            Me.Size = New System.Drawing.Size(Variables.FormLargeur, Variables.FormHauteur)
            ' ==================================
            ' Positionnement de la feuille
            Me.Top = 0
            Me.Left = 0
            ' ==================================
            ' Localisation du message Bonjour
            Label1.Location = New Point(((Me.Size.Width - Label1.Width) / 2), ((Me.Size.Height - Label1.Height) / 2))
            ' ==================================
            ' Apparition du mot "Bonjour"
            Label1.Visible = True
            ' ==================================
            ' Déclenchement du Timer
            Timer12.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de disparition du masque
    Private Sub Timer12_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer12.Tick
        ' ==================================
        ' Arrêt du Timer
        Timer12.Enabled = False
        ' ==================================
        ' Disparition du mot "Bonjour"
        Label1.Visible = False
        ' ==================================
        ' Adaptation du DataGridView 1
        DataGridView1.Height = My.Computer.Screen.WorkingArea.Size.Height - 56
        DataGridView1.Width = My.Computer.Screen.WorkingArea.Size.Width - 5
        ' ==================================
        ' Adaptation du DataGridView 2
        DataGridView2.Height = My.Computer.Screen.WorkingArea.Size.Height - 56
        DataGridView2.Width = My.Computer.Screen.WorkingArea.Size.Width - 5
        ' ==================================
        ' Mise en visibilité du DataGridView1
        DataGridView1.Visible = True
        ' ==================================
        ' Mise en cache du DataGridview 2
        DataGridView2.Top = 25 - DataGridView2.Height
        ' ==================================
        ' Mise en visibilité du DataGridView2
        DataGridView2.Visible = True
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Base non chargée"
        ' ==================================
        ' Chargement de la base
        Timer2.Enabled = True
    End Sub


    '=============================================================================================================================================
    ' PROCÉDURE DE FERMETURE DE LA FEUILLE PARENT
    '=============================================================================================================================================
    ' Déclenchement de fermeture de la feuille
    Private Sub FermetureFeuille()
        ' ==================================
        ' Curseur de la souris en flèche
        Cursor = Cursors.Default
        ' ==================================
        ' Déclenchement de fermeture de la feuille
        Timer15.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'apparition du masque et de réduction de la feuille
    Private Sub Timer13_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer13.Tick
        ' ==================================
        ' Décrémentation de la largeur et la hauteur de la feuille
        Variables.Largeur = Variables.Largeur - Variables.PasLargeur
        Variables.Hauteur = Variables.Hauteur - Variables.PasHauteur
        ' ==================================
        ' Affectation des variables de dimension à la feuille
        Me.Size = New System.Drawing.Size(Variables.Largeur, Variables.Hauteur)
        ' ==================================
        ' Positionnement de la feuille au centre de l'écran
        Me.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (Me.Size.Width / 2), (My.Computer.Screen.Bounds.Height / 2) - (Me.Size.Height / 2))
        ' ==================================
        ' Décrémentation des couleurs
        Variables.CoulRouge = Variables.CoulRouge - Variables.PasRouge : If Variables.CoulRouge < 0 Then Variables.CoulRouge = 0
        Variables.CoulVert = Variables.CoulVert - Variables.PasVert : If Variables.CoulVert < 0 Then Variables.CoulVert = 0
        Variables.CoulBleu = Variables.CoulBleu - Variables.PasBleu : If Variables.CoulBleu < 0 Then Variables.CoulBleu = 0
        ' ==================================
        ' affectation des couleurs à la zone backcolor du masque et de la feuille
        Me.BackColor = Color.FromArgb(Variables.CoulRouge, Variables.CoulVert, Variables.CoulBleu)
        ' ==================================
        ' Vérification de la réduction de la feuille avec suppression de la feuille à la fin
        If Variables.Largeur < 210 Then
            ' ==================================
            ' Arrêt de la procédure de réduction
            Timer13.Enabled = False
            ' ==================================
            ' Affectation de la hauteur et la largeur à la feuille
            Me.Width = Label1.Width
            Me.Height = Label1.Height
            ' ==================================
            ' Positionnement du libellé "Au revoir"
            Label1.Location = New Point(0, 0)
            ' ==================================
            ' Apparition du libellé
            Label1.Visible = True
            ' ==================================
            ' Procédure de fermeture de la feuille après vision du libellé "Au revoir"
            Timer14.Enabled = True
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de disparition de la feuille
    Private Sub Timer14_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer14.Tick
        ' ==================================
        ' Arrêt du Timer
        Timer14.Enabled = False
        ' ==================================
        ' Fin de la procédure
        Me.Dispose()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de préparation de réduction de la feuille
    Private Sub Timer15_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer15.Tick
        ' ==================================
        ' Arrêt du Timer
        Timer15.Enabled = False
        ' ==================================
        ' Suppression du mot "Bonjour" en cas de fermeture avant l'ouverture totale
        Label1.Visible = False
        ' ==================================
        ' Blocage de certains timers de la feuille Parent afin d'éviter des collisions ouverture/fermeture
        Timer11.Enabled = False
        Timer12.Enabled = False
        ' ==================================
        ' Initialistion des variables couleurs
        Variables.CoulRouge = 230 : Variables.CoulVert = 230 : Variables.CoulBleu = 230
        ' ==================================
        ' Initialisation des variables d'incrémentation de pas avec la couleur à obtenir
        Variables.PasRouge = 230 / 100 : Variables.PasVert = 230 / 100 : Variables.PasBleu = 230 / 100
        ' ==================================
        ' Disparition des listes éventuellement affichées pour permettre une fermeture rapide
        DataGridView1.Visible = False
        ' ==================================
        ' Suppression de la barre supérieure de la feuille
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        ' ==================================
        ' Suppression de la barre des menus
        MenuStrip1.Visible = False
        ' ==================================
        ' Disparition du dataGridView1
        DataGridView1.Visible = False
        ' ==================================
        ' Disparition des listes éventuellement affichées pour permettre une fermeture rapide
        DataGridView2.Visible = False
        ' ==================================
        ' Affectation des variables de dimension
        Variables.Largeur = Variables.FormLargeur
        Variables.Hauteur = Variables.FormHauteur
        ' ==================================
        ' Affectation des variables de pas
        Variables.PasLargeur = (Variables.FormLargeur - Label1.Width) / 100   '   250
        Variables.PasHauteur = (Variables.FormHauteur - Label1.Height) / 100   '   230
        ' ==================================
        ' Mise en place du mot
        Label1.Text = " AU REVOIR ... "
        ' ==================================
        ' Lancement de la procédure
        Timer13.Enabled = True
    End Sub


    '    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    'DataGridView1.Sort(Column1, ComponentModel.ListSortDirection.Ascending)

    '        With Me.DataGridView1
    '    ' Add columns to the DataGridView.
    '    '            .ColumnCount = 3
    '
    '    ' Set the properties of the DataGridView columns.
    '            .Columns(0).Name = "ID"
    '            .Columns(1).Name = "Name"
    '            .Columns(2).Name = "City"
    '            .Columns("ID").HeaderText = "ID"
    '            .Columns("Name").HeaderText = "Name"
    '            .Columns("City").HeaderText = "City"
    '        End With




    '    Dim i As Integer
    '    ' Add rows of data to the DataGridView.
    '        For i = 0 To 200
    '            With Me.DataGridView1.Rows
    '                .Add(New String() {"1", "9", "Seattle"})
    '                .Add(New String() {"2", "4", "9"})
    '                .Add(New String() {"3", "5", "Seattle"})
    '                .Add(New String() {"4", "4", "8"})
    '                .Add(New String() {"5", "4", "7"})
    '                .Add(New String() {"6", "8", "7"})
    '                .Add(New String() {"1", "10", "6"})
    '                .Add(New String() {"2", "4", "5"})
    '                .Add(New String() {"3", "6", "4"})
    '                .Add(New String() {"4", "4", "3"})
    '                .Add(New String() {"5", "4", "4"})
    '                .Add(New String() {"6", "2", "6"})
    '
    '                .Add(New String() {"1", "7", "8"})
    '                .Add(New String() {"2", "4", "7"})
    '                .Add(New String() {"3", "6", "7"})
    '                .Add(New String() {"4", "10", "3"})
    '                .Add(New String() {"5", "A", "4"})
    '                .Add(New String() {"6", "2", "7"})
    '
    '                .Add(New String() {"1", "7", "9"})
    '                .Add(New String() {"2", "4", "0"})
    '                .Add(New String() {"3", "Z", "4"})
    '                .Add(New String() {"4", "4", "1"})
    '                .Add(New String() {"5", "4", "2"})
    '                .Add(New String() {"6", "2", "9"})
    '                .Add(New String() {"6", "11", "9"})
    '            End With
    '        Next
    '    ' Autosize the columns.
    '        Me.DataGridView1.AutoResizeColumns()
    '
    '
    '    '  MsgBox(DataGridView1.RowCount)
    '
    '
    '    Dim Rempli As String
    '
    '        For i = 0 To DataGridView1.RowCount - 1
    '            Rempli = DataGridView1.Item(1, i).Value
    '            If Len(Rempli) = 1 Then Rempli = "0" & Rempli
    '            DataGridView1.Item(3, i).Value = DataGridView1.Item(2, i).Value & Rempli
    '        Next
    '
    '        DataGridView1.Sort(Column4, ComponentModel.ListSortDirection.Ascending)
    '
    '    End Sub

    '    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
    '        DataGridView1.Sort(Column4, ComponentModel.ListSortDirection.Ascending)
    '    End Sub

    '    Private Sub DataGridView1_SortCompare( _
    '        ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewSortCompareEventArgs) _
    '       Handles DataGridView1.SortCompare
    '
    '    ' Try to sort based on the contents of the cell in the current column.
    '        e.SortResult = System.String.Compare(e.CellValue1.ToString(), _
    '            e.CellValue2.ToString())
    '    'If the cells are equal, sort based on the ID column.
    '    'If (e.SortResult = 0) AndAlso Not (e.Column.Name = "Name") Then
    '
    '
    '        If e.SortResult = 0 Then
    '            e.SortResult = System.String.Compare( _
    '                DataGridView1.Rows(e.RowIndex1).Cells("Name").Value.ToString(), _
    '              DataGridView1.Rows(e.RowIndex2).Cells("Name").Value.ToString())
    '        End If
    '
    '        e.Handled = True
    '
    '    End Sub

    '    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        DataGridView1.Sort(Column4, ComponentModel.ListSortDirection.Descending)
    '    End Sub



End Class