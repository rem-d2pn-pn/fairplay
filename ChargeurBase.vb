﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class ChargeurBase

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub NuméroProgramme_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub ChargeurBase_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button5.Select()
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button5.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de sélection du nom du fichier de la Base dans la liste
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
        '==============================================
        ' Récupération du nom du fichier sélectionné
        Variables.NomFichierBase = "\" & Strings.LTrim(ListBox1.SelectedItem) & ".txt"
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de validation
    ' Traitement appui 
    Private Sub Button2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button2.MouseDown
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en cours ..."
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    ' Traitement relachement
    Private Sub Button2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button2.MouseUp
        '==============================================
        ' Changement visuel de sélection de bouton
        Button5.Select()
        ' ====================================================================================
        ' Définition des variables
        Dim NomFichier As StreamReader
        Dim LectureLigne As String
        Dim Aiguilleur As Integer
        Dim i As Integer
        Dim j As Integer
        Dim TabMin As Integer
        Dim TabMax As Integer
        ' ====================================================================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If Variables.NomFichierBase = "" Then
            MsgBox("Vous n'avez pas réalisé de sélection de fichier!  " & vbCrLf & vbCrLf & "       La base ne peut pas être chargée.", MsgBoxStyle.Exclamation, " Absence de sélection")
            TextBox1.Text = "Chargement de la base en attente"
            TextBox1.BackColor = Color.FromArgb(255, 210, 210)
            Exit Sub
        End If
        ' ====================================================================================
        ' Vérification s'il y a au moins un case à cocher valider
        If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False And CheckBox6.Checked = False And CheckBox7.Checked = False Then
            MsgBox("Vous n'avez pas réalisé de sélection !  " & vbCrLf & vbCrLf & "  La base ne peut pas être chargée.", MsgBoxStyle.Exclamation, " Absence de sélection")
            TextBox1.Text = "Chargement de la base en attente"
            TextBox1.BackColor = Color.FromArgb(255, 210, 210)
            Exit Sub
        End If
        ' ====================================================================================
        ' Pointeur sablier
        '       Cursor = Cursors.WaitCursor
        Cursor = Cursors.PanNorth
        ' ====================================================================================
        ' Renvoi pour le traitement des erreurs 
        On Error GoTo TraitErreur2
        ' ====================================================================================
        ' Positionnement de l'aiguilleur pour le traitement des erreurs 
        Aiguilleur = 1
        ' ====================================================================================
        ' Récupération des informations de la Base Fairplay
        ' ==================================
        ' Ouverture du fichier de la base
        NomFichier = New StreamReader(Variables.Repertoire & Variables.NomFichierBase, System.Text.Encoding.Default)
        ' ==================================
        ' Valeur de la variable de la dernière ligne de la base à 0
        Variables.FinBase = 0
        ' ==================================
        ' Lecture du fichier de la base
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Incrémentation de la variable pour le changement de position du curseur de la souris
            j = j + 1
            ' ==================================
            ' Incrémentation de la dernère ligne de la base
            Variables.FinBase = Variables.FinBase + 1
            ' ==================================
            ' Lecture d'une ligne du fichier de la base
            LectureLigne = NomFichier.ReadLine
            ' ==================================
            ' Remise à zéro de la colonne de tri et de la colonne État
            Variables.Base(Variables.FinBase, 0) = 0
            Variables.Base(Variables.FinBase, 3) = ""
            ' ==================================
            ' Positionnement du pointeur sur le premier caractère de la ligne
            TabMin = 1
            ' ==================================
            ' Boucle d'affectation des informations pour les 41 colonnes
            For i = 1 To 41
                ' ==================================
                ' Recherche de la prochaine position de la tabulation
                TabMax = Strings.InStr(TabMin, LectureLigne, Chr("9"))
                ' ==================================
                ' Chargement de la colonne définie avec ses informations
                Variables.Base(Variables.FinBase, i) = Strings.Mid(LectureLigne, TabMin, TabMax - TabMin)
                ' ==================================
                ' Positionnement de la prochaine recherche de tabulation
                TabMin = TabMax + 1
                ' ==================================
                ' Vérification de l'état du navire
                If i = 3 Then
                    ' ==================================
                    ' Vérification si "D" est voulue et si le navire est "D"
                    If CheckBox1.Checked = False And Variables.Base(Variables.FinBase, 3) = "D" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                    ' ==================================
                    ' Vérification si "P" est voulue et si le navire est "P"
                    If CheckBox2.Checked = False And Variables.Base(Variables.FinBase, 3) = "P" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                    ' ==================================
                    ' Vérification si "U" est voulue et si le navire est "U"
                    If CheckBox3.Checked = False And Variables.Base(Variables.FinBase, 3) = "U" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                    ' ==================================
                    ' Vérification si "L" est voulue et si le navire est "L"
                    If CheckBox4.Checked = False And Variables.Base(Variables.FinBase, 3) = "L" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                    ' ==================================
                    ' Vérification si "S" est voulue et si le navire est "S"
                    If CheckBox5.Checked = False And Variables.Base(Variables.FinBase, 3) = "S" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                    ' ==================================
                    ' Vérification si "X" est voulue et si le navire est "X"
                    If CheckBox6.Checked = False And Variables.Base(Variables.FinBase, 3) = "X" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                    ' ==================================
                    ' Vérification si "Z" est voulue et si le navire est "Z"
                    If CheckBox7.Checked = False And Variables.Base(Variables.FinBase, 3) = "Z" Then
                        Variables.FinBase = Variables.FinBase - 1
                        Exit For
                    End If
                End If
            Next
            ' ==================================
            ' Chargement de la dernière colonne la 42° si le navire n'est pas perdu ou détruit
            If i <> 3 Then
                Variables.Base(Variables.FinBase, 42) = Strings.Right(LectureLigne, Len(LectureLigne) - TabMax)
                ' ==================================
                ' Mise en forme de la date d'achat
                If Variables.Base(Variables.FinBase, 8) <> "" Then
                    Variables.Base(Variables.FinBase, 8) = FormatDateTime(Variables.Base(Variables.FinBase, 8), DateFormat.ShortDate)
                End If
                ' ==================================
                ' Mise en forme de la date de livraison
                If Variables.Base(Variables.FinBase, 12) <> "" Then
                    Variables.Base(Variables.FinBase, 12) = FormatDateTime(Variables.Base(Variables.FinBase, 12), DateFormat.ShortDate)
                End If
                ' ==================================
                ' Mise en forme de la date de perte
                If Variables.Base(Variables.FinBase, 18) <> "" Then
                    Variables.Base(Variables.FinBase, 18) = FormatDateTime(Variables.Base(Variables.FinBase, 18), DateFormat.ShortDate)
                End If
            End If
            ' ==================================
            ' Changement de la position du curseur de la souris au bout de 10 000 lignes chargées
            If j = 10000 Then
                ' ==================================
                ' Initialisation du compteur de ligne
                j = 0
                ' ==================================
                ' Changement de l'aspect du curseur
                If Cursor = Cursors.PanNorth Then
                    Cursor = Cursors.PanNE
                    GoTo ici
                End If
                If Cursor = Cursors.PanNE Then
                    Cursor = Cursors.PanEast
                    GoTo ici
                End If
                If Cursor = Cursors.PanEast Then
                    Cursor = Cursors.PanSE
                    GoTo ici
                End If
                If Cursor = Cursors.PanSE Then
                    Cursor = Cursors.PanSouth
                    GoTo ici
                End If
                If Cursor = Cursors.PanSouth Then
                    Cursor = Cursors.PanSW
                    GoTo ici
                End If
                If Cursor = Cursors.PanSW Then
                    Cursor = Cursors.PanWest
                    GoTo ici
                End If
                If Cursor = Cursors.PanWest Then
                    Cursor = Cursors.PanNW
                    GoTo ici
                End If
                If Cursor = Cursors.PanNW Then
                    Cursor = Cursors.PanNorth
                End If
            End If
ici:
        Loop
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Base chargée"
        ' ==================================
        ' Affichage
        TextBox1.Text = "Base chargée avec " & Variables.FinBase & " navires"
        TextBox1.BackColor = Color.PaleGreen
        ' ==================================
        ' Initialisation du nom du fichier pour les titres
        i = Val(Strings.Mid(Variables.NomFichierBase, 7, 2))
        If i = 1 Then Variables.NomBaseTitre = "Janvier " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 2 Then Variables.NomBaseTitre = "Février " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 3 Then Variables.NomBaseTitre = "Mars " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 4 Then Variables.NomBaseTitre = "Avril " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 5 Then Variables.NomBaseTitre = "Mai " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 6 Then Variables.NomBaseTitre = "Juin " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 7 Then Variables.NomBaseTitre = "Juillet " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 8 Then Variables.NomBaseTitre = "Août " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 9 Then Variables.NomBaseTitre = "Septembre " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 10 Then Variables.NomBaseTitre = "Octobre " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 11 Then Variables.NomBaseTitre = "Novembre " & Strings.Mid(Variables.NomFichierBase, 2, 4)
        If i = 12 Then Variables.NomBaseTitre = "Décembre " & Strings.Mid(Variables.NomFichierBase, 2, 4)
Suite1:
        ' ==================================
        ' Pointeur flèche
        Cursor = Cursors.Default
        Exit Sub
        ' ====================================================================================
        ' Traitement des erreurs
TraitErreur2:
        ' ==================================
        ' Traitement si l'erreur est l'absence du fichier du nom de la base
        If Err.Number = 53 And Aiguilleur = 1 Then
            MsgBox("Pas de fichier du nom de la base !", MsgBoxStyle.Critical, " Chargeur Base Fairplay - Erreur")
            ' ==================================
            ' Affichage état de l'application
            Variables.EtatAppli = "Problème Base"
            Resume Suite1
        End If
        ' Traitement si l'erreur inconnue
        MsgBox("Numéro d'erreur =" & Err.Number & "    Signification = " & ErrorToString(), MsgBoxStyle.Critical, " Chargeur Base Fairplay - Erreur")
        ' ==================================
        ' Pointeur flèche
        Cursor = Cursors.Default
    End Sub

    ' ==============================================================================================================================================
    ' Validation de toutes les cases à cocher des navires existants
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If CheckBox1.Checked = True And CheckBox2.Checked = True And CheckBox3.Checked = True Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
        Else
            CheckBox1.Checked = True
            CheckBox2.Checked = True
            CheckBox3.Checked = True
        End If
        '==============================================
        ' Changement visuel de sélection de bouton
        Button5.Select()
    End Sub

    ' ==============================================================================================================================================
    ' Validation de toutes les cases à cocher des navires non existants
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If CheckBox4.Checked = True And CheckBox5.Checked = True And CheckBox6.Checked = True And CheckBox7.Checked = True Then
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        Else
            CheckBox4.Checked = True
            CheckBox5.Checked = True
            CheckBox6.Checked = True
            CheckBox7.Checked = True
        End If
        '==============================================
        ' Changement visuel de sélection de bouton
        Button5.Select()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        '==============================================
        ' Mise à jour du titre
        Form1.Text = " Explorateur des informations de la Base FAIRPLAY - " & Variables.NomBaseTitre
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Modification de l'indicateur de chargement de la base si un Checkbox est modifié
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub
    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        '==============================================
        ' Réinitialisation de l'indicateur
        TextBox1.Text = "Chargement de la base en attente"
        TextBox1.BackColor = Color.FromArgb(255, 210, 210)
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub ChargeurBase_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE DE RÉDUCTION DES FENÊTRES
    '=============================================================================================================================================
    Private Sub ChargeurBase_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Réorientation du focus
    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        '==============================================
        ' Changement visuel de sélection de bouton
        Button5.Select()
    End Sub

End Class