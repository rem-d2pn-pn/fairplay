﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Extracteur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Extracteur))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TextBox19 = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.TextBox18 = New System.Windows.Forms.TextBox
        Me.TextBox17 = New System.Windows.Forms.TextBox
        Me.TextBox16 = New System.Windows.Forms.TextBox
        Me.TextBox15 = New System.Windows.Forms.TextBox
        Me.TextBox14 = New System.Windows.Forms.TextBox
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Button8 = New System.Windows.Forms.Button
        Me.TextBox39 = New System.Windows.Forms.TextBox
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.TextBox37 = New System.Windows.Forms.TextBox
        Me.TextBox36 = New System.Windows.Forms.TextBox
        Me.TextBox35 = New System.Windows.Forms.TextBox
        Me.TextBox34 = New System.Windows.Forms.TextBox
        Me.TextBox33 = New System.Windows.Forms.TextBox
        Me.TextBox32 = New System.Windows.Forms.TextBox
        Me.TextBox31 = New System.Windows.Forms.TextBox
        Me.TextBox30 = New System.Windows.Forms.TextBox
        Me.TextBox29 = New System.Windows.Forms.TextBox
        Me.TextBox28 = New System.Windows.Forms.TextBox
        Me.TextBox27 = New System.Windows.Forms.TextBox
        Me.TextBox26 = New System.Windows.Forms.TextBox
        Me.TextBox25 = New System.Windows.Forms.TextBox
        Me.TextBox24 = New System.Windows.Forms.TextBox
        Me.TextBox23 = New System.Windows.Forms.TextBox
        Me.TextBox22 = New System.Windows.Forms.TextBox
        Me.TextBox21 = New System.Windows.Forms.TextBox
        Me.TextBox38 = New System.Windows.Forms.TextBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.TextBox41 = New System.Windows.Forms.TextBox
        Me.TextBox47 = New System.Windows.Forms.TextBox
        Me.TextBox48 = New System.Windows.Forms.TextBox
        Me.TextBox49 = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.TextBox46 = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.RadioButton8 = New System.Windows.Forms.RadioButton
        Me.RadioButton7 = New System.Windows.Forms.RadioButton
        Me.RadioButton6 = New System.Windows.Forms.RadioButton
        Me.RadioButton5 = New System.Windows.Forms.RadioButton
        Me.RadioButton4 = New System.Windows.Forms.RadioButton
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.TextBox42 = New System.Windows.Forms.TextBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Button13 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.TextBox20 = New System.Windows.Forms.TextBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Button15 = New System.Windows.Forms.Button
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer4 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.TextBox45 = New System.Windows.Forms.TextBox
        Me.Timer5 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer6 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.TextBox40 = New System.Windows.Forms.TextBox
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.TextBox43 = New System.Windows.Forms.TextBox
        Me.Timer7 = New System.Windows.Forms.Timer(Me.components)
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.TextBox44 = New System.Windows.Forms.TextBox
        Me.Timer8 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer9 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer10 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer11 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox19)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.TextBox18)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.TextBox16)
        Me.GroupBox1.Controls.Add(Me.TextBox15)
        Me.GroupBox1.Controls.Add(Me.TextBox14)
        Me.GroupBox1.Controls.Add(Me.TextBox13)
        Me.GroupBox1.Controls.Add(Me.TextBox12)
        Me.GroupBox1.Controls.Add(Me.TextBox11)
        Me.GroupBox1.Controls.Add(Me.TextBox10)
        Me.GroupBox1.Controls.Add(Me.TextBox9)
        Me.GroupBox1.Controls.Add(Me.TextBox8)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(15, 15)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(500, 460)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = " Traitements sur la Base "
        '
        'TextBox19
        '
        Me.TextBox19.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox19.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox19.Location = New System.Drawing.Point(15, 385)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.ShortcutsEnabled = False
        Me.TextBox19.Size = New System.Drawing.Size(470, 22)
        Me.TextBox19.TabIndex = 0
        Me.TextBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(365, 420)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(120, 25)
        Me.Button5.TabIndex = 20
        Me.Button5.Text = "Insérer au dessus"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(190, 420)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 25)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "Tout supprimer"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(15, 420)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 25)
        Me.Button3.TabIndex = 18
        Me.Button3.Text = "Supprimer la sélection"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox18
        '
        Me.TextBox18.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox18.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox18.Location = New System.Drawing.Point(15, 365)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.ShortcutsEnabled = False
        Me.TextBox18.Size = New System.Drawing.Size(470, 22)
        Me.TextBox18.TabIndex = 17
        Me.TextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox17
        '
        Me.TextBox17.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox17.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox17.Location = New System.Drawing.Point(15, 345)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.ShortcutsEnabled = False
        Me.TextBox17.Size = New System.Drawing.Size(470, 22)
        Me.TextBox17.TabIndex = 16
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox16
        '
        Me.TextBox16.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox16.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox16.Location = New System.Drawing.Point(15, 325)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.ShortcutsEnabled = False
        Me.TextBox16.Size = New System.Drawing.Size(470, 22)
        Me.TextBox16.TabIndex = 15
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox15
        '
        Me.TextBox15.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox15.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox15.Location = New System.Drawing.Point(15, 305)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.ShortcutsEnabled = False
        Me.TextBox15.Size = New System.Drawing.Size(470, 22)
        Me.TextBox15.TabIndex = 14
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox14
        '
        Me.TextBox14.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox14.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(15, 285)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.ShortcutsEnabled = False
        Me.TextBox14.Size = New System.Drawing.Size(470, 22)
        Me.TextBox14.TabIndex = 13
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox13
        '
        Me.TextBox13.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox13.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(15, 265)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.ShortcutsEnabled = False
        Me.TextBox13.Size = New System.Drawing.Size(470, 22)
        Me.TextBox13.TabIndex = 12
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox12.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox12.Location = New System.Drawing.Point(15, 245)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.ShortcutsEnabled = False
        Me.TextBox12.Size = New System.Drawing.Size(470, 22)
        Me.TextBox12.TabIndex = 11
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox11
        '
        Me.TextBox11.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox11.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(15, 225)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.ShortcutsEnabled = False
        Me.TextBox11.Size = New System.Drawing.Size(470, 22)
        Me.TextBox11.TabIndex = 10
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox10
        '
        Me.TextBox10.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox10.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox10.Location = New System.Drawing.Point(15, 205)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.ShortcutsEnabled = False
        Me.TextBox10.Size = New System.Drawing.Size(470, 22)
        Me.TextBox10.TabIndex = 9
        Me.TextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox9
        '
        Me.TextBox9.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox9.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(15, 185)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.ShortcutsEnabled = False
        Me.TextBox9.Size = New System.Drawing.Size(470, 22)
        Me.TextBox9.TabIndex = 8
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox8
        '
        Me.TextBox8.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox8.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(15, 165)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.ShortcutsEnabled = False
        Me.TextBox8.Size = New System.Drawing.Size(470, 22)
        Me.TextBox8.TabIndex = 7
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox7.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(15, 145)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.ShortcutsEnabled = False
        Me.TextBox7.Size = New System.Drawing.Size(470, 22)
        Me.TextBox7.TabIndex = 6
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox6.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(15, 125)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.ShortcutsEnabled = False
        Me.TextBox6.Size = New System.Drawing.Size(470, 22)
        Me.TextBox6.TabIndex = 5
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox5.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(15, 105)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.ShortcutsEnabled = False
        Me.TextBox5.Size = New System.Drawing.Size(470, 22)
        Me.TextBox5.TabIndex = 4
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox4.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(15, 85)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.ShortcutsEnabled = False
        Me.TextBox4.Size = New System.Drawing.Size(470, 22)
        Me.TextBox4.TabIndex = 3
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox3.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(15, 65)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.ShortcutsEnabled = False
        Me.TextBox3.Size = New System.Drawing.Size(470, 22)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(15, 45)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.ShortcutsEnabled = False
        Me.TextBox2.Size = New System.Drawing.Size(470, 22)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(15, 25)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.ShortcutsEnabled = False
        Me.TextBox1.Size = New System.Drawing.Size(470, 22)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ListBox3
        '
        Me.ListBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.ItemHeight = 16
        Me.ListBox3.Items.AddRange(New Object() {" 1 " & Global.Microsoft.VisualBasic.ChrW(9) & "Dry Cargo", " 2 " & Global.Microsoft.VisualBasic.ChrW(9) & "Container", " 3 " & Global.Microsoft.VisualBasic.ChrW(9) & "Tanker", " 4 " & Global.Microsoft.VisualBasic.ChrW(9) & "Bulker", " 5 " & Global.Microsoft.VisualBasic.ChrW(9) & "Passengers / Ferry", " 6 " & Global.Microsoft.VisualBasic.ChrW(9) & "Miscellaneous", " 7 " & Global.Microsoft.VisualBasic.ChrW(9) & "Roro", " 8 " & Global.Microsoft.VisualBasic.ChrW(9) & "Reefer", " 9 " & Global.Microsoft.VisualBasic.ChrW(9) & "Combination", " 10" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore"})
        Me.ListBox3.Location = New System.Drawing.Point(15, 35)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(205, 164)
        Me.ListBox3.TabIndex = 21
        Me.ListBox3.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button8)
        Me.GroupBox2.Controls.Add(Me.TextBox39)
        Me.GroupBox2.Controls.Add(Me.Button7)
        Me.GroupBox2.Controls.Add(Me.Button6)
        Me.GroupBox2.Controls.Add(Me.TextBox37)
        Me.GroupBox2.Controls.Add(Me.TextBox36)
        Me.GroupBox2.Controls.Add(Me.TextBox35)
        Me.GroupBox2.Controls.Add(Me.TextBox34)
        Me.GroupBox2.Controls.Add(Me.TextBox33)
        Me.GroupBox2.Controls.Add(Me.TextBox32)
        Me.GroupBox2.Controls.Add(Me.TextBox31)
        Me.GroupBox2.Controls.Add(Me.TextBox30)
        Me.GroupBox2.Controls.Add(Me.TextBox29)
        Me.GroupBox2.Controls.Add(Me.TextBox28)
        Me.GroupBox2.Controls.Add(Me.TextBox27)
        Me.GroupBox2.Controls.Add(Me.TextBox26)
        Me.GroupBox2.Controls.Add(Me.TextBox25)
        Me.GroupBox2.Controls.Add(Me.TextBox24)
        Me.GroupBox2.Controls.Add(Me.TextBox23)
        Me.GroupBox2.Controls.Add(Me.TextBox22)
        Me.GroupBox2.Controls.Add(Me.TextBox21)
        Me.GroupBox2.Controls.Add(Me.TextBox38)
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(530, 15)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(500, 460)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = " Traitements sur le dernier résultat "
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(365, 420)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(120, 25)
        Me.Button8.TabIndex = 40
        Me.Button8.Text = "Insérer au dessus"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'TextBox39
        '
        Me.TextBox39.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox39.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox39.Location = New System.Drawing.Point(15, 385)
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.ReadOnly = True
        Me.TextBox39.ShortcutsEnabled = False
        Me.TextBox39.Size = New System.Drawing.Size(470, 22)
        Me.TextBox39.TabIndex = 0
        Me.TextBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(195, 420)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(125, 25)
        Me.Button7.TabIndex = 39
        Me.Button7.Text = "Tout supprimer"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(15, 420)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(125, 25)
        Me.Button6.TabIndex = 38
        Me.Button6.Text = "Supprimer la sélection"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'TextBox37
        '
        Me.TextBox37.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox37.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox37.Location = New System.Drawing.Point(15, 345)
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.ReadOnly = True
        Me.TextBox37.ShortcutsEnabled = False
        Me.TextBox37.Size = New System.Drawing.Size(470, 22)
        Me.TextBox37.TabIndex = 36
        Me.TextBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox36
        '
        Me.TextBox36.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox36.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox36.Location = New System.Drawing.Point(15, 325)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.ReadOnly = True
        Me.TextBox36.ShortcutsEnabled = False
        Me.TextBox36.Size = New System.Drawing.Size(470, 22)
        Me.TextBox36.TabIndex = 35
        Me.TextBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox35
        '
        Me.TextBox35.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox35.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox35.Location = New System.Drawing.Point(15, 305)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.ReadOnly = True
        Me.TextBox35.ShortcutsEnabled = False
        Me.TextBox35.Size = New System.Drawing.Size(470, 22)
        Me.TextBox35.TabIndex = 34
        Me.TextBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox34
        '
        Me.TextBox34.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox34.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox34.Location = New System.Drawing.Point(15, 285)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.ReadOnly = True
        Me.TextBox34.ShortcutsEnabled = False
        Me.TextBox34.Size = New System.Drawing.Size(470, 22)
        Me.TextBox34.TabIndex = 33
        Me.TextBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox33
        '
        Me.TextBox33.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox33.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox33.Location = New System.Drawing.Point(15, 265)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.ReadOnly = True
        Me.TextBox33.ShortcutsEnabled = False
        Me.TextBox33.Size = New System.Drawing.Size(470, 22)
        Me.TextBox33.TabIndex = 32
        Me.TextBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox32
        '
        Me.TextBox32.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox32.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox32.Location = New System.Drawing.Point(15, 245)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.ReadOnly = True
        Me.TextBox32.ShortcutsEnabled = False
        Me.TextBox32.Size = New System.Drawing.Size(470, 22)
        Me.TextBox32.TabIndex = 31
        Me.TextBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox31
        '
        Me.TextBox31.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox31.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox31.Location = New System.Drawing.Point(15, 225)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.ReadOnly = True
        Me.TextBox31.ShortcutsEnabled = False
        Me.TextBox31.Size = New System.Drawing.Size(470, 22)
        Me.TextBox31.TabIndex = 30
        Me.TextBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox30
        '
        Me.TextBox30.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox30.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox30.Location = New System.Drawing.Point(15, 205)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.ReadOnly = True
        Me.TextBox30.ShortcutsEnabled = False
        Me.TextBox30.Size = New System.Drawing.Size(470, 22)
        Me.TextBox30.TabIndex = 29
        Me.TextBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox29
        '
        Me.TextBox29.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox29.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox29.Location = New System.Drawing.Point(15, 185)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.ReadOnly = True
        Me.TextBox29.ShortcutsEnabled = False
        Me.TextBox29.Size = New System.Drawing.Size(470, 22)
        Me.TextBox29.TabIndex = 28
        Me.TextBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox28
        '
        Me.TextBox28.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox28.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox28.Location = New System.Drawing.Point(15, 165)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.ReadOnly = True
        Me.TextBox28.ShortcutsEnabled = False
        Me.TextBox28.Size = New System.Drawing.Size(470, 22)
        Me.TextBox28.TabIndex = 27
        Me.TextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox27
        '
        Me.TextBox27.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox27.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox27.Location = New System.Drawing.Point(15, 145)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.ReadOnly = True
        Me.TextBox27.ShortcutsEnabled = False
        Me.TextBox27.Size = New System.Drawing.Size(470, 22)
        Me.TextBox27.TabIndex = 26
        Me.TextBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox26
        '
        Me.TextBox26.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox26.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox26.Location = New System.Drawing.Point(15, 125)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.ReadOnly = True
        Me.TextBox26.ShortcutsEnabled = False
        Me.TextBox26.Size = New System.Drawing.Size(470, 22)
        Me.TextBox26.TabIndex = 25
        Me.TextBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox25
        '
        Me.TextBox25.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox25.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox25.Location = New System.Drawing.Point(15, 105)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.ReadOnly = True
        Me.TextBox25.ShortcutsEnabled = False
        Me.TextBox25.Size = New System.Drawing.Size(470, 22)
        Me.TextBox25.TabIndex = 24
        Me.TextBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox24
        '
        Me.TextBox24.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox24.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox24.Location = New System.Drawing.Point(15, 85)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.ReadOnly = True
        Me.TextBox24.ShortcutsEnabled = False
        Me.TextBox24.Size = New System.Drawing.Size(470, 22)
        Me.TextBox24.TabIndex = 23
        Me.TextBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox23
        '
        Me.TextBox23.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox23.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox23.Location = New System.Drawing.Point(15, 65)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.ReadOnly = True
        Me.TextBox23.ShortcutsEnabled = False
        Me.TextBox23.Size = New System.Drawing.Size(470, 22)
        Me.TextBox23.TabIndex = 22
        Me.TextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox22
        '
        Me.TextBox22.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox22.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox22.Location = New System.Drawing.Point(15, 45)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.ReadOnly = True
        Me.TextBox22.ShortcutsEnabled = False
        Me.TextBox22.Size = New System.Drawing.Size(470, 22)
        Me.TextBox22.TabIndex = 21
        Me.TextBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox21
        '
        Me.TextBox21.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox21.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox21.Location = New System.Drawing.Point(15, 25)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.ReadOnly = True
        Me.TextBox21.ShortcutsEnabled = False
        Me.TextBox21.Size = New System.Drawing.Size(470, 22)
        Me.TextBox21.TabIndex = 0
        Me.TextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox38
        '
        Me.TextBox38.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox38.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox38.Location = New System.Drawing.Point(15, 365)
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.ReadOnly = True
        Me.TextBox38.ShortcutsEnabled = False
        Me.TextBox38.Size = New System.Drawing.Size(470, 22)
        Me.TextBox38.TabIndex = 37
        Me.TextBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ListBox2
        '
        Me.ListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.ItemHeight = 16
        Me.ListBox2.Items.AddRange(New Object() {" D" & Global.Microsoft.VisualBasic.ChrW(9) & "En service", " L" & Global.Microsoft.VisualBasic.ChrW(9) & "Perdu", " P" & Global.Microsoft.VisualBasic.ChrW(9) & "En fabrication", " S" & Global.Microsoft.VisualBasic.ChrW(9) & "Détruit", " U" & Global.Microsoft.VisualBasic.ChrW(9) & "En commande", " X" & Global.Microsoft.VisualBasic.ChrW(9) & "Erreur", " Z" & Global.Microsoft.VisualBasic.ChrW(9) & "Annulé"})
        Me.ListBox2.Location = New System.Drawing.Point(15, 35)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(174, 116)
        Me.ListBox2.TabIndex = 0
        Me.ListBox2.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button2)
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.Panel4)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.Controls.Add(Me.Panel2)
        Me.GroupBox3.Controls.Add(Me.Panel1)
        Me.GroupBox3.ForeColor = System.Drawing.Color.Black
        Me.GroupBox3.Location = New System.Drawing.Point(15, 485)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1015, 95)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = " Élaboration d'un traitement "
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(930, 20)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(70, 25)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "Effacer"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(930, 55)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(70, 25)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Valider"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.TextBox41)
        Me.Panel4.Controls.Add(Me.TextBox47)
        Me.Panel4.Controls.Add(Me.TextBox48)
        Me.Panel4.Controls.Add(Me.TextBox49)
        Me.Panel4.Location = New System.Drawing.Point(565, 20)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(355, 60)
        Me.Panel4.TabIndex = 5
        '
        'TextBox41
        '
        Me.TextBox41.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox41.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox41.Location = New System.Drawing.Point(9, 19)
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.ShortcutsEnabled = False
        Me.TextBox41.Size = New System.Drawing.Size(335, 22)
        Me.TextBox41.TabIndex = 3
        Me.TextBox41.Text = "Sélection d'une valeur de comparaison (clavier ou clic)"
        Me.TextBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox47
        '
        Me.TextBox47.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox47.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox47.Location = New System.Drawing.Point(9, 19)
        Me.TextBox47.Name = "TextBox47"
        Me.TextBox47.ReadOnly = True
        Me.TextBox47.ShortcutsEnabled = False
        Me.TextBox47.Size = New System.Drawing.Size(113, 22)
        Me.TextBox47.TabIndex = 4
        Me.TextBox47.Text = "Min  =  Clic"
        Me.TextBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox48
        '
        Me.TextBox48.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox48.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox48.Location = New System.Drawing.Point(121, 19)
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.ShortcutsEnabled = False
        Me.TextBox48.Size = New System.Drawing.Size(113, 22)
        Me.TextBox48.TabIndex = 5
        Me.TextBox48.Text = "Autre  =  Clic"
        Me.TextBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox49
        '
        Me.TextBox49.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox49.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox49.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox49.Location = New System.Drawing.Point(232, 19)
        Me.TextBox49.Name = "TextBox49"
        Me.TextBox49.ReadOnly = True
        Me.TextBox49.ShortcutsEnabled = False
        Me.TextBox49.Size = New System.Drawing.Size(112, 22)
        Me.TextBox49.TabIndex = 6
        Me.TextBox49.Text = "Max  =  Clic"
        Me.TextBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.TextBox46)
        Me.Panel3.Location = New System.Drawing.Point(120, 20)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(260, 60)
        Me.Panel3.TabIndex = 4
        '
        'TextBox46
        '
        Me.TextBox46.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox46.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox46.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox46.Location = New System.Drawing.Point(9, 19)
        Me.TextBox46.Name = "TextBox46"
        Me.TextBox46.ReadOnly = True
        Me.TextBox46.ShortcutsEnabled = False
        Me.TextBox46.Size = New System.Drawing.Size(240, 22)
        Me.TextBox46.TabIndex = 0
        Me.TextBox46.Text = "Sélection d'un sujet de traitement (clic)"
        Me.TextBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.RadioButton8)
        Me.Panel2.Controls.Add(Me.RadioButton7)
        Me.Panel2.Controls.Add(Me.RadioButton6)
        Me.Panel2.Controls.Add(Me.RadioButton5)
        Me.Panel2.Controls.Add(Me.RadioButton4)
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Location = New System.Drawing.Point(385, 20)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(175, 60)
        Me.Panel2.TabIndex = 2
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.Location = New System.Drawing.Point(130, 30)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(37, 24)
        Me.RadioButton8.TabIndex = 5
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "≠"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.Location = New System.Drawing.Point(65, 30)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(47, 24)
        Me.RadioButton7.TabIndex = 4
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "=>"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.Location = New System.Drawing.Point(10, 30)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(37, 24)
        Me.RadioButton6.TabIndex = 3
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = ">"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.Location = New System.Drawing.Point(130, 5)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(37, 24)
        Me.RadioButton5.TabIndex = 2
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "="
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.Location = New System.Drawing.Point(65, 5)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(47, 24)
        Me.RadioButton4.TabIndex = 1
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "<="
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(10, 5)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(37, 24)
        Me.RadioButton3.TabIndex = 0
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "<"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Location = New System.Drawing.Point(15, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(100, 60)
        Me.Panel1.TabIndex = 0
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(10, 31)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(74, 20)
        Me.RadioButton2.TabIndex = 2
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Je retire"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(10, 6)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(86, 20)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Je prends"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'TextBox42
        '
        Me.TextBox42.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox42.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox42.Location = New System.Drawing.Point(150, 22)
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.ReadOnly = True
        Me.TextBox42.ShortcutsEnabled = False
        Me.TextBox42.Size = New System.Drawing.Size(130, 22)
        Me.TextBox42.TabIndex = 3
        Me.TextBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button11)
        Me.GroupBox4.Controls.Add(Me.Button10)
        Me.GroupBox4.Controls.Add(Me.Button9)
        Me.GroupBox4.Controls.Add(Me.TextBox42)
        Me.GroupBox4.ForeColor = System.Drawing.Color.Black
        Me.GroupBox4.Location = New System.Drawing.Point(380, 590)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(565, 60)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = " Lancement, résultat et affichage d'une recherche "
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(375, 20)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(175, 25)
        Me.Button11.TabIndex = 6
        Me.Button11.Text = "Transférer dans le Presse Papier"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Enabled = False
        Me.Button10.Location = New System.Drawing.Point(290, 20)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 25)
        Me.Button10.TabIndex = 5
        Me.Button10.Text = "Lister"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(15, 20)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(125, 25)
        Me.Button9.TabIndex = 4
        Me.Button9.Text = "Lancer une recherche"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Button13)
        Me.GroupBox5.Controls.Add(Me.Button12)
        Me.GroupBox5.ForeColor = System.Drawing.Color.Black
        Me.GroupBox5.Location = New System.Drawing.Point(15, 590)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(350, 60)
        Me.GroupBox5.TabIndex = 5
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = " Configuration de traitements "
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(175, 20)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(160, 25)
        Me.Button13.TabIndex = 1
        Me.Button13.Text = "Sauvegarder la configuration"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(15, 20)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(150, 25)
        Me.Button12.TabIndex = 0
        Me.Button12.Text = "Charger une configuration"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(960, 610)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(70, 25)
        Me.Button14.TabIndex = 7
        Me.Button14.Text = "Fermer"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.ListBox1)
        Me.Panel5.Controls.Add(Me.TextBox20)
        Me.Panel5.Location = New System.Drawing.Point(390, -50)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(267, 50)
        Me.Panel5.TabIndex = 7
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 16
        Me.ListBox1.Items.AddRange(New Object() {" Capacité en balles", " Capacité en céréales", " Capacité en conteneurs", " Capacité en conteneurs frigorifiques", " Capacité en liquide", " Capacité en minerai", " Classe du navire", " Code état du navire", " Date d'achat", " Date de livraison", " Date de perte ou de destruction", " Hauteur totale", " Largeur", " Longueur", " Nom du navire", " Nom du propriétaire", " Nombre de barges amarrables", " Nombre de cales", " Nombre de conteneurs", " Nombre d'écoutilles", " Nombre de passagers", " Nombre de ponts", " Nombre de portes latérales droites", " Nombre de portes latérales gauches", " Nombre de remorques", " Nombre de réservoirs", " Nombre de voitures", " Nombre de wagons", " Numéro FAIRPLAY", " Numéro LLOYDS", " Pavillon du navire", " Port en lourd", " Profondeur", " Routes fréquentées", " Tirant d'eau", " Tonnage", " Type du navire", " Vitesse"})
        Me.ListBox1.Location = New System.Drawing.Point(15, 35)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(235, 612)
        Me.ListBox1.TabIndex = 9
        Me.ListBox1.Visible = False
        '
        'TextBox20
        '
        Me.TextBox20.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox20.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox20.Location = New System.Drawing.Point(15, 12)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(235, 22)
        Me.TextBox20.TabIndex = 10
        Me.TextBox20.Text = "Liste des sujets d'extraction"
        Me.TextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'Timer2
        '
        Me.Timer2.Interval = 1
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(970, 615)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(15, 15)
        Me.Button15.TabIndex = 0
        Me.Button15.Text = "Button15"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Timer3
        '
        Me.Timer3.Interval = 10
        '
        'Timer4
        '
        Me.Timer4.Interval = 10
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.TextBox45)
        Me.Panel6.Controls.Add(Me.ListBox2)
        Me.Panel6.Location = New System.Drawing.Point(420, -50)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(205, 50)
        Me.Panel6.TabIndex = 8
        '
        'TextBox45
        '
        Me.TextBox45.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox45.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox45.Location = New System.Drawing.Point(15, 12)
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.ReadOnly = True
        Me.TextBox45.Size = New System.Drawing.Size(174, 22)
        Me.TextBox45.TabIndex = 1
        Me.TextBox45.Text = "Liste des codes États"
        Me.TextBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer5
        '
        Me.Timer5.Interval = 1
        '
        'Timer6
        '
        Me.Timer6.Interval = 1500
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.TextBox40)
        Me.Panel7.Controls.Add(Me.ListBox3)
        Me.Panel7.Location = New System.Drawing.Point(405, -50)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(235, 50)
        Me.Panel7.TabIndex = 9
        '
        'TextBox40
        '
        Me.TextBox40.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox40.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox40.Location = New System.Drawing.Point(15, 12)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.ReadOnly = True
        Me.TextBox40.Size = New System.Drawing.Size(205, 22)
        Me.TextBox40.TabIndex = 22
        Me.TextBox40.Text = "Liste des codes Classes"
        Me.TextBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ListBox4
        '
        Me.ListBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox4.FormattingEnabled = True
        Me.ListBox4.ItemHeight = 16
        Me.ListBox4.Items.AddRange(New Object() {" A  " & Global.Microsoft.VisualBasic.ChrW(9) & "Australie", " B  " & Global.Microsoft.VisualBasic.ChrW(9) & "Belgique", " C  " & Global.Microsoft.VisualBasic.ChrW(9) & "Cuba", " D  " & Global.Microsoft.VisualBasic.ChrW(9) & "Allemagne", " E  " & Global.Microsoft.VisualBasic.ChrW(9) & "Espagne", " F  " & Global.Microsoft.VisualBasic.ChrW(9) & "France", " G  " & Global.Microsoft.VisualBasic.ChrW(9) & "Guinée", " H  " & Global.Microsoft.VisualBasic.ChrW(9) & "Hongrie", " I  " & Global.Microsoft.VisualBasic.ChrW(9) & "Italie", " N  " & Global.Microsoft.VisualBasic.ChrW(9) & "Norvège", " P  " & Global.Microsoft.VisualBasic.ChrW(9) & "Portugal", " Q  " & Global.Microsoft.VisualBasic.ChrW(9) & "Qatar", " R  " & Global.Microsoft.VisualBasic.ChrW(9) & "Roumanie", " S  " & Global.Microsoft.VisualBasic.ChrW(9) & "Suède", " T  " & Global.Microsoft.VisualBasic.ChrW(9) & "Thaïlande", " U  " & Global.Microsoft.VisualBasic.ChrW(9) & "Uruguay", " AF " & Global.Microsoft.VisualBasic.ChrW(9) & "Afghanistan", " AG " & Global.Microsoft.VisualBasic.ChrW(9) & "Algérie", " AL " & Global.Microsoft.VisualBasic.ChrW(9) & "Albanie", " AN " & Global.Microsoft.VisualBasic.ChrW(9) & "Angola", " AT " & Global.Microsoft.VisualBasic.ChrW(9) & "Antigua", " AZ " & Global.Microsoft.VisualBasic.ChrW(9) & "Ile des Açores", " BA " & Global.Microsoft.VisualBasic.ChrW(9) & "Birmanie", " BB " & Global.Microsoft.VisualBasic.ChrW(9) & "Barbade", " BE " & Global.Microsoft.VisualBasic.ChrW(9) & "Ile des Bermudes", " BG " & Global.Microsoft.VisualBasic.ChrW(9) & "Bulgarie", " BL " & Global.Microsoft.VisualBasic.ChrW(9) & "Belize", " BR " & Global.Microsoft.VisualBasic.ChrW(9) & "Brésil", " BS " & Global.Microsoft.VisualBasic.ChrW(9) & "Bahamas", " BW " & Global.Microsoft.VisualBasic.ChrW(9) & "Botswana", " BX " & Global.Microsoft.VisualBasic.ChrW(9) & "Bruneï", " CA " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles des Canaries", " CH " & Global.Microsoft.VisualBasic.ChrW(9) & "Suisse", " CI " & Global.Microsoft.VisualBasic.ChrW(9) & "Cote d'Ivoire", " CL " & Global.Microsoft.VisualBasic.ChrW(9) & "Sri Lanka", " CN " & Global.Microsoft.VisualBasic.ChrW(9) & "Congo", " CO " & Global.Microsoft.VisualBasic.ChrW(9) & "Colombie", " CR " & Global.Microsoft.VisualBasic.ChrW(9) & "Costa Rica", " CS " & Global.Microsoft.VisualBasic.ChrW(9) & "Tchécoslovaquie", " CY " & Global.Microsoft.VisualBasic.ChrW(9) & "Chypre", " DJ " & Global.Microsoft.VisualBasic.ChrW(9) & "Djibouti", " DK " & Global.Microsoft.VisualBasic.ChrW(9) & "Danemark", " EQ " & Global.Microsoft.VisualBasic.ChrW(9) & "Équateur", " ES " & Global.Microsoft.VisualBasic.ChrW(9) & "El Savador", " ET " & Global.Microsoft.VisualBasic.ChrW(9) & "Égypte", " FA " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Féroé", " FI " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Falkland", " FJ " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Fidji", " GB " & Global.Microsoft.VisualBasic.ChrW(9) & "Grande Bretagne", " GD " & Global.Microsoft.VisualBasic.ChrW(9) & "Guadeloupe", " GE " & Global.Microsoft.VisualBasic.ChrW(9) & "Groenland", " GH " & Global.Microsoft.VisualBasic.ChrW(9) & "Ghana", " GR " & Global.Microsoft.VisualBasic.ChrW(9) & "Grèce", " GS " & Global.Microsoft.VisualBasic.ChrW(9) & "Géorgie du Sud", " GU " & Global.Microsoft.VisualBasic.ChrW(9) & "Guinée - Bissau", " GY " & Global.Microsoft.VisualBasic.ChrW(9) & "Guyane", " HK " & Global.Microsoft.VisualBasic.ChrW(9) & "Honk Kong", " IL " & Global.Microsoft.VisualBasic.ChrW(9) & "Israël", " IR " & Global.Microsoft.VisualBasic.ChrW(9) & "Iran", " IS " & Global.Microsoft.VisualBasic.ChrW(9) & "Islande", " KI " & Global.Microsoft.VisualBasic.ChrW(9) & "Kiribati", " LY " & Global.Microsoft.VisualBasic.ChrW(9) & "Libye", " MA " & Global.Microsoft.VisualBasic.ChrW(9) & "Maroc", " MB " & Global.Microsoft.VisualBasic.ChrW(9) & "Mozambique", " MC " & Global.Microsoft.VisualBasic.ChrW(9) & "Monaco", " MG " & Global.Microsoft.VisualBasic.ChrW(9) & "Mongolie", " ML " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Marshall", " MS " & Global.Microsoft.VisualBasic.ChrW(9) & "Ile Maurice", " MV " & Global.Microsoft.VisualBasic.ChrW(9) & "Ile des Maldives", " MY " & Global.Microsoft.VisualBasic.ChrW(9) & "Myanmar", " NA " & Global.Microsoft.VisualBasic.ChrW(9) & "Antilles Pays-Bas", " NC " & Global.Microsoft.VisualBasic.ChrW(9) & "Nouvelle Calédonie", " NF " & Global.Microsoft.VisualBasic.ChrW(9) & "Ile Norfolk", " NK " & Global.Microsoft.VisualBasic.ChrW(9) & "Corée du Nord", " NL " & Global.Microsoft.VisualBasic.ChrW(9) & "Pays-Bas", " NZ " & Global.Microsoft.VisualBasic.ChrW(9) & "Nouvelle Zélande", " OM " & Global.Microsoft.VisualBasic.ChrW(9) & "Oman", " PA " & Global.Microsoft.VisualBasic.ChrW(9) & "Panama", " PE " & Global.Microsoft.VisualBasic.ChrW(9) & "Pérou", " PI " & Global.Microsoft.VisualBasic.ChrW(9) & "Philippines", " PL " & Global.Microsoft.VisualBasic.ChrW(9) & "Pologne", " PR " & Global.Microsoft.VisualBasic.ChrW(9) & "Porto Rico", " PY " & Global.Microsoft.VisualBasic.ChrW(9) & "Paraguay", " RA " & Global.Microsoft.VisualBasic.ChrW(9) & "Argentine", " RH " & Global.Microsoft.VisualBasic.ChrW(9) & "Haiti", " RI " & Global.Microsoft.VisualBasic.ChrW(9) & "Indonésie", " RL " & Global.Microsoft.VisualBasic.ChrW(9) & "Liban", " RU " & Global.Microsoft.VisualBasic.ChrW(9) & "Russie", " RW " & Global.Microsoft.VisualBasic.ChrW(9) & "Rwanda", " SA " & Global.Microsoft.VisualBasic.ChrW(9) & "Saé Tomé et Principe", " SE " & Global.Microsoft.VisualBasic.ChrW(9) & "Seychelles", " SF " & Global.Microsoft.VisualBasic.ChrW(9) & "Finlande", " SI " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Salomon", " SK " & Global.Microsoft.VisualBasic.ChrW(9) & "Corée du Sud", " SL " & Global.Microsoft.VisualBasic.ChrW(9) & "Sierra Léone", " SM " & Global.Microsoft.VisualBasic.ChrW(9) & "Maroc Espagnol", " SN " & Global.Microsoft.VisualBasic.ChrW(9) & "Sénégal", " SR " & Global.Microsoft.VisualBasic.ChrW(9) & "Suriname", " ST " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Saint Hélène", " SU " & Global.Microsoft.VisualBasic.ChrW(9) & "Russie", " SV " & Global.Microsoft.VisualBasic.ChrW(9) & "Saint Vincent", " TN " & Global.Microsoft.VisualBasic.ChrW(9) & "Tunisie", " TO " & Global.Microsoft.VisualBasic.ChrW(9) & "Tonga", " TP " & Global.Microsoft.VisualBasic.ChrW(9) & "Timor Oriental", " TR " & Global.Microsoft.VisualBasic.ChrW(9) & "Turquie", " TT " & Global.Microsoft.VisualBasic.ChrW(9) & "Trinité et Tobago", " UA " & Global.Microsoft.VisualBasic.ChrW(9) & "Émirats Arabes Unis", " UG " & Global.Microsoft.VisualBasic.ChrW(9) & "Ouganda", " VA " & Global.Microsoft.VisualBasic.ChrW(9) & "Vanuatu", " VN " & Global.Microsoft.VisualBasic.ChrW(9) & "Vietnam", " WF " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Wallis et Futuna", " WS " & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Samoa", " YU " & Global.Microsoft.VisualBasic.ChrW(9) & "Yougoslavie", " YV " & Global.Microsoft.VisualBasic.ChrW(9) & "Vénézuela", " ZA " & Global.Microsoft.VisualBasic.ChrW(9) & "Afrique du Sud", " ZB " & Global.Microsoft.VisualBasic.ChrW(9) & "Zimbabwe", " ZZ " & Global.Microsoft.VisualBasic.ChrW(9) & "Tanzanie", " ABW" & Global.Microsoft.VisualBasic.ChrW(9) & "Aruba", " AGO" & Global.Microsoft.VisualBasic.ChrW(9) & "Angola", " AGL" & Global.Microsoft.VisualBasic.ChrW(9) & "Anguilla", " AIA" & Global.Microsoft.VisualBasic.ChrW(9) & "Anguilla", " ALB" & Global.Microsoft.VisualBasic.ChrW(9) & "Albanie", " ALD" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Aland", " AND" & Global.Microsoft.VisualBasic.ChrW(9) & "Andorre", " ANT" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Néerlandaises", " ARE" & Global.Microsoft.VisualBasic.ChrW(9) & "Émirats Arabes Unis", " ARG" & Global.Microsoft.VisualBasic.ChrW(9) & "Argentine", " ARM" & Global.Microsoft.VisualBasic.ChrW(9) & "Arménie", " ARU" & Global.Microsoft.VisualBasic.ChrW(9) & "Aruba", " ATF" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Kerguélen", " ATG" & Global.Microsoft.VisualBasic.ChrW(9) & "Antigua et Barbude", " AUS" & Global.Microsoft.VisualBasic.ChrW(9) & "Australie", " AUT" & Global.Microsoft.VisualBasic.ChrW(9) & "Autriche", " AZB" & Global.Microsoft.VisualBasic.ChrW(9) & "Azerbaidjan", " AZE" & Global.Microsoft.VisualBasic.ChrW(9) & "Azerbaidjan", " BEL" & Global.Microsoft.VisualBasic.ChrW(9) & "Belgique", " BEN" & Global.Microsoft.VisualBasic.ChrW(9) & "Bénin", " BDI" & Global.Microsoft.VisualBasic.ChrW(9) & "Burundi", " BGD" & Global.Microsoft.VisualBasic.ChrW(9) & "Bangladesh", " BGR" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulgarie", " BHR" & Global.Microsoft.VisualBasic.ChrW(9) & "Bahrein", " BHS" & Global.Microsoft.VisualBasic.ChrW(9) & "Bahamas", " BIO" & Global.Microsoft.VisualBasic.ChrW(9) & "Territoires Anglais Océan Indien", " BKA" & Global.Microsoft.VisualBasic.ChrW(9) & "Burquina Fasso", " BLZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Belize", " BMU" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles des Bermudes", " BNG" & Global.Microsoft.VisualBasic.ChrW(9) & "Bangladesh", " BOL" & Global.Microsoft.VisualBasic.ChrW(9) & "Bolivie", " BOS" & Global.Microsoft.VisualBasic.ChrW(9) & "Bosnie Herzégovine", " BRA" & Global.Microsoft.VisualBasic.ChrW(9) & "Brésil", " BRB" & Global.Microsoft.VisualBasic.ChrW(9) & "Barbade", " BRN" & Global.Microsoft.VisualBasic.ChrW(9) & "Bruneï", " BRU" & Global.Microsoft.VisualBasic.ChrW(9) & "Biélorussie", " CAB" & Global.Microsoft.VisualBasic.ChrW(9) & "Cambodge", " CAM" & Global.Microsoft.VisualBasic.ChrW(9) & "Caméroun", " CAN" & Global.Microsoft.VisualBasic.ChrW(9) & "Canada", " CAY" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Caïman", " CHA" & Global.Microsoft.VisualBasic.ChrW(9) & "Tchad", " CHE" & Global.Microsoft.VisualBasic.ChrW(9) & "Suisse", " CHI" & Global.Microsoft.VisualBasic.ChrW(9) & "Chine", " CHL" & Global.Microsoft.VisualBasic.ChrW(9) & "Chili", " RCH" & Global.Microsoft.VisualBasic.ChrW(9) & "Chili", " CHN" & Global.Microsoft.VisualBasic.ChrW(9) & "République Populaire de Chine", " CIV" & Global.Microsoft.VisualBasic.ChrW(9) & "Cote d'Ivoire", " CMI" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Christmas", " CMR" & Global.Microsoft.VisualBasic.ChrW(9) & "Caméroun", " COC" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Cocos", " COG" & Global.Microsoft.VisualBasic.ChrW(9) & "Congo", " CON" & Global.Microsoft.VisualBasic.ChrW(9) & "Congo", " COL" & Global.Microsoft.VisualBasic.ChrW(9) & "Colombie", " COM" & Global.Microsoft.VisualBasic.ChrW(9) & "Comores", " COO" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Cook", " CPV" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles du Cap-Vert", " CVI" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles du Cap-Vert", " CRI" & Global.Microsoft.VisualBasic.ChrW(9) & "Costa Rica", " CRO" & Global.Microsoft.VisualBasic.ChrW(9) & "Croatie", " CSR" & Global.Microsoft.VisualBasic.ChrW(9) & "Définition en attente", " CUB" & Global.Microsoft.VisualBasic.ChrW(9) & "Cuba", " CUR" & Global.Microsoft.VisualBasic.ChrW(9) & "Définition en attente", " CYM" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Caïman", " CYP" & Global.Microsoft.VisualBasic.ChrW(9) & "Chypre", " CZE" & Global.Microsoft.VisualBasic.ChrW(9) & "République Tchèque", " CZR" & Global.Microsoft.VisualBasic.ChrW(9) & "République Tchèque", " DDR" & Global.Microsoft.VisualBasic.ChrW(9) & "Allemagne de l'Est", " DEU" & Global.Microsoft.VisualBasic.ChrW(9) & "Allemagne", " DIS" & Global.Microsoft.VisualBasic.ChrW(9) & "Registre International Danois", " DJI" & Global.Microsoft.VisualBasic.ChrW(9) & "Djibouti", " DMA" & Global.Microsoft.VisualBasic.ChrW(9) & "Dominique", " DMC" & Global.Microsoft.VisualBasic.ChrW(9) & "Dominique", " DNK" & Global.Microsoft.VisualBasic.ChrW(9) & "Danemark", " DOM" & Global.Microsoft.VisualBasic.ChrW(9) & "République Dominicaine", " DZA" & Global.Microsoft.VisualBasic.ChrW(9) & "Algérie", " ECU" & Global.Microsoft.VisualBasic.ChrW(9) & "Équateur", " EGY" & Global.Microsoft.VisualBasic.ChrW(9) & "Égypte", " EGU" & Global.Microsoft.VisualBasic.ChrW(9) & "Guinée Équatoriale", " ERI" & Global.Microsoft.VisualBasic.ChrW(9) & "Érythrée", " ESP" & Global.Microsoft.VisualBasic.ChrW(9) & "Espagne", " EST" & Global.Microsoft.VisualBasic.ChrW(9) & "Estonie", " ETH" & Global.Microsoft.VisualBasic.ChrW(9) & "Éthiopie", " FAS" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Féroé", " FRO" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Féroé", " FDR" & Global.Microsoft.VisualBasic.ChrW(9) & "Allemagne de l'Est", " FGU" & Global.Microsoft.VisualBasic.ChrW(9) & "Guyane", " FIN" & Global.Microsoft.VisualBasic.ChrW(9) & "Finlande", " FIS" & Global.Microsoft.VisualBasic.ChrW(9) & "France(FIS)", " FRA" & Global.Microsoft.VisualBasic.ChrW(9) & "France", " FJI" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Fidji", " FNR" & Global.Microsoft.VisualBasic.ChrW(9) & "Définition en attente", " FPO" & Global.Microsoft.VisualBasic.ChrW(9) & "Polynésie Française", " FUN" & Global.Microsoft.VisualBasic.ChrW(9) & "Pavillon Inconnu", " GAB" & Global.Microsoft.VisualBasic.ChrW(9) & "Gabon", " GAM" & Global.Microsoft.VisualBasic.ChrW(9) & "Gambie", " GBR" & Global.Microsoft.VisualBasic.ChrW(9) & "Grande Bretagne", " GBC" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Anglo-Normandes", " GCA" & Global.Microsoft.VisualBasic.ChrW(9) & "Guatémala", " GEO" & Global.Microsoft.VisualBasic.ChrW(9) & "Géorgie", " GHA" & Global.Microsoft.VisualBasic.ChrW(9) & "Ghana", " GIB" & Global.Microsoft.VisualBasic.ChrW(9) & "Gibraltar", " GIN" & Global.Microsoft.VisualBasic.ChrW(9) & "Guinée", " GMB" & Global.Microsoft.VisualBasic.ChrW(9) & "Gambie", " GNB" & Global.Microsoft.VisualBasic.ChrW(9) & "Guinée Bissau", " GNQ" & Global.Microsoft.VisualBasic.ChrW(9) & "Guinée Équatoriale", " GRC" & Global.Microsoft.VisualBasic.ChrW(9) & "Grèce", " GRD" & Global.Microsoft.VisualBasic.ChrW(9) & "Grenade", " GRL" & Global.Microsoft.VisualBasic.ChrW(9) & "Groenland", " GRN" & Global.Microsoft.VisualBasic.ChrW(9) & "Grenade", " GTM" & Global.Microsoft.VisualBasic.ChrW(9) & "Guatémala", " GUE" & Global.Microsoft.VisualBasic.ChrW(9) & "Définition en attente", " GUY" & Global.Microsoft.VisualBasic.ChrW(9) & "Guyana", " HKG" & Global.Microsoft.VisualBasic.ChrW(9) & "Hong Kong", " HND" & Global.Microsoft.VisualBasic.ChrW(9) & "Honduras", " HON" & Global.Microsoft.VisualBasic.ChrW(9) & "Honduras", " GUA" & Global.Microsoft.VisualBasic.ChrW(9) & "Ile de Guam", " HRV" & Global.Microsoft.VisualBasic.ChrW(9) & "Croatie", " HTI" & Global.Microsoft.VisualBasic.ChrW(9) & "Haiti", " HUN" & Global.Microsoft.VisualBasic.ChrW(9) & "Hongrie", " IBN" & Global.Microsoft.VisualBasic.ChrW(9) & "Arabie Saoudite", " IDN" & Global.Microsoft.VisualBasic.ChrW(9) & "Indonésie", " IND" & Global.Microsoft.VisualBasic.ChrW(9) & "Inde", " INT" & Global.Microsoft.VisualBasic.ChrW(9) & "Compagnies Internationales", " IOM" & Global.Microsoft.VisualBasic.ChrW(9) & "Ile de Man", " IRL" & Global.Microsoft.VisualBasic.ChrW(9) & "République d'Irlande", " IRN" & Global.Microsoft.VisualBasic.ChrW(9) & "Iran", " IRQ" & Global.Microsoft.VisualBasic.ChrW(9) & "Irak", " ISL" & Global.Microsoft.VisualBasic.ChrW(9) & "Islande", " ISR" & Global.Microsoft.VisualBasic.ChrW(9) & "Israël", " ITA" & Global.Microsoft.VisualBasic.ChrW(9) & "Italie", " JAM" & Global.Microsoft.VisualBasic.ChrW(9) & "Jamaïque", " JAP" & Global.Microsoft.VisualBasic.ChrW(9) & "Japon", " JER" & Global.Microsoft.VisualBasic.ChrW(9) & "Définition en attente", " JPN" & Global.Microsoft.VisualBasic.ChrW(9) & "Japon", " JOR" & Global.Microsoft.VisualBasic.ChrW(9) & "Jordanie", " KAZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Kazakhstan", " KEN" & Global.Microsoft.VisualBasic.ChrW(9) & "Kenya", " KER" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Kerguélen", " KHM" & Global.Microsoft.VisualBasic.ChrW(9) & "Cambodge", " KIR" & Global.Microsoft.VisualBasic.ChrW(9) & "Kiribati (Anciennes Iles Gilbert)", " KOR" & Global.Microsoft.VisualBasic.ChrW(9) & "Corée du Nord", " KWT" & Global.Microsoft.VisualBasic.ChrW(9) & "Koweit", " LAO" & Global.Microsoft.VisualBasic.ChrW(9) & "Laos", " LAT" & Global.Microsoft.VisualBasic.ChrW(9) & "Léttonie", " LBN" & Global.Microsoft.VisualBasic.ChrW(9) & "Lebanon", " LBR" & Global.Microsoft.VisualBasic.ChrW(9) & "Libéria", " LIB" & Global.Microsoft.VisualBasic.ChrW(9) & "Libéria", " LBY" & Global.Microsoft.VisualBasic.ChrW(9) & "Libye", " LCA" & Global.Microsoft.VisualBasic.ChrW(9) & "Sainte Lucie", " LIC" & Global.Microsoft.VisualBasic.ChrW(9) & "Liechtenstein", " LIT" & Global.Microsoft.VisualBasic.ChrW(9) & "Lithuanie", " LTU" & Global.Microsoft.VisualBasic.ChrW(9) & "Lithuanie", " LKA" & Global.Microsoft.VisualBasic.ChrW(9) & "Sri Lanka", " LUX" & Global.Microsoft.VisualBasic.ChrW(9) & "Luxembourg", " LVA" & Global.Microsoft.VisualBasic.ChrW(9) & "Léttonie", " MAC" & Global.Microsoft.VisualBasic.ChrW(9) & "Macao", " MAD" & Global.Microsoft.VisualBasic.ChrW(9) & "Madeire", " MAP" & Global.Microsoft.VisualBasic.ChrW(9) & "Madeire(MAR)", " MAR" & Global.Microsoft.VisualBasic.ChrW(9) & "Martinique", " MAS" & Global.Microsoft.VisualBasic.ChrW(9) & "Madagascar", " MDG" & Global.Microsoft.VisualBasic.ChrW(9) & "Madagascar", " MAU" & Global.Microsoft.VisualBasic.ChrW(9) & "Mauritanie", " MCD" & Global.Microsoft.VisualBasic.ChrW(9) & "Macédonie", " MCO" & Global.Microsoft.VisualBasic.ChrW(9) & "Monaco", " MDV" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Maldives", " MEX" & Global.Microsoft.VisualBasic.ChrW(9) & "Mexique", " MHL" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Marshall", " MIC" & Global.Microsoft.VisualBasic.ChrW(9) & "Micronésie", " MLI" & Global.Microsoft.VisualBasic.ChrW(9) & "Mali", " MLT" & Global.Microsoft.VisualBasic.ChrW(9) & "Malte", " MLW" & Global.Microsoft.VisualBasic.ChrW(9) & "Malawi", " MMR" & Global.Microsoft.VisualBasic.ChrW(9) & "Myanmar (Birmanie)", " MNG" & Global.Microsoft.VisualBasic.ChrW(9) & "Mongolie", " MOL" & Global.Microsoft.VisualBasic.ChrW(9) & "Moldavie", " MON" & Global.Microsoft.VisualBasic.ChrW(9) & "Montserrat", " MOZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Mozambique", " MRA" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Mariannes du Nord", " MRT" & Global.Microsoft.VisualBasic.ChrW(9) & "Mauritanie", " MTN" & Global.Microsoft.VisualBasic.ChrW(9) & "Monténégro", " MUS" & Global.Microsoft.VisualBasic.ChrW(9) & "Ile Maurice", " MYS" & Global.Microsoft.VisualBasic.ChrW(9) & "Malaisie", " MYT" & Global.Microsoft.VisualBasic.ChrW(9) & "Mayotte", " NAM" & Global.Microsoft.VisualBasic.ChrW(9) & "Namibie", " NAU" & Global.Microsoft.VisualBasic.ChrW(9) & "Nauru", " NGA" & Global.Microsoft.VisualBasic.ChrW(9) & "Nigéria", " WAN" & Global.Microsoft.VisualBasic.ChrW(9) & "Nigéria", " NGR" & Global.Microsoft.VisualBasic.ChrW(9) & "Niger", " NIG" & Global.Microsoft.VisualBasic.ChrW(9) & "Niger", " NIC" & Global.Microsoft.VisualBasic.ChrW(9) & "Nicaragua", " NIS" & Global.Microsoft.VisualBasic.ChrW(9) & "Registre International Norvégien", " NIU" & Global.Microsoft.VisualBasic.ChrW(9) & "Niue", " NLD" & Global.Microsoft.VisualBasic.ChrW(9) & "Pays-Bas", " NOR" & Global.Microsoft.VisualBasic.ChrW(9) & "Norvège", " NPL" & Global.Microsoft.VisualBasic.ChrW(9) & "Népal", " NRU" & Global.Microsoft.VisualBasic.ChrW(9) & "Nauru", " NZL" & Global.Microsoft.VisualBasic.ChrW(9) & "Nouvelle Zélande", " OMN" & Global.Microsoft.VisualBasic.ChrW(9) & "Oman", " PAK" & Global.Microsoft.VisualBasic.ChrW(9) & "Pakistan", " PAN" & Global.Microsoft.VisualBasic.ChrW(9) & "Panama", " PER" & Global.Microsoft.VisualBasic.ChrW(9) & "Pérou", " PHL" & Global.Microsoft.VisualBasic.ChrW(9) & "Philippines", " PLW" & Global.Microsoft.VisualBasic.ChrW(9) & "Palaos", " PMD" & Global.Microsoft.VisualBasic.ChrW(9) & "Madère", " PNG" & Global.Microsoft.VisualBasic.ChrW(9) & "Papouasie Nouvelle Guinée", " POL" & Global.Microsoft.VisualBasic.ChrW(9) & "Pologne", " PRK" & Global.Microsoft.VisualBasic.ChrW(9) & "Corée du Nord", " PRT" & Global.Microsoft.VisualBasic.ChrW(9) & "Portugal", " PRY" & Global.Microsoft.VisualBasic.ChrW(9) & "Paraguay", " PTM" & Global.Microsoft.VisualBasic.ChrW(9) & "Malaysie", " QAT" & Global.Microsoft.VisualBasic.ChrW(9) & "Qatar", " REU" & Global.Microsoft.VisualBasic.ChrW(9) & "Réunion", " ROM" & Global.Microsoft.VisualBasic.ChrW(9) & "Roumanie", " RUS" & Global.Microsoft.VisualBasic.ChrW(9) & "Russie", " SAM" & Global.Microsoft.VisualBasic.ChrW(9) & "Samoa Américaines", " WSM" & Global.Microsoft.VisualBasic.ChrW(9) & "Samoa Occidentales", " SAU" & Global.Microsoft.VisualBasic.ChrW(9) & "Arabie Saoudite", " SDN" & Global.Microsoft.VisualBasic.ChrW(9) & "Soudan", " SEM" & Global.Microsoft.VisualBasic.ChrW(9) & "Serbie et Monténégro", " SEN" & Global.Microsoft.VisualBasic.ChrW(9) & "Sénégal", " SGP" & Global.Microsoft.VisualBasic.ChrW(9) & "Singapour", " SKN" & Global.Microsoft.VisualBasic.ChrW(9) & "Saint Christophe et Niévès", " SLB" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Salomon", " SLE" & Global.Microsoft.VisualBasic.ChrW(9) & "Sierra Léone", " SLO" & Global.Microsoft.VisualBasic.ChrW(9) & "Slovénie", " SLU" & Global.Microsoft.VisualBasic.ChrW(9) & "Sainte Lucie", " SLV" & Global.Microsoft.VisualBasic.ChrW(9) & "Slovéquie", " SMA" & Global.Microsoft.VisualBasic.ChrW(9) & "San Marin", " SNG" & Global.Microsoft.VisualBasic.ChrW(9) & "Singapour", " SOM" & Global.Microsoft.VisualBasic.ChrW(9) & "Somalie", " SPM" & Global.Microsoft.VisualBasic.ChrW(9) & "Saint Pierre et Miquelon", " SRB" & Global.Microsoft.VisualBasic.ChrW(9) & "Serbie", " STP" & Global.Microsoft.VisualBasic.ChrW(9) & "Sao Tome et Principe", " SUD" & Global.Microsoft.VisualBasic.ChrW(9) & "Soudan", " SUR" & Global.Microsoft.VisualBasic.ChrW(9) & "Surinam", " SVB" & Global.Microsoft.VisualBasic.ChrW(9) & "Spitzberg", " SVK" & Global.Microsoft.VisualBasic.ChrW(9) & "Slovaquie", " SVN" & Global.Microsoft.VisualBasic.ChrW(9) & "Slovénie", " SWE" & Global.Microsoft.VisualBasic.ChrW(9) & "Suède", " SYC" & Global.Microsoft.VisualBasic.ChrW(9) & "Seychelles", " SYR" & Global.Microsoft.VisualBasic.ChrW(9) & "Syrie", " TAH" & Global.Microsoft.VisualBasic.ChrW(9) & "Tahiti", " TAI" & Global.Microsoft.VisualBasic.ChrW(9) & "Taïwan", " TCI" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Turques et Caïques", " TGO" & Global.Microsoft.VisualBasic.ChrW(9) & "Togo", " THA" & Global.Microsoft.VisualBasic.ChrW(9) & "Thaïlande", " TKM" & Global.Microsoft.VisualBasic.ChrW(9) & "Turkménistan", " TOG" & Global.Microsoft.VisualBasic.ChrW(9) & "Togo", " TON" & Global.Microsoft.VisualBasic.ChrW(9) & "Tonga", " TTO" & Global.Microsoft.VisualBasic.ChrW(9) & "Trinité et Tobago", " TUN" & Global.Microsoft.VisualBasic.ChrW(9) & "Tunisie", " TUR" & Global.Microsoft.VisualBasic.ChrW(9) & "Turquie", " TUV" & Global.Microsoft.VisualBasic.ChrW(9) & "Tuvalu", " TWN" & Global.Microsoft.VisualBasic.ChrW(9) & "Taïwan", " TZA" & Global.Microsoft.VisualBasic.ChrW(9) & "Tanzanie", " TZZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Définition en attente", " UGA" & Global.Microsoft.VisualBasic.ChrW(9) & "Ouganda", " UKR" & Global.Microsoft.VisualBasic.ChrW(9) & "Ukraine", " URY" & Global.Microsoft.VisualBasic.ChrW(9) & "Uruguay", " USA" & Global.Microsoft.VisualBasic.ChrW(9) & "États Unis", " UZB" & Global.Microsoft.VisualBasic.ChrW(9) & "Ousbékistan", " VCT" & Global.Microsoft.VisualBasic.ChrW(9) & "Saint Vincent et Grenadines", " VEN" & Global.Microsoft.VisualBasic.ChrW(9) & "Vénézuela", " VGB" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Vierges", " VIB" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Vierges (Grande Bretagne)", " VIU" & Global.Microsoft.VisualBasic.ChrW(9) & "Iles Vierges (États Unis)", " VNM" & Global.Microsoft.VisualBasic.ChrW(9) & "Vietnam", " VUT" & Global.Microsoft.VisualBasic.ChrW(9) & "Vanuatu", " WSA" & Global.Microsoft.VisualBasic.ChrW(9) & "Sahara Occidental", " YEM" & Global.Microsoft.VisualBasic.ChrW(9) & "Yémen", " ZAF" & Global.Microsoft.VisualBasic.ChrW(9) & "Afrique du Sud", " ZAM" & Global.Microsoft.VisualBasic.ChrW(9) & "Zambie", " ZAR" & Global.Microsoft.VisualBasic.ChrW(9) & "Zaïre"})
        Me.ListBox4.Location = New System.Drawing.Point(15, 35)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(295, 612)
        Me.ListBox4.TabIndex = 21
        Me.ListBox4.Visible = False
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.TextBox43)
        Me.Panel8.Controls.Add(Me.ListBox4)
        Me.Panel8.Location = New System.Drawing.Point(360, -50)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(325, 50)
        Me.Panel8.TabIndex = 10
        '
        'TextBox43
        '
        Me.TextBox43.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox43.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox43.Location = New System.Drawing.Point(15, 12)
        Me.TextBox43.Name = "TextBox43"
        Me.TextBox43.ReadOnly = True
        Me.TextBox43.Size = New System.Drawing.Size(295, 22)
        Me.TextBox43.TabIndex = 22
        Me.TextBox43.Text = "Liste des Pavillons"
        Me.TextBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer7
        '
        '
        'ListBox5
        '
        Me.ListBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox5.FormattingEnabled = True
        Me.ListBox5.ItemHeight = 16
        Me.ListBox5.Items.AddRange(New Object() {" AA" & Global.Microsoft.VisualBasic.ChrW(9) & "Accommodation Vessel", " AB" & Global.Microsoft.VisualBasic.ChrW(9) & "Ro-Ro Barge", " AC" & Global.Microsoft.VisualBasic.ChrW(9) & "Container Barge", " AD" & Global.Microsoft.VisualBasic.ChrW(9) & "Drilling Barge", " AH" & Global.Microsoft.VisualBasic.ChrW(9) & "Hopper Barge", " AJ" & Global.Microsoft.VisualBasic.ChrW(9) & "Asphalt Barge", " AM" & Global.Microsoft.VisualBasic.ChrW(9) & "Cement Storage Barge", " AN" & Global.Microsoft.VisualBasic.ChrW(9) & "Barge", " AO" & Global.Microsoft.VisualBasic.ChrW(9) & "Oil Barge", " AP" & Global.Microsoft.VisualBasic.ChrW(9) & "Pusher Tug", " AR" & Global.Microsoft.VisualBasic.ChrW(9) & "Crane/Derrick Barge", " AT" & Global.Microsoft.VisualBasic.ChrW(9) & "Tug & Barge Combination", " AU" & Global.Microsoft.VisualBasic.ChrW(9) & "Oil Storage Barge", " AV" & Global.Microsoft.VisualBasic.ChrW(9) & "Transshipment Vessel", " AZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Pontoon", " BC" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk/Container Carrier", " BH" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk Wood Chip Carrier", " BK" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk Limestone Carrier", " BL" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulker - Great Lakes Only", " BM" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk Cement Carrier", " BN" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulker", " BO" & Global.Microsoft.VisualBasic.ChrW(9) & "Ore Carrier", " BS" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk Carrier Ore Strengthened", " BV" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk Vehicle Carrier", " BX" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulker Great Lakes only / Dumb", " BZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulker Open Hatch", " CF" & Global.Microsoft.VisualBasic.ChrW(9) & "Container Ship/all Reefer", " CN" & Global.Microsoft.VisualBasic.ChrW(9) & "Container Ship", " CR" & Global.Microsoft.VisualBasic.ChrW(9) & "Container/Reefer General Cargo", " DA" & Global.Microsoft.VisualBasic.ChrW(9) & "Semi-Sub Heavy Lift Vessel", " DC" & Global.Microsoft.VisualBasic.ChrW(9) & "Multi-Purpose Ship", " DH" & Global.Microsoft.VisualBasic.ChrW(9) & "Heavy Lift Ship", " DK" & Global.Microsoft.VisualBasic.ChrW(9) & "Livestock Carrier", " DL" & Global.Microsoft.VisualBasic.ChrW(9) & "Log Tipping Vessel", " DN" & Global.Microsoft.VisualBasic.ChrW(9) & "General Cargo Ship", " DR" & Global.Microsoft.VisualBasic.ChrW(9) & "General Cargo/Part Refrigerated Ship", " DS" & Global.Microsoft.VisualBasic.ChrW(9) & "Deck Cargo Ship", " DY" & Global.Microsoft.VisualBasic.ChrW(9) & "Yacht Carrier", " FL" & Global.Microsoft.VisualBasic.ChrW(9) & "Live Fish Carrier", " FP" & Global.Microsoft.VisualBasic.ChrW(9) & "Fisheries Protection Vessel", " FR" & Global.Microsoft.VisualBasic.ChrW(9) & "Fisheries Research", " FS" & Global.Microsoft.VisualBasic.ChrW(9) & "Sealing Vessel", " FT" & Global.Microsoft.VisualBasic.ChrW(9) & "Fisheries Training", " FV" & Global.Microsoft.VisualBasic.ChrW(9) & "Fishing Vessel", " FW" & Global.Microsoft.VisualBasic.ChrW(9) & "Whaling Vessel", " GB" & Global.Microsoft.VisualBasic.ChrW(9) & "Bucket Dredger", " GC" & Global.Microsoft.VisualBasic.ChrW(9) & "Cutter Suction Dredger", " GD" & Global.Microsoft.VisualBasic.ChrW(9) & "Dragger Dredger", " GF" & Global.Microsoft.VisualBasic.ChrW(9) & "Hopper Suction Dredger", " GG" & Global.Microsoft.VisualBasic.ChrW(9) & "Grab Dredger", " GH" & Global.Microsoft.VisualBasic.ChrW(9) & "Dipper Dredger", " GK" & Global.Microsoft.VisualBasic.ChrW(9) & "Cutter Dredger", " GL" & Global.Microsoft.VisualBasic.ChrW(9) & "Sand Loading Dredger", " GN" & Global.Microsoft.VisualBasic.ChrW(9) & "Dredger", " GS" & Global.Microsoft.VisualBasic.ChrW(9) & "Suction Dredger", " IB" & Global.Microsoft.VisualBasic.ChrW(9) & "Icebreaker/Buoy Tender", " IN" & Global.Microsoft.VisualBasic.ChrW(9) & "Icebreaker", " IR" & Global.Microsoft.VisualBasic.ChrW(9) & "Icebreaker/Research Vessel", " IS" & Global.Microsoft.VisualBasic.ChrW(9) & "Salvage Vessel", " IT" & Global.Microsoft.VisualBasic.ChrW(9) & "Salvage Tug", " IZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Search And Rescue Vessel", " JN" & Global.Microsoft.VisualBasic.ChrW(9) & "Training Vessel", " JP" & Global.Microsoft.VisualBasic.ChrW(9) & "Pleasure Craft", " JX" & Global.Microsoft.VisualBasic.ChrW(9) & "Sail Training Vessel", " LN" & Global.Microsoft.VisualBasic.ChrW(9) & "Cable Ship", " LZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Cable Repair Ship", " MA" & Global.Microsoft.VisualBasic.ChrW(9) & "Product/Ore/Bulk/Oil", " MB" & Global.Microsoft.VisualBasic.ChrW(9) & "Ore/Bulk/Oil Carrier", " MN" & Global.Microsoft.VisualBasic.ChrW(9) & "Bulk/Oil Carrier", " MS" & Global.Microsoft.VisualBasic.ChrW(9) & "Ore/Oil Carrier", " NA" & Global.Microsoft.VisualBasic.ChrW(9) & "Anti-Pollution Vessel", " NB" & Global.Microsoft.VisualBasic.ChrW(9) & "Buoy Tender", " NC" & Global.Microsoft.VisualBasic.ChrW(9) & "Pilot Vessel", " ND" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Dock", " NE" & Global.Microsoft.VisualBasic.ChrW(9) & "Maintenance/Utility Vessel", " NF" & Global.Microsoft.VisualBasic.ChrW(9) & "Fire Fighting Vessel", " NG" & Global.Microsoft.VisualBasic.ChrW(9) & "Desalination Vessel", " NL" & Global.Microsoft.VisualBasic.ChrW(9) & "Lightship", " NP" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Nuclear Power Station", " NR" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Wave Powered Power Station", " NS" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Power Station", " NT" & Global.Microsoft.VisualBasic.ChrW(9) & "Tug", " NU" & Global.Microsoft.VisualBasic.ChrW(9) & "Patrol Vessel", " NV" & Global.Microsoft.VisualBasic.ChrW(9) & "Naval Vessel", " NW" & Global.Microsoft.VisualBasic.ChrW(9) & "Radioactive Waste Carrier", " OA" & Global.Microsoft.VisualBasic.ChrW(9) & "Anchor Handling/Tug/Supply", " OB" & Global.Microsoft.VisualBasic.ChrW(9) & "Anchor Handling/Tug", " OC" & Global.Microsoft.VisualBasic.ChrW(9) & "Crewboat", " OD" & Global.Microsoft.VisualBasic.ChrW(9) & "Aht/Salvage", " OE" & Global.Microsoft.VisualBasic.ChrW(9) & "Diving Support Vessel", " OG" & Global.Microsoft.VisualBasic.ChrW(9) & "Pipe Carrier/Platform Supply", " OI" & Global.Microsoft.VisualBasic.ChrW(9) & "Seismic Survey Vessel", " OJ" & Global.Microsoft.VisualBasic.ChrW(9) & "Safety Standby Vessel", " OK" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore Maintenance/Utility Vessel", " OL" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore Cargo Barge", " OR" & Global.Microsoft.VisualBasic.ChrW(9) & "Survey Ship Rov Support", " OS" & Global.Microsoft.VisualBasic.ChrW(9) & "Supply Vessel", " OV" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore Support Vessel", " PC" & Global.Microsoft.VisualBasic.ChrW(9) & "Multi-Hull Passenger Ferry", " PD" & Global.Microsoft.VisualBasic.ChrW(9) & "Multi-Hull Passenger/Vehicle Ferry", " PE" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger Excursion Vessel", " PF" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger/Cargo Ship", " PG" & Global.Microsoft.VisualBasic.ChrW(9) & "Casino Ship", " PH" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger Hovercraft", " PI" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger/Vehicle Hovercraft", " PM" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger/Train/Vehicle Ferry", " PN" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger Vessel", " PR" & Global.Microsoft.VisualBasic.ChrW(9) & "River Cruise Ship", " PS" & Global.Microsoft.VisualBasic.ChrW(9) & "Surface Effect Passenger Ferry", " PT" & Global.Microsoft.VisualBasic.ChrW(9) & "Surface Effect Passenger/Vehicle Ferry", " PU" & Global.Microsoft.VisualBasic.ChrW(9) & "Cruise Ship", " PV" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger/Vehicle Ferry", " PY" & Global.Microsoft.VisualBasic.ChrW(9) & "Passenger Hydrofoil", " QA" & Global.Microsoft.VisualBasic.ChrW(9) & "Aircraft Transport", " QB" & Global.Microsoft.VisualBasic.ChrW(9) & "Hatch Cover Carrier", " QC" & Global.Microsoft.VisualBasic.ChrW(9) & "Hospital Ship", " QE" & Global.Microsoft.VisualBasic.ChrW(9) & "Geophysical Research Vessel", " QN" & Global.Microsoft.VisualBasic.ChrW(9) & "Research Vessel", " QP" & Global.Microsoft.VisualBasic.ChrW(9) & "Polar Research Vessel", " QR" & Global.Microsoft.VisualBasic.ChrW(9) & "Radio Station", " QS" & Global.Microsoft.VisualBasic.ChrW(9) & "Oceanographic Vessel", " QT" & Global.Microsoft.VisualBasic.ChrW(9) & "Satellite Tracking Ship", " QV" & Global.Microsoft.VisualBasic.ChrW(9) & "Survey Ship", " QW" & Global.Microsoft.VisualBasic.ChrW(9) & "Weather Ship", " QX" & Global.Microsoft.VisualBasic.ChrW(9) & "Rocket Launch Vessel", " QY" & Global.Microsoft.VisualBasic.ChrW(9) & "Survey/Research Vessel", " RF" & Global.Microsoft.VisualBasic.ChrW(9) & "Reefer", " RH" & Global.Microsoft.VisualBasic.ChrW(9) & "Refrigerated Fish Carrier", " SD" & Global.Microsoft.VisualBasic.ChrW(9) & "Spent Nuclear Fuel Carrier", " SF" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Hotel", " SM" & Global.Microsoft.VisualBasic.ChrW(9) & "Museum Ship", " SP" & Global.Microsoft.VisualBasic.ChrW(9) & "Support Ship", " SQ" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Car Park", " SR" & Global.Microsoft.VisualBasic.ChrW(9) & "Restaurant Ship", " SV" & Global.Microsoft.VisualBasic.ChrW(9) & "Logistics Vessel", " SX" & Global.Microsoft.VisualBasic.ChrW(9) & "Exhibition Vessel", " SZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Houseboat", " TA" & Global.Microsoft.VisualBasic.ChrW(9) & "Asphalt Tanker", " TB" & Global.Microsoft.VisualBasic.ChrW(9) & "Bitumen Tanker", " TC" & Global.Microsoft.VisualBasic.ChrW(9) & "Chemical Tanker", " TD" & Global.Microsoft.VisualBasic.ChrW(9) & "Fruit Juice Tanker", " TG" & Global.Microsoft.VisualBasic.ChrW(9) & "LPG Carrier", " TH" & Global.Microsoft.VisualBasic.ChrW(9) & "LNG Carrier", " TI" & Global.Microsoft.VisualBasic.ChrW(9) & "Storage Tanker", " TK" & Global.Microsoft.VisualBasic.ChrW(9) & "Bunker Tanker", " TN" & Global.Microsoft.VisualBasic.ChrW(9) & "Tanker", " TO" & Global.Microsoft.VisualBasic.ChrW(9) & "Crude Oil Tanker", " TP" & Global.Microsoft.VisualBasic.ChrW(9) & "Parcels Tanker", " TQ" & Global.Microsoft.VisualBasic.ChrW(9) & "Chemical/Oil Tanker", " TR" & Global.Microsoft.VisualBasic.ChrW(9) & "Products Tanker", " TS" & Global.Microsoft.VisualBasic.ChrW(9) & "Replenishment Tanker", " TT" & Global.Microsoft.VisualBasic.ChrW(9) & "Water Tanker", " TW" & Global.Microsoft.VisualBasic.ChrW(9) & "Wine Tanker", " TY" & Global.Microsoft.VisualBasic.ChrW(9) & "Ethylene Tanker", " TZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Sulphur Tanker", " UN" & Global.Microsoft.VisualBasic.ChrW(9) & "Unknown", " WA" & Global.Microsoft.VisualBasic.ChrW(9) & "Ro-Ro", " WB" & Global.Microsoft.VisualBasic.ChrW(9) & "Ro-Lo", " WC" & Global.Microsoft.VisualBasic.ChrW(9) & "Multi Hull Ro-Ro Freight", " WD" & Global.Microsoft.VisualBasic.ChrW(9) & "Vehicle Carrier", " WF" & Global.Microsoft.VisualBasic.ChrW(9) & "Pallet Vessel", " WG" & Global.Microsoft.VisualBasic.ChrW(9) & "Barge Carrier", " WH" & Global.Microsoft.VisualBasic.ChrW(9) & "Ro-Ro/Heavylift", " WL" & Global.Microsoft.VisualBasic.ChrW(9) & "Ro-Ro/Cellular", " WN" & Global.Microsoft.VisualBasic.ChrW(9) & "Ro-Ro/General Cargo", " XC" & Global.Microsoft.VisualBasic.ChrW(9) & "Tank Cleaning Vessel", " XD" & Global.Microsoft.VisualBasic.ChrW(9) & "Waste Disposal Vessel(Liq)", " XI" & Global.Microsoft.VisualBasic.ChrW(9) & "Incinerator & Waste Disposal Vessel", " XM" & Global.Microsoft.VisualBasic.ChrW(9) & "Diamond Mining Vessel", " XP" & Global.Microsoft.VisualBasic.ChrW(9) & "Pile Driving Vessel", " XS" & Global.Microsoft.VisualBasic.ChrW(9) & "Sludge Carrier", " XW" & Global.Microsoft.VisualBasic.ChrW(9) & "Self Elevating Wind Turbine Installation Vessel", " XY" & Global.Microsoft.VisualBasic.ChrW(9) & "SWATH Wind Turbine Installation Vessel", " ZA" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore Accommodation Vessel", " ZB" & Global.Microsoft.VisualBasic.ChrW(9) & "Multi Function Service Vessel", " ZC" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore Construction Vessel", " ZD" & Global.Microsoft.VisualBasic.ChrW(9) & "Rock Laying Ship", " ZE" & Global.Microsoft.VisualBasic.ChrW(9) & "Self-Elevating Mobile Offshore Drilling Unit", " ZF" & Global.Microsoft.VisualBasic.ChrW(9) & "FPSO", " ZG" & Global.Microsoft.VisualBasic.ChrW(9) & "Oil Well Stimulation Vessel", " ZH" & Global.Microsoft.VisualBasic.ChrW(9) & "Self-Elevating Production Unit", " ZN" & Global.Microsoft.VisualBasic.ChrW(9) & "Drillship", " ZO" & Global.Microsoft.VisualBasic.ChrW(9) & "Offshore Drilling Barge", " ZP" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Production Unit", " ZS" & Global.Microsoft.VisualBasic.ChrW(9) & "Floating Storage Offtake", " ZT" & Global.Microsoft.VisualBasic.ChrW(9) & "Oil Well Production Test Vessel", " ZU" & Global.Microsoft.VisualBasic.ChrW(9) & "Semi Submersible Mobile Offshore Drilling Unit", " ZV" & Global.Microsoft.VisualBasic.ChrW(9) & "Pipelay Vessel", " ZX" & Global.Microsoft.VisualBasic.ChrW(9) & "Semi-Sub Pipe Laybarge", " ZY" & Global.Microsoft.VisualBasic.ChrW(9) & "Pipe Laybarge", " ZZ" & Global.Microsoft.VisualBasic.ChrW(9) & "Trenching Vessel"})
        Me.ListBox5.Location = New System.Drawing.Point(15, 35)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(375, 612)
        Me.ListBox5.TabIndex = 21
        Me.ListBox5.Visible = False
        '
        'Panel9
        '
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.TextBox44)
        Me.Panel9.Controls.Add(Me.ListBox5)
        Me.Panel9.Location = New System.Drawing.Point(320, -50)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(406, 50)
        Me.Panel9.TabIndex = 11
        '
        'TextBox44
        '
        Me.TextBox44.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox44.Cursor = System.Windows.Forms.Cursors.Default
        Me.TextBox44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox44.Location = New System.Drawing.Point(15, 12)
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.ReadOnly = True
        Me.TextBox44.Size = New System.Drawing.Size(375, 22)
        Me.TextBox44.TabIndex = 22
        Me.TextBox44.Text = "Liste des codes de Type"
        Me.TextBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer8
        '
        Me.Timer8.Interval = 1
        '
        'Timer9
        '
        Me.Timer9.Interval = 1
        '
        'Timer10
        '
        Me.Timer10.Interval = 1
        '
        'Timer11
        '
        Me.Timer11.Interval = 1
        '
        'Extracteur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1045, 663)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button15)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Extracteur"
        Me.Opacity = 0
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Extracteur des informations de la Base FAIRPLAY"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox36 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox35 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox34 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox33 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox32 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox31 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox30 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox29 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox41 As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox38 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox37 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox42 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents Timer4 As System.Windows.Forms.Timer
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents Timer5 As System.Windows.Forms.Timer
    Friend WithEvents Timer6 As System.Windows.Forms.Timer
    Friend WithEvents TextBox39 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents TextBox40 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents TextBox43 As System.Windows.Forms.TextBox
    Friend WithEvents Timer7 As System.Windows.Forms.Timer
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents TextBox44 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox45 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox46 As System.Windows.Forms.TextBox
    Friend WithEvents Timer8 As System.Windows.Forms.Timer
    Friend WithEvents Timer9 As System.Windows.Forms.Timer
    Friend WithEvents Timer10 As System.Windows.Forms.Timer
    Friend WithEvents Timer11 As System.Windows.Forms.Timer
    Friend WithEvents TextBox49 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox48 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox47 As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
