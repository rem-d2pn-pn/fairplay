﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class Analyseur

    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub Analyseur_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        '==============================================
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la page
    Private Sub Analyseur_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' ==================================
        ' Affichage état de l'application
        If Variables.EtatAppli = "Problème Base" Or Variables.EtatAppli = "Base non chargée" Then
            EtatAppliRouge()
            ' ==================================
            ' Pointeur flèche
            Cursor = Cursors.Default
        End If
        ' ==================================
        ' Affichage état de l'application
        If Variables.EtatAppli = "Base chargée" Then
            EtatAppliVert()
        End If
        ' ==================================
        ' Affichage du nombre de ligne dans le module Analyseur
        If Val(TextBox43.Text) <> Variables.FinBase Then
            EffaceChamps()
        End If
        TextBox43.Text = Variables.FinBase
        '==============================================
        ' Mise à jour du titre
        Me.Text = " Analyseur des informations de la Base FAIRPLAY - " & Variables.NomBaseTitre
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Bouton Nombre d'informations par colonne
    ' Traitement appui 
    Private Sub Button1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button1.MouseDown
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche en cours"
        EtatAppliRouge()
    End Sub
    ' Traitement relachement
    Private Sub Button1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button1.MouseUp
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Définition des variables
        Dim i01 As Integer
        Dim i02 As Integer
        Dim i03 As Integer
        Dim i04 As Integer
        Dim i05 As Integer
        Dim i06 As Integer
        Dim i07 As Integer
        Dim i08 As Integer
        Dim i09 As Integer
        Dim i10 As Integer
        Dim i11 As Integer
        Dim i12 As Integer
        Dim i13 As Integer
        Dim i14 As Integer
        Dim i15 As Integer
        Dim i16 As Integer
        Dim i17 As Integer
        Dim i18 As Integer
        Dim i19 As Integer
        Dim i20 As Integer
        Dim i21 As Integer
        Dim i22 As Integer
        Dim i23 As Integer
        Dim i24 As Integer
        Dim i25 As Integer
        Dim i26 As Integer
        Dim i27 As Integer
        Dim i28 As Integer
        Dim i29 As Integer
        Dim i30 As Integer
        Dim i31 As Integer
        Dim i32 As Integer
        Dim i33 As Integer
        Dim i34 As Integer
        Dim i35 As Integer
        Dim i36 As Integer
        Dim i37 As Integer
        Dim i38 As Integer
        Dim i39 As Integer
        Dim i40 As Integer
        Dim i41 As Integer
        Dim i42 As Integer
        ' ==================================
        ' Incrémentation des compteurs respectifs
        For i = 1 To Variables.FinBase
            If Variables.Base(i, 1) <> "" Then i01 = i01 + 1
            If Variables.Base(i, 2) <> "" Then i02 = i02 + 1
            If Variables.Base(i, 3) <> "" Then i03 = i03 + 1
            If Variables.Base(i, 4) <> "" Then i04 = i04 + 1
            If Variables.Base(i, 5) <> "" Then i05 = i05 + 1
            If Variables.Base(i, 6) <> "" Then i06 = i06 + 1
            If Variables.Base(i, 7) <> "" Then i07 = i07 + 1
            If Variables.Base(i, 8) <> "" Then i08 = i08 + 1
            If Variables.Base(i, 9) <> "" Then i09 = i09 + 1
            If Variables.Base(i, 10) <> "" Then i10 = i10 + 1

            If Variables.Base(i, 11) <> "" Then i11 = i11 + 1
            If Variables.Base(i, 12) <> "" Then i12 = i12 + 1
            If Variables.Base(i, 13) <> "" Then i13 = i13 + 1
            If Variables.Base(i, 14) <> "" Then i14 = i14 + 1
            If Variables.Base(i, 15) <> "" Then i15 = i15 + 1
            If Variables.Base(i, 16) <> "" Then i16 = i16 + 1
            If Variables.Base(i, 17) <> "" Then i17 = i17 + 1
            If Variables.Base(i, 18) <> "" Then i18 = i18 + 1
            If Variables.Base(i, 19) <> "" Then i19 = i19 + 1

            If Variables.Base(i, 20) <> "" Then i20 = i20 + 1
            If Variables.Base(i, 21) <> "" Then i21 = i21 + 1
            If Variables.Base(i, 22) <> "" Then i22 = i22 + 1
            If Variables.Base(i, 23) <> "" Then i23 = i23 + 1
            If Variables.Base(i, 24) <> "" Then i24 = i24 + 1
            If Variables.Base(i, 25) <> "" Then i25 = i25 + 1
            If Variables.Base(i, 26) <> "" Then i26 = i26 + 1
            If Variables.Base(i, 27) <> "" Then i27 = i27 + 1
            If Variables.Base(i, 28) <> "" Then i28 = i28 + 1
            If Variables.Base(i, 29) <> "" Then i29 = i29 + 1

            If Variables.Base(i, 30) <> "" Then i30 = i30 + 1
            If Variables.Base(i, 31) <> "" Then i31 = i31 + 1
            If Variables.Base(i, 32) <> "" Then i32 = i32 + 1
            If Variables.Base(i, 33) <> "" Then i33 = i33 + 1
            If Variables.Base(i, 34) <> "" Then i34 = i34 + 1
            If Variables.Base(i, 35) <> "" Then i35 = i35 + 1
            If Variables.Base(i, 36) <> "" Then i36 = i36 + 1
            If Variables.Base(i, 37) <> "" Then i37 = i37 + 1
            If Variables.Base(i, 38) <> "" Then i38 = i38 + 1
            If Variables.Base(i, 39) <> "" Then i39 = i39 + 1

            If Variables.Base(i, 40) <> "" Then i40 = i40 + 1
            If Variables.Base(i, 41) <> "" Then i41 = i41 + 1
            If Variables.Base(i, 42) <> "" Then i42 = i42 + 1
        Next
        ' ==================================
        ' Affectation des compteurs aux champs correspondants
        TextBox1.Text = i01
        TextBox2.Text = i02
        TextBox3.Text = i03
        TextBox4.Text = i04
        TextBox5.Text = i05
        TextBox6.Text = i06
        TextBox7.Text = i07
        TextBox8.Text = i08
        TextBox9.Text = i09
        TextBox10.Text = i10

        TextBox11.Text = i11
        TextBox12.Text = i12
        TextBox13.Text = i13
        TextBox14.Text = i14
        TextBox15.Text = i15
        TextBox16.Text = i16
        TextBox17.Text = i17
        TextBox18.Text = i18
        TextBox19.Text = i19
        TextBox20.Text = i20

        TextBox21.Text = i21
        TextBox22.Text = i22
        TextBox23.Text = i23
        TextBox24.Text = i24
        TextBox25.Text = i25
        TextBox26.Text = i26
        TextBox27.Text = i27
        TextBox28.Text = i28
        TextBox29.Text = i29
        TextBox30.Text = i30

        TextBox31.Text = i31
        TextBox32.Text = i32
        TextBox33.Text = i33
        TextBox34.Text = i34
        TextBox35.Text = i35
        TextBox36.Text = i36
        TextBox37.Text = i37
        TextBox38.Text = i38
        TextBox39.Text = i39
        TextBox40.Text = i40

        TextBox41.Text = i41
        TextBox42.Text = i42
        ' ==================================
        ' Effacement du champ "Réponse/Nombre de réponse"
        TextBox46.Text = ""
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche terminée"
        EtatAppliVert()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton Recherche d'une information égale pour une colonne
    ' Traitement appui 
    Private Sub Button2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button2.MouseDown
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné
        If TextBox45.Text = "" Then Exit Sub
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche en cours"
        EtatAppliRouge()
    End Sub
    ' Traitement relachement
    Private Sub Button2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button2.MouseUp
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné avec message d'avertissement
        If TextBox45.Text = "" Then
            MsgBox(" Vous devez renseigner le N° de Colonne ! ", MsgBoxStyle.Exclamation, " Absence Numéro de Colonne ")
            Exit Sub
        End If
        ' ==================================
        ' Définition des variables
        Dim Colonne As Integer
        Dim i As Integer
        ' ==================================
        ' Initialisation de la variable
        Colonne = TextBox45.Text
        ' ==================================
        ' RAZ de la colonne 0
        RazColonneTriBase()
        ' ==================================
        ' Initialisation de la variable
        Variables.IndexBase = 0
        ' ==================================
        ' Effacement du champ "Réponse/Nombre de réponse"
        TextBox46.Text = ""
        ' ==================================
        ' Initialisation de la variable
        Variables.NombreRéponse = 0
        ' ==================================
        ' Recherche d'égalité
        For i = 1 To Variables.FinBase
            ' ==================================
            ' Comparaison d'égalité en ne tenant pas compte des majuscules et minuscules
            If StrComp(Variables.Base(i, Colonne), TextBox44.Text, CompareMethod.Text) = 0 Then
                ' ==================================
                ' Incrémentation de l'index de base
                Variables.IndexBase = Variables.IndexBase + 1
                ' ==================================
                ' Rangement dans la colonne 0 du numéro de la ligne égale à la recherche
                Variables.Base(Variables.IndexBase, 0) = i
                ' ==================================
                ' Incrémentation du nombre de réponse
                Variables.NombreRéponse = Variables.NombreRéponse + 1
            End If
        Next
        ' ==================================
        ' Vérification s'il y a eu au moins une réponse
        If Variables.IndexBase > 0 Then
            ' ==================================
            ' Positionnement de l'index de base sur la première réponse
            Variables.IndexBase = 1
            ' ==================================
            ' Affichage des informations correspondants à la première réponse
            InfosIndexBase()
        End If
        ' ==================================
        ' Vérification s'il y a eu au moins une réponse
        If Variables.NombreRéponse > 0 Then
            ' ==================================
            ' Affichage de la réponse visualisée sur le nombre de réponse
            TextBox46.Text = Variables.IndexBase & " / " & Variables.NombreRéponse
        Else
            ' ==================================
            ' Affichage s'il n'y a pas eu de réponse
            TextBox46.Text = "0 / 0"
        End If
        ' ==================================
        ' Désactivation des boutons "Valeurs minimale et maximale"
        Button5.Enabled = False
        Button6.Enabled = False
        ' ==================================
        ' Activation du bouton "Valeur minimale"
        If Variables.NombreRéponse > 1 Then
            Button6.Enabled = True
        End If
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche terminée"
        EtatAppliVert()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton Recherche d'une information comprise pour une colonne
    ' Traitement appui 
    Private Sub Button4_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button4.MouseDown
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné
        If TextBox45.Text = "" Then Exit Sub
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche en cours"
        EtatAppliRouge()
    End Sub
    ' Traitement relachement
    Private Sub Button4_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button4.MouseUp
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné avec message d'avertissement
        If TextBox45.Text = "" Then
            MsgBox(" Vous devez renseigner le N° de Colonne ! ", MsgBoxStyle.Exclamation, " Absence Numéro de Colonne ")
            Exit Sub
        End If
        ' ==================================
        ' Définition des variables
        Dim Infos As String
        Dim Colonne As Integer
        Dim i As Integer
        ' ==================================
        ' Initialisation des variables
        Infos = TextBox44.Text
        Colonne = TextBox45.Text
        ' ==================================
        ' RAZ de la colonne 0
        RazColonneTriBase()
        ' ==================================
        ' Initialisation de la variable
        Variables.IndexBase = 0
        ' ==================================
        ' Effacement du champ "Réponse/Nombre de réponse"
        TextBox46.Text = ""
        ' ==================================
        ' Initialisation de la variable
        Variables.NombreRéponse = 0
        ' ==================================
        ' Recherche d'une partie d'égalité
        For i = 0 To Variables.FinBase
            ' ==================================
            ' Vérification de la présence de l'information recherchée en ne tenant pas compte des majuscules et minuscules
            If Strings.InStr(Variables.Base(i, Colonne), TextBox44.Text, CompareMethod.Text) > 0 Then
                ' ==================================
                ' Incrémentation de l'index de base
                Variables.IndexBase = Variables.IndexBase + 1
                ' ==================================
                ' Rangement dans la colonne 0 du numéro de la ligne égale à la recherche
                Variables.Base(Variables.IndexBase, 0) = i
                ' ==================================
                ' Incrémentation du nombre de réponse
                Variables.NombreRéponse = Variables.NombreRéponse + 1
            End If
        Next
        ' ==================================
        ' Vérification s'il y a eu au moins une réponse
        If Variables.IndexBase > 0 Then
            ' ==================================
            ' Positionnement de l'index de base sur la première réponse
            Variables.IndexBase = 1
            ' ==================================
            ' Affichage des informations correspondants à la première réponse
            InfosIndexBase()
        End If
        ' ==================================
        ' Vérification s'il y a eu au moins une réponse
        If Variables.NombreRéponse > 0 Then
            ' ==================================
            ' Affichage de la réponse visualisée sur le nombre de réponse
            TextBox46.Text = Variables.IndexBase & " / " & Variables.NombreRéponse
        Else
            ' ==================================
            ' Affichage s'il n'y a pas eu de réponse
            TextBox46.Text = "0 / 0"
        End If
        ' ==================================
        ' Désactivation des boutons "Valeurs minimale et maximale"
        Button5.Enabled = False
        Button6.Enabled = False
        ' ==================================
        ' Activation du bouton "Valeur maximale"
        If Variables.NombreRéponse > 1 Then
            Button6.Enabled = True
        End If
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche terminée"
        EtatAppliVert()
    End Sub

    ' ==============================================================================================================================================
    ' Remise à zéro de la colonne de tri dans la base
    Private Sub RazColonneTriBase()
        ' ==================================
        ' Définition des variables
        Dim i As Integer
        ' ==================================
        ' RAZ de la colonne 0 de la base utilisée comme colonne de tri
        For i = 0 To Variables.FinBase
            Variables.Base(i, 0) = 0
        Next
    End Sub

    ' ==============================================================================================================================================
    ' Affichage des informations d'un bateau en fonction de l'index de la base
    Private Sub InfosIndexBase()
        ' ==================================
        ' Définition des variables
        Dim i As Integer
        ' ==================================
        ' Chargement de la variable avec le numéro de l'index de la table pointé à la colonne 0
        i = Variables.Base(Variables.IndexBase, 0)
        ' ==================================
        ' Chargement des champs avec les données de la base correspondants
        TextBox1.Text = Variables.Base(i, 1)
        TextBox2.Text = Variables.Base(i, 2)
        TextBox3.Text = Variables.Base(i, 3)
        TextBox4.Text = Variables.Base(i, 4)
        TextBox5.Text = Variables.Base(i, 5)
        TextBox6.Text = Variables.Base(i, 6)
        TextBox7.Text = Variables.Base(i, 7)
        TextBox8.Text = Variables.Base(i, 8)
        TextBox9.Text = Variables.Base(i, 9)
        TextBox10.Text = Variables.Base(i, 10)

        TextBox11.Text = Variables.Base(i, 11)
        TextBox12.Text = Variables.Base(i, 12)
        TextBox13.Text = Variables.Base(i, 13)
        TextBox14.Text = Variables.Base(i, 14)
        TextBox15.Text = Variables.Base(i, 15)
        TextBox16.Text = Variables.Base(i, 16)
        TextBox17.Text = Variables.Base(i, 17)
        TextBox18.Text = Variables.Base(i, 18)
        TextBox19.Text = Variables.Base(i, 19)
        TextBox20.Text = Variables.Base(i, 20)

        TextBox21.Text = Variables.Base(i, 21)
        TextBox22.Text = Variables.Base(i, 22)
        TextBox23.Text = Variables.Base(i, 23)
        TextBox24.Text = Variables.Base(i, 24)
        TextBox25.Text = Variables.Base(i, 25)
        TextBox26.Text = Variables.Base(i, 26)
        TextBox27.Text = Variables.Base(i, 27)
        TextBox28.Text = Variables.Base(i, 28)
        TextBox29.Text = Variables.Base(i, 29)
        TextBox30.Text = Variables.Base(i, 30)

        TextBox31.Text = Variables.Base(i, 31)
        TextBox32.Text = Variables.Base(i, 32)
        TextBox33.Text = Variables.Base(i, 33)
        TextBox34.Text = Variables.Base(i, 34)
        TextBox35.Text = Variables.Base(i, 35)
        TextBox36.Text = Variables.Base(i, 36)
        TextBox37.Text = Variables.Base(i, 37)
        TextBox38.Text = Variables.Base(i, 38)
        TextBox39.Text = Variables.Base(i, 39)
        TextBox40.Text = Variables.Base(i, 40)

        TextBox41.Text = Variables.Base(i, 41)
        TextBox42.Text = Variables.Base(i, 42)
    End Sub

    ' ==============================================================================================================================================
    ' Bouton arrière de la présentation d'une réponse
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Activation du bouton "Valeur maximale"
        Button6.Enabled = True
        ' ==================================
        ' Décrémentation de l'index de base
        Variables.IndexBase = Variables.IndexBase - 1
        ' ==================================
        ' Affichage jusqu'à première réponse atteinte
        If Variables.IndexBase >= 1 Then
            InfosIndexBase()
        End If
        ' ==================================
        ' Désactivation du bouton "Valeur minimale"
        If Variables.IndexBase = 1 Then
            Button5.Enabled = False
        End If
        ' ==================================
        ' Affichage de la réponse visualisée sur le nombre de réponse
        TextBox46.Text = Variables.IndexBase & " / " & Variables.NombreRéponse
    End Sub

    ' ==============================================================================================================================================
    ' Bouton avance de la présentation d'une réponse
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Activation du bouton "Valeur minimale"
        Button5.Enabled = True
        ' ==================================
        ' Incrémentation de l'index de base
        Variables.IndexBase = Variables.IndexBase + 1
        ' ==================================
        ' Affichage si dernière réponse non atteinte
        If Variables.IndexBase <= Variables.NombreRéponse Then
            InfosIndexBase()
        End If
        ' ==================================
        ' Désactivation du bouton "Valeur maximale"
        If Variables.IndexBase = Variables.NombreRéponse Then
            Button6.Enabled = False
        End If
        ' ==================================
        ' Affichage de la réponse visualisée sur le nombre de réponse
        TextBox46.Text = Variables.IndexBase & " / " & Variables.NombreRéponse
    End Sub

    ' ==============================================================================================================================================
    ' Bouton de Sélection Numéro Colonne / Nom Colonne
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ====================================================================================
        ' Affichage Numéro Colonne si le texte du bouton le demande
        If Button3.Text = "Numéro Colonne" Then

            GroupBox1.Text = " Colonne 1 "
            GroupBox2.Text = " Colonne 2 "
            GroupBox3.Text = " Colonne 3 "
            GroupBox4.Text = " Colonne 4 "
            GroupBox5.Text = " Colonne 5 "
            GroupBox6.Text = " Colonne 6 "
            GroupBox7.Text = " Colonne 7 "
            GroupBox8.Text = " Colonne 8 "
            GroupBox9.Text = " Colonne 9 "
            GroupBox10.Text = " Colonne 10 "

            GroupBox11.Text = " Colonne 11 "
            GroupBox12.Text = " Colonne 12 "
            GroupBox13.Text = " Colonne 13 "
            GroupBox14.Text = " Colonne 14 "
            GroupBox15.Text = " Colonne 15 "
            GroupBox16.Text = " Colonne 16 "
            GroupBox17.Text = " Colonne 17 "
            GroupBox18.Text = " Colonne 18 "
            GroupBox19.Text = " Colonne 19 "
            GroupBox20.Text = " Colonne 20 "

            GroupBox21.Text = " Colonne 21 "
            GroupBox22.Text = " Colonne 22 "
            GroupBox23.Text = " Colonne 23 "
            GroupBox24.Text = " Colonne 24 "
            GroupBox25.Text = " Colonne 25 "
            GroupBox26.Text = " Colonne 26 "
            GroupBox27.Text = " Colonne 27 "
            GroupBox28.Text = " Colonne 28 "
            GroupBox29.Text = " Colonne 29 "
            GroupBox30.Text = " Colonne 30 "

            GroupBox31.Text = " Colonne 31 "
            GroupBox32.Text = " Colonne 32 "
            GroupBox33.Text = " Colonne 33 "
            GroupBox34.Text = " Colonne 34 "
            GroupBox35.Text = " Colonne 35 "
            GroupBox36.Text = " Colonne 36 "
            GroupBox37.Text = " Colonne 37 "
            GroupBox38.Text = " Colonne 38 "
            GroupBox39.Text = " Colonne 39 "
            GroupBox40.Text = " Colonne 40 "

            GroupBox41.Text = " Colonne 41 "
            GroupBox42.Text = " Colonne 42 "
            ' ==================================
            ' Changement du texte du bouton
            Button3.Text = "Nom Colonne"
            Exit Sub
        End If

        ' ====================================================================================
        ' Affichage Nom Colonne si le texte du bouton le demande

        GroupBox1.Text = " N° FAIRPLAY "
        GroupBox2.Text = " Ex n° FAIRPLAY "
        GroupBox3.Text = " Code état du Navire "
        GroupBox4.Text = " Classe du Navire "
        GroupBox5.Text = " Nom du Navire "
        GroupBox6.Text = " Pavillon du Navire "
        GroupBox7.Text = " Nom du Propriétaire "
        GroupBox8.Text = " Date d'achat "
        GroupBox9.Text = " Port en lourd "
        GroupBox10.Text = " Tonnage brut "

        GroupBox11.Text = " N° LLOYDS "
        GroupBox12.Text = " Date de livraison "
        GroupBox13.Text = " Longueur "
        GroupBox14.Text = " Largeur "
        GroupBox15.Text = " Tirant d'eau "
        GroupBox16.Text = " Profondeur "
        GroupBox17.Text = " Vitesse "
        GroupBox18.Text = " Date perte / destruction "
        GroupBox19.Text = " Capacité en céréales "
        GroupBox20.Text = " Capacité en balles "

        GroupBox21.Text = " Capacité en liquide "
        GroupBox22.Text = " Capacité frigorifique "
        GroupBox23.Text = " Capacité conteneurs "
        GroupBox24.Text = " Arrangement des conteneurs "
        GroupBox25.Text = " Nombre de conteneurs "
        GroupBox26.Text = " Nombre de cales "
        GroupBox27.Text = " Nombre d'écoutilles "
        GroupBox28.Text = " Nombre de réservoirs "
        GroupBox29.Text = " Capacité en minerai "
        GroupBox30.Text = " Porte latérale bâbord "

        GroupBox31.Text = " Porte latérale tribord "
        GroupBox32.Text = " Nombre de passagers "
        GroupBox33.Text = " Route fréquentée "
        GroupBox34.Text = " Barges amarrables "
        GroupBox35.Text = " Nombre de ponts "
        GroupBox36.Text = " FHMS "
        GroupBox37.Text = " Nombre de voitures "
        GroupBox38.Text = " Nombre de remorques "
        GroupBox39.Text = " Répartition voiture / remorque "
        GroupBox40.Text = " Nombre de wagons "

        GroupBox41.Text = " Type du Navire "
        GroupBox42.Text = " Hauteur totale "
        ' ==================================
        ' Changement du texte du bouton
        Button3.Text = "Numéro Colonne"

    End Sub

    ' ==============================================================================================================================================
    ' Bouton de fermeture de l'application
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        '==============================================
        ' Demande de fermeture par Fondu
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton valeur minimale d'une colonne
    ' Traitement appui 
    Private Sub Button8_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button8.MouseDown
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné
        If TextBox45.Text = "" Then Exit Sub
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche en cours"
        EtatAppliRouge()
    End Sub
    ' Traitement relachement
    Private Sub Button8_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button8.MouseUp
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné avec message d'avertissement
        If TextBox45.Text = "" Then
            MsgBox(" Vous devez renseigner le N° de Colonne ! ", MsgBoxStyle.Exclamation, " Absence Numéro de Colonne ")
            Exit Sub
        End If
        ' ==================================
        ' Définition des variables
        Dim Colonne As Integer
        Dim i As Integer
        Dim Min As Double
        Dim InfoMin As String = ""
        ' ==================================
        ' Initialisation de la variable
        Colonne = TextBox45.Text
        ' ==================================
        ' Effacement du champ "Réponse/Nombre de réponse"
        TextBox46.Text = ""
        ' ==================================
        ' Désactivation des boutons "Valeurs minimale et maximale"
        Button5.Enabled = False
        Button6.Enabled = False
        ' ==================================
        ' Initialisation de la variable
        Min = -1
        ' ==================================
        ' Trouver une valeur éventuellement différente de 0 pour Min
        For i = 1 To Variables.FinBase
            ' ==================================
            ' Recherche de l'existence d'une valeur
            If Variables.Base(i, Colonne) <> "" Then
                ' ==================================
                ' Recherche de la première valeur minimale de la base
                If Min < Val(Variables.Base(i, Colonne)) Then
                    ' ==================================
                    ' Affectation de la valeur trouvée
                    Min = Val(Variables.Base(i, Colonne))
                End If
                ' ==================================
                ' Sortie de la boucle
                Exit For
            End If
        Next
        ' ==================================
        ' Recherche de la valeur minimale de la colonne
        For i = 1 To Variables.FinBase
            ' ==================================
            ' Recherche de l'existence d'une valeur
            If Variables.Base(i, Colonne) <> "" Then
                ' ==================================
                ' Comparaison d'infériorité
                If Min >= Val(Variables.Base(i, Colonne)) Then
                    ' ==================================
                    ' Affectation de la valeur trouvée
                    Min = Val(Variables.Base(i, Colonne))
                    ' ==================================
                    ' Affectation de la valeur trouvée
                    InfoMin = Variables.Base(i, Colonne)
                End If
            End If
        Next
        ' ==================================
        ' Information trouvée dans le champ "Information recherchée"
        TextBox44.Text = InfoMin
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche terminée"
        EtatAppliVert()
    End Sub

    ' ==============================================================================================================================================
    ' Bouton valeur maximale d'une colonne
    ' Traitement appui 
    Private Sub Button9_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button9.MouseDown
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné
        If TextBox45.Text = "" Then Exit Sub
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche en cours"
        EtatAppliRouge()
    End Sub
    ' Traitement relachement
    Private Sub Button9_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Button9.MouseUp
        ' ==================================
        ' Renvoi du select sur le bouton caché
        Button10.Select()
        ' ==================================
        ' Sortie si le numéro de colonne n'est pas renseigné avec message d'avertissement
        If TextBox45.Text = "" Then
            MsgBox(" Vous devez renseigner le N° de Colonne ! ", MsgBoxStyle.Exclamation, " Absence Numéro de Colonne ")
            Exit Sub
        End If
        ' ==================================
        ' Définition des variables
        Dim Colonne As Integer
        Dim i As Integer
        Dim Max As Double
        Dim InfoMax As String = ""
        ' ==================================
        ' Initialisation de la variable
        Colonne = TextBox45.Text
        ' ==================================
        ' Effacement du champ "Réponse/Nombre de réponse"
        TextBox46.Text = ""
        ' ==================================
        ' Désactivation des boutons "Valeurs minimale et maximale"
        Button5.Enabled = False
        Button6.Enabled = False
        ' ==================================
        ' Initialisation de la variable
        Max = 0
        ' ==================================
        ' Recherche de la valeur maximale de la colonne
        For i = 1 To Variables.FinBase
            ' ==================================
            ' Recherche de l'existence d'une valeur
            If Variables.Base(i, Colonne) <> "" Then
                ' ==================================
                ' Comparaison de supériorité
                If Max <= Val(Variables.Base(i, Colonne)) Then
                    ' ==================================
                    ' Affectation de la valeur trouvée
                    Max = Val(Variables.Base(i, Colonne))
                    ' ==================================
                    ' Affectation de la valeur trouvée
                    InfoMax = Variables.Base(i, Colonne)
                End If
            End If
        Next
        ' ==================================
        ' Information trouvée dans le champ "Information recherchée"
        TextBox44.Text = InfoMax
        ' ==================================
        ' Affichage état de l'application
        Variables.EtatAppli = "Recherche terminée"
        EtatAppliVert()
    End Sub

    ' ==============================================================================================================================================
    ' Invalidation des boutons Information Égale et Contenue si N° Colonne non renseignée
    ' Invalidation des boutons Valeur Minimale et Maximale si N° Colonne non numérique
    Private Sub TextBox45_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox45.TextChanged
        ' ==================================
        ' Désactivation des boutons "Valeurs minimale et maximale"
        Button8.Enabled = False : Button9.Enabled = False
        ' ==================================
        ' Vérification de l'activation des boutons "Valeurs minimale et maximale"
        If TextBox45.Text = "1" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "2" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "4" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "9" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "10" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "11" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "13" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "14" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "15" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "16" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "17" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "19" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "20" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "21" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "22" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "23" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "24" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "25" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "26" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "27" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "28" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "29" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "30" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "31" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "32" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "34" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "35" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "36" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "37" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "38" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "40" Then Button8.Enabled = True : Button9.Enabled = True
        If TextBox45.Text = "42" Then Button8.Enabled = True : Button9.Enabled = True
        ' ==================================
        ' Désactivation des boutons "Informations égale et contenue"
        Button2.Enabled = False : Button4.Enabled = False
        ' ==================================
        ' Vérification de l'activation des boutons "Informations égale et contenue"
        If TextBox45.Text <> "" Then Button2.Enabled = True : Button4.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Affichage "État de l'application - rouge"
    Private Sub EtatAppliRouge()
        ' Information de la variable dans le champ
        TextBox47.Text = Variables.EtatAppli
        ' ==================================
        ' Colorisation rouge clair
        TextBox47.BackColor = Color.FromArgb(255, 200, 200)
        ' ==================================
        ' Pointeur sablier
        Cursor = Cursors.WaitCursor
    End Sub

    ' ==============================================================================================================================================
    ' Affichage "État de l'application - vert"
    Private Sub EtatAppliVert()
        ' Information de la variable dans le champ
        TextBox47.Text = Variables.EtatAppli
        ' ==================================
        ' Colorisation vert clair
        TextBox47.BackColor = Color.FromArgb(200, 255, 200)
        ' ==================================
        ' Pointeur flèche
        Cursor = Cursors.Default
    End Sub

    ' ==============================================================================================================================================
    ' Suppression du curseur dans les champs en lecture seule par sélection du bouton caché
    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox6.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox7.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox8.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox10.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox11_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox11.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox12_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox13_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox13.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox14_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox14.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox15.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox16.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox17.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox18.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox19.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox20_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox20.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox21_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox21.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox22_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox22.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox23_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox23.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox24_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox24.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox25_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox25.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox26_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox26.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox27_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox27.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox28_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox28.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox29_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox29.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox30_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox30.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox31_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox31.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox32_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox32.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox33_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox33.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox34_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox34.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox35_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox35.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox36_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox36.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox37_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox37.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox38_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox38.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox39_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox39.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox40_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox40.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox41_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox41.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox42_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox42.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox43_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox43.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox46_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox46.GotFocus
        Button10.Select()
    End Sub
    Private Sub TextBox47_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox47.GotFocus
        Button10.Select()
    End Sub

    '=============================================================================================================================================
    ' Effacement des champs
    Private Sub EffaceChamps()
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        TextBox17.Text = ""
        TextBox18.Text = ""
        TextBox19.Text = ""
        TextBox20.Text = ""
        TextBox21.Text = ""
        TextBox22.Text = ""
        TextBox23.Text = ""
        TextBox24.Text = ""
        TextBox25.Text = ""
        TextBox26.Text = ""
        TextBox27.Text = ""
        TextBox28.Text = ""
        TextBox29.Text = ""
        TextBox30.Text = ""
        TextBox31.Text = ""
        TextBox32.Text = ""
        TextBox33.Text = ""
        TextBox34.Text = ""
        TextBox35.Text = ""
        TextBox36.Text = ""
        TextBox37.Text = ""
        TextBox38.Text = ""
        TextBox39.Text = ""
        TextBox40.Text = ""
        TextBox41.Text = ""
        TextBox42.Text = ""
        TextBox43.Text = ""
        TextBox44.Text = ""
        TextBox45.Text = ""
        TextBox46.Text = ""
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub Analyseur_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE D'OUVERTURE ET DE FERMETURE
    '=============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Form1.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    '=============================================================================================================================================
    ' PROCÉDURE DE RÉDUCTION DES FENÊTRES
    '=============================================================================================================================================
    Private Sub Analyseur_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        ' ============================================
        ' Passage de la feuille principale transparente quand on réduit cette feuille
        If Me.WindowState = 1 Then
            Form1.Opacity = 0
        End If
        ' ============================================
        ' Passage de la feuille principale opaque quand on rétablit cette feuille
        If Me.WindowState = 0 Then
            Form1.Opacity = 1
        End If
    End Sub

End Class
