﻿' Importation des systèmes utilisés
Imports System
Imports System.IO
Imports System.IO.Directory
Imports System.Text

Public Class ChgtConfig
    '=============================================================================================================================================
    ' Procédure lors de la fermeture de la feuille par la croix
    Private Sub ChgtConfig_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Blocage de la procédure de fermeture automatique
        e.Cancel = True
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure au chargement de la feuille
    Private Sub ChgtConfig_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Recherche des fichiers de Base
        ListBox1.Items.Clear()
        For Each FichiersConfig As String In My.Computer.FileSystem.GetFiles(My.Computer.FileSystem.CurrentDirectory, FileIO.SearchOption.SearchTopLevelOnly, "*.cfg")
            ' ==================================
            ' Mise en forme des noms des fichiers trouvés
            FichiersConfig = "   " & Strings.Right(FichiersConfig, Len(FichiersConfig) - Strings.InStrRev(FichiersConfig, "\"))
            FichiersConfig = Strings.Left(FichiersConfig, Len(FichiersConfig) - 4)
            ' ==================================
            ' Rangement des noms de fichiers dans la liste
            ListBox1.Items.Add(FichiersConfig)
        Next
        '==============================================
        ' Demande d'ouverture par Fondu
        Me.Opacity = 0
        Timer1.Enabled = True
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture de la feuille
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        '==============================================
        Fermeture()
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de sélection du nom du fichier de configuration dans la liste
    Private Sub ListBox1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListBox1.MouseDoubleClick
        '==============================================
        ' Récupération du nom du fichier sélectionné
        Variables.NomConfig = "\" & Strings.LTrim(ListBox1.SelectedItem) & ".cfg"
        '==============================================
        ' Changement visuel de sélection de bouton
        Button3.Select()
        ' ==================================
        ' Initialisation des variables
        Dim NomFichier As StreamReader
        Dim i As Integer
        Dim j As Integer
        ' ==================================
        ' Traitement des erreurs
        On Error GoTo TraitErreur2
        ' ====================================================================================
        ' Vérification si la sélection d'un nom de fichier a été réalisée
        If Variables.NomConfig = "" Then
            MsgBox("Vous n'avez pas réalisé de sélection de fichier !", MsgBoxStyle.Exclamation, " Absence de sélection")
            Exit Sub
        End If
        ' ==================================
        ' Ouverture du fichier de chargement
        NomFichier = New StreamReader(Variables.Repertoire & Variables.NomConfig, System.Text.Encoding.Default)
        ' ==================================
        ' Lecture du fichier de la base
        Do Until NomFichier.EndOfStream = True
            ' ==================================
            ' Affectation des textes de la base des informations du fichier correspondantes
            Extracteur.TextBox1.Text = NomFichier.ReadLine
            Extracteur.TextBox2.Text = NomFichier.ReadLine
            Extracteur.TextBox3.Text = NomFichier.ReadLine
            Extracteur.TextBox4.Text = NomFichier.ReadLine
            Extracteur.TextBox5.Text = NomFichier.ReadLine
            Extracteur.TextBox6.Text = NomFichier.ReadLine
            Extracteur.TextBox7.Text = NomFichier.ReadLine
            Extracteur.TextBox8.Text = NomFichier.ReadLine
            Extracteur.TextBox9.Text = NomFichier.ReadLine
            Extracteur.TextBox10.Text = NomFichier.ReadLine
            Extracteur.TextBox11.Text = NomFichier.ReadLine
            Extracteur.TextBox12.Text = NomFichier.ReadLine
            Extracteur.TextBox13.Text = NomFichier.ReadLine
            Extracteur.TextBox14.Text = NomFichier.ReadLine
            Extracteur.TextBox15.Text = NomFichier.ReadLine
            Extracteur.TextBox16.Text = NomFichier.ReadLine
            Extracteur.TextBox17.Text = NomFichier.ReadLine
            Extracteur.TextBox18.Text = NomFichier.ReadLine
            Extracteur.TextBox19.Text = NomFichier.ReadLine
            ' ==================================
            ' Affectation des textes du dernier résultat des informations du fichier correspondantes
            Extracteur.TextBox21.Text = NomFichier.ReadLine
            Extracteur.TextBox22.Text = NomFichier.ReadLine
            Extracteur.TextBox23.Text = NomFichier.ReadLine
            Extracteur.TextBox24.Text = NomFichier.ReadLine
            Extracteur.TextBox25.Text = NomFichier.ReadLine
            Extracteur.TextBox26.Text = NomFichier.ReadLine
            Extracteur.TextBox27.Text = NomFichier.ReadLine
            Extracteur.TextBox28.Text = NomFichier.ReadLine
            Extracteur.TextBox29.Text = NomFichier.ReadLine
            Extracteur.TextBox30.Text = NomFichier.ReadLine
            Extracteur.TextBox31.Text = NomFichier.ReadLine
            Extracteur.TextBox32.Text = NomFichier.ReadLine
            Extracteur.TextBox33.Text = NomFichier.ReadLine
            Extracteur.TextBox34.Text = NomFichier.ReadLine
            Extracteur.TextBox35.Text = NomFichier.ReadLine
            Extracteur.TextBox36.Text = NomFichier.ReadLine
            Extracteur.TextBox37.Text = NomFichier.ReadLine
            Extracteur.TextBox38.Text = NomFichier.ReadLine
            Extracteur.TextBox39.Text = NomFichier.ReadLine
            ' ==================================
            ' Chargement du tableau de traitements de la Base
            For i = 1 To 19
                For j = 0 To 4
                    Variables.TabTraitBase(i, j) = NomFichier.ReadLine
                Next
            Next
            ' ==================================
            ' Sauvegarde du tableau de traitements du dernier résultat
            For i = 1 To 19
                For j = 0 To 4
                    Variables.TabTraitResul(i, j) = NomFichier.ReadLine
                Next
            Next
        Loop
        ' ==================================
        ' Fermeture du fichier
        NomFichier.Close()
        ' ==================================
        ' Recopie du nom du fichier de configuration chargé
        TextBox1.Text = Strings.LTrim(ListBox1.SelectedItem)
        SauveConfig.TextBox1.Text = Strings.LTrim(ListBox1.SelectedItem)
        ' ==================================
        ' Effacement du nom du fichier de configuration chargé
        Variables.NomConfig = ""
        ' ==================================
        ' Fermeture de l'application
        Fermeture()
        ' ==================================
TraitErreur2:
        If Err.Number <> 0 Then
            MsgBox(Err.Number & "    " & Err.Description, MsgBoxStyle.Exclamation, " Erreur de chargement")
        End If
    End Sub

    ' ==============================================================================================================================================
    ' Procédure de fermeture
    Private Sub Fermeture()
        ' ============================================
        ' Essai pour éviter qu'une application ne passe devant la feuille ayant demandé celle-ci
        Extracteur.Activate()
        ' ============================================
        Me.Visible = False
    End Sub

    ' ==============================================================================================================================================
    ' Procédure d'ouverture par Fondu
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.06
        If Me.Opacity >= 0.95 Then
            Timer1.Enabled = False
            Me.Opacity = 1
        End If
    End Sub

    '=============================================================================================================================================
    ' Traitement du repositionnement automatique de la feuille quand on l'a déplacée
    Private Sub ChgtConfig_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseEnter
        Me.CenterToScreen()
    End Sub

End Class