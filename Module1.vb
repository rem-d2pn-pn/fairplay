﻿Module ListeVariables

    Public Class Variables

        ' ==================================
        ' Variables de l'Extracteur de la Base
        ' ==================================
        ' Variable du texte du traitement
        Public Shared AiguilleurExtracteur As Integer
        ' ==================================
        ' Variable du numéro de panel pour l'affichage des Listbox
        Public Shared NumeroList As Integer
        Public Shared NumeroListSuivant As Integer
        ' ==================================
        ' Variable pour le stockage des traitements sur la base
        Public Shared TabTraitBase(19, 5) As String
        Public Shared IndexTabTraitBase As Integer
        ' ==================================
        ' Variable pour le stockage des traitements sur le dernier résultat
        Public Shared TabTraitResul(19, 5) As String
        Public Shared IndexTabTraitResul As Integer
        ' ==================================
        ' Variables de traitement de suppression des informations d'un texte
        Public Shared SupDirectBase As Boolean
        Public Shared SupDirectResu As Boolean
        ' ==================================
        ' Variables Min et Max 
        Public Shared ValeurMin As Double
        Public Shared ValeurMax As Double
        Public Shared Barillet As Integer
        Public Shared Barillet2 As Integer
        Public Shared ValeurDateMin As Date
        Public Shared ValeurDateMax As Date

        ' ==================================
        ' Variables du Tableau de la Base
        ' ==================================
        ' Tableau recevant la base : Colonne 0 = Réponses Analyseur : Colonne 43 = Réponses Extracteur Base : Colonne 44 =  
        Public Shared Base(400000, 45) As String
        ' ==================================
        ' Variable de pointage de la dernière valeur de la base
        Public Shared FinBase As Integer
        ' ==================================
        ' Variable d'index de la colonne 0 pour les réponses
        Public Shared IndexBase As Integer
        ' ==================================
        ' Variable du nombre de réponses trouvées
        Public Shared NombreRéponse As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la valeur numérique de borne pour les colonnes
        Public Shared BorneNumCol As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la valeur numérique du pas pour les colonnes
        Public Shared PasNumCol As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la valeur numérique de borne pour les lignes
        Public Shared BorneNumLig As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la valeur numérique du pas pour les lignes
        Public Shared PasNumLig As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la date de borne pour les colonnes
        Public Shared BorneDateCol As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la date du pas pour les colonnes
        Public Shared PasDateCol As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la date de borne pour les lignes
        Public Shared BorneDateLig As Integer
        ' ==================================
        ' Variables d'indicateur du numéro du texte de la date du pas pour les lignes
        Public Shared PasDateLig As Integer

        ' ==================================
        ' Variables de l'Information sélectionnée
        ' ==================================
        ' Variable du sujet à transférer
        Public Shared TransSujet As String
        ' ==================================
        ' Variable du numéro de la ligne de la base à afficher
        Public Shared TransNumLigne As String
        ' ==================================
        ' Variable de la comparaison à transférer
        Public Shared TransComparaison As String
        ' ==================================
        ' Variable du numéro de la ligne du DataGrid sélectionnée
        Public Shared NumLigSel As Integer

        ' ==================================
        ' Variables du Chargeur de configuration
        ' ==================================
        Public Shared NomConfig As String

        ' ==================================
        ' Variables du Chargeur de sélection
        ' ==================================
        Public Shared NomSelect As String
        Public Shared SelectNumColMinMax As Integer
        Public Shared SelectNumLigMinMax As Integer

        ' ==================================
        ' Variables du Tableau
        ' ==================================
        Public Shared SujetLigCol As Boolean
        Public Shared AncienneCouleur As Color
        Public Shared AncienCol As Integer
        Public Shared AncienLig As Integer
        Public Shared NumCritereLigne As Integer
        Public Shared NumCritereColonne As Integer
        Public Shared CfgColDescente As String
        Public Shared CfgColDescenteSuivant As String
        Public Shared CfgLigDescente As String
        Public Shared CfgLigDescenteSuivant As String
        Public Shared ListeColonneLigne As Boolean

        ' ==================================
        ' Variables du Tableau
        ' ==================================
        Public Shared TabDateBornePasCol(135, 1) As String
        Public Shared TabDateBornePasLig(135, 1) As String
        Public Shared TabValeurBornePasCol(113, 1) As Double
        Public Shared TabValeurBornePasLig(113, 1) As Double
        Public Shared TitreColonne As String
        Public Shared TitreLigne As String

        ' ==================================
        ' Variables Ouverture Fermeture
        ' ==================================
        Public Shared Largeur As Double
        Public Shared Hauteur As Double
        Public Shared PasLargeur As Double
        Public Shared PasHauteur As Double
        Public Shared PasRouge As Integer
        Public Shared PasVert As Integer
        Public Shared PasBleu As Integer
        Public Shared CoulRouge As Integer
        Public Shared CoulVert As Integer
        Public Shared CoulBleu As Integer
        Public Shared Largeur2 As Double
        Public Shared Hauteur2 As Double
        Public Shared PasLargeur2 As Double
        Public Shared PasHauteur2 As Double
        Public Shared PasRouge2 As Integer
        Public Shared PasVert2 As Integer
        Public Shared PasBleu2 As Integer
        Public Shared CoulRouge2 As Integer
        Public Shared CoulVert2 As Integer
        Public Shared CoulBleu2 As Integer
        Public Shared LargeurTri As Integer
        Public Shared HauteurTri As Integer
        Public Shared PasLargeurTri As Integer
        Public Shared PasHauteurTri As Integer
        Public Shared FormHauteur As Integer
        Public Shared FormLargeur As Integer

        ' ==================================
        ' Variables diverses
        ' ==================================
        ' Variable pour l'application indiquant l'état en cours de l'application
        Public Shared EtatAppli As String
        ' Variable pour le chemin des fichiers à charger
        Public Shared Repertoire As String
        ' Variable du nom du fichier de la Base
        Public Shared NomFichierBase As String
        ' Variable du nom de la Base pour les bandeaux titres
        Public Shared NomBaseTitre As String
        ' Variable de balayage des lignes sélectionnées dans le DataGridView
        Public Shared NumLigneData As Integer
        ' Variable de balayage des lignes de la Base pour le DataGridView
        Public Shared NumLigneBase As Integer

        ' variable suite au problème de la roulette avec Windows 10
        Public Shared AugmenteDiminu As Integer

    End Class


End Module

